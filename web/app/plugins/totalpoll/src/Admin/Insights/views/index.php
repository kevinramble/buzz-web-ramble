<?php ! defined( 'ABSPATH' ) && exit(); ?><div id="totalpoll-insights" class="wrap totalpoll-page" ng-app="insights">
    <h1 class="totalpoll-page-title"><?php _e( 'Insights', 'totalpoll' ); ?></h1>
    <insights-browser></insights-browser>
    <script type="text/ng-template" id="insights-browser-component-template">
        <div class="totalpoll-insights" ng-class="{'totalpoll-processing': $ctrl.isProcessing()}">
            <div class="totalpoll-box totalpoll-insights-header">
                <div class="totalpoll-insights-header-polls">
                    <select ng-model="$ctrl.filters.poll" ng-options="poll.id as poll.title for poll in $ctrl.polls" ng-change="$ctrl.loadMetrics()">
                        <option value=""><?php _e( 'Please select a poll', 'totalpoll' ); ?></option>
                    </select>
                </div>
                <div class="totalpoll-insights-header-date">
                    <span><?php _e( 'From', 'totalpoll' ); ?></span>
                    <input type="text" datetime-picker='{"timepicker":false, "mask":true, "format": "Y-m-d"}' ng-model="$ctrl.filters.from">
                    <span><?php _e( 'To', 'totalpoll' ); ?></span>
                    <input type="text" datetime-picker='{"timepicker":false, "mask":true, "format": "Y-m-d"}' ng-model="$ctrl.filters.to">
                    <div class="button-group">
                        <button class="button" ng-click="$ctrl.resetFilters()" ng-disabled="!($ctrl.filters.from || $ctrl.filters.to)">
							<?php _e( 'Clear', 'totalpoll' ); ?>
                        </button>
                        <button class="button button-primary" ng-click="$ctrl.loadMetrics()">
							<?php _e( 'Apply', 'totalpoll' ); ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="totalpoll-insights-charts" ng-if="$ctrl.metrics.day">
            <div class="totalpoll-box">
                <div class="totalpoll-box-section">
                    <chart type="line" ng-model="$ctrl.metrics.day"></chart>
                </div>
            </div>

            <div class="totalpoll-row">
                <div class="totalpoll-column">
                    <div class="totalpoll-box">
                        <div class="totalpoll-box-section">
                            <chart type="bar" ng-model="$ctrl.metrics.month"></chart>
                        </div>
                    </div>
                </div>
                <div class="totalpoll-column totalpoll-width-25">
                    <div class="totalpoll-box">
                        <div class="totalpoll-box-section">
                            <chart type="bar" ng-model="$ctrl.metrics.year"></chart>
                        </div>
                    </div>
                </div>
            </div>

            <div class="totalpoll-row">
                <div class="totalpoll-column">
                    <div class="totalpoll-box">
                        <div class="totalpoll-box-section">
                            <chart type="doughnut" ng-model="$ctrl.metrics.browser"></chart>
                        </div>
                    </div>
                </div>
                <div class="totalpoll-column">
                    <div class="totalpoll-box">
                        <div class="totalpoll-box-section">
                            <chart type="doughnut" ng-model="$ctrl.metrics.os"></chart>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </script>

</div>