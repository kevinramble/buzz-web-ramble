<?php

namespace TotalPoll\Admin\Insights;
! defined( 'ABSPATH' ) && exit();


use TotalPollVendors\TotalCore\Admin\Pages\Page as TotalCoreAdminPage;

/**
 * Class Page
 * @package TotalPoll\Admin\Insights
 */
class Page extends TotalCoreAdminPage {

	/**
	 * Page assets.
	 */
	public function assets() {
		/**
		 * @asset-script totalpoll-admin-insights
		 */
		wp_enqueue_script( 'totalpoll-admin-insights' );
		/**
		 * @asset-style totalpoll-admin-insights
		 */
		wp_enqueue_style( 'totalpoll-admin-insights' );

		// Some variables for frontend controller
		wp_localize_script(
			'totalpoll-admin-insights',
			'TotalPollInsights',
			[ 'pollId' => $this->request->query( 'poll' ) ]
		);
	}

	/**
	 * Page content.
	 */
	public function render() {
		include __DIR__ . '/views/index.php';
	}
}