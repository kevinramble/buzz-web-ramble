<?php ! defined( 'ABSPATH' ) && exit(); ?><div id="totalpoll-options" class="wrap totalpoll-page" ng-app="options" ng-controller="OptionsCtrl as $ctrl">
    <h1 class="totalpoll-page-title"><?php _e( 'Options', 'totalpoll' ); ?></h1>

    <div class="totalpoll-tabs-container has-tabs totalpoll-settings totalpoll-options">
        <div class="totalpoll-tabs">
			<?php $firstTab = key( $tabs ) ?>
			<?php foreach ( $tabs as $tabId => $tab ): ?>
                <div class="totalpoll-tabs-item <?php echo $tabId == $firstTab ? 'active' : ''; ?>" tab-switch="options><?php echo esc_attr( $tabId ); ?>">
                    <span class="dashicons dashicons-<?php echo esc_attr( $tab['icon'] ); ?>"></span>
					<?php echo esc_html( $tab['label'] ); ?>
                </div>
			<?php endforeach; ?>
        </div>
        <div class="totalpoll-tabs-content">
			<?php foreach ( $tabs as $tabId => $tab ): ?>
                <div class="totalpoll-tab-content <?php echo $tabId == $firstTab ? 'active' : ''; ?>" tab="options><?php echo esc_attr( $tabId ); ?>">
                    <div class="totalpoll-tabs-container">
                        <div class="totalpoll-tab-content active">
							<?php
							/**
							 * Fires before options tab content.
							 *
							 * @since 4.0.0
							 */
							do_action( 'totalpoll/actions/before/admin/options/tabs/content', $tabId );

							$path = empty( $tab['file'] ) ? __DIR__ . "/tabs/{$tabId}.php" : $tab['file'];
							if ( file_exists( $path ) ):
								include_once $path;
							endif;

							/**
							 * Fires after options tab content.
							 *
							 * @since 4.0.0
							 */
							do_action( 'totalpoll/actions/after/admin/options/tabs/content', $tabId );
							?>
                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
        </div>
    </div>
</div>