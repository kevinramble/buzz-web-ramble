<?php ! defined( 'ABSPATH' ) && exit(); ?><div id="totalpoll-log" class="wrap totalpoll-page" ng-app="log">
    <h1 class="totalpoll-page-title"><?php _e( 'Log', 'totalpoll' ); ?></h1>
    <log-browser></log-browser>
    <script type="text/ng-template" id="log-browser-component-template">
        <table class="wp-list-table widefat striped totalpoll-log-browser-list" ng-class="{'totalpoll-processing': $ctrl.isProcessing()}">
            <thead class="totalpoll-log-browser-list-header-wrapper">
            <tr>
                <td colspan="<?php echo count( $columns ); ?>">
                    <div class="totalpoll-log-browser-list-header">
                        <div class="totalpoll-log-browser-list-header-visible-columns">
							<?php foreach ( $columns as $columnId => $column ): ?>
                                <label><input type="checkbox" ng-init="$ctrl.columns['<?php echo $columnId ?>'] = <?php echo empty( $column['default'] ) ? 'false' : 'true'; ?>" ng-model="$ctrl.columns.<?php echo esc_attr( $columnId ); ?>"><?php echo esc_html( $column['label'] ); ?></label>
							<?php endforeach; ?>
                        </div>
                        <div class="totalpoll-log-browser-list-header-date">
                            <span><?php _e( 'From', 'totalpoll' ); ?></span>
                            <input type="text" datetime-picker='{"timepicker":false, "mask":true, "format": "Y-m-d"}' ng-model="$ctrl.filters.from">
                            <span><?php _e( 'To', 'totalpoll' ); ?></span>
                            <input type="text" datetime-picker='{"timepicker":false, "mask":true, "format": "Y-m-d"}' ng-model="$ctrl.filters.to">
							<?php
							/**
							 * Fires after filter inputs in log browser interface.
							 *
							 * @since 4.0.0
							 */
							do_action( 'totalpoll/actions/admin/log/filters' );
							?>
                            <div class="button-group">
                                <button class="button" ng-click="$ctrl.resetFilters()" ng-disabled="!($ctrl.filters.from || $ctrl.filters.to)">
									<?php _e( 'Clear', 'totalpoll' ); ?>
                                </button>
                                <button class="button button-primary" ng-click="$ctrl.loadPage(1)">
									<?php _e( 'Apply', 'totalpoll' ); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
				<?php foreach ( $columns as $columnId => $column ): ?>
                    <th scope="col" <?php if ( empty( $column['compact'] ) ): ?>class="totalpoll-log-browser-list-collapsed"<?php endif; ?> ng-show="$ctrl.columns.<?php echo esc_attr( $columnId ); ?>"><?php echo esc_html( $column['label'] ); ?></th>
				<?php endforeach; ?>
            </tr>
            </thead>
            <tbody>
            <tr class="totalpoll-log-browser-list-entry" ng-repeat="entry in $ctrl.entries track by $index">
				<?php foreach ( $columns as $columnId => $column ): ?>
					<?php if ( $columnId === 'status' ): ?>
                        <td class="totalpoll-log-browser-list-collapsed" ng-class="{'success': entry.isAccepted(), 'error': entry.isRejected()}" ng-show="$ctrl.columns.status">{{entry.getStatus()}}</td>
					<?php elseif ( $columnId === 'action' ): ?>
                        <td class="totalpoll-log-browser-list-collapsed" ng-show="$ctrl.columns.action">{{entry.getAction()}}</td>
					<?php elseif ( $columnId === 'date' ): ?>
                        <td class="totalpoll-log-browser-list-collapsed" ng-show="$ctrl.columns.date" title="{{entry.getDate()|date:'yyyy-MM-dd @ HH:mm'}} (<?php esc_attr_e( 'Local Time', 'totalpoll' ); ?>)">{{ entry.getUTCDate() }} (UTC)</td>
					<?php elseif ( $columnId === 'poll' ): ?>
                        <td class="totalpoll-log-browser-list-collapsed" ng-show="$ctrl.columns.poll"><a target="_blank" ng-href="{{entry.getPollEditLink()}}">{{entry.getPollTitle()}}</a></td>
					<?php elseif ( $columnId === 'user_id' ): ?>
                        <td class="totalpoll-log-browser-list-collapsed" ng-show="$ctrl.columns.user_id">{{entry.getUser('id') || 'N/A' }}</td>
					<?php elseif ( $columnId === 'user_login' ): ?>
                        <td class="totalpoll-log-browser-list-collapsed" ng-show="$ctrl.columns.user_login">{{entry.getUser('login') || 'N/A'}}</td>
					<?php elseif ( $columnId === 'user_name' ): ?>
                        <td class="totalpoll-log-browser-list-collapsed" ng-show="$ctrl.columns.user_name">{{entry.getUser('name') || 'Anonymous'}}</td>
					<?php elseif ( $columnId === 'user_email' ): ?>
                        <td class="totalpoll-log-browser-list-collapsed" ng-show="$ctrl.columns.user_email">{{entry.getUser('email') || 'N/A'}}</td>
					<?php elseif ( $columnId === 'browser' ): ?>
                        <td class="totalpoll-log-browser-list-collapsed" ng-show="$ctrl.columns.browser">{{(entry.getUseragent()|platform).description}}</td>
					<?php elseif ( $columnId === 'ip' ): ?>
                        <td class="totalpoll-log-browser-list-collapsed" ng-show="$ctrl.columns.ip">{{entry.getIP()}}</td>
					<?php elseif ( $columnId === 'details' ): ?>
                        <td class="totalpoll-log-browser-list-compact" ng-show="$ctrl.columns.details" ng-bind-html="entry.getDetails()|table"></td>
					<?php else: ?>
                        <td class="totalpoll-log-browser-list-collapsed" ng-show="$ctrl.columns['<?php echo esc_attr( $columnId ); ?>']"><?php echo empty( $column['content'] ) ? $column['content'] : ''; ?></td>
					<?php endif; ?>
				<?php endforeach; ?>
            </tr>
            <tr ng-if="!$ctrl.entries.length">
                <td colspan="<?php echo count( $columns ); ?>"><?php _e( 'Nothing. Nada. Niente. Nickts. Rien.', 'totalpoll' ); ?></td>
            </tr>
            </tbody>
            <tfoot>
            <tr class="totalpoll-log-browser-list-footer-wrapper">
                <td scope="col" colspan="<?php echo count( $columns ); ?>">
                    <div class="totalpoll-log-browser-list-footer">
                        <div class="totalpoll-log-browser-list-footer-pagination">
                            <div class="button-group">
                                <button class="button" ng-class="{'button-primary': $ctrl.hasPreviousPage()}" ng-click="$ctrl.previousPage()" ng-disabled="$ctrl.isFirstPage()"><?php _e( 'Previous', 'totalpoll' ); ?></button>
                                <button class="button" ng-class="{'button-primary': $ctrl.hasNextPage()}" ng-click="$ctrl.nextPage()" ng-disabled="$ctrl.isLastPage()"><?php _e( 'Next', 'totalpoll' ); ?></button>
                            </div>
                        </div>
                        <div class="totalpoll-log-browser-list-footer-export">
                            <span><?php _e( 'Download as', 'totalpoll' ); ?></span>
                            <div class="button-group">
								<?php foreach ( $formats as $format => $label ): ?>
                                    <button class="button" ng-class="{'button-primary': $ctrl.canExport()}" ng-click="$ctrl.exportAs('<?php echo esc_js( $format ); ?>')" ng-disabled="!$ctrl.canExport()"><?php echo esc_html( $label ); ?></button>
								<?php endforeach; ?>
                            </div>
                        </div>

                    </div>
                </td>
            </tr>
            </tfoot>

        </table>
    </script>

</div>
