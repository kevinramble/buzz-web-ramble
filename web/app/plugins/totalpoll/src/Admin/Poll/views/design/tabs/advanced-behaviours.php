<?php ! defined( 'ABSPATH' ) && exit(); ?><customizer-control
	type="checkbox"
	label="<?php _e( 'AJAX', 'totalpoll' ); ?>"
	ng-model="$root.settings.design.behaviours.ajax"
	help="<?php _e( 'Load poll in-place without reloading the whole page.', 'totalpoll' ); ?>"></customizer-control>
<customizer-control
	type="checkbox"
	label="<?php _e( 'Scroll up after vote submission', 'totalpoll' ); ?>"
	ng-model="$root.settings.design.behaviours.scrollUp"
	help="<?php _e( 'Scroll up to poll viewport after submitting a vote.', 'totalpoll' ); ?>"></customizer-control>
<customizer-control
	type="checkbox"
	label="<?php _e( 'One-click vote', 'totalpoll' ); ?>"
	ng-model="$root.settings.design.behaviours.oneClick"
	help="<?php _e( 'The user will be able to vote by clicking on the choice directly.', 'totalpoll' ); ?>"></customizer-control>
<customizer-control
	type="checkbox"
	label="<?php _e( 'Question by question', 'totalpoll' ); ?>"
	ng-model="$root.settings.design.behaviours.slider"
	help="<?php _e( 'Display questions one by one.', 'totalpoll' ); ?>"></customizer-control>