<?php

namespace TotalPoll\Contracts\Entry;
! defined( 'ABSPATH' ) && exit();


/**
 * Poll repository
 * @package TotalPoll\Entry
 * @since   1.0.0
 */
interface Repository {
	/**
	 * Get entries.
	 *
	 * @param $query
	 *
	 * @return mixed
	 * @since 1.0.0
	 */
	public function get( $query );

	/**
	 * Create entry.
	 *
	 * @param $attributes
	 *
	 * @since 1.0.0
	 * @return array|\WP_Error
	 */
	public function create( $attributes );

	/**
	 * Delete entries.
	 *
	 * @param $query array
	 *
	 * @return bool|\WP_Error
	 * @since 1.0.0
	 */
	public function delete( $query );

	/**
	 * Count entries.
	 *
	 * @param $query
	 *
	 * @return mixed
	 * @since 1.0.0
	 */
	public function count( $query );

	/**
	 * Anonymize entries.
	 *
	 * @param $query
	 *
	 * @return mixed
	 * @since 1.0.0
	 */
	public function anonymize( $query );
}