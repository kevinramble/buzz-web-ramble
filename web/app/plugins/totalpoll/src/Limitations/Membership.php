<?php

namespace TotalPoll\Limitations;
! defined( 'ABSPATH' ) && exit();



use TotalPollVendors\TotalCore\Limitations\Limitation;

/**
 * Membership Limitation.
 * @package TotalPoll\Limitations
 */
class Membership extends Limitation {
	/**
	 * Limitation check logic.
	 *
	 * @return bool|\WP_Error
	 */
	public function check() {
		$roles = empty( $this->args['roles'] ) ? [] : (array) $this->args['roles'];

		if ( is_user_logged_in() ):
			if ( ! empty( $roles ) && ! in_array( $GLOBALS['current_user']->roles[0], $roles ) ):
				$roles = implode( ', ', array_map( 'translate_user_role', $roles ) );

				return new \WP_Error(
					'membership_type',
					sprintf(
						__( 'To continue, you must be a part of these roles: %s.', 'totalpoll' ),
						$roles
					)
				);

			endif;
		else:
			return new \WP_Error(
				'logged_in',
				sprintf(
					__( 'To continue, please <a href="%s">sign in</a> or <a href="%s">register</a>.', 'totalpoll' ),
					wp_login_url( home_url( add_query_arg( null, null ) ) ),
					wp_registration_url()
				)
			);
		endif;

		return true;
	}
}