<?php

namespace TotalPoll\Modules\Extensions\DisableArchive;
! defined( 'ABSPATH' ) && exit();


/**
 * Class Extension
 * @package TotalPoll\Modules\Extensions\DisableArchive
 */
class Extension extends \TotalPoll\Modules\Extension {
	protected $root = __FILE__;

	/**
	 * Run the extension.
	 *
	 * @return mixed
	 */
	public function run() {
		add_filter( 'totalpoll/filters/cpt/args', [ $this, 'disableArchive' ] );
	}

	public function disableArchive( $args ) {
		$args['has_archive'] = false;

		return $args;
	}

	public static function onActivate() {
		flush_rewrite_rules();
	}

	public static function onDeactivate() {
		flush_rewrite_rules();
	}
}