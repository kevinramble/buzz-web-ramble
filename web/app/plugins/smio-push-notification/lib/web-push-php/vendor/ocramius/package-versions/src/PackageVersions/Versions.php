<?php

namespace PackageVersions;

/**
 * This class is generated by ocramius/package-versions, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 */
final class Versions
{
    const ROOT_PACKAGE_NAME = 'minishlink/web-push';
    const VERSIONS = array (
  'fgrosse/phpasn1' => 'v2.1.1@7ebf2a09084a7bbdb7b879c66fdf7ad80461bbe8',
  'guzzlehttp/guzzle' => '6.3.3@407b0cb880ace85c9b63c5f9551db498cb2d50ba',
  'guzzlehttp/promises' => 'v1.3.1@a59da6cf61d80060647ff4d3eb2c03a2bc694646',
  'guzzlehttp/psr7' => '1.5.2@9f83dded91781a01c63574e387eaa769be769115',
  'paragonie/random_compat' => 'v9.99.99@84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95',
  'paragonie/sodium_compat' => 'v1.8.0@5e280b50cdaf8da4cc4810e0847a9618be29703d',
  'psr/http-message' => '1.0.1@f6561bf28d520154e4b0ec72be95418abe6d9363',
  'ralouphie/getallheaders' => '2.0.5@5601c8a83fbba7ef674a7369456d12f1e0d0eafa',
  'spomky-labs/base64url' => 'v2.0.1@3eb46a1de803f0078962d910e3a2759224a68c61',
  'web-token/jwt-core' => 'v1.2.3@c134cc93eae28f39aa95ccddcb5b024e77a7c039',
  'web-token/jwt-key-mgmt' => 'v1.2.3@718cdba796a77744dbf037cd241465d3a2b35ae6',
  'web-token/jwt-signature' => 'v1.2.3@f5b1536d8e1411806ea24f9f65deb3a415d583c3',
  'web-token/jwt-signature-algorithm-ecdsa' => 'v1.2.3@f85bc587347a8353adaf75e9d720ba5670429500',
  'web-token/jwt-signature-algorithm-eddsa' => 'v1.2.3@2d6b7ccf8eafb44bd23619b0cd14dd3dc5ea3f5e',
  'web-token/jwt-signature-algorithm-hmac' => 'v1.2.3@889d91ac877e41cf7b1d95d51bdb4c2a8d089c07',
  'web-token/jwt-signature-algorithm-none' => 'v1.2.3@6dc96e10e300fe704087214342beced1ecc69a92',
  'web-token/jwt-signature-algorithm-rsa' => 'v1.2.3@851ed5727ad96f59fa42baa48f835bf7ec423151',
  'web-token/jwt-util-ecc' => 'v1.2.3@0efffa077a2fd20c145df806cd6d86aace4e3293',
  'doctrine/instantiator' => '1.1.0@185b8868aa9bf7159f5f953ed5afb2d7fcdc3bda',
  'jean85/pretty-package-versions' => '1.2@75c7effcf3f77501d0e0caa75111aff4daa0dd48',
  'myclabs/deep-copy' => '1.8.1@3e01bdad3e18354c3dce54466b7fbe33a9f9f7f8',
  'nette/bootstrap' => 'v2.4.6@268816e3f1bb7426c3a4ceec2bd38a036b532543',
  'nette/di' => 'v2.4.14@923da3e2c0aa53162ef455472c0ac7787b096c5a',
  'nette/finder' => 'v2.4.2@ee951a656cb8ac622e5dd33474a01fd2470505a0',
  'nette/neon' => 'v2.4.3@5e72b1dd3e2d34f0863c5561139a19df6a1ef398',
  'nette/php-generator' => 'v3.0.5@ea90209c2e8a7cd087b2742ca553c047a8df5eff',
  'nette/robot-loader' => 'v3.1.0@fc76c70e740b10f091e502b2e393d0be912f38d4',
  'nette/utils' => 'v2.5.3@17b9f76f2abd0c943adfb556e56f2165460b15ce',
  'nikic/php-parser' => 'v3.1.5@bb87e28e7d7b8d9a7fda231d37457c9210faf6ce',
  'ocramius/package-versions' => '1.3.0@4489d5002c49d55576fa0ba786f42dbb009be46f',
  'phar-io/manifest' => '1.0.3@7761fcacf03b4d4f16e7ccb606d4879ca431fcf4',
  'phar-io/version' => '2.0.1@45a2ec53a73c70ce41d55cedef9063630abaf1b6',
  'phpdocumentor/reflection-common' => '1.0.1@21bdeb5f65d7ebf9f43b1b25d404f87deab5bfb6',
  'phpdocumentor/reflection-docblock' => '4.3.0@94fd0001232e47129dd3504189fa1c7225010d08',
  'phpdocumentor/type-resolver' => '0.4.0@9c977708995954784726e25d0cd1dddf4e65b0f7',
  'phpspec/prophecy' => '1.8.0@4ba436b55987b4bf311cb7c6ba82aa528aac0a06',
  'phpstan/phpdoc-parser' => '0.2@02f909f134fe06f0cd4790d8627ee24efbe84d6a',
  'phpstan/phpstan' => '0.9.2@e59541bcc7cac9b35ca54db6365bf377baf4a488',
  'phpunit/php-code-coverage' => '6.1.4@807e6013b00af69b6c5d9ceb4282d0393dbb9d8d',
  'phpunit/php-file-iterator' => '2.0.2@050bedf145a257b1ff02746c31894800e5122946',
  'phpunit/php-text-template' => '1.2.1@31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
  'phpunit/php-timer' => '2.0.0@8b8454ea6958c3dee38453d3bd571e023108c91f',
  'phpunit/php-token-stream' => '3.0.1@c99e3be9d3e85f60646f152f9002d46ed7770d18',
  'phpunit/phpunit' => '7.5.1@c23d78776ad415d5506e0679723cb461d71f488f',
  'sebastian/code-unit-reverse-lookup' => '1.0.1@4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
  'sebastian/comparator' => '3.0.2@5de4fc177adf9bce8df98d8d141a7559d7ccf6da',
  'sebastian/diff' => '3.0.1@366541b989927187c4ca70490a35615d3fef2dce',
  'sebastian/environment' => '4.0.1@febd209a219cea7b56ad799b30ebbea34b71eb8f',
  'sebastian/exporter' => '3.1.0@234199f4528de6d12aaa58b612e98f7d36adb937',
  'sebastian/global-state' => '2.0.0@e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
  'sebastian/object-enumerator' => '3.0.3@7cfd9e65d11ffb5af41198476395774d4c8a84c5',
  'sebastian/object-reflector' => '1.1.1@773f97c67f28de00d397be301821b06708fca0be',
  'sebastian/recursion-context' => '3.0.0@5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
  'sebastian/resource-operations' => '2.0.1@4d7a795d35b889bf80a0cc04e08d77cedfa917a9',
  'sebastian/version' => '2.0.1@99732be0ddb3361e16ad77b68ba41efc8e979019',
  'symfony/console' => 'v4.2.1@4dff24e5d01e713818805c1862d2e3f901ee7dd0',
  'symfony/contracts' => 'v1.0.2@1aa7ab2429c3d594dd70689604b5cf7421254cdf',
  'symfony/finder' => 'v4.2.1@e53d477d7b5c4982d0e1bfd2298dbee63d01441d',
  'symfony/polyfill-mbstring' => 'v1.10.0@c79c051f5b3a46be09205c73b80b346e4153e494',
  'theseer/tokenizer' => '1.1.0@cb2f008f3f05af2893a87208fe6a6c4985483f8b',
  'webmozart/assert' => '1.3.0@0df1908962e7a3071564e857d86874dad1ef204a',
  'minishlink/web-push' => 'No version set (parsed as 1.0.0)@',
);

    private function __construct()
    {
    }

    /**
     * @throws \OutOfBoundsException if a version cannot be located
     */
    public static function getVersion(string $packageName) : string
    {
        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new \OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: cannot detect its version'
        );
    }
}
