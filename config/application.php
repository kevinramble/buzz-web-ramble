<?php

/** @var string Directory containing all of the site's files */
$root_dir = dirname(__DIR__);

/** @var string Document Root */
$webroot_dir = $root_dir . '/web';

/**
 * Expose global env() function from oscarotero/env
 */
Env::init();

/**
 * Use Dotenv to set required environment variables and load .env file in root
 */
$dotenv = new Dotenv\Dotenv($root_dir);
if (file_exists($root_dir . '/.env')) {
    $dotenv->load();
    $dotenv->required(['DB_NAME', 'DB_USER', 'DB_PASSWORD', 'WP_HOME', 'WP_SITEURL']);
}

/**
 * Set up our global environment constant and load its config first
 * Default: production
 */
define('WP_ENV', env('WP_ENV') ?: 'production');

$env_config = __DIR__ . '/environments/' . WP_ENV . '.php';

if (file_exists($env_config)) {
    require_once $env_config;
}

/**
 * URLs
 */
define('WP_HOME', env('WP_HOME'));
define('WP_SITEURL', env('WP_SITEURL'));

/**
 * Custom Content Directory
 */
define('CONTENT_DIR', '/app');
define('WP_CONTENT_DIR', $webroot_dir . CONTENT_DIR);
define('WP_CONTENT_URL', WP_HOME . CONTENT_DIR);

/**
 * DB settings
 */
define('DB_NAME', env('DB_NAME'));
define('DB_USER', env('DB_USER'));
define('DB_PASSWORD', env('DB_PASSWORD'));
define('DB_HOST', env('DB_HOST') ?: 'localhost');
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');
$table_prefix = env('DB_PREFIX') ?: 'wp_';

/**
 * SMTP Settings
 */
define('SMTP_HOST', env('SMTP_HOST'));
define('SMTP_PORT', env('SMTP_PORT'));
define('SMTP_USERNAME', env('SMTP_USERNAME'));
define('SMTP_PASSWORD', env('SMTP_PASSWORD'));
define('SMTP_SECURE', env('SMTP_SECURE'));
define('SMTP_FROM', env('SMTP_FROM'));
define('SMTP_FROMNAME', env('SMTP_FROMNAME'));

/**
 * Custom Settings
 */
define('AUTOMATIC_UPDATER_DISABLED', true);
define('DISABLE_WP_CRON', env('DISABLE_WP_CRON') ?: false);
define('DISALLOW_FILE_EDIT', true);

/**
 * Bootstrap WordPress
 */
if (!defined('ABSPATH')) {
    define('ABSPATH', $webroot_dir . '/wp/');
}

if(env('MULTISITE') == true){
    /* Multisites */
    define('WP_ALLOW_MULTISITE', true);

    define('MULTISITE', true);
    define('SUBDOMAIN_INSTALL', false);
    define('DOMAIN_CURRENT_SITE', env('DOMAIN_CURRENT_SITE'));
    define('PATH_CURRENT_SITE', env('PATH_CURRENT_SITE') ?: '/');
    define('SITE_ID_CURRENT_SITE', env('SITE_ID_CURRENT_SITE') ?: 1);
    define('BLOG_ID_CURRENT_SITE', env('BLOG_ID_CURRENT_SITE') ?: 1);

    define('AUTH_KEY', ')@*d*AVF KB)c_6e$b^|vJ>od5-_[/>{xI),&$#<x4u^B*yQdRp7Fa]#6*R.xc3%');
    define('SECURE_AUTH_KEY', '<d+#e}1`I]4~[Y|DXQ<</iOsh*`!c4SQskl5@y5WLc>,.{(*expx9V9ibEXo]o+X');
    define('LOGGED_IN_KEY', 'l|&Cd&rcF&8Pa/QqacP63PzEU0V#b+6H?A`vh|s#9#^Z1d?KG|[o`&gSu}Mrv;M<');
    define('NONCE_KEY', ';}7X)z~Vt+(}G/Q}!ZesS$EQQ+)s/MqV_SQU5mKKW|N,@@B+.Vy-ef<jRjtXOW45');
    define('AUTH_SALT', '2eE1`v=Pr@f!)WU/l&dCX-v|=x1u?-+ifO>apH[?qSAKbC<_9+Fcr$t~0PAJGEBT');
    define('SECURE_AUTH_SALT', 'By*ZS!YpE4~&Y,61`_Z#ArGaD#-2r31SAo*`W~t+48Re}~Lj%bzZ(d*gCA+W6/z_');
    define('LOGGED_IN_SALT', '|o-=1A^^=r4|c?@7yjrN?-F/wh*5CbKCT_ ,~yo+)(F=,z F<Y.h--7=2kV .Wz=');
    define('NONCE_SALT', '&)*-qo|OW}SGpB,d^/k@gSu$T R)SAo!l=o{i&5T|ZRB:ik[-Gim|M-PhvIFY9#N');
}

define('WP_DEBUG', true);

?>