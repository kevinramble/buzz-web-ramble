<section class="c-newsletter">
    <div class="container">
        <h3 class="c-newsletter__title">Sign up for our newsletter</h3>
        <form action="" class="c-newsletter__form">
            <input type="text" class="c-newsletter__input" placeholder="Your e-mail">
            <button class="js-newsletter-modal c-newsletter__btn">send</button>
        </form>
    </div>
</section>

<?php
if (is_user_logged_in()) {
    $userID = get_current_user_id();
    $user = get_user_meta($userID);
    $firstName = ($user['first_name'][0]) ? $user['first_name'][0] : '';
    $lastName = ($user['last_name'][0]) ? $user['last_name'][0] : '';
} else {
    $firstName = '';
    $lastName = '';
}
?>

<div class="modal newsletter">
    <div class="modal__dialog">
        <button class="modal__close" aria-label="Close"></button>

        <h1 class="c-title-headline">Get the Buzz</h1>

        <div class="c-description-headline mt-4">
            <p>No spam. No junk. Just exclusive updates & special offers with our Newsletter</p>
        </div>

        <form action="https://ramblemediagroup.us20.list-manage.com/subscribe/post?u=d8543a93a46b349dda640219a&amp;id=6d90265345" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate class="c-form c-form--dark mt-4">
            <?php $securitynumber1 = rand(0,9); $securitynumber2 = rand(0,9); ?>

            <input type="text" class="c-form__input" placeholder="First name" value="<?php echo $firstName; ?>" name="FNAME" id="mce-FNAME">
            <input type="text" class="c-form__input mt-2" placeholder="Last name" value="<?php echo $lastName; ?>" name="LNAME" id="mce-LNAME">
            <input type="text" class="c-form__input mt-2 js-email" placeholder="E-mail" name="EMAIL" id="mce-EMAIL">

            <label class="security-question">
                <span>Prove your humanity:</span>
                <input class="text" placeholder="<?php echo $securitynumber1. ' + ' .$securitynumber2; ?>" name="securityanswer" id="securityanswer">
            </label><!-- .security-question -->
            <input type="hidden" name="securitynumber1" id="securitynumber1" value="<?php echo $securitynumber1; ?>">
            <input type="hidden" name="securitynumber2" id="securitynumber2" value="<?php echo $securitynumber2; ?>">

            <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
            </div>
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d8543a93a46b349dda640219a_6d90265345" tabindex="-1" value=""></div>
            <div class="clear"><input type="submit" value="Sign up" name="subscribe" id="mc-embedded-subscribe" class="c-form__btn mt-3 inactive"></div>
        </form>
        <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
        <script type='text/javascript'>
            (function($) {
                window.fnames = new Array();
                window.ftypes = new Array();
                fnames[0] = 'EMAIL';
                ftypes[0] = 'email';
                fnames[1] = 'FNAME';
                ftypes[1] = 'text';
                fnames[2] = 'LNAME';
                ftypes[2] = 'text';
            }(jQuery));
            var $mcj = jQuery.noConflict(true);
        </script>
    </div>
</div>