<section class="c-more-like">
    <h2 class="c-more-like__title">More like This</h2>
    <div class="c-more-like__grid">
        <div class="row gutter">
            <div class="col-md-6 col-lg-4">
                <a href="#0" class="c-card-more-like">
                    <div class="c-card-more-like__wrap_thumb">
                        <div class="c-card-more-like__thumb" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/content/thumb-post-1.jpg')"></div>
                    </div>
                    <p class="c-card-more-like__description">Brazilian Crew Bicicleta Sem Freio Sticks by the Brush at POW! WOW! Hawaii</p>
                    <div class="c-card-more-like__author">
                        <p class="c-card-more-like__author__txt">by <strong>Tulio Ferreira</strong></p>
                        <p class="c-card-more-like__author__txt">dec 16,2019</p>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4">
                <a href="#0" class="c-card-more-like">
                    <div class="c-card-more-like__wrap_thumb">
                        <div class="c-card-more-like__thumb" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/content/thumb-post-1.jpg')"></div>
                    </div>
                    <p class="c-card-more-like__description">Brazilian Crew Bicicleta Sem Freio Sticks by the Brush at POW! WOW! Hawaii</p>
                    <div class="c-card-more-like__author">
                        <p class="c-card-more-like__author__txt">by <strong>Tulio Ferreira</strong></p>
                        <p class="c-card-more-like__author__txt">dec 16,2019</p>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4">
                <a href="#0" class="c-card-more-like">
                    <div class="c-card-more-like__wrap_thumb">
                        <div class="c-card-more-like__thumb" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/content/thumb-post-1.jpg')"></div>
                    </div>
                    <p class="c-card-more-like__description">Brazilian Crew Bicicleta Sem Freio Sticks by the Brush at POW! WOW! Hawaii</p>
                    <div class="c-card-more-like__author">
                        <p class="c-card-more-like__author__txt">by <strong>Tulio Ferreira</strong></p>
                        <p class="c-card-more-like__author__txt">dec 16,2019</p>
                    </div>
                </a>
            </div>
        </div>
        <a href="" class="c-more-like__bt-load">load more</a>
    </div>
</section>