<section class="c-post-content">
    <?php the_content(); ?>
    
    <div class="container" style="padding-left:0; padding-right:0;">
    <?php

            $section = get_the_category(get_the_ID())[0]->cat_name;
            $post_tags = get_the_tags();
            // get_the_terms returns an array of WP_Term objects
            foreach ($post_tags as $tag_obj)
                $_post_tags[] = $tag_obj->term_id; // get the id from the WP_Term object
                
            $cats = !empty($cat) ? $array : '';
            $postid = get_the_ID();
            $popular = new WP_Query(
                array('posts_per_page' => 4, 
                        'post__not_in'=> array($postid),
                        'orderby' => 'id',
                        'order' => 'DESC', 
                        'tag__in' => $_post_tags
                    ));

            // echo $popular->post_count;
            // print_r($popular);

            $cnt = 0;
            $postcount = $popular->post_count;

    ?>
    <?php if($popular->have_posts()){ ?><h3 class="c-list__title" style=";">YOU MAY ALSO LIKE... </h3> <?php } ?>
    <div id="carouselRelatedControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
        <?php 
                   

                    while ($popular->have_posts()) : $popular->the_post();  
                    $postID = $posts->ID;

                    if(has_post_thumbnail(get_the_ID())){
                        $image = get_the_post_thumbnail_url(get_the_ID(),'full');
                    } else {
                        $image = catch_that_image(get_the_ID());
                    } 

                    $imagem = preg_replace("/([-]\d{3,4}x\d{3}[.])/", ".", $image);
                    $thumbnail = ($imagem) ? explode(".", $imagem) : '';
                    $ext = end($thumbnail);
                    $thumb = str_replace(".".$ext,"-150x150.".$ext, $imagem);
        ?>
                <div class="carousel-item <?php if ($cnt == 1) { echo ' active'; } ?>">
                <div class="row" style="">
                    <div id="related-container" class="col-sm">
                       <a  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> 
                       <?php
                            $category = get_the_category(get_the_ID())[0]->name;
                            if($category == "Life")
                                $color = "#ffaa3c";
                            elseif($category == "Hot")
                                $color = "#ff8282";
                            else
                                $color = "#2896c8";
                               // echo $color. ' - '. $category;
                       ?>
                            <img id="related-image" class="d-block" style="border-color: <?php echo $color; ?>" src="<?php echo thumb_exist($thumb); ?>" alt="<?=$cnt?> slide">
                            <h4 id="related-header"><?php echo get_the_title(); ?></h4>
                            <div class="d-none d-lg-block">
                                <small id="related-excerpt"><?php echo substr(get_the_excerpt(), 0,165)."…"; ?></small>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <?php $cnt++; endwhile; wp_reset_postdata(); ?>
        </div>
        <a class="carousel-control-prev" style="width:5%" href="#carouselRelatedControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" style="width:5%" href="#carouselRelatedControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>
</div>




    <div id="tags-social" class="row">
        <div class="col-12 col-md-9">
            <div class="c-post-tags">
                <?php the_tags('', '', ''); ?>
            </div>
        </div>
        <section>
        
</section>


        <div class="col-12 col-md-3">
            <div class="c-social">
                <div class="c-social__book c-social__icon--bk-outline"><?php the_favorites_button($post->ID, get_current_blog_id()); ?></div>
                <a class="c-social__icon c-social__icon--fb-outline" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"></a>
                <a class="c-social__icon c-social__icon--tw-outline" href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank"></a>
                <div class="d-block d-sm-none">
                    <a class="c-social__icon c-social__icon--wa-outline" data-action="share/whatsapp/share" href="https://api.whatsapp.com/send?text=<?php the_permalink(); ?>" target="_blank">
                    <i class="fa fa-whatsapp"></i></a>
                </div>
            </div>
        </div>
    </div>
    <section>


</section>
    <?php /* <div id="outdoor-duplo">
        <div class="c-banner-outdoor-duplo">
            outdoor duplo<br>750x200
        </div>
    </div>
    <div id="banner-large">
        <div class="c-banner-large">
            Large<br>320x100
        </div>
    </div>*/ ?>
</section>