<?php
    $country = get_country();
    switch ($country) {
        case '_stlucia':
            $countryTitle = "St. Lucia";
            $countryTerm = "st-lucia";
            break;
        case '_jamaica':
            $countryTitle = "Jamaica";
            $countryTerm = "jamaica";
        break;
        default:
            $countryTitle = "";
            $countryTerm = "";
            break;
    }
   
    //$related = get_related_posts(get_the_ID(), 6);
    //query posts to return top posts in desc order for the specified categories
    
    //get top 10 posts for the last 14 days
    $getCountry = get_term_by('name', $countryTitle, 'countries');
    $countryID = $getCountry->term_id;

    $section = get_the_category(get_the_ID())[0]->category_nicename;
    $postID = get_the_ID();

    $tax_query = array(
        'relation' => 'AND',
        array(
            'taxonomy' => 'countries',
            'field'    => 'term_id',
            'terms'    => $countryID,
        ),
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => $section
        )
    );

    if($section == "news"){

        $sidebar_title = "Latest News";
        $cats = "4"; //News
        $posts = new WP_Query(
            array(  
                'taxonomy' => 'countries', 
                'term' => $countryTerm,
                'tax_query' => ($country) ? $tax_query : '',
                'posts_per_page' => 6,
                'post__not_in'=> array($postID),
                'cat' => $cats,
                'order' => 'DESC'
                    ));
    //print_r($posts);

    }else{
        $sidebar_title = "Trending Now";
        $cat = "2,3"; //Life & Hot
        $temps = explode(',', $cat); //common delimited list of categories
        $array = array();
        foreach ($temps as $temp) $array[] = trim($temp);
        
        $cats = !empty($cat) ? $array : '';
        $posts = new WP_Query(
            array(
                    'taxonomy' => 'countries', 
                    'term' => $countryTerm,
                    'tax_query' => ($country) ? $tax_query : '',
                    'post_type' => array('post', 'video'), 
                    'posts_per_page' => 6, 
                    'post__not_in'=> array($postID),
                    'meta_key' => 'top_posts', 
                    'orderby' => 'meta_value_num', 
                    'order' => 'DESC', 
                    'category__in' => $cats,
                    //'year' => date('Y'),
                    //'monthnum' => intval(date('m')),
                    'date_query' => array(
                        array(
                            'column' => 'post_date_gmt',
                            'after'  => '2 days ago', //TBD - updated to 2 days
                        )
                    )
                ));
                //print_r($posts);
    }


    if($posts->have_posts()): ?>
        <div class="c-post-sidebar">
        <h4 class="c-list__title"><?=$sidebar_title; ?> </h4>
            <?php while($posts->have_posts()) : $posts->the_post();
            $category = get_the_category(get_the_ID());
                if(has_post_thumbnail()){
                    $image = get_the_post_thumbnail_url();
                    
                } else {
                    $image = catch_that_image();
                } 
                $imagem = preg_replace("/([-]\d{3,4}x\d{3}[.])/", ".",$image);

                $thumbnail = ($imagem) ? explode(".", $imagem) : '';
                $ext = end($thumbnail);
                $thumb = str_replace(".".$ext,"-150x150.".$ext, $imagem);
                
                ?>
                
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="c-post-sidebar__item">
                    <div class="c-post-sidebar__wrap-thumb">
                        <div class="c-post-sidebar__thumb" style="background-image:url('<?php echo $thumb; ?>')"></div>
                    </div>
                    <p class="c-post-sidebar__description"><?php the_title(); ?></p>
                </a>
            <?php endwhile; ?>
        </div>
        <?php
    endif; wp_reset_query(); wp_reset_postdata();
?>