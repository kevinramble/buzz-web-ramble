<header class="c-post-header">
    <h1><?php the_title(); ?></h1>
    <?php if (has_excerpt()) : ?>
        <h4><?php the_excerpt(); ?></h4>
    <?php endif; ?>
    <div class="c-post-header__content">
        <div class="c-post-author">
            <div class="c-post-author__img" style="background-image:url('<?php echo get_avatar_url(get_the_author_meta('ID')); ?>')"></div>
            <div class="c-post-author__content">
                <p class="c-post-author__txt">By <span><?php echo get_the_author_meta('display_name'); ?></span></p>
                <p class="c-post-author__txt"><?php the_time('M d, Y'); ?></p>
            </div>
        </div>

        <div class="c-social d-none d-md-flex">
            <div class="c-social__book c-social__icon--bk-outline"><?php the_favorites_button($post->ID, get_current_blog_id()); ?></div>
            <a class="c-social__icon c-social__icon--fb-outline" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"></a>
            <a class="c-social__icon c-social__icon--tw-outline" href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank"></a>
        </div>
    </div>
    <?php if ($sponsored_img_url) : ?>
        <div class="c-post-header__sponsored">
            <div class="c-post-sponsored">
                <?php if ($sponsored_url) : ?><a href="<?php echo $sponsored_url; ?>" target="_blank" title="<?php echo $sponsored_url; ?>"><?php endif; ?>
                    <img src="<?php echo $sponsored_img_url; ?>" alt="Sponsored article" class="c-post-sponsored__img">
                    <?php if ($sponsored_url) : ?></a><?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
</header>