<?php
	$minimumPosts = 3;
	$postAmount = null;
	$totalPosts = 0;
	$watch_next_function = 'watch_next' . $country;

	for ($d = 1; $d <= 9; $d++){
		$postID = $watch_next_function('watch_next'. $country .'_post_id'.$d);
		if($postID) $postAmount[] = $postID;
	}

	if($postAmount) $totalPosts = count($postAmount);
	if($totalPosts >= $minimumPosts) :
?>

	<section class="c-trending-now">
		<h2 class="c-trending-now__title"><?php echo $areaTitle; ?></h2>

		<div class="c-trending-now__wrapper js-featured-carousel owl-carousel">
			<?php
			for ($c = 1; $c <= 9; $c++) :
				$postID = $watch_next_function('watch_next'. $country .'_post_id'.$c);
				$postTitle = $watch_next_function('watch_next'. $country .'_post_title'.$c);
				$customTitle = $watch_next_function('watch_next'. $country .'_title'.$c);
				$title = ($customTitle) ? $customTitle : $postTitle;
				$customImage = $watch_next_function('watch_next'. $country .'_img'.$c);
				$image = '';

				if(!$customImage){
					if(has_post_thumbnail($postID)){
						$image = get_the_post_thumbnail_url($postID);
					} else {
						$image = catch_that_image($postID);
					}
				} else {
					$image = $customImage;
				}

				if($postID) : ?>
				<a href="<?php echo get_permalink($postID); ?>" class="c-card-trending-now">
					<div class="c-card-trending-now__img" style="background-image: url('<?php echo $image; ?>')"></div>
					<div class="c-card-trending-now__wrapper">
						<div class="c-card-trending-now__content">
							<span class="c-card-trending-now__tag cat-<?php echo get_the_category($postID)[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postID, 'sub-category'); echo $subcategory[0]->name; ?></span>
							<h3 class="c-card-trending-now__title"><?php echo $title; ?></h3>
							<button class="c-card-trending-now__bt is-video">Watch now</button>
						</div>
					</div>
				</a>
			<?php endif; endfor; ?>
		</div>
	</section>

<?php endif; ?>
