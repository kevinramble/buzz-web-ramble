<?php
    $postInfo = null;
    $main_video_function = 'main_video' . $country;

	for($d = 1; $d <= 3; $d++){
		$postID = $main_video_function('main_video'. $country .'_post_id'.$d);
		$postTitle = $main_video_function('main_video'. $country .'_post_title'.$d);
		$customTitle = $main_video_function('main_video'. $country .'_title'.$d);
		$title = ($customTitle) ? $customTitle : $postTitle;
		$customImage = $main_video_function('main_video'. $country .'_img'.$d);
		$image = '';

		if(!$customImage){
			if(has_post_thumbnail($postID)){
				$image = get_the_post_thumbnail_url($postID);
			} else {
				$image = catch_that_image($postID);
			}
		} else {
			$image = $customImage;
		}

		if($postID){
			$postInfo[] = array(
				'postID' => $postID,
				'title' => $title,
				'image' => $image
			);
		}
    } // for get content
?>

<section class="c-block-videos">
    <h1 class="c-block-videos__title">Play</h1>

    <div class="row no-gutters">
        <div class="col-12 col-lg-7">
            <?php if(isset($postInfo[0])){ ?>
            <div class="c-block-videos__one">
                <div class="c-card-video c-card-video--bigger">
                    <a href="<?php echo get_permalink($postInfo[0]['postID']); ?>" class="c-card-video__img" title="<?php echo $postInfo[0]['title']; ?>" style="background-image: url('<?php echo $postInfo[0]['image']; ?>')"></a>

					<div class="c-card-video__wrapper">
                        <div class="c-card-video__content">
                            <small class="c-card-video__date"><?php echo get_the_date('d.m.Y', $postInfo[0]['postID']); ?></small>
                            <h3 class="c-card-video__title"><?php echo $postInfo[0]['title']; ?></h3>

                            <div class="c-card-video__tools">
                                <a href="<?php echo get_permalink($postInfo[0]['postID']); ?>" title="<?php echo $postInfo[0]['title']; ?>" class="c-card-video__bt">Watch now</a>

                                <div class="c-social c-social--light">
                                    <a class="c-social__icon c-social__icon--fb" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink($postInfo[0]['postID']); ?>" target="_blank" aria-label="Facebook"></a>
                                    <a class="c-social__icon c-social__icon--tw" href="http://twitter.com/share?text=<?php echo $postInfo[0]['title']; ?>&url=<?php the_permalink($postInfo[0]['postID']); ?>" target="_blank" aria-label="Twitter"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } //if isset ?>
        </div>
        <div class="col-12 col-lg-5">
            <div class="row no-gutters">
                <?php if(isset($postInfo[1])){ ?>
                <div class="c-block-videos__two col-12 col-md-6 col-lg-12">
                    <div class="c-card-video">
                        <a href="<?php echo get_permalink($postInfo[1]['postID']); ?>" title="<?php echo $postInfo[1]['title']; ?>" class="c-card-video__img" style="background-image: url('<?php echo $postInfo[1]['image']; ?>')"></a>
                        <div class="c-card-video__wrapper">
                            <div class="c-card-video__content">
                                <h3 class="c-card-video__title"><?php echo $postInfo[1]['title']; ?></h3>

                                <div class="c-card-video__tools">
                                    <a href="<?php echo get_permalink($postInfo[1]['postID']); ?>" title="<?php echo $postInfo[1]['title']; ?>" class="c-card-video__bt">Watch now</a>

                                    <div class="c-social c-social--light">
                                        <a class="c-social__icon c-social__icon--fb" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink($postInfo[1]['postID']); ?>" target="_blank" aria-label="Facebook"></a>
                                        <a class="c-social__icon c-social__icon--tw" href="http://twitter.com/share?text=<?php echo $postInfo[1]['title']; ?>&url=<?php the_permalink($postInfo[1]['postID']); ?>" target="_blank" aria-label="Twitter"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } //if isset
                if(isset($postInfo[2])){ ?>
                <div class="c-block-videos__three no-gutters col-12 col-md-6 col-lg-12">
                    <div class="c-card-video c-card-video--height">
                        <a href="<?php echo get_permalink($postInfo[2]['postID']); ?>" title="<?php echo $postInfo[2]['title']; ?>" class="c-card-video__img" style="background-image: url('<?php echo $postInfo[2]['image']; ?>')"></a>
                        <div class="c-card-video__wrapper">
                            <div class="c-card-video__content">
                                <h3 class="c-card-video__title"><?php echo $postInfo[2]['title']; ?></h3>

                                <div class="c-card-video__tools">
                                    <a href="<?php echo get_permalink($postInfo[2]['postID']); ?>" title="<?php echo $postInfo[2]['title']; ?>" class="c-card-video__bt">Watch now</a>

                                    <div class="c-social c-social--light">
                                        <a class="c-social__icon c-social__icon--fb" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink($postInfo[2]['postID']); ?>" target="_blank" aria-label="Facebook"></a>
                                        <a class="c-social__icon c-social__icon--tw" href="http://twitter.com/share?text=<?php echo $postInfo[2]['title']; ?>&url=<?php the_permalink($postInfo[2]['postID']); ?>" target="_blank" aria-label="Twitter"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } //if isset ?>
            </div>
        </div>
    </div>
</section>
