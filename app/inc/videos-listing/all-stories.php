<section>
    <div class="container">
        <div class="row no-gutters">
            <section class="c-all-stories">
                <h2 class="c-all-stories__title"><?php echo $areaTitle; ?></h2>
                <div class="c-all-stories__grid">
                    <?php $args = array('post_type' => 'video', 'posts_per_page' => 12, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged); query_posts($args);
                    if (have_posts()) : ?>
                    <div class="row js-posts-list">
                        <?php while (have_posts()) : the_post();
                            loop_genericcard_small($post->ID);
                        endwhile; ?>
                    </div><!-- .js-posts-list -->
                    <?php else : ?>
                        <h3 class="results-title error">Nenhum post encontrado.</h3>
                    <?php endif;

                    // load more ajax
                    $wp_query->query_vars['search_orderby_title'] = ''; // necessario pro search
                    $load_posts = $wp_query->query_vars; $load_current_page = $wp_query->query_vars['paged']; $load_max_page = $wp_query->max_num_pages;
                    loadmore_button($load_posts, $load_current_page, $load_max_page);
                    if($wp_query->max_num_pages > 1){ ?>
                        <span class="js-loadmore c-bt-load">Load more</span>
                    <?php } else { ?>
                        <span class="js-loadmore c-bt-load hidden">Load more</span>
                    <?php } // end load more ajax

                    wp_reset_query(); wp_reset_postdata(); ?>
                </div>
            </section>
        </div>
    </div>
</section>