<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<?php
if (is_home()) {
    ?>
    <script>
        window.googletag = window.googletag || {
            cmd: []
        };
        googletag.cmd.push(function() {

            //Desktop
            googletag.defineSlot('/21850622224/Buzz_Home_Leaderboard_Below_Hero', [970, 250], 'div-gpt-ad-1572047490984-0').addService(googletag.pubads());
            googletag.defineSlot('/21850622224/Buzz_Home_Leaderboard_Below_Trending', [970, 250], 'div-gpt-ad-1570538697183-0').addService(googletag.pubads());
            googletag.defineSlot('/21850622224/Buzz_Home_Leaderboard_Below_Categories', [970, 250], 'div-gpt-ad-1572047537304-0').addService(googletag.pubads());
            googletag.defineSlot('/21850622224/Buzz_AboveJobs_Desktop', [970, 250], 'div-gpt-ad-1570479828392-0').addService(googletag.pubads());
            //Mobile
            googletag.defineSlot('/21850622224/Buzz_Home_Below_Hero_Mobile', [320, 100], 'div-gpt-ad-1570538641848-0').addService(googletag.pubads());
            googletag.defineSlot('/21850622224/Buzz_Home_Below_Trending_Mobile', [320, 100], 'div-gpt-ad-1570538742200-0').addService(googletag.pubads());
            googletag.defineSlot('/21850622224/Buzz_Home_Below_Categories_Mobile', [320, 100], 'div-gpt-ad-1570538542819-0').addService(googletag.pubads());
            googletag.defineSlot('/21850622224/Buzz_Home_Above_Jobs_Mobile', [320, 100], 'div-gpt-ad-1570538362786-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<?php
} else if (is_page_template('jobs.php')) {
    ?>
    <script>
        window.googletag = window.googletag || {
            cmd: []
        };
        googletag.cmd.push(function() {
            //Desktop
            googletag.defineSlot('/21850622224/Buzz_JobsList_RightSideBar_Desktop', [300, 600], 'div-gpt-ad-1570539074988-0').addService(googletag.pubads());
            //Mobile
            googletag.defineSlot('/21850622224/Buzz_Jobs_AboveListing_Mobile', [320, 100], 'div-gpt-ad-1570538904609-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<?php
} else if (is_page_template('offers.php')) {
    ?>
    <script>
        window.googletag = window.googletag || {
            cmd: []
        };
        googletag.cmd.push(function() {
            //Desktop
            //Mobile
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<?php
} else if (is_page_template('videos.php')) {
    ?>
    <script>
        window.googletag = window.googletag || {
            cmd: []
        };
        googletag.cmd.push(function() {
            //Desktop
            //Mobile
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<?php
} else if (is_post_type_archive('out')) {
    ?>
    <script>
        window.googletag = window.googletag || {
            cmd: []
        };
        googletag.cmd.push(function() {
            //Desktop
            //Mobile
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<?php
} else if (is_page_template('categories.php')) {
    ?>
    <script>
        window.googletag = window.googletag || {
            cmd: []
        };
        googletag.cmd.push(function() {
            //Desktop
            googletag.defineSlot('/21850622224/Buzz_Above_AllStories_News_Desktop', [970, 250], 'div-gpt-ad-1570479794550-0').addService(googletag.pubads());
            googletag.defineSlot('/21850622224/Buzz_Above_AllStories_Hot_Desktop', [970, 250], 'div-gpt-ad-1570479388071-0').addService(googletag.pubads());
            googletag.defineSlot('/21850622224/Buzz_Above_AllStories_Life_Desktop', [970, 250], 'div-gpt-ad-1570479580087-0').addService(googletag.pubads());
            //Mobile
            googletag.defineSlot('/21850622224/Buzz_News_Above_All_Stories_Mobile', [320, 50], 'div-gpt-ad-1570539349933-0').addService(googletag.pubads());
            googletag.defineSlot('/21850622224/Buzz_Hot_Stories_Mobile', [300, 250], 'div-gpt-ad-1570538878634-0').addService(googletag.pubads());
            googletag.defineSlot('/21850622224/Buzz_Life_Stories_Mobile', [300, 250], 'div-gpt-ad-1570539319908-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<?php
} else  if (is_search()) {
    ?>
    <script>
        window.googletag = window.googletag || {
            cmd: []
        };
        googletag.cmd.push(function() {
            //Desktop
            googletag.defineSlot('/21850622224/Buzz_Search_Results_Desktop', [300, 600], 'div-gpt-ad-1570539410042-0').addService(googletag.pubads());
            //Mobile
            googletag.defineSlot('/21850622224/Buzz_Search_Mobile', [300, 250], 'div-gpt-ad-1570539377887-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<?php
} else if (is_single() && 'post' == get_post_type()) {
    ?>
    <script>
        window.googletag = window.googletag || {
            cmd: []
        };
        googletag.cmd.push(function() {
            //Desktop
            googletag.defineSlot('/21850622224/Buzz_Article_RightSideBar_Desktop', [300, 600], 'div-gpt-ad-1570479897664-0').addService(googletag.pubads());
            //Mobile
            googletag.defineSlot('/21850622224/Buzz_Hot_Article_Mobile', [320, 250], 'div-gpt-ad-1570538852690-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<?php
} else if (is_single() && 'job' == get_post_type()) {
    ?>
    <script>
        window.googletag = window.googletag || {
            cmd: []
        };
        googletag.cmd.push(function() {
            //Desktop
            googletag.defineSlot('/21850622224/Buzz_JobPost_Right_Desktop', [300, 600], 'div-gpt-ad-1570538937641-0').addService(googletag.pubads());
            //Mobile
            googletag.defineSlot('/21850622224/Buzz_JobsPost_Mobile', [320, 250], 'div-gpt-ad-1570539208617-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    </script>
<?php
}
?>