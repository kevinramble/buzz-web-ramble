<div class="l-box-search js-box-search">
	<div class="container">
		<form action="<?php bloginfo('url'); ?>" class="c-search-form c-search-form--negative" method="get">
			<input id="s" name="s" type="search" class="c-search-form__input" placeholder="Search">
		</form>
	</div>
</div>