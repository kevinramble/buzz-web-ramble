<div class="l-menu-mobile js-box-menu-mobile">
    <div class="l-menu-mobile__wrapper">
        <div class="l-menu-mobile__item">
            <!-- Remove country picker -->
            <div class="l-menu-mobile__item" style="margin: 0px 5px 0px 5px;">
                <div class="c-select c-select--highlight" style="height:40px;">
                    <select name="country" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                        <option value="?country=global" <?php if($country != '_jamaica' || $country != '_stlucia' /*|| $country != '_turksandcaicos' || $country != '_barbados'|| $country != '_bahamas'|| $country != '_cayman'*/) echo 'selected="selected"'; ?>>Global</option>
                        <option value="?country=jamaica" <?php if($country == '_jamaica') echo 'selected="selected"'; ?>>Jamaica</option>
                        <option value="?country=stlucia" <?php if($country == '_stlucia') echo 'selected="selected"'; ?>>St. Lucia</option>
                        <option value="?country=turksandcaicos" <?php if($country == '_turksandcaicos') echo 'selected="selected"'; ?>>Turks and Caicos</option>
                        <!-- New Countries -->
                        <option value="?country=barbados" <?php if($country == '_barbados') echo 'selected="selected"'; ?>>Barbados</option>
                        <option value="?country=bahamas" <?php if($country == '_bahamas') echo 'selected="selected"'; ?>>Bahamas</option>
                        <option value="?country=cayman" <?php if($country == '_cayman') echo 'selected="selected"'; ?>>Cayman</option>

                    </select>
                </div>
            </div>
        </div>
        <!-- End Remove country picker -->
        <ul class="c-navbar c-navbar--stacked">
            <li class="c-navbar__item">
                <a href="<?php echo get_bloginfo('url'); ?>/news" class="c-navbar__link">News</a>
            </li>
            <li class="c-navbar__item">
                <a href="<?php echo get_bloginfo('url'); ?>/hot" class="c-navbar__link">Hot</a>
            </li>
            <li class="c-navbar__item">
                <a href="<?php echo get_bloginfo('url'); ?>/life" class="c-navbar__link">Life</a>
            </li>
            <li class="c-navbar__item">
                <a href="<?php echo get_bloginfo('url'); ?>/play" class="c-navbar__link">Play</a>
            </li>
            <li class="c-navbar__item">
                <a href="<?php echo get_bloginfo('url'); ?>/out" class="c-navbar__link">Out</a>
            </li>
            <li class="c-navbar__item">
                <a href="<?php echo get_bloginfo('url'); ?>/jobs" class="c-navbar__link">Jobs</a>
            </li>
            <li class="c-navbar__item">
                <a href="<?php echo get_bloginfo('url'); ?>/offers" class="c-navbar__link">Offers</a>
            </li>
            
            
            <!-- <li class="c-navbar__item">
                <a href="#0" class="c-navbar__link js-toggle-drop-mobile">
                    <span class="icon icon-more"></span>
                </a>
                <ul class="c-navbar__drop c-navbar__drop--outline js-box-drop-mobile">
                    <li class="c-navbar__drop-item">
                        <a href="#0" class="c-navbar__drop-link c-navbar__drop-link--outline">Hot</a>
                    </li>
                    <li class="c-navbar__drop-item">
                        <a href="#0" class="c-navbar__drop-link c-navbar__drop-link--outline">Hot</a>
                    </li>
                </ul>
            </li> -->
        </ul>
            
            

        <div class="l-menu-mobile__item">
            <ul class="c-navbar c-navbar--stacked">
                <li class="c-navbar__item border-0">
                    <a href="#0" class="c-navbar__link c-navbar__link--toggle text-left js-toggle-drop-mobile">
                        <span class="icon icon-signup"></span>
                    </a>
                    <?php if(is_user_logged_in()) : ?>
                        <ul class="c-navbar__drop js-box-drop-mobile">
                            <li class="c-navbar__drop-item">
                                <a href="<?php echo get_bloginfo('url'); ?>/user/?tab=favorites" class="c-navbar__drop-link c-navbar__drop-link--secondary">Favorites</a>
                            </li>
                            <li class="c-navbar__drop-item">
                                <a href="<?php echo get_bloginfo('url'); ?>/user/?tab=offers" class="c-navbar__drop-link c-navbar__drop-link--secondary">My Offers</a>
                            </li>
                            <li class="c-navbar__drop-item">
                                <a href="<?php echo get_bloginfo('url'); ?>/account" class="c-navbar__drop-link c-navbar__drop-link--secondary">Settings</a>
                            </li>
                            <li class="c-navbar__drop-item">
                                <a href="<?php echo get_bloginfo('url'); ?>/logout" class="c-navbar__drop-link c-navbar__drop-link--secondary">Logout</a>
                            </li>
                        </ul>
                    <?php else : ?>
                        <ul class="c-navbar__drop js-box-drop-mobile">
                            <li class="c-navbar__drop-item">
                                <a href="<?php echo get_bloginfo('url'); ?>/login" class="c-navbar__drop-link c-navbar__drop-link--secondary">Sign in</a>
                            </li>
                            <li class="c-navbar__drop-item">
                                <a href="<?php echo get_bloginfo('url'); ?>/register" class="c-navbar__drop-link c-navbar__drop-link--secondary">Register</a>
                            </li>
                        </ul>
                    <?php endif; ?>
                </li>
            </ul>
        </div>

        <?php /*<div class="l-menu-mobile__item">
            <div class="c-select">
                <select name="country">
                    <option value="jamaica">Jamaica</option>
                    <option value="country">Country</option>
                </select>
            </div>
        </div>*/ ?>

        <div class="l-menu-mobile__item">
            <div class="c-input-group c-input-group--addon">
                <input type="text" class="c-input c-input--negative" placeholder="Subscribe">
                <button class="c-button c-button--negative">Send</button>
            </div>
        </div>

        <ul class="l-buttons justify-content-center">
            <li class="l-buttons__item">
                <a href="http://facebook.com/werbuzz" target="_blank" class="c-button c-button--icon c-button--negative c-button--circle"><span class="icon icon-facebook"></span></a>
            </li>
            <li class="l-buttons__item">
                <a href="http://twitter.com/werbuzz" target="_blank" class="c-button c-button--icon c-button--negative c-button--circle"><span class="icon icon-twitter"></span></a>
            </li>
            <li class="l-buttons__item">
                <a href="https://www.instagram.com/buzzcaribbean/" target="_blank" class="c-button c-button--icon c-button--negative c-button--circle"><span class="icon icon-instagram"></span></a>
            </li>
            <li class="l-buttons__item">
                <a href="https://www.youtube.com/channel/UCtSyhurnO8mOyJxXXDZ7t0A" target="_blank" class="c-button c-button--icon c-button--negative c-button--circle"><span class="icon icon-youtube-play"></span></a>
            </li>
        </ul>
    </div>
</div>
