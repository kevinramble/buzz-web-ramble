<ul class="c-navbar">
    <li class="c-navbar__item">
        <a href="<?php echo get_bloginfo('url'); ?>/news" class="c-navbar__link has-hover is-news <?php echo (is_page('news')) ? 'is-active' : '' ?>">News</a>
    </li>
    <li class="c-navbar__item">
        <a href="<?php echo get_bloginfo('url'); ?>/hot" class="c-navbar__link has-hover is-hot <?php echo (is_page('hot')) ? 'is-active' : '' ?>">Hot</a>
    </li>
    <li class="c-navbar__item">
        <a href="<?php echo get_bloginfo('url'); ?>/life" class="c-navbar__link has-hover is-life <?php echo (is_page('life')) ? 'is-active' : '' ?>">Life</a>
    </li>
    <li class="c-navbar__item">
        <a href="<?php echo get_bloginfo('url'); ?>/play" class="c-navbar__link has-hover is-video <?php echo (is_page_template('videos.php')) ? 'is-active' : '' ?>">Play</a>
    </li>
    <li class="c-navbar__item">
        <a href="<?php echo get_bloginfo('url'); ?>/out" class="c-navbar__link has-hover is-out <?php echo (is_post_type_archive('out')) ? 'is-active' : '' ?>">Out</a>
    </li>
    <li class="c-navbar__item">
        <a href="<?php echo get_bloginfo('url'); ?>/jobs" class="c-navbar__link has-hover is-job <?php echo (is_page_template('jobs.php')) ? 'is-active' : '' ?>">Jobs</a>
    </li>
    <li class="c-navbar__item">
        <a href="<?php echo get_bloginfo('url'); ?>/offers" class="c-navbar__link has-hover <?php echo (is_page_template('offers.php')) ? 'is-active' : '' ?>">Offers</a>
    </li>


    <?php if(is_user_logged_in()) : ?>
        <li class="c-navbar__item js-toggle-dropdown">
            <a href="<?php echo get_bloginfo('url'); ?>/user" class="c-navbar__link"><span class="icon icon-signup"></span></a>

            <div class="c-navbar__drop--floating js-box-dropdown">
                <ul class="c-navbar__drop-list">
                    <li class="c-navbar__drop-item">
                        <a href="<?php echo get_bloginfo('url'); ?>/user/?tab=favorites" class="c-navbar__drop-link">Favorites</a>
                    </li>
                    <li class="c-navbar__drop-item">
                        <a href="<?php echo get_bloginfo('url'); ?>/user/?tab=offers" class="c-navbar__drop-link">My offers</a>
                    </li>
                    <li class="c-navbar__drop-item">
                        <a href="<?php echo get_bloginfo('url'); ?>/account" class="c-navbar__drop-link">Settings</a>
                    </li>
                    <li class="c-navbar__drop-item">
                        <a href="<?php echo get_bloginfo('url'); ?>/logout" class="c-navbar__drop-link is-dark">Logout</a>
                    </li>
                </ul>
            </div>
        </li>
    <?php else : ?>
        <li class="c-navbar__item js-toggle-dropdown">
            <a href="<?php echo get_bloginfo('url'); ?>/login" class="c-navbar__link"><span class="icon icon-signup"></span></a>

            <div class="c-navbar__drop--floating js-box-dropdown">
                <ul class="c-navbar__drop-list">
                    <li class="c-navbar__drop-item">
                        <a href="<?php echo get_bloginfo('url'); ?>/login" class="c-navbar__drop-link">Sign in</a>
                    </li>
                    <li class="c-navbar__drop-item">
                        <a href="<?php echo get_bloginfo('url'); ?>/register" class="c-navbar__drop-link">Register</a>
                    </li>
                </ul>
            </div>
        </li>
    <?php endif; ?>

    <li class="c-navbar__item js-toggle-search">
        <a href="#0" class="c-navbar__link"><span class="icon icon-search"></span></a>
    </li>
</ul>