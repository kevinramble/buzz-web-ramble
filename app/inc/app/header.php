<!DOCTYPE html>
<html class="no-js" lang="en-US" xmlns:og="http://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php bloginfo('name'); ?></title>

	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="author" content="Buzz">

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Assistant:200,400,700">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/styles/main.css">

	<script src="<?php echo get_template_directory_uri(); ?>/scripts/modernizr.js"></script>

	<?php include_once('../shared/ads.php'); ?>

	<?php wp_head(); ?>
</head>

<?php $class_main =  (is_singular('video')) ? 'is-dark' : ''; ?>

<body <?php body_class($class_main); ?>>

	<main class="l-main <?php echo $class_main; ?>">
