<div class="c-post-sidebar webview">
    <!-- /21850622224/Buzz_News_Above_All_Stories_Mobile -->
    <div id='div-gpt-ad-1570539349933-0' class="mx-auto mt-3" style='width: 320px; height: 50px;'>
        <script>
        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570539349933-0'); });
        </script>
    </div>
    <?php
            $post_tags = get_the_tags();
            // get_the_terms returns an array of WP_Term objects
            foreach ($post_tags as $tag_obj)
                $_post_tags[] = $tag_obj->term_id; // get the id from the WP_Term object
                
            $cats = !empty($cat) ? $array : '';
            $postid = get_the_ID();
            $related = new WP_Query(
                array('posts_per_page' => 4, 
                        'post__not_in'=> array($postid),
                        'orderby' => 'id',
                        'order' => 'DESC', 
                        'tag__in' => $_post_tags
                    ));
            $cnt = 0;
            $relatedcount = $related->post_count;

    ?>

    <?php
        //$related = get_related_posts(get_the_ID(), 4);
        if($related->have_posts()): ?>
            <h2>More like this</h2>

            <?php while($related->have_posts()) : $related->the_post();
                if(has_post_thumbnail()){
                    $image = get_the_post_thumbnail_url();
                } else {
                    $image = catch_that_image();
                } ?>
                <a href="<?php the_permalink(); ?>?webview=true" title="<?php the_title(); ?>" class="c-post-sidebar__item">
                    <div class="c-post-sidebar__wrap-thumb">
                        <div class="c-post-sidebar__thumb" style="background-image:url('<?php echo thumb_exist($image); ?>')"></div>
                    </div>
                    <p class="c-post-sidebar__description"><?php the_title(); ?></p>
                </a>
            <?php endwhile; ?>
        <?php endif; wp_reset_query(); wp_reset_postdata();
    ?>
</div>