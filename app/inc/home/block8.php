<?php
    $minimumPosts = 10;
    $postInfo = null;
    $arr = null;
    $totalPosts = 0;

    
   
    function latestNews($country){
        switch ($country) {
            case '_stlucia':
                $countryTitle = "St. Lucia";
                $countryTerm = "st-lucia";
                break;
            case '_jamaica':
                $countryTitle = "Jamaica";
                $countryTerm = "jamaica";
            break;
            default:
                $countryTitle = "";
                $countryTerm = "";
                break;
        }

        $getCountry = get_term_by('name', $countryTitle, 'countries');
        $countryID = $getCountry->term_id;
        $image = '';
        $buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
        $buttonType = ($type == 'video') ? ' is-video' : '';

        $args = new WP_Query( 
            array(
            'taxonomy' => 'countries', 
            'term' => $countryTerm,           
            'posts_per_page' => 10, 'orderby' => 'date', 
            'post_type' => array('post', 'video'), 
            'order' => 'DESC', 'childcat' => 1)
        ); 
        //query_posts($args);
        //print_r($args);
        
         if ($args->have_posts()){
             while ($args->have_posts()) : $args->the_post();

             if(has_post_thumbnail(get_the_ID())){
                $image = get_the_post_thumbnail_url(get_the_ID(),'full');
            } else {
                $image = catch_that_image(get_the_ID());
            }
                $arr[] = array(
                    'postID' => get_the_ID(),
                    'title' => get_the_title(),
                    'image' => $image,
                    'buttonTitle' => $buttonTitle,
                    'buttonType' => $buttonType
                );
            endwhile;
         }
            
        wp_reset_postdata();
        return $arr;
    }
   
   function topTen($country){

        $top_ten_function = 'top_ten' . $country;

        for($f = 1; $f <= 10; $f++){
            $category = $top_ten_function('top_ten'. $country .'_cat'.$f);
            $type = $top_ten_function('top_ten'. $country .'_type'.$f);
            $postID = $top_ten_function('top_ten'. $country .'_post_id'.$f);
            $postTitle = $top_ten_function('top_ten'. $country .'_post_title'.$f);
            $customTitle = $top_ten_function('top_ten'. $country .'_title'.$f);
            $title = ($customTitle) ? $customTitle : $postTitle;
            $customImage = $top_ten_function('top_ten'. $country .'_img'.$f);
            $image = '';
            $buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
            $buttonType = ($type == 'video') ? ' is-video' : '';
    
            if(!$customImage){
                if(has_post_thumbnail($postID)){
                    $image = get_the_post_thumbnail_url($postID);
                } else {
                    $image = catch_that_image($postID);
                }
            } else {
                $image = $customImage;
            }

            if($postID){
                $arr[] = array(
                    'postID' => $postID,
                    'title' => $title,
                    'image' => $image,
                    'buttonTitle' => $buttonTitle,
                    'buttonType' => $buttonType
                );
            }
        } // for get content
        return $arr;
    }

    //$postInfo = topTen($country);
    $postInfo = ($country) ? latestNews($country) : topTen($country);;
    //var_dump($postI);
    if($postInfo) $totalPosts = count($postInfo);
    if($totalPosts >= $minimumPosts) :
?>

    <section class="c-top-10-stories">
        <div id="desktop-stories">
            <div class="row no-gutters">
                <div class="c-top-10-stories-hero__left">
                    <h1 class="c-top-10-stories-hero__title"> <?php echo ($country == '_stlucia') ? 'Latest News' : 'Top 10 Stories'; ?> </h1>

                    <?php if(isset($postInfo[0])){ ?>
                        <a href="<?php echo get_permalink($postInfo[0]['postID']); ?>" title="<?php echo $postInfo[0]['title']; ?>" class="c-top-10-stories-card">
                            <div class="c-top-10-stories-card__img" style="background-image: url('<?php echo $postInfo[0]['image']; ?>')"></div>
                            <div class="c-top-10-stories-card__wrapper">
                                <div class="c-top-10-stories-card__content">
                                    <span class="c-top-10-stories-card__tag cat-<?php echo get_the_category($postInfo[0]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[0]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
                                    <h3 class="c-top-10-stories-card__title c-top-10-stories-card__title--big"><?php echo $postInfo[0]['title']; ?></h3>
                                    <button class="c-top-10-stories-card__bt <?php echo $postInfo[0]['buttonType']; ?>"><?php echo $postInfo[0]['buttonTitle']; ?></button>
                                </div>
                            </div>
                        </a>
                    <?php } //isset ?>
                </div>
                <div class="c-top-10-stories-hero__right">
                    <?php if(isset($postInfo[1])){ ?>
                    <div class="c-block-two__img-top">
                        <a href="<?php echo get_permalink($postInfo[1]['postID']); ?>" title="<?php echo $postInfo[1]['title']; ?>" class="c-top-10-stories-card c-top-10-stories-card--wide">
                            <div class="c-top-10-stories-card__img" style="background-image: url('<?php echo $postInfo[1]['image']; ?>')"></div>
                            <div class="c-top-10-stories-card__wrapper">
                                <div class="c-top-10-stories-card__content">
                                    <span class="c-top-10-stories-card__tag cat-<?php echo get_the_category($postInfo[1]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[1]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
                                    <h3 class="c-top-10-stories-card__title"><?php echo $postInfo[1]['title']; ?></h3>
                                    <button class="c-top-10-stories-card__bt <?php echo $postInfo[1]['buttonType']; ?>"><?php echo $postInfo[1]['buttonTitle']; ?></button>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php } if(isset($postInfo[2])){ ?>
                    <div class="c-block-two__img-bottom">
                        <a href="<?php echo get_permalink($postInfo[2]['postID']); ?>" title="<?php echo $postInfo[2]['title']; ?>" class="c-top-10-stories-card c-top-10-stories-card--square">
                            <div class="c-top-10-stories-card__img" style="background-image: url('<?php echo $postInfo[2]['image']; ?>')"></div>
                            <div class="c-top-10-stories-card__wrapper">
                                <div class="c-top-10-stories-card__content">
                                    <span class="c-top-10-stories-card__tag cat-<?php echo get_the_category($postInfo[2]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[2]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
                                    <h3 class="c-top-10-stories-card__title"><?php echo $postInfo[2]['title']; ?></h3>
                                    <button class="c-top-10-stories-card__bt <?php echo $postInfo[2]['buttonType']; ?>"><?php echo $postInfo[2]['buttonTitle']; ?></button>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php } //isset ?>
                </div>
            </div>

            <div class="row no-gutters">
            <?php for($e = 3; $e <= 5; $e++){ // side highlights
                if(isset($postInfo[$e])){ ?>
                <div class="col-lg-4">
                    <a href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>" class="c-top-10-stories-card c-top-10-stories-card--square">
                        <div class="c-top-10-stories-card__img" style="background-image: url('<?php echo $postInfo[$e]['image']; ?>')"></div>
                        <div class="c-top-10-stories-card__wrapper">
                            <div class="c-top-10-stories-card__content">
                                <span class="c-top-10-stories-card__tag cat-<?php echo get_the_category($postInfo[$e]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[$e]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
                                <h3 class="c-top-10-stories-card__title c-top-10-stories-card__title--small"><?php echo $postInfo[$e]['title']; ?></h3>
                                <button class="c-top-10-stories-card__bt <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></button>
                            </div>
                        </div>
                    </a>
                </div>
                <?php } //if isset
            } //for side highlights ?>
            </div>

            <div class="row no-gutters">
                <?php if(isset($postInfo[6])){ ?>
                <div class="col-lg-4">
                    <a href="<?php echo get_permalink($postInfo[6]['postID']); ?>" title="<?php echo $postInfo[6]['title']; ?>" class="c-top-10-stories-card c-top-10-stories-card--square">
                        <div class="c-top-10-stories-card__img" style="background-image: url('<?php echo $postInfo[6]['image']; ?>')"></div>
                        <div class="c-top-10-stories-card__wrapper">
                            <div class="c-top-10-stories-card__content">
                                <span class="c-top-10-stories-card__tag cat-<?php echo get_the_category($postInfo[6]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[6]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
                                <h3 class="c-top-10-stories-card__title c-top-10-stories-card__title--small"><?php echo $postInfo[6]['title']; ?></h3>
                                <button class="c-top-10-stories-card__bt <?php echo $postInfo[6]['buttonType']; ?>"><?php echo $postInfo[6]['buttonTitle']; ?></button>
                            </div>
                        </div>
                    </a>
                </div>
                <?php } if(isset($postInfo[7])){ ?>
                <div class="col-lg-8">
                    <a href="<?php echo get_permalink($postInfo[7]['postID']); ?>" title="<?php echo $postInfo[7]['title']; ?>" class="c-top-10-stories-card c-top-10-stories-card--square">
                        <div class="c-top-10-stories-card__img" style="background-image: url('<?php echo $postInfo[7]['image']; ?>')"></div>
                        <div class="c-top-10-stories-card__wrapper">
                            <div class="c-top-10-stories-card__content">
                                <span class="c-top-10-stories-card__tag cat-<?php echo get_the_category($postInfo[7]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[7]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
                                <h3 class="c-top-10-stories-card__title"><?php echo $postInfo[7]['title']; ?></h3>
                                <button class="c-top-10-stories-card__bt <?php echo $postInfo[7]['buttonType']; ?>"><?php echo $postInfo[7]['buttonTitle']; ?></button>
                            </div>
                        </div>
                    </a>
                </div>
                <?php } //if isset ?>
            </div>

            <div class="row no-gutters">
                <?php if(isset($postInfo[8])){
                // se tiver ad, mudar essa classe pra col-lg-4 e adicionar o ad no final ?>
                <div class="col-lg-8">
                    <a href="<?php echo get_permalink($postInfo[8]['postID']); ?>" title="<?php echo $postInfo[8]['title']; ?>" class="c-top-10-stories-card c-top-10-stories-card--square">
                        <div class="c-top-10-stories-card__img" style="background-image: url('<?php echo $postInfo[8]['image']; ?>')"></div>
                        <div class="c-top-10-stories-card__wrapper">
                            <div class="c-top-10-stories-card__content">
                                <span class="c-top-10-stories-card__tag cat-<?php echo get_the_category($postInfo[8]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[8]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
                                <h3 class="c-top-10-stories-card__title c-top-10-stories-card__title--small"><?php echo $postInfo[8]['title']; ?></h3>
                                <button class="c-top-10-stories-card__bt <?php echo $postInfo[8]['buttonType']; ?>"><?php echo $postInfo[8]['buttonTitle']; ?></button>
                            </div>
                        </div>
                    </a>
                </div>
                <?php } if(isset($postInfo[9])){ ?>
                <div class="col-lg-4">
                    <a href="<?php echo get_permalink($postInfo[9]['postID']); ?>" title="<?php echo $postInfo[9]['title']; ?>" class="c-top-10-stories-card c-top-10-stories-card--square">
                        <div class="c-top-10-stories-card__img" style="background-image: url('<?php echo $postInfo[9]['image']; ?>')"></div>
                        <div class="c-top-10-stories-card__wrapper">
                            <div class="c-top-10-stories-card__content">
                                <span class="c-top-10-stories-card__tag cat-<?php echo get_the_category($postInfo[9]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[9]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
                                <h3 class="c-top-10-stories-card__title c-top-10-stories-card__title--small"><?php echo $postInfo[9]['title']; ?></h3>
                                <button class="c-top-10-stories-card__bt <?php echo $postInfo[9]['buttonType']; ?>"><?php echo $postInfo[9]['buttonTitle']; ?></button>
                            </div>
                        </div>
                    </a>
                </div>
                <?php } //if isset ?>

                <?php /*<div class="col-lg-4">
                    <div class="c-top-10-stories-sponsorship">
                        <div class="c-top-10-stories-sponsorship__square">
                            Square<br>250x250
                        </div>
                    </div>
                </div>*/ ?>
            </div>
        </div>

        <!-- Mobile -->
        <div id="mobile-stories">
            <div class="c-top-10-stories-carousel">
                <h1 class="c-top-10-stories-carousel__title"><?php echo ($country == '_stlucia') ? 'Latest News' : 'Top 10 Stories'; ?></h1>
                <div class="c-top-10-stories-carousel__wrapper js-carousel-stories owl-carousel">

                    <?php for($e = 0; $e <= 9; $e++){ // side highlights
                        if(isset($postInfo[$e])){ ?>
                        <a href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>" class="c-top-10-stories-card">
                            <div class="c-top-10-stories-card__img" style="background-image: url('<?php echo $postInfo[$e]['image']; ?>')"></div>
                            <div class="c-top-10-stories-card__wrapper">
                                <div class="c-top-10-stories-card__content">
                                    <span class="c-top-10-stories-card__tag cat-<?php echo get_the_category($postInfo[$e]['postID'])[0]->slug; ?>"><?php echo get_the_category($postInfo[$e]['postID'])[0]->name; ?></span>
                                    <h3 class="c-top-10-stories-card__title c-top-10-stories-card__title--carousel"><?php echo $postInfo[$e]['title']; ?></h3>
                                    <button class="c-top-10-stories-card__bt <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></button>
                                </div>
                            </div>
                        </a>
                        <?php } //if isset
                    } //for side highlights ?>

                    <?php /* se tiver ad, adicionar ele na terceira posição
                    <div class="c-top-10-stories-sponsorship">
                        <div class="c-top-10-stories-sponsorship__square">
                            Square<br>250x250
                        </div>
                    </div>*/ ?>
                </div>
            </div>
        </div>

    </section>
<?php endif; ?>