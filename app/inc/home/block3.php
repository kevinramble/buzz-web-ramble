<?php
	$minimumPosts = 2;
	$postInfo = null;
	$totalPosts = 0;
	$editors_choice_function = 'editors_choice' . $country;

	for($d = 1; $d <= 4; $d++){
		$category = $editors_choice_function('editors_choice'. $country .'_cat'.$d);
		$type = $editors_choice_function('editors_choice'. $country .'_type'.$d);
		$postID = $editors_choice_function('editors_choice'. $country .'_post_id'.$d);
		$postTitle = $editors_choice_function('editors_choice'. $country .'_post_title'.$d);
		$customTitle = $editors_choice_function('editors_choice'. $country .'_title'.$d);
		$title = ($customTitle) ? $customTitle : $postTitle;
		$customImage = $editors_choice_function('editors_choice'. $country .'_img'.$d);
		$image = '';
		$buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
		$buttonType = ($type == 'video') ? ' is-video' : '';

		if(!$customImage){
			if(has_post_thumbnail($postID)){
				$image = get_the_post_thumbnail_url($postID);
			} else {
				$image = catch_that_image($postID);
			}
		} else {
			$image = $customImage;
		}

		if($postID){
			$postInfo[] = array(
				'postID' => $postID,
				'title' => $title,
				'image' => $image,
				'buttonTitle' => $buttonTitle,
				'buttonType' => $buttonType
			);
		}
	} // for get content

	if($postInfo) $totalPosts = count($postInfo);
	if($totalPosts >= $minimumPosts) :
?>

	<section>
		<div class="row no-gutters">
			<div class="col-12 col-xl-8">
				<a href="<?php echo get_permalink($postInfo[0]['postID']); ?>" title="<?php echo $postInfo[0]['title']; ?>" class="c-card-entry c-card-entry--headline">
					<div class="c-card-entry__img" style="background-image: url('<?php echo $postInfo[0]['image']; ?>')"></div>
					<div class="c-card-entry__wrapper">
						<div class="c-card-entry__content">
							<span class="c-card-entry__tag cat-<?php echo get_the_category($postInfo[0]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[0]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
							<h3 class="c-card-entry__title"><?php echo $postInfo[0]['title']; ?></h3>
							<button class="c-card-entry__bt <?php echo $postInfo[0]['buttonType']; ?>"><?php echo $postInfo[0]['buttonTitle']; ?></button>
						</div>
					</div>
				</a>
			</div>
			<div class="col-12 col-xl-4">
				<div class="c-headline-list">

				<?php for($e = 1; $e <= 3; $e++){ // block 3
					if(isset($postInfo[$e])){ ?>
					<div class="c-headline-list__item">
						<a href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>">
							<small class="c-headline-list__date"><?php echo get_the_date('d.m.Y', $postInfo[$e]['postID']); ?></small>
							<h3 class="c-headline-list__title"><?php echo $postInfo[$e]['title']; ?></h3>
						</a>
					</div>
					<?php } //if isset
				} //for block 3 ?>

				</div>
			</div>
		</div>
	</section>

<?php endif; ?>
