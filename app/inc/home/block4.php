<?php
	$minimumPosts = 3;
	$postInfo = null;
	$totalPosts = 0;
	$categories_block1_function = 'categories_block1' . $country;

	for($f = 1; $f <= 3; $f++){
		$category = $categories_block1_function('categories_block1'. $country .'_cat'.$f);
		$type = $categories_block1_function('categories_block1'. $country .'_type'.$f);
		$postID = $categories_block1_function('categories_block1'. $country .'_post_id'.$f);
		$postTitle = $categories_block1_function('categories_block1'. $country .'_post_title'.$f);
		$customTitle = $categories_block1_function('categories_block1'. $country .'_title'.$f);
		$title = ($customTitle) ? $customTitle : $postTitle;
		$customImage = $categories_block1_function('categories_block1'. $country .'_img'.$f);
		$image = '';
		$buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
		$buttonType = ($type == 'video') ? ' is-video' : '';

		if(!$customImage){
			if(has_post_thumbnail($postID)){
				$image = get_the_post_thumbnail_url($postID);
			} else {
				$image = catch_that_image($postID);
			}
		} else {
			$image = $customImage;
		}

		if($postID){
			$postInfo[] = array(
				'postID' => $postID,
				'title' => $title,
				'image' => $image,
				'buttonTitle' => $buttonTitle,
				'buttonType' => $buttonType
            );
		}
	} // for get content

	if($postInfo) $totalPosts = count($postInfo);
	if($totalPosts >= $minimumPosts) :
?>

	<section class="c-featured-blocks">
		<h2 class="c-featured-blocks__title">Hot</h2>

		<div class="row no-gutters js-blocks-carousel">
			<?php for($e = 0; $e <= 2; $e++){ // side cats
				if(isset($postInfo[$e])){ ?>
				<div class="c-featured-blocks__item">
					<a href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>" class="c-card-entry">
						<div class="c-card-entry__img" style="background-image: url('<?php echo $postInfo[$e]['image']; ?>')"></div>
						<div class="c-card-entry__wrapper">
							<div class="c-card-entry__content">
								<span class="c-card-entry__tag cat-<?php echo get_the_category($postInfo[$e]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[$e]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
								<h3 class="c-card-entry__title"><?php echo $postInfo[$e]['title']; ?></h3>
								<button class="c-card-entry__bt <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></button>
							</div>
						</div>
					</a>
				</div>
			<?php } //if isset
			} //for side cats ?>
		</div>
	</section>

<?php endif; ?>

<?php
	$postInfo = null;
	$totalPosts = 0;
	$categories_block2_function = 'categories_block2' . $country;

	for($f = 1; $f <= 3; $f++){
		$category = $categories_block2_function('categories_block2'. $country .'_cat'.$f);
		$type = $categories_block2_function('categories_block2'. $country .'_type'.$f);
		$postID = $categories_block2_function('categories_block2'. $country .'_post_id'.$f);
		$postTitle = $categories_block2_function('categories_block2'. $country .'_post_title'.$f);
		$customTitle = $categories_block2_function('categories_block2'. $country .'_title'.$f);
		$title = ($customTitle) ? $customTitle : $postTitle;
		$customImage = $categories_block2_function('categories_block2'. $country .'_img'.$f);
		$image = '';
		$buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
		$buttonType = ($type == 'video') ? ' is-video' : '';

		if(!$customImage){
			if(has_post_thumbnail($postID)){
				$image = get_the_post_thumbnail_url($postID);
			} else {
				$image = catch_that_image($postID);
			}
		} else {
			$image = $customImage;
		}

		if($postID){
			$postInfo[] = array(
				'postID' => $postID,
				'title' => $title,
				'image' => $image,
				'buttonTitle' => $buttonTitle,
				'buttonType' => $buttonType
            );
		}
	} // for get content

	if($postInfo) $totalPosts = count($postInfo);
	if($totalPosts >= $minimumPosts) :
?>

	<section class="c-featured-blocks">
		<h2 class="c-featured-blocks__title">Life</h2>

		<div class="row no-gutters js-blocks-carousel">
			<?php for($e = 0; $e <= 2; $e++){ // side cats
				if(isset($postInfo[$e])){ ?>
				<div class="c-featured-blocks__item">
					<a href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>" class="c-card-entry">
						<div class="c-card-entry__img" style="background-image: url('<?php echo $postInfo[$e]['image']; ?>')"></div>
						<div class="c-card-entry__wrapper">
							<div class="c-card-entry__content">
								<span class="c-card-entry__tag cat-<?php echo get_the_category($postInfo[$e]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[$e]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
								<h3 class="c-card-entry__title"><?php echo $postInfo[$e]['title']; ?></h3>
								<button class="c-card-entry__bt <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></button>
							</div>
						</div>
					</a>
				</div>
			<?php } //if isset
			} //for side cats ?>
		</div>
	</section>

<?php endif; ?>

<?php
	$postInfo = null;
	$totalPosts = 0;
	$categories_block3_function = 'categories_block3' . $country;

	for($f = 1; $f <= 3; $f++){
		$category = $categories_block3_function('categories_block3'. $country .'_cat'.$f);
		$type = $categories_block3_function('categories_block3'. $country .'_type'.$f);
		$postID = $categories_block3_function('categories_block3'. $country .'_post_id'.$f);
		$postTitle = $categories_block3_function('categories_block3'. $country .'_post_title'.$f);
		$customTitle = $categories_block3_function('categories_block3'. $country .'_title'.$f);
		$title = ($customTitle) ? $customTitle : $postTitle;
		$customImage = $categories_block3_function('categories_block3'. $country .'_img'.$f);
		$image = '';
		$buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
		$buttonType = ($type == 'video') ? ' is-video' : '';

		if(!$customImage){
			if(has_post_thumbnail($postID)){
				$image = get_the_post_thumbnail_url($postID);
			} else {
				$image = catch_that_image($postID);
			}
		} else {
			$image = $customImage;
		}

		if($postID){
			$postInfo[] = array(
				'postID' => $postID,
				'title' => $title,
				'image' => $image,
				'buttonTitle' => $buttonTitle,
				'buttonType' => $buttonType
            );
		}
	} // for get content

	if($postInfo) $totalPosts = count($postInfo);
	if($totalPosts >= $minimumPosts) :
?>

	<section class="c-featured-blocks">
		<h2 class="c-featured-blocks__title">News</h2>

		<div class="row no-gutters js-blocks-carousel">
			<?php for($e = 0; $e <= 2; $e++){ // side cats
				if(isset($postInfo[$e])){ ?>
				<div class="c-featured-blocks__item">
					<a href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>" class="c-card-entry">
						<div class="c-card-entry__img" style="background-image: url('<?php echo $postInfo[$e]['image']; ?>')"></div>
						<div class="c-card-entry__wrapper">
							<div class="c-card-entry__content">
								<span class="c-card-entry__tag cat-<?php echo get_the_category($postInfo[$e]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[$e]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
								<h3 class="c-card-entry__title"><?php echo $postInfo[$e]['title']; ?></h3>
								<button class="c-card-entry__bt <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></button>
							</div>
						</div>
					</a>
				</div>
			<?php } //if isset
			} //for side cats ?>
		</div>
	</section>

<?php endif; ?>

<section class="l-featured-banner">
	<div class="c-banner d-none d-lg-block"><!-- DESKTOP -->
		<!-- /21850622224/Buzz_Home_Leaderboard_Below_Categories -->
		<div id='div-gpt-ad-1572047537304-0' style='width: 970px; height: 250px;'>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572047537304-0'); });
			</script>
		</div>
    </div>

	<div class="c-banner d-block d-lg-none"><!-- MOBILE -->
		<!-- /21850622224/Buzz_Home_Below_Categories_Mobile -->
		<div id='div-gpt-ad-1570538542819-0' style='width: 320px; height: 100px;'>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570538542819-0'); });
			</script>
		</div>
    </div>
</section>