<?php
    $big_block_function = 'big_block' . $country;

    $category = $big_block_function('big_block'. $country .'_cat');
    $type = $big_block_function('big_block'. $country .'_type');
    $postID = $big_block_function('big_block'. $country .'_post_id');
    $postTitle = $big_block_function('big_block'. $country .'_post_title');
    $customTitle = $big_block_function('big_block'. $country .'_title');
    $title = ($customTitle) ? $customTitle : $postTitle;
    $customImage = $big_block_function('big_block'. $country .'_img');
    $image = '';
    $buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
    $buttonType = ($type == 'video') ? ' is-video' : '';

    if(!$customImage){
        if(has_post_thumbnail($postID)){
            $image = get_the_post_thumbnail_url($postID);
        } else {
            $image = catch_that_image($postID);
        }
    } else {
        $image = $customImage;
    }

    if($postID) :
?>

    <section>
        <div class="c-card-entry c-card-entry--full">
            <a href="<?php echo get_permalink($postID); ?>" title="<?php echo $title; ?>" class="c-card-entry__img" style="background-image: url('<?php echo $image; ?>')"></a>
            <div class="c-card-entry__wrapper">
                <div class="c-card-entry__content">
                    <span class="c-card-entry__tag cat-<?php echo get_the_category($postID)[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postID, 'sub-category'); echo $subcategory[0]->name; ?></span>
                    <h3 class="c-card-entry__title"><?php echo $title; ?></h3>
                    <a href="<?php echo get_permalink($postID); ?>" title="<?php echo $title; ?>" class="c-card-entry__bt <?php echo $buttonType; ?>"><?php echo $buttonTitle; ?></a>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>