<?php
	$minimumPosts = 3;
	$postInfo = null;
	$totalPosts = 0;
	$featured_stories_function = 'featured_stories' . $country;

	for($f = 1; $f <= 9; $f++){
		$category = $featured_stories_function('featured_stories'. $country .'_cat'.$f);
		$type = $featured_stories_function('featured_stories'. $country .'_type'.$f);
		$postID = $featured_stories_function('featured_stories'. $country .'_post_id'.$f);
		$postTitle = $featured_stories_function('featured_stories'. $country .'_post_title'.$f);
		$customTitle = $featured_stories_function('featured_stories'. $country .'_title'.$f);
		$title = ($customTitle) ? $customTitle : $postTitle;
		$customImage = $featured_stories_function('featured_stories'. $country .'_img'.$f);
		$image = '';
		$buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
		$buttonType = ($type == 'video') ? ' is-video' : '';

		if(!$customImage){
			if(has_post_thumbnail($postID)){
				$image = get_the_post_thumbnail_url($postID);
			} else {
				$image = catch_that_image($postID);
			}
		} else {
			$image = $customImage;
		}

		if($postID){
			$postInfo[] = array(
				'postID' => $postID,
				'title' => $title,
				'image' => $image,
				'buttonTitle' => $buttonTitle,
				'buttonType' => $buttonType
            );
		}
	} // for get content

	if($postInfo) $totalPosts = count($postInfo);
	if($totalPosts >= $minimumPosts) :
?>

	<section class="c-featured-carousel">
		<h2 class="c-featured-carousel__title">Trending now</h2>

		<div class="c-featured-carousel__wrapper js-featured-carousel owl-carousel">
		<?php for($e = 0; $e <= 8; $e++){ // side trending now
			if(isset($postInfo[$e])){ ?>
				<a href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>" class="c-card-entry">
					<div class="c-card-entry__img" style="background-image:url('<?php echo $postInfo[$e]['image']; ?>')"></div>
					<div class="c-card-entry__wrapper">
						<div class="c-card-entry__content">
							<span class="c-card-entry__tag cat-<?php echo get_the_category($postInfo[$e]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[$e]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
							<h3 class="c-card-entry__title"><?php echo $postInfo[$e]['title']; ?></h3>
							<button class="c-card-entry__bt <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></button>
						</div>
					</div>
				</a>
			<?php } //if isset
			} //for side trending now ?>
		</div>
	</section>
<?php endif; ?>

<section class="l-featured-banner">
	<div class="c-banner d-none d-lg-block"><!-- DESKTOP -->
        <!-- /21850622224/Buzz_Home_Leaderboard_Below_Trending -->
		<div id='div-gpt-ad-1570538697183-0' style='width: 970px; height: 250px;'>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570538697183-0'); });
			</script>
		</div>
	</div>
	
	<div class="c-banner d-block d-lg-none"><!-- MOBILE -->
        <!-- /21850622224/Buzz_Home_Below_Trending_Mobile -->
		<div id='div-gpt-ad-1570538742200-0' style='width: 320px; height: 100px;'>
			<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570538742200-0'); });
			</script>
		</div>
    </div>
</section>