<section class="l-featured-banner">
	<div class="c-banner d-none d-lg-block"><!-- DESKTOP -->
        <!-- /21850622224/Buzz_AboveJobs_Desktop -->
        <div id='div-gpt-ad-1570479828392-0' style='width: 970px; height: 250px;'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570479828392-0'); });
            </script>
        </div>
    </div>
	
	<div class="c-banner d-block d-lg-none"><!-- MOBILE -->
        <!-- /21850622224/Buzz_Home_Above_Jobs_Mobile -->
        <div id='div-gpt-ad-1570538362786-0' style='width: 320px; height: 100px;'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570538362786-0'); });
            </script>
        </div>
    </div>
</section>

<?php
if($country == '_jamaica'){
    $countryQuery = array(
        'key' => 'job_country',
        'value' => 'Jamaica',
        'compare' => '='
    );
} elseif($country == '_stlucia'){
    $countryQuery = array(
        'key' => 'job_country',
        'value' => 'St. Lucia',
        'compare' => '='
    );
} elseif($country == '_turksandcaicos'){
    $countryQuery = array(
        'key' => 'job_country',
        'value' => 'Turks and Caicos',
        'compare' => '='
    );
} elseif($country == '_barbados'){
    $countryQuery = array(
        'key' => 'job_country',
        'value' => 'Barbados',
        'compare' => '='
    );
} elseif($country == '_bahamas'){
    $countryQuery = array(
        'key' => 'job_country',
        'value' => 'Bahamas',
        'compare' => '='
    );
} elseif($country == '_cayman'){
    $countryQuery = array(
        'key' => 'job_country',
        'value' => 'Cayman',
        'compare' => '='
    );
} else {
    $countryQuery = '';
}

$args = array('post_type' => 'job', 'posts_per_page' => 3, 'paged' => $paged, 'orderby' => 'date', 'order' => 'DESC',
'meta_query' => array('relation' => 'AND', array('key' => 'job_expiration', 'value' => date('Y-m-d'), 'compare' => '>='), $countryQuery));
query_posts($args);

if($wp_query->have_posts()) : ?>

    <section class="c-latest-job">
        <div class="container">
            <div class="row no-gutters">
                <div class="c-latest-job__header">
                    <h1 class="c-latest-job__title">Latest Job Openings</h1>
                    <a href="<?php echo get_bloginfo('url'); ?>/jobs" class="c-latest-job__more">more in jobs</a>
                </div>
                <div class="c-latest-job__list">
                    <?php while ($wp_query->have_posts()) : $wp_query->the_post();
                        loop_joblisting($post->ID);
                    endwhile; ?>
                </div>
                <a href="<?php echo get_bloginfo('url'); ?>/jobs" class="c-latest-job__more-mobile">see more</a>
            </div>
        </div>
    </section>
    
<?php endif; wp_reset_query(); wp_reset_postdata(); ?>