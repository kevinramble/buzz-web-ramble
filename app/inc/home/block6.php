<?php
	$country = get_country();
    switch ($country) {
        case '_stlucia':
            $countryTitle = "St. Lucia";
            $countryTerm = "st-lucia";
            break;
        case '_jamaica':
            $countryTitle = "Jamaica";
            $countryTerm = "jamaica";
        break;
        default:
            $countryTitle = "";
            $countryTerm = "";
            break;
	}
	$getCountry = get_term_by('name', $countryTitle, 'countries');
    $countryID = $getCountry->term_id;
?>

<section>
	<div class="row no-gutters">
		<!-- top stories -->
			<div class="col-12 col-lg-4">
				<article class="c-list c-list--blue">
					<div class="c-list__content">
						<h1 class="c-list__title">Top 10 Stories</h1>
						<div class="c-list__carousel js-list-carousel owl-carousel">
						<?php 
						
						//query posts to return top posts in desc order for the specified categories
						$cat = "2,3,4,5";
						$temps = explode(',', $cat); //common delimited list of categories
						$array = array();
						foreach ($temps as $temp) $array[] = trim($temp);
						
						$cats = !empty($cat) ? $array : '';
						//get top 10 posts for the last 14 days
						$topTen = new WP_Query(
							array(
								'taxonomy' => 'countries', 
								'term' => $countryTerm, 
								'posts_per_page' => 10, 
								'meta_key' => 'top_posts', 
								'orderby' => 'meta_value_num', 
								'order' => 'DESC', 
								'category__in' => $cats,
								//'year' => date('Y'),
								//'monthnum' => intval(date('m')),
								'date_query' => array(
									array(
										'column' => 'post_date_gmt',
										'after'  => '30 days ago',
									)
								)
							));
				
					while ($topTen->have_posts()) : $topTen->the_post();  ?>
						<?php 

							if(has_post_thumbnail(get_the_ID())){
								$img = get_the_post_thumbnail_url(get_the_ID(),'full');
							} else {
								$img = catch_that_image(get_the_ID());
							} ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="c-item-list">
								<div class="c-item-list__img" style="background-image: url('<?php echo $img; ?>');"></div>
								<div class="c-item-list__content">
									<?php $subcategory = wp_get_post_terms(get_the_ID(), 'sub-category');
									if($subcategory){
										echo '<span class="c-card-entry__tag">' . $subcategory[0]->name . '</span>';
									} else {
										$category = get_the_category(get_the_ID());
										if($category) echo '<span class="c-card-entry__tag">' . get_the_category(get_the_ID())[0]->name . '</span>';
									} ?>
									<p class="c-item-list__date"><?php the_time('d.m.Y'); ?></p>
									<p class="c-item-list__description"><?php the_title(); ?></p>
								</div>
							</a>
						<?php endwhile; wp_reset_postdata(); ?>
						</div>
						</div>
				</article>
			</div>

		<!-- top video -->
			<div class="col-12 col-lg-4">
				<article class="c-list c-list--pink">
					<div class="c-list__content">
						<h1 class="c-list__title">Top 10 Videos</h1>
						<div class="c-list__carousel js-list-carousel owl-carousel">
						<?php 

						//get top 10 videos for the last 14 days
						$videos = new WP_Query(
							array(
								'taxonomy' => 'countries', 
								'term' => $countryTerm, 
								'posts_per_page' => 10, 
								'meta_key' => 'top_posts', 
								'orderby' => 'meta_value_num', 
								'order' => 'DESC', 
								'post_type' => 'video',
								'date_query' => array(
									array(
										'column' => 'post_date_gmt',
										'after'  => '30 days ago',
									)
								)
							));	

						while ($videos->have_posts()) : $videos->the_post();  ?>
						<?php 
							
							if(has_post_thumbnail(get_the_ID())){
								$img = get_the_post_thumbnail_url(get_the_ID(),'full');
							} else {
								$img = catch_that_image(get_the_ID());
							} ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="c-item-list">
								<div class="c-item-list__img" style="background-image: url('<?php echo $img; ?>');"></div>
								<div class="c-item-list__content">
									<?php $subcategory = wp_get_post_terms(get_the_ID(), 'sub-category');
									if($subcategory){
										echo '<span class="c-card-entry__tag">' . $subcategory[0]->name . '</span>';
									} else {
										$category = get_the_category(get_the_ID());
										if($category) echo '<span class="c-card-entry__tag">' . get_the_category(get_the_ID())[0]->name . '</span>';
									} ?>
									<p class="c-item-list__date"><?php the_time('d.m.Y'); ?></p>
									<p class="c-item-list__description"><?php the_title(); ?></p>
								</div>
							</a>
						<?php endwhile; wp_reset_postdata(); ?>
						</div>
						</div>
				</article>
			</div>

		<!-- hot now -->
			<div class="col-12 col-lg-4">
				<article class="c-list c-list--yellow">
					<div class="c-list__content">
						<h1 class="c-list__title">Hot Now</h1>
						<div class="c-list__carousel js-list-carousel owl-carousel">
						<?php 
						//get top 10 posts of any post type for last 14 days
						$hot = new WP_Query(
							array(
								'taxonomy' => 'countries', 
								'term' => $countryTerm, 
								'posts_per_page' => 10, 
								'meta_key' => 'top_posts', 
								'orderby' => 'meta_value_num', 
								'order' => 'DESC', 
								'year' => date('Y'),
								'post_type' => 'any',
								'date_query' => array(
									array(
										'column' => 'post_date_gmt',
										'after'  => '7 days ago',
									),
								),
								// 'date_query' => array(
								// 	array(
								// 		'after'     => 'midnight 2 days ago',  
								// 		'inclusive' => true,
								// 	),
								// ),
								));

					while ($hot->have_posts()) : $hot->the_post();  ?>
						<?php 

							if(has_post_thumbnail(get_the_ID())){
								$img = get_the_post_thumbnail_url(get_the_ID(),'full');
							} else {
								$img = catch_that_image(get_the_ID());
							} ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="c-item-list">
								<div class="c-item-list__img" style="background-image: url('<?php echo $img; ?>');"></div>
								<div class="c-item-list__content">
									<?php $subcategory = wp_get_post_terms(get_the_ID(), 'sub-category');
									if($subcategory){
										echo '<span class="c-card-entry__tag">' . $subcategory[0]->name . '</span>';
									} else {
										$category = get_the_category(get_the_ID());
										if($category) echo '<span class="c-card-entry__tag">' . get_the_category(get_the_ID())[0]->name . '</span>';
									} ?>
									<p class="c-item-list__date"><?php the_time('d.m.Y'); ?></p>
									<p class="c-item-list__description"><?php the_title(); ?></p>
								</div>
							</a>
						<?php endwhile; wp_reset_postdata(); ?>
					</div>
				</div>
				</article>
			</div>
	</div>
</section>