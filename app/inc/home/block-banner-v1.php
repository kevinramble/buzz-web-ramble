<?php
	$postInfo = null;
	$main_banner_function = 'main_banner' . $country;

	for($d = 1; $d <= 5; $d++){
		$category = $main_banner_function('main_banner'. $country .'_cat'.$d);
		$type = $main_banner_function('main_banner'. $country .'_type'.$d);
		$postID = $main_banner_function('main_banner'. $country .'_post_id'.$d);
		$postTitle = $main_banner_function('main_banner'. $country .'_post_title'.$d);
		$customTitle = $main_banner_function('main_banner'. $country .'_title'.$d);
		$title = ($customTitle) ? $customTitle : $postTitle;
		$customImage = $main_banner_function('main_banner'. $country .'_img'.$d);
		$image = '';
		$buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
		$buttonType = ($type == 'video') ? ' is-video' : '';

		$video = '';
		if(!$customImage){
			if(has_post_thumbnail($postID)){
				$image = get_the_post_thumbnail_url($postID);
			} else {
				$image = catch_that_image($postID);
			}
		} else {
			$image = $customImage;
			$img_id = attachment_url_to_postid($image);

			if($img_id){
				$img_info = wp_get_attachment_metadata($img_id);
				if(!isset($img_info['image_meta'])){
					$video = 'true';
				} else {
					$video = '';
				}
			}
		}

		if($postID){
			$postInfo[] = array(
				'postID' => $postID,
				'title' => $title,
				'image' => $image,
				'video' => $video,
				'buttonTitle' => $buttonTitle,
				'buttonType' => $buttonType
			);
		}
	} // for get content
?>

<section class="c-block-headlines">
    <div class="row no-gutters">
        <div class="col-12 col-lg-8">
            <div class="c-block-headlines__one">
                <div class="c-headline">
					<div class="c-headline__full owl-carousel js-headlines-full">
						<?php for($e = 0; $e < 3; $e++){ // main banner
							if(isset($postInfo[$e])){ ?>
							<a class="c-headline__full-item" href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>">
								<?php if($postInfo[$e]['video'] != 'true') : ?>
									<div class="c-headline__img">
										<div class="c-headline__img-src" style="background-image: url('<?php echo $postInfo[$e]['image']; ?>')"></div>

								<?php elseif($postInfo[$e]['video'] == 'true') : ?>
									<div class="c-headline__img has-video">
										<video autoplay loop muted playsinline class="c-video-player">
											<source src="<?php echo $postInfo[$e]['image']; ?>" type="video/mp4">
										</video>
									<?php endif; ?>

									<button class="c-headline__bt-full d-inline-flex d-lg-none <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></button>
								</div>
								<div class="c-headline__info">
									<small class="c-headline__date"><?php echo get_the_date('d.m.Y', $postInfo[$e]['postID']); ?></small>
									<h1 class="c-headline__title"><?php echo $postInfo[$e]['title']; ?></h1>
									<button class="c-headline__bt <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></button>
								</div>
							</a>
						<?php } //if isset
						} //for main banner ?>
					</div><!-- .owl-carousel.js-headlines-full -->

					<div class="c-headline__call-list custom-scrollbar">
						<div class="row flex-nowrap flex-lg-wrap">
						<?php for($e = 0; $e < 3; $e++){ // main banner scroll
							if(isset($postInfo[$e])){
								$firstActive = ($e == 0) ? 'is-active' : ''; ?>
								<div class="c-headline__call js-headlines-call <?php echo $firstActive; ?>">
									<div class="c-headline__progress <?php echo $firstActive; ?>">
										<div class="c-headline__bar js-headlines-progress"></div>
									</div>
									<small class="c-headline__date d-block d-lg-none"><?php echo get_the_date('d.m.Y', $postInfo[$e]['postID']); ?></small>
									<p><?php echo $postInfo[$e]['title']; ?></h2>
								</div>
							<?php } //if isset
							} //for main banner scroll ?>
						</div>
					</div><!-- .c-headline__call-list.custom-scrollbar -->
				</div><!-- .c-headline -->
            </div>
        </div>
        <div class="col-4 d-none d-lg-block">
		<?php for($e = 3; $e < 5; $e++){ // side highlights
			if(isset($postInfo[$e])){ ?>
            <div class="c-block-headlines__card">
				<a href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>" class="c-card-entry c-card-entry--height">
					<div class="c-card-entry__img" style="background-image: url('<?php echo $postInfo[$e]['image']; ?>')"></div>
                    <div class="c-card-entry__wrapper">
                        <div class="c-card-entry__content">
                            <span class="c-card-entry__tag cat-<?php echo get_the_category($postInfo[$e]['postID'])[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postInfo[$e]['postID'], 'sub-category'); echo $subcategory[0]->name; ?></span>
                            <h3 class="c-card-entry__title"><?php echo $postInfo[$e]['title']; ?></h3>
                            <button class="c-card-entry__bt <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></button>
                        </div>
                    </div>
                </a>
            </div>
            <?php } //if isset
			} //for side highlights ?>
        </div>
    </div>
</section>
