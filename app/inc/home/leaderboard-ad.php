<section class="js-leaderboard-wrapper desktop <?php echo ($_COOKIE['ad-cookies']) ? 'd-none' : ''; ?>">
    <section class="c-leaderboard-banner">
        <div class="c-banner">
            <button class="c-banner__close js-leaderboard-close" aria-label="Close"></button>
            <!-- /21850622224/Buzz_Home_Leaderboard_Desktop -->
            <div id='div-gpt-ad-1570538778182-0' style='width: 1240px; height: 400px;'>
                <script>
                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570538778182-0'); });
                </script>
            </div>
        </div>
    </section>
</section>

<section class="js-leaderboard-wrapper mobile <?php echo ($_COOKIE['ad-cookies']) ? 'd-none' : ''; ?>">
    <section class="c-leaderboard-banner">
        <div class="c-banner">
            <button class="c-banner__close js-leaderboard-close" aria-label="Close"></button>
            <!-- /21850622224/Buzz_Home_Leaderboard_Mobile -->
            <div id='div-gpt-ad-1570538818523-0' style='width: 320px; height: 100px;'>
                <script>
                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570538818523-0'); });
                </script>
            </div>
        </div>
    </section>
</section>