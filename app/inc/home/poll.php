<?php
    $home_poll_function = 'home_poll' . $country;
    
	$poll_status = $home_poll_function('home_poll'. $country .'_status');
	$poll_shortcode = $home_poll_function('home_poll'. $country .'_shortcode');
    if($poll_status == 1 && $poll_shortcode != ''){ ?>

        <section class="c-home-poll js-home-poll container">
            <?php echo do_shortcode($poll_shortcode); ?>
        </section>
    <?php }
?>