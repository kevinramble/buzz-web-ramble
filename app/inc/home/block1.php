<?php
    $minimumPosts = 3;
    $postInfo = null;
    $totalPosts = 0;
    $hot_now_function = 'hot_now' . $country;

	for($f = 1; $f <= 6; $f++){
		$category = $hot_now_function('hot_now'. $country .'_cat'.$f);
		$type = $hot_now_function('hot_now'. $country .'_type'.$f);
		$postID = $hot_now_function('hot_now'. $country .'_post_id'.$f);
		$postTitle = $hot_now_function('hot_now'. $country .'_post_title'.$f);
		$customTitle = $hot_now_function('hot_now'. $country .'_title'.$f);
		$title = ($customTitle) ? $customTitle : $postTitle;
		$customImage = $hot_now_function('hot_now'. $country .'_img'.$f);
		$image = '';
        $buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
        $buttonType = ($type == 'video') ? ' is-video' : '';

		if(!$customImage){
			if(has_post_thumbnail($postID)){
				$image = get_the_post_thumbnail_url($postID);
			} else {
				$image = catch_that_image($postID);
			}
		} else {
			$image = $customImage;
		}

		if($postID){
			$postInfo[] = array(
				'postID' => $postID,
				'title' => $title,
				'image' => $image,
                'buttonTitle' => $buttonTitle,
                'buttonType' => $buttonType
            );
		}
    } // for get content

    if($postInfo) $totalPosts = count($postInfo);
	if($totalPosts >= $minimumPosts) :
?>

    <section class="c-block-two">
        <div class="c-block-two__carousel js-block-two-carousel owl-carousel">
        <?php for($e = 2; $e <= 5; $e++){ // side highlights
            if(isset($postInfo[$e])){ ?>
            <a href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>" class="c-card-block-two">
                <div class="c-card-block-two__img"  style="background-image: url('<?php echo $postInfo[$e]['image']; ?>')"></div>
                <div class="c-card-block-two__wrapper">
                    <div class="c-card-block-two__content">
                        <span class="c-card-entry__tag c-card-block-two__tag cat-<?php echo get_the_category($postInfo[$e]['postID'])[0]->slug; ?>">
                            <?php $subcategory = wp_get_post_terms($postInfo[$e]['postID'], 'sub-category');
                            if($subcategory){
                                echo $subcategory[0]->name;
                            } else {
                                echo get_the_category($postInfo[$e]['postID'])[0]->name;
                            } ?>
                        </span>
                        <h3 class="c-card-block-two__title c-card-block-two__title--big"><?php echo $postInfo[$e]['title']; ?></h3>
                        <button class="c-card-block-two__bt <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></button>
                    </div>
                </div>
            </a>
            <?php } //if isset
            } //for side highlights ?>
        </div>

        <div class="c-block-two__col-right">
            <?php if(isset($postInfo[0])){ ?>
                <div class="c-block-two__img-top">
                    <a href="<?php echo get_permalink($postInfo[0]['postID']); ?>" class="c-card-block-two c-card-block-two--wide">
                        <div class="c-card-block-two__img" style="background-image: url('<?php echo $postInfo[0]['image']; ?>')"></div>
                        <div class="c-card-block-two__wrapper">
                            <div class="c-card-block-two__content">
                                <span class="c-card-entry__tag c-card-block-two__tag cat-<?php echo get_the_category($postInfo[0]['postID'])[0]->slug; ?>">
                                    <?php $subcategory = wp_get_post_terms($postInfo[0]['postID'], 'sub-category');
                                    if($subcategory){
                                        echo $subcategory[0]->name;
                                    } else {
                                        echo get_the_category($postInfo[0]['postID'])[0]->name;
                                    } ?>
                                </span>
                                <h3 class="c-card-block-two__title"><?php echo $postInfo[0]['title']; ?></h3>
                                <button class="c-card-block-two__bt c-card-block-two__bt--play <?php echo $postInfo[0]['buttonType']; ?>"><?php echo $postInfo[0]['buttonTitle']; ?></button>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } if(isset($postInfo[1])){ //if isset ?>
            <div class="c-block-two__img-bottom">
                <a href="<?php echo get_permalink($postInfo[1]['postID']); ?>" class="c-card-block-two c-card-block-two--square">
                    <div class="c-card-block-two__img" style="background-image: url('<?php echo $postInfo[1]['image']; ?>')"></div>
                    <div class="c-card-block-two__wrapper">
                        <div class="c-card-block-two__content">
                            <span class="c-card-entry__tag c-card-block-two__tag cat-<?php echo get_the_category($postInfo[1]['postID'])[0]->slug; ?>">
                                <?php $subcategory = wp_get_post_terms($postInfo[1]['postID'], 'sub-category');
                                if($subcategory){
                                    echo $subcategory[0]->name;
                                } else {
                                    echo get_the_category($postInfo[1]['postID'])[0]->name;
                                } ?>
                            </span>
                            <h3 class="c-card-block-two__title"><?php echo $postInfo[1]['title']; ?></h3>
                            <button class="c-card-block-two__bt <?php echo $postInfo[1]['buttonType']; ?>"><?php echo $postInfo[1]['buttonTitle']; ?></button>
                        </div>
                    </div>
                </a>
            </div>
            <?php } // if isset ?>
        </div>
    </section>
<?php endif; ?>


<?php include('poll.php'); ?>