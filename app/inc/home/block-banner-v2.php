<?php
	$postInfo = null;
	$main_banner_function = 'main_banner' . $country;

	for($d = 1; $d <= 4; $d++){
		$category = $main_banner_function('main_banner'. $country .'_cat'.$d);
		$type = $main_banner_function('main_banner'. $country .'_type'.$d);
		$postID = $main_banner_function('main_banner'. $country .'_post_id'.$d);
		$postTitle = $main_banner_function('main_banner'. $country .'_post_title'.$d);
		$customTitle = $main_banner_function('main_banner'. $country .'_title'.$d);
		$title = ($customTitle) ? $customTitle : $postTitle;
		$customImage = $main_banner_function('main_banner'. $country .'_img'.$d);
		$image = '';
		$buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
		$buttonType = ($type == 'video') ? ' is-video' : '';

		$video = '';
		if(!$customImage){
			if(has_post_thumbnail($postID)){
				$image = get_the_post_thumbnail_url($postID);
			} else {
				$image = catch_that_image($postID);
			}
		} else {
			$image = $customImage;
			$img_id = attachment_url_to_postid($image);

			if($img_id){
				$img_info = wp_get_attachment_metadata($img_id);
				if(!isset($img_info['image_meta'])){
					$video = 'true';
				} else {
					$video = '';
				}
			}
		}

		if($postID){
			$postInfo[] = array(
				'postID' => $postID,
				'title' => $title,
				'image' => $image,
				'video' => $video,
				'buttonTitle' => $buttonTitle,
				'buttonType' => $buttonType
			);
		}
	} // for get content
?>

<section class="c-block-highlights">
	<div class="c-block-highlights__full owl-carousel js-highlights-full">
		<?php for($e = 0; $e < 4; $e++){ // main banner
			if(isset($postInfo[$e])){ ?>
			<div class="c-block-highlights__full-item">
				<?php if($postInfo[$e]['video'] != 'true') : ?>
					<a class="c-hero" style="background-image: url('<?php echo $postInfo[$e]['image']; ?>')" href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>">

				<?php elseif($postInfo[$e]['video'] == 'true') : ?>
					<a class="c-hero has-video" href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" title="<?php echo $postInfo[$e]['title']; ?>">
					<video autoplay loop muted playsinline class="c-video-player">
						<source src="<?php echo $postInfo[$e]['image']; ?>" type="video/mp4">
					</video>
				<?php endif; ?>

					<div class="c-hero__info">
						<small class="c-hero__date"><?php echo get_the_date('d.m.Y', $postInfo[$e]['postID']); ?></small>
						<div class="c-hero__mask-text">
							<h1 class="c-hero__title"><?php echo $postInfo[$e]['title']; ?></h1>
						</div>
						<button class="c-hero__bt <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></button>
					</div>
				</a>
			</div>
		<?php } //if isset
		} //for main banner ?>
	</div>

	<div class="c-block-highlights__progress js-highlights-progress"></div>

	<div class="c-block-highlights__thumbs-list custom-scrollbar">
		<?php for($e = 0; $e < 4; $e++){ // main banner scroll
			if(isset($postInfo[$e])){
			$firstActive = ($e == 0) ? 'is-active' : ''; ?>
			<div class="c-block-highlights__thumb js-highlights-thumb">
				<div class="c-call-thumb">
					<div class="c-call-thumb__mask">
						<?php if($postInfo[$e]['video'] != 'true') : ?>
							<div class="c-call-thumb__img" style="background-image: url('<?php echo $postInfo[$e]['image']; ?>')"></div>

						<?php elseif($postInfo[$e]['video'] == 'true') :
							if(has_post_thumbnail($postInfo[$e]['postID'])){
								$thumb = get_the_post_thumbnail_url($postInfo[$e]['postID']);
							} else {
								$thumb = catch_that_image($postInfo[$e]['postID']);
							} ?>
							<div class="c-call-thumb__img" style="background-image: url('<?php echo $thumb; ?>')"></div>

						<?php endif; ?>
					</div>
					<div class="call-thumb__info">
						<h2 class="c-call-thumb__title <?php echo $firstActive; ?>"><?php echo $postInfo[$e]['title']; ?></h2>
						<a href="<?php echo get_permalink($postInfo[$e]['postID']); ?>" class="c-call-thumb__bt <?php echo $postInfo[$e]['buttonType']; ?>"><?php echo $postInfo[$e]['buttonTitle']; ?></a>
					</div>
				</div>
			</div>
		<?php } //if isset
		} //for main banner scroll ?>
	</div>
</section>
