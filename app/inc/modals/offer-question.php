<div class="modal offer">
    <div class="modal__dialog">
        <button class="modal__close" aria-label="Close"></button>

        <h1 class="c-title-headline"><?php echo $offer_company; ?></h1>
        <h2 class="c-subtitle-headline"><?php echo $offer_place; ?></h2>

        <div class="js-offer-modal-content">
            <div class="c-description-headline c-description-headline--small mt-3 mb-4">
                <p>Answer the question below to redeem your code.</p>
            </div>

            <?php echo do_shortcode($offer_shortcode); ?>
        </div>

        <script>
            var offerID = '<?php echo $post->ID; ?>',
                offerCompany = '<?php echo $offer_company; ?>',
				offerCompanyURL = '<?php echo $offer_companyurl; ?>';
		</script>
    </div>
</div>