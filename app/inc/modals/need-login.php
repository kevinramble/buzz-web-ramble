<div class="modal needlogin">
    <div class="modal__dialog">
        <button class="modal__close" aria-label="Close"></button>

        <div class="js-content-wrapper">
            <div class="c-badge c-badge--locked"></div>

            <div class="c-description-headline my-3">
                <p>Sign in to save your favourite articles, access special offers and more great stuff!</p>
            </div>

            <a href="<?php echo get_bloginfo('url'); ?>/login?redirect_to=<?php the_permalink(); ?>" class="c-button c-button--negative c-button--full mb-2">Click here to login</a>
            <a href="<?php echo get_bloginfo('url'); ?>/register?redirect_to=<?php the_permalink(); ?>" class="c-button c-button--secondary c-button--full">Not a member? Register now</a>
        </div><!-- .content-wrapper -->
    </div>
</div>