<div class="modal job">
    <div class="modal__dialog">
        <button class="modal__close" aria-label="Close"></button>

        <div class="js-content-wrapper">
            <h1 class="c-title-headline">Apply for this job</h1>

            <div class="c-profile-info mt-3">
                <?php $current_user = wp_get_current_user();
                $current_user_name = ($current_user->user_firstname) ? ($current_user->user_firstname.' '.$current_user->user_lastname) : $current_user->display_name;
                $countryCode = um_user('country'); ?>

                <p class="c-profile-info__name"><?php echo $current_user_name; ?></p>
                <p class="c-profile-info__description"><?php echo um_user('city') .', '. get_country_name($countryCode); ?><br> <?php echo $current_user->user_email; ?></p>
            </div>

            <form action="?" class="c-form c-form--dark mt-4 js-job-form" data-url="<?php bloginfo('template_directory'); ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8" boundary="----WebKitFormBoundary0BPm0koKA">
                <?php $securitynumber1 = ord(mt_rand(0,9)); $securitynumber2 = ord(mt_rand(0,9)); ?>
                <input type="text" class="c-form__input" placeholder="Linkedin" id="joblinkedin" name="joblinkedin">

                <div class="c-form__file mt-2">
                    <input class="c-form__file-input js-input-file" type="file" name="cv" id="cv">
                    <label for="cv" class="c-form__file-label">
                        <span class="c-form__file-name" data-title="Add your cv"></span>
                        <span class="c-form__file-btn">Upload</span>
                    </label>
                </div>

                <label class="security-question light">
                    <span>Prove your humanity:</span>
                    <input class="text" placeholder="<?php echo '&#'.$securitynumber1. ' + &#' .$securitynumber2; ?>" name="securityanswer" id="securityanswer">
                </label><!-- .security-question -->
                <input type="hidden" name="securitynumber1" id="securitynumber1" value="<?php echo $securitynumber1; ?>">
                <input type="hidden" name="securitynumber2" id="securitynumber2" value="<?php echo $securitynumber2; ?>">

                <?php
                    $current_user = wp_get_current_user();
                    $current_user_name = ($current_user->user_firstname) ? ($current_user->user_firstname.' '.$current_user->user_lastname) : $current_user->display_name;
                ?>
                <input type="hidden" name="user_name" id="user_name" value="<?php echo $current_user_name; ?>">
                <input type="hidden" name="user_email" id="user_email" value="<?php echo $current_user->user_email; ?>">
                
                <?php
                    $job_company = esc_html(get_post_meta($post->ID, 'job_company', true));
                    $job_company_email = esc_html(get_post_meta($post->ID, 'job_company_email', true));
                    $job_info = $job_position . '. Job ID: ' . $post->ID;
                ?>
                <input type="hidden" name="job_info" id="job_info" value="<?php echo $job_info; ?>">
                <input type="hidden" name="company" id="company" value="<?php echo $job_company; ?>">
                <input type="hidden" name="company_email" id="company_email" value="<?php echo $job_company_email; ?>">

                <button class="c-form__btn mt-3 js-send-button">Apply now</button>

                <div class="notice"></div>
            </form>
        </div><!-- .content-wrapper -->
    </div>
</div>