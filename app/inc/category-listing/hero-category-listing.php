<section class="c-hero-category">
    <div class="row no-gutters">
        <div class="col-12 col-lg-7">
            <h1 class="c-hero-category__title cat-<?php echo $currentslug; ?>"><?php echo $areaTitle; ?></h1>

            <?php
                $functionName = 'main_banner_' . $currentslug . $country;

                for($c = 1; $c <= 3; $c++){
                    $postID = $functionName('main_banner_'.$currentslug . $country .'_post_id'.$c);
                    $type = $functionName('main_banner_'.$currentslug . $country .'_type'.$c);
                    $postTitle = $functionName('main_banner_'.$currentslug . $country .'_post_title'.$c);
                    $customTitle = $functionName('main_banner_'.$currentslug . $country .'_title'.$c);
                    $title = ($customTitle) ? $customTitle : $postTitle;
                    $customImage = $functionName('main_banner_'.$currentslug . $country .'_img'.$c);
                    $image = '';
                    $authorID = get_post_field('post_author', $postID);
                    $postAuthor = get_the_author_meta('display_name', $authorID);
                    $buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
                    $buttonType = ($type == 'video') ? ' is-video' : '';

                    if(!$customImage){
                        if(has_post_thumbnail($postID)){
                            $image = get_the_post_thumbnail_url($postID);
                        } else {
                            $image = catch_that_image($postID);
                        }
                    } else {
                        $image = $customImage;
                    }

                    if($c == 1){ //main image ?>
                        <div class="c-card-hero-category">
                            <a href="<?php echo get_permalink($postID); ?>" title="<?php echo $title; ?>" class="c-card-hero-category__img" style="background-image: url('<?php echo $image; ?>')"></a>
                            <div class="c-card-hero-category__wrapper">
                                <div>
                                    <span class="c-card-hero-category__tag cat-<?php echo get_the_category($postID)[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postID, 'sub-category'); echo $subcategory[0]->name; ?></span>
                                    <h3 class="c-card-hero-category__title"><?php echo $title; ?></h3>
                                    <div class="c-card-hero-category__author">
                                        <p class="c-card-hero-category__author__txt">BY <strong><?php echo $postAuthor; ?></strong></p>
                                        <p class="c-card-hero-category__author__txt"><?php echo get_the_date('M d, Y', $postID); ?></p>
                                    </div>
								</div>

                                <div class="c-social c-social--light d-none d-md-flex">
                                <?php if($type != 'video') : ?>
                                    <div class="c-social__book" aria-label="Bookmark"><?php the_favorites_button($postID, get_current_blog_id()); ?></div>
                                <?php endif; ?>
                                    <a class="c-social__icon c-social__icon--fb" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink($postID); ?>" target="_blank" aria-label="Facebook"></a>
                                    <a class="c-social__icon c-social__icon--tw" href="http://twitter.com/share?text=<?php echo $title; ?>&url=<?php the_permalink($postID); ?>" target="_blank" aria-label="Twitter"></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-lg-5">
                        <div class="row no-gutters">

                    <?php } else { // 3 listings
                        if($postID){ ?>
                            <div class="col-12 col-md-6 col-lg-12 l-page-category-listing__item">
								<div class="c-card-list c-card-list--height">
									<a href="<?php echo get_permalink($postID); ?>" title="<?php echo $title; ?>" class="c-card-list__img" style="background-image: url('<?php echo $image; ?>')"></a>
									<div class="c-card-list__wrapper">
										<div class="c-card-list__content">
											<span class="c-card-list__tag cat-<?php echo get_the_category($postID)[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postID, 'sub-category'); echo $subcategory[0]->name; ?></span>
											<time class="c-card-list__date"><?php echo get_the_date('d.m.Y', $postID); ?></time>
											<h3 class="c-card-list__title"><?php echo $title; ?></h3>

											<div class="c-card-list__tools">
												<a href="<?php echo get_permalink($postID); ?>" title="<?php echo $title; ?>" class="c-card-list__bt <?php echo $buttonType; ?>"><?php echo $buttonTitle; ?></a>

												<div class="c-social c-social--light">
                                                    <div class="c-social__book" aria-label="Bookmark"><?php the_favorites_button($postID, get_current_blog_id()); ?></div>
													<a class="c-social__icon c-social__icon--fb" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink($postID); ?>" target="_blank" aria-label="Facebook"></a>
													<a class="c-social__icon c-social__icon--tw" href="http://twitter.com/share?text=<?php echo $title; ?>&amp;url=<?php echo get_permalink($postID); ?>" target="_blank" aria-label="Twitter"></a>
												</div>
											</div>
										</div>
									</div>
								</div>
                            </div>
                    <?php }
                    }
                } // for ?>
            </div>
        </div>
    </div>
</section>