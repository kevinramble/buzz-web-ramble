<?php
	$minimumPosts = 3;
	$postAmount = null;
	$totalPosts = 0;
	$functionName = 'must_watch_' . $currentslug . $country;

	for ($d = 1; $d <= 9; $d++){
		$postID = $functionName('must_watch_'.$currentslug . $country .'_post_id'.$d);
		if($postID) $postAmount[] = $postID;
	}

	if($postAmount) $totalPosts = count($postAmount);
	if($totalPosts >= $minimumPosts) :
?>

	<section class="c-must-watch">
		<h2 class="c-must-watch__title cat-<?php echo $currentslug; ?>"><?php echo $areaTitle; ?></h2>

		<div class="c-must-watch__wrapper js-featured-carousel owl-carousel">
			<?php
			for ($c = 1; $c <= 9; $c++) :
				$postID = $functionName('must_watch_'.$currentslug . $country .'_post_id'.$c);
				$postTitle = $functionName('must_watch_'.$currentslug . $country .'_post_title'.$c);
				$customTitle = $functionName('must_watch_'.$currentslug . $country .'_title'.$c);
				$title = ($customTitle) ? $customTitle : $postTitle;
				$customImage = $functionName('must_watch_'.$currentslug . $country .'_img'.$c);
				$image = '';
				
				if(!$customImage){
					if(has_post_thumbnail($postID)){
						$image = get_the_post_thumbnail_url($postID);
					} else {
						$image = catch_that_image($postID);
					}
				} else {
					$image = $customImage;
				}

				if($postID) : ?>
				<a href="<?php echo get_permalink($postID); ?>" class="c-card-must-watch" style="background-image: url('<?php echo $image; ?>')">
					<div class="c-card-must-watch__wrapper">
						<div class="c-card-must-watch__content">
							<span class="c-card-must-watch__tag cat-<?php echo get_the_category($postID)[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postID, 'sub-category'); echo $subcategory[0]->name; ?></span>
							<h3 class="c-card-must-watch__title"><?php echo $title; ?></h3>
							<button class="c-card-must-watch__bt is-video">Watch now</button>
						</div>
					</div>
				</a>
			<?php endif; endfor; ?>
		</div>
	</section>

<?php endif; ?>