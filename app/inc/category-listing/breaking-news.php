<?php
	$minimumPosts = 4;
    $postAmount = null;
    $totalPosts = 0;
	$functionName = 'breaking_news_' . $currentslug . $country;

	for ($d = 1; $d <= 9; $d++){
		$postID = $functionName('breaking_news_'. $currentslug . $country .'_post_id'.$d);
		if($postID) $postAmount[] = $postID;
	}

	if($postAmount) $totalPosts = count($postAmount);
	if($totalPosts >= $minimumPosts) :
?>

    <div class="container">
        <div class="row no-gutters">
            <section class="c-breaking-news">
                <h2 class="c-breaking-news__title"><?php echo $areaTitle; ?></h2>
                <div class="c-breaking-news__grid">
                    <div class="row">
                        <?php
                        for ($c = 1; $c <= 8; $c++) :
                            $postID = $functionName('breaking_news_'.$currentslug . $country . '_post_id'.$c);
                            $postTitle = $functionName('breaking_news_'.$currentslug . $country . '_post_title'.$c);
                            $customTitle = $functionName('breaking_news_'.$currentslug . $country . '_title'.$c);
                            $title = ($customTitle) ? $customTitle : $postTitle;
                            $customImage = $functionName('breaking_news_'.$currentslug . $country . '_img'.$c);
                            $image = '';
                            $authorID = get_post_field('post_author', $postID);
                            $postAuthor = get_the_author_meta('display_name', $authorID);
                            
                            $type = $functionName('breaking_news_'.$currentslug . $country . '_type'.$c);
                            $buttonType = ($type == 'video') ? ' is-video' : '';

                            if(!$customImage){
                                if(has_post_thumbnail($postID)){
                                    $image = get_the_post_thumbnail_url($postID);
                                } else {
                                    $image = catch_that_image($postID);
                                }
                            } else {
                                $image = $customImage;
                            }

                            if($postID) : ?>
                            <div class="col-md-6 col-lg-3">
                                <a href="<?php echo get_permalink($postID); ?>" class="c-card-breaking-news">
                                    <div class="c-card-breaking-news__wrap_thumb <?php echo $buttonType; ?>">
                                        <div class="c-card-breaking-news__thumb" style="background-image:url('<?php echo $image; ?>')"></div>
									</div>
									<div>
                                        <p class="c-card-breaking-news__tag"><?php $subcategory = wp_get_post_terms($postID, 'sub-category'); echo $subcategory[0]->name; ?></p>
                                        <p class="c-card-breaking-news__description"><?php echo $title; ?></p>
                                        <div class="c-card-breaking-news__author">
                                            <p class="c-card-breaking-news__author__txt">by <span><?php echo $postAuthor; ?></span></p>
                                            <p class="c-card-breaking-news__author__txt"><?php echo get_the_date('M d, Y', $postID); ?></p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endif; endfor; ?>
                    </div>
                </div>
            </section>
        </div>
    </div>

<?php endif; ?>
