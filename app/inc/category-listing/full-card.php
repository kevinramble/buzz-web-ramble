<?php
    $functionName = 'full_block_' . $currentslug . $country;

    $postID = $functionName('full_block_'.$currentslug . $country .'_post_id');
    $type = $functionName('full_block_'.$currentslug . $country .'_type');
    $postTitle = $functionName('full_block_'.$currentslug . $country .'_post_title');
    $customTitle = $functionName('full_block_'.$currentslug . $country .'_title');
    $title = ($customTitle) ? $customTitle : $postTitle;
    $customImage = $functionName('full_block_'.$currentslug . $country .'_img');
    $image = '';
    $buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
    $buttonType = ($type == 'video') ? ' is-video' : '';

    if(!$customImage){
        if(has_post_thumbnail($postID)){
            $image = get_the_post_thumbnail_url($postID);
        } else {
            $image = catch_that_image($postID);
        }
    } else {
        $image = $customImage;
    }

    if($postID) : ?>

    <section>
        <div class="c-card-entry c-card-entry--full">
            <a href="<?php echo get_permalink($postID); ?>" title="<?php echo $title; ?>" class="c-card-entry__img" style="background-image: url('<?php echo $image; ?>')"></a>
            <div class="c-card-entry__wrapper">
                <div class="c-card-entry__content">
                    <span class="c-card-entry__tag cat-<?php echo $currentslug; ?>"><?php $subcategory = wp_get_post_terms($postID, 'sub-category'); echo $subcategory[0]->name; ?></span>
                    <h3 class="c-card-entry__title"><?php echo $title; ?></h3>
                    <a href="<?php echo get_permalink($postID); ?>" class="c-card-entry__bt <?php echo $buttonType; ?>"><?php echo $buttonTitle; ?></a>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>