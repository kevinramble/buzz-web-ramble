<?php if($currentslug === 'news'){
    $adSlotDesktop = '1570479794550-0';
    $adSlotMobile = '1570539349933-0';

} else if($currentslug === 'hot'){
    $adSlotDesktop = '1570479388071-0';
    $adSlotMobile = '1570538878634-0';

} else if($currentslug === 'life'){
    $adSlotDesktop = '1570479580087-0';
    $adSlotMobile = '1570539319908-0';
} ?>

<section class="ads mt-4 mb-4">
    <div class="c-banner d-none d-lg-block"><!-- DESKTOP -->
        <!-- /21850622224/Buzz_Above_AllStories_CAT_Desktop -->
        <div id='div-gpt-ad-<?php echo $adSlotDesktop; ?>' style='width: 970px; height: 250px;'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-<?php echo $adSlotDesktop; ?>'); });
            </script>
        </div>
    </div>
    
    <div class="c-banner d-block d-lg-none"><!-- MOBILE -->
        <!-- /21850622224/Buzz_Life_Stories_Mobile -->
        <div id='div-gpt-ad-<?php echo $adSlotMobile; ?>' style='width: 300px; height: 250px;'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-<?php echo $adSlotMobile; ?>'); });
            </script>
        </div>
    </div>
</section>

<div class="container">
    <div class="row no-gutters">
        <section class="c-all-stories">
            <div class="row title-filter">
                <div class="col-8">
                    <h2 class="c-all-stories__title"><?php echo $areaTitle; ?></h2>
                </div>
                <!-- Remove country filter -->
                <div class="col-4">
                    <div class="c-select">
                        <select class="js-child-cats">
                            <option value="">Filter by country</option>
                            <option value="<?php echo get_bloginfo('url') . '/countries?category=' . $currentslug; ?>">All countries</option>
                            <?php
                                $terms = get_terms(array('taxonomy' => 'countries', 'hide_empty' => 0));
                                foreach ($terms as $term){
                                    $active = ($term->name == $countryTitle) ? 'selected="selected"' : '';
                                    echo '<option '. $active .' value="'. get_category_link($term->term_id) . '?category=' . $currentslug .'">'. $term->name .'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="c-all-stories__grid">
                <?php $args = array('cat' => $catID, 'post_type' => 'post', 'posts_per_page' => 12, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged); query_posts($args);
               
                if (have_posts()) : ?>
                <div class="row js-posts-list">
                    <?php while (have_posts()) : the_post();
                        loop_genericcard_small($post->ID);
                    endwhile; ?>
                </div><!-- .js-posts-list -->
                <?php else : ?>
                    <h4 class="title-nocontent">Nothing to show.</h4>
                <?php endif;

                // load more ajax
                $wp_query->query_vars['search_orderby_title'] = ''; // necessario pro search
                $load_posts = $wp_query->query_vars; $load_current_page = $wp_query->query_vars['paged']; $load_max_page = $wp_query->max_num_pages;
                loadmore_button($load_posts, $load_current_page, $load_max_page);
                if($wp_query->max_num_pages > 1){ ?>
                    <span class="js-loadmore c-bt-load">Load more</span>
                <?php } else { ?>
                    <span class="js-loadmore c-bt-load hidden">Load more</span>
                <?php } // end load more ajax

                wp_reset_query(); wp_reset_postdata(); ?>
            </div>
        </section>
    </div>
</div>
