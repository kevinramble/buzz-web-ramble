<?php
	$minimumPosts = 3;
	$postAmount = null;
	$totalPosts = 0;
	$functionName = 'trending_now_' . $currentslug . $country;

	for ($d = 1; $d <= 9; $d++){
		$postID = $functionName('trending_now_'.$currentslug . $country .'_post_id'.$d);
		if($postID) $postAmount[] = $postID;
	}

	if($postAmount) $totalPosts = count($postAmount);
	if($totalPosts >= $minimumPosts) :
?>

	<section class="c-trending-now">
		<h2 class="c-trending-now__title cat-<?php echo $currentslug; ?>"><?php echo $areaTitle; ?></h2>

		<div class="c-trending-now__wrapper js-featured-carousel owl-carousel">
			<?php
			for ($c = 1; $c <= 9; $c++) :
				$postID = $functionName('trending_now_'.$currentslug . $country .'_post_id'.$c);
				$type = $functionName('trending_now_'.$currentslug . $country .'_type'.$c);
				$postTitle = $functionName('trending_now_'.$currentslug . $country .'_post_title'.$c);
				$customTitle = $functionName('trending_now_'.$currentslug . $country .'_title'.$c);
				$title = ($customTitle) ? $customTitle : $postTitle;
				$customImage = $functionName('trending_now_'.$currentslug . $country .'_img'.$c);
				$image = '';
				$buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
				$buttonType = ($type == 'video') ? ' is-video' : '';

				if(!$customImage){
					if(has_post_thumbnail($postID)){
						$image = get_the_post_thumbnail_url($postID);
					} else {
						$image = catch_that_image($postID);
					}
				} else {
					$image = $customImage;
				}

				if($postID) : ?>
				<a href="<?php echo get_permalink($postID); ?>" class="c-card-trending-now">
                    <div class="c-card-trending-now__img" style="background-image: url('<?php echo $image; ?>')"></div>
					<div class="c-card-trending-now__wrapper">
						<div class="c-card-trending-now__content">
							<span class="c-card-trending-now__tag cat-<?php echo get_the_category($postID)[0]->slug; ?>"><?php $subcategory = wp_get_post_terms($postID, 'sub-category'); echo $subcategory[0]->name; ?></span>
							<h3 class="c-card-trending-now__title"><?php echo $title; ?></h3>
							<button class="c-card-trending-now__bt <?php echo $buttonType; ?>"><?php echo $buttonTitle; ?></button>
						</div>
					</div>
				</a>
			<?php endif; endfor; ?>
		</div>
	</section>

<?php endif; ?>
