<?php get_header(); ?>

<?php if (is_day()) {
	$thisTitle = get_the_time('F jS, Y');
} elseif (is_month()) {
	$thisTitle = get_the_time('F, Y');
} elseif (is_year()) { 
	$thisTitle = get_the_time('Y');
} elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
	$thisTitle = 'Archives';
} ?>

<section class="l-page-list">
	<div class="c-list-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6">
					<h1 class="c-list-header__title"><?php echo $thisTitle; ?></h1>
				</div>
			</div>
		</div>
	</div>

	<div class="l-page-list__results">
		<div class="container">
			<div class="row js-posts-list">				
				<?php $args = array_merge($wp_query->query, array('posts_per_page' => 8, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged, 'childcat' => 1)); query_posts($args); ?>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php loop_genericcard($post->ID, '4'); ?>
					<?php endwhile; ?>
				<?php else: ?>
					<h4 class="title-nocontent">Nothing to show.</h4>
				<?php endif; ?>
			</div>

			<?php // load more ajax
			$wp_query->query_vars['search_orderby_title'] = ''; // necessario pro search
			$load_posts = $wp_query->query_vars; $load_current_page = $wp_query->query_vars['paged']; $load_max_page = $wp_query->max_num_pages;
			loadmore_button($load_posts, $load_current_page, $load_max_page);
			if($wp_query->max_num_pages > 1){ ?>
				<span class="js-loadmore c-bt-load">Load more</span>
			<?php } else { ?>
				<span class="js-loadmore c-bt-load hidden">Load more</span>
			<?php } // end load more ajax
				
			wp_reset_query(); wp_reset_postdata(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>