<?php get_header(); ?>

<?php
	$hero_layout_function = 'hero_layout' . $country;

	if($hero_layout_function('hero_layout'. $country .'_version') === '1' || $hero_layout_function('hero_layout'. $country .'_version') == false){
		include_once('inc/home/block-banner-v1.php');
		
	}  elseif($hero_layout_function('hero_layout'. $country .'_version') === '2'){
		include_once('inc/home/block-banner-v2.php');
	} // end hero layout version ?>

	<section class="l-featured-banner">
		<div class="c-banner d-none d-lg-block"><!-- DESKTOP -->
			<!-- /21850622224/Buzz_Home_Leaderboard_Below_Hero -->
			<div id='div-gpt-ad-1572047490984-0' style='width: 970px; height: 250px;'>
				<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572047490984-0'); });
				</script>
			</div>
		</div>

		<div class="c-banner d-block d-lg-none"><!-- MOBILE -->
			<!-- /21850622224/Buzz_Home_Below_Hero_Mobile -->
			<div id='div-gpt-ad-1570538641848-0' style='width: 320px; height: 100px;'>
				<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570538641848-0'); });
				</script>
			</div>
		</div>
	</section>

	<?php
	$home_order_function = 'home_order' . $country;

	for($b = 1; $b <= 8; $b++){
		$optionValue = $home_order_function('home_order'. $country . $b);

		$block = 'inc/home/block'. $optionValue .'.php';
		include_once($block);
	} ?>

<?php get_footer(); ?>