<?php
    /* Template Name: Jobs page */
    get_header(); ?>

    <section class="l-page-list l-page-list--dark">
        <div class="c-list-header">
            <div class="container">
                <div class="row align-items-center pos-relative">
                    <div class="col-md-2">
                        <h1 class="c-list-header__title">Jobs</h1>
                    </div>

                    <span class="js-toggle-filters"></span>
                    <div class="col-md-10 d-lg-block js-job-filters js-filters">
                        <div class="row">
                            <div class="col-8">
                                <div class="row mx-n1">
                                    <div class="col-4 px-1">
                                        <div class="c-select">
                                            <select class="js-filter-select" data-type="company">
                                                <option value="">Company</option>
                                                <option value="ASC">A-Z</option>
                                                <option value="DESC">Z-A</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4 px-1">
                                        <div class="c-select">
                                            <select class="js-filter-select" data-type="type">
                                                <option value="">Type</option>
                                                <option value="Full-time">Full-time</option>
                                                <option value="Part-time">Part-time</option>
                                                <option value="Freelance">Freelance</option>
                                                <option value="Internship">Internship</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4 px-1">
                                        <div class="c-select">
                                            <select class="js-filter-select" data-type="country" id="job_country">
                                                <option value="">All countries</option>
                                                <option value="Anguilla">Anguilla</option>
                                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                <option value="ABC Islands">ABC Islands</option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Bermuda">Bermuda</option>
                                                <option value="British Virgin Islands">British Virgin Islands</option>
                                                <option value="Cayman Islands">Cayman Islands</option>
                                                <option value="Dominica">Dominica</option>
                                                <option value="FWI">FWI</option>
                                                <option value="Grenada">Grenada</option>
                                                <option value="Guyana">Guyana</option>
                                                <option value="Haiti">Haiti</option>
                                                <option value="Jamaica" <?php if($country == '_jamaica') echo 'selected="selected"'; ?>>Jamaica</option>
                                                <option value="Montserrat">Montserrat</option>
                                                <option value="St. Kitts and Nevis">St. Kitts and Nevis</option>
                                                <option value="St. Lucia" <?php if($country == '_stlucia') echo 'selected="selected"'; ?>>St. Lucia</option>
                                                <option value="St. Martin">St. Martin</option>
                                                <option value="St. Vincent and the Grenadines">St. Vincent and the Grenadines</option>
                                                <option value="Suriname">Suriname</option>
                                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                <option value="Turks and Caicos" <?php if($country == '_turksandcaicos') echo 'selected="selected"'; ?>>Turks and Caicos</option>
                                            </select>
                                        </div>
                                    </div>
                                    <?php /*<div class="col-3 px-1">
                                        <div class="c-select">
                                            <select class="js-filter-select" id="job_state" disabled="disabled" data-type="state">
                                                <option value="">State</option>
                                            </select>
                                        </div>
                                    </div>*/ ?>
                                </div>
                            </div>
                            <div class="col-4">
                                <form action="?" class="c-search-form js-submit-search">
                                    <input type="search" class="c-search-form__input" placeholder="Search jobs">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <?php
                    if($country == '_jamaica'){
                        $countryQuery = array(
                            'key' => 'job_country',
                            'value' => 'Jamaica',
                            'compare' => '='
                        );
                    } elseif($country == '_stlucia'){
                        $countryQuery = array(
                            'key' => 'job_country',
                            'value' => 'St. Lucia',
                            'compare' => '='
                        );
                    } elseif($country == '_turksandcaicos'){
                        $countryQuery = array(
                            'key' => 'job_country',
                            'value' => 'Turks and Caicos',
                            'compare' => '='
                        );
                    } else {
                        $countryQuery = '';
                    }

                    $args = array('post_type' => 'job', 'posts_per_page' => 10, 'paged' => $paged, 'orderby' => 'date', 'order' => 'DESC',
                    'meta_query' => array('relation' => 'AND', array('key' => 'job_expiration', 'value' => date('Y-m-d'), 'compare' => '>='), $countryQuery));

                    query_posts($args);

                    if($wp_query->have_posts()) : ?>
                        <div class="js-posts-list">
                            <?php while ($wp_query->have_posts()) : $wp_query->the_post();
                                loop_joblisting($post->ID);
                            endwhile; ?>
                        </div><!-- .posts-list -->
                    <?php endif;

                    // load more ajax
                    $wp_query->query_vars['search_orderby_title'] = ''; // necessario pro search
                    $load_posts = $wp_query->query_vars; $load_current_page = $wp_query->query_vars['paged']; $load_max_page = $wp_query->max_num_pages;
                    loadmore_button($load_posts, $load_current_page, $load_max_page);
                    if($wp_query->max_num_pages > 1){ ?>
                        <span class="js-loadmore c-bt-load c-bt-load--outline mt-2 mt-md-4">Load more</span>
                    <?php } else { ?>
                        <span class="js-loadmore c-bt-load c-bt-load--outline mt-2 mt-md-4 hidden">Load more</span>
                    <?php } // end load more ajax

                    wp_reset_query(); wp_reset_postdata(); ?>
                </div>
                <div class="col-lg-3">
                    <section class="ads">
                        <div class="c-banner d-none d-lg-block"><!-- DESKTOP -->
                            <!-- /21850622224/Buzz_JobsList_RightSideBar_Desktop -->
                            <div id='div-gpt-ad-1570539074988-0' style='width: 300px; height: 600px;'>
                                <script>
                                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570539074988-0'); });
                                </script>
                            </div>
                        </div>
                        
                        <div class="c-banner d-block d-lg-none mt-4"><!-- MOBILE -->
                            <!-- /21850622224/Buzz_Jobs_AboveListing_Mobile -->
                            <div id='div-gpt-ad-1570538904609-0' style='width: 320px; height: 100px;'>
                                <script>
                                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570538904609-0'); });
                                </script>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <?php /*<script src="<?php echo get_template_directory_uri(); ?>/scripts/vendor/country-states.js"></script>
    <?php if($country){ ?>
        <script>set_city_state(job_country,job_state)</script>
    <?php }*/ ?>

<?php get_footer(); ?>