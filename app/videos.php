<?php
    /* Template Name: Video page */
    get_header(); ?>

    <div class="l-page-videos-listing">
        <?php include_once('inc/videos-listing/block-videos.php'); ?>

        <?php
            $areaTitle = "Watch next";
            include_once('inc/videos-listing/watch-next.php');
            
            $areaTitle = "More from buzz";
            include_once('inc/videos-listing/all-stories.php');
            ?>
    </div>

<?php get_footer(); ?>