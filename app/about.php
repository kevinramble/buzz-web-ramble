<?php
/* Template Name: About us */

$webview = isset($_GET['webview']) ? $_GET['webview'] : '';
if(!$webview || $webview != 'true') :
// normal, desktop e mobile

    get_header(); ?>

    <section class="l-page-about">
        <div class="c-hero-page" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/content/full.jpg')">
            <video autoplay loop muted playsinline class="c-hero-page__video">
                <source src="<?php echo get_template_directory_uri(); ?>/media/video.mp4" type="video/mp4">
            </video>
        </div>
        <div class="c-content c-about-text">
            <h2>We r Buzz</h2>
            <p>We inform. We inspire. We educate. We celebrate.<br> Our stories are told by a team across the Caribbean.<br> We share a different angle for the curious.</p>
        </div>
    </section>

    <?php get_footer(); ?>

<?php elseif($webview && $webview == 'true') :
	// apenas do app
	include_once('inc/app/header.php'); ?>

    <section class="l-page-about webview">
        <div class="c-hero-page" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/content/full.jpg')">
            <video autoplay loop muted playsinline class="c-hero-page__video">
                <source src="<?php echo get_template_directory_uri(); ?>/media/video.mp4" type="video/mp4">
            </video>
        </div>
        <div class="c-content c-about-text">
            <h2>We r Buzz</h2>
            <p>We inform. We inspire. We educate. We celebrate.<br> Our stories are told by a team across the Caribbean.<br> We share a different angle for the curious.</p>
        </div>
    </section>

	<?php include_once('inc/app/footer.php');
endif; ?>