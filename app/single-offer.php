<?php get_header(); ?>

<section class="l-page-list">
	<div class="c-list-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6">
					<h1 class="c-list-header__title">Offers</h1>
				</div>
			</div>
		</div>
	</div>
	<?php if(have_posts()) : while (have_posts()) : the_post();
		$offer_country = esc_html(get_post_meta($post->ID, 'offer_country', true));
		$offer_state = esc_html(get_post_meta($post->ID, 'offer_state', true));
		$offer_description = esc_html(get_post_meta($post->ID, 'offer_description', true));
		$offer_fullprice = esc_html(get_post_meta($post->ID, 'offer_fullprice', true));
		$offer_discount = esc_html(get_post_meta($post->ID, 'offer_discount', true));
		$offer_price = esc_html(get_post_meta($post->ID, 'offer_price', true));
		$offer_expiration = esc_html(get_post_meta($post->ID, 'offer_expiration', true));
		$offer_company = esc_html(get_post_meta($post->ID, 'offer_company', true));
		$offer_companyurl = esc_html(get_post_meta($post->ID, 'offer_companyurl', true));
		$offer_shortcode = get_post_meta($post->ID, 'offer_shortcode', true);
		$offer_category = wp_get_post_terms($post->ID, 'offer-category', array('fields' => 'all'));

		if($offer_description == ''){
			$offer_description = get_the_excerpt();
			$offer_description = substr($offer_description, 0, 130) . '...';
		}

		$dealExpired = false;
		$expiration = remainingTime($offer_expiration);
		if($expiration < 0){
			$dealExpired = true;
			$expirationText = 'Deal expired';
		} elseif($expiration == 0) {
			$expirationText = 'Deal ends <strong>today</strong>';
		} elseif($expiration > 0) {
			$expirationText = 'Ends within <strong>'. $expiration .' days</strong>';
		} ?>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<?php
						$gallery = get_post_meta($post->ID, 'post_gallery_id', true);
						if($gallery){ ?>
							<div class="c-carousel-default owl-carousel js-carousel-default">
								<?php foreach ($gallery as $image) {
									$imageURL = wp_get_attachment_url($image);
									echo '<img src="'. $imageURL .'">';
								} ?>
							</div>
						<?php } elseif(has_post_thumbnail()){
							$imageURL = get_the_post_thumbnail_url(); ?>
							<div class="c-carousel-default">
								<img src="<?php echo $imageURL; ?>">
							</div>
						<?php }
					?>
				</div>
				<div class="col-md-6">
					<div class="c-box-info">
						<div class="c-box-info__content">
							<p class="c-box-info__deal"><?php echo $expirationText; ?></p>
							<h2 class="c-box-info__title"><?php the_title(); ?></h2>
							<p class="c-box-info__company"><a href="<?php echo $offer_companyurl; ?>" target="_blank" title="<?php echo $offer_company; ?>"><?php echo $offer_company; ?></a></p>
							<p class="c-box-info__location"><?php if($offer_state) echo $offer_state . ', '; echo $offer_country; ?></p>
							<p class="c-box-info__description"><?php echo $offer_description; ?></p>

							<p class="c-box-info__price">
							<?php if($offer_discount > 0){ ?>
								<span class="c-box-info__price-off"><?php echo $offer_discount; ?>% OFF</span>
							<?php } ?>
								<?php /*<span class="c-box-info__price-old">$<?php echo $offer_fullprice; ?></span>
								<span class="c-box-info__price-current">$<?php echo $offer_price; ?></span>*/ ?>
							</p>

							<div class="c-social c-social--vertical d-none d-md-flex">
								<div class="c-social__book" aria-label="Bookmark"><?php the_favorites_button($post->ID, get_current_blog_id()); ?></div>
								<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="c-social__icon c-social__icon--fb-outline" aria-label="Facebook"></a>
								<a href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank" class="c-social__icon c-social__icon--tw-outline" aria-label="Twitter"></a>
							</div>

							<div class="c-social d-flex d-md-none">
								<div class="c-social__book" aria-label="Bookmark"><?php the_favorites_button($post->ID, get_current_blog_id()); ?></div>
								<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="c-social__icon c-social__icon--fb-outline" aria-label="Facebook"></a>
								<a href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank" class="c-social__icon c-social__icon--tw-outline" aria-label="Twitter"></a>
							</div>

							<?php
								if(is_user_logged_in()){
									$redeemedOffers = get_redeemedoffers();
									if(!$redeemedOffers) $redeemedOffers = array();
									$offer_code = esc_html(get_post_meta($post->ID, 'offer_code', true));
									if(!$dealExpired){
										if (!in_array($post->ID, $redeemedOffers)){ ?>
											<div class="js-offer-code-container">
												<a href="#0" class="c-box-info__bt-offer js-open-modal" data-modal="offer" data-offerid="<?php echo $post->ID; ?>">Get offer code now</a>
											</div>
											<?php include_once('inc/modals/offer-question.php'); ?>
										<?php } else { ?>
											<div class="c-card-post__code mt-3">
												<p class="c-card-post__code-number js-box-copy"><?php echo $offer_code; ?></p>
												<button class="c-card-post__code-btn js-toggle-copy">Copy</button>
											</div>
										<?php }
									} else {
										if (in_array($post->ID, $redeemedOffers)){ ?>
											<div class="c-card-post__code is-off mt-3">
												<p class="c-card-post__code-number"><?php echo $offer_code; ?></p>
											</div>
										<?php }
									}
								} else {
									if(!$dealExpired){ ?>
										<a href="#0" class="c-box-info__bt-offer js-open-modal" data-modal="needlogin">Get offer code now</a>
										<?php include_once('inc/modals/need-login.php'); ?>
									<?php }
								}
							?>
						</div>
					</div>
				</div>
			</div>

			<div class="c-post my-4 my-md-5">
				<?php the_content(); ?>
			</div>
		</div>

	<?php endwhile;
		$related = get_related_posts(get_the_ID(), 4);
		if($related->have_posts()): ?>
			<div class="c-list-cards">
				<div class="container">
					<h2 class="c-list-cards__title">Related offers</h2>

					<div class="c-list-cards__grid js-posts-list">
						<div class="row">
							<?php while($related->have_posts()) : $related->the_post();
								loop_offerlisting($post->ID);
							endwhile; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; wp_reset_query(); wp_reset_postdata(); //related ?>
	<?php endif; ?>
</section>

<?php get_footer(); ?>