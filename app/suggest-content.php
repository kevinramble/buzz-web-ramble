<?php
    /* Template Name: Suggest content */
    get_header(); ?>

<section class="l-page-sugest">
    <div class="c-header-logged">
        <div class="container">
            <img src="<?php echo get_avatar_url(um_user('ID'), 120); ?>" class="c-header-logged__avatar">
            <p class="c-header-logged__name"><?php echo um_user('display_name'); ?></p>

            <nav class="c-header-logged__menu">
                <a href="<?php echo get_bloginfo('url'); ?>/user/?tab=favorites" class="c-header-logged__menu-item">Favorites (<?php echo get_user_favorites_count(); ?>)</a>
                <a href="<?php echo get_bloginfo('url'); ?>/user/?tab=offers" class="c-header-logged__menu-item">My offers</a>
                <a href="<?php echo get_bloginfo('url'); ?>/account" class="c-header-logged__menu-item">Settings</a>
            </nav>
        </div>
    </div>

    <div class="l-page-sugest__wrapper container mt-4">
		<div class="c-contact-intro">
            <h1 class="c-contact-intro__title">Contribute to Buzz</h1>
            <h2 class="c-contact-intro__subtitle">Do you have an inspiring story, idea or something you think could be the latest buzz? Let us know and we'll get in touch </h2>
        </div>
        <form action="?" class="c-contact-form js-suggest-content-form" data-url="<?php bloginfo('template_directory'); ?>" method="post">
            <?php $securitynumber1 = ord(mt_rand(0,9)); $securitynumber2 = ord(mt_rand(0,9)); ?>
            <div class="row">
                <div class="col-md-6">
                    <input type="text" class="c-contact-form__input" name="article_title" id="article_title" placeholder="Article title">
                </div>
                <div class="col-md-6">
                    <div class="c-contact-form__select">
                        <select name="article_category" id="article_category">
                            <option value="">Category</option>
                            <?php
                                $categories = get_categories(array('orderby' => 'name', 'order' => 'ASC', 'parent' => 0, 'empty' => 0));
                                foreach($categories as $category) echo '<option value="'. $category->name .'">'. $category->name .'</option>';
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <textarea class="c-contact-form__textarea" cols="30" rows="10" placeholder="Your copy here" name="article_content" id="article_content"></textarea>

            <label class="security-question">
                <span>Prove your humanity:</span>
                <input class="text" placeholder="<?php echo '&#'.$securitynumber1. ' + &#' .$securitynumber2; ?>" name="securityanswer" id="securityanswer">
            </label><!-- .security-question -->
            <input type="hidden" name="securitynumber1" id="securitynumber1" value="<?php echo $securitynumber1; ?>">
            <input type="hidden" name="securitynumber2" id="securitynumber2" value="<?php echo $securitynumber2; ?>">

            <?php
                $current_user = wp_get_current_user();
                $current_user_name = ($current_user->user_firstname) ? ($current_user->user_firstname.' '.$current_user->user_lastname) : $current_user->display_name;
            ?>
            <input type="hidden" name="user_name" id="user_name" value="<?php echo $current_user_name; ?>">
            <input type="hidden" name="user_email" id="user_email" value="<?php echo $current_user->user_email; ?>">
            <input type="hidden" name="user_username" id="user_username" value="<?php echo $current_user->user_login; ?>">

            <button class="c-contact-form__btn js-send-button">Send now</button>

            <div class="notice"></div>
        </form>
	</div>
</section>

<div class="modal suggestion-successful">
	<div class="modal__dialog">
		<button class="modal__close" aria-label="Close"></button>

		<h1 class="c-title-headline">Thank you!</h1>

        <div class="c-description-headline mt-4">
            <p>We're buzzing with excitement at your suggestion! Someone from our team will be in touch shortly.</p>

            <a href="<?php echo get_bloginfo('url'); ?>" class="c-button c-button--negative c-button--full mt-4">Back to homepage</a>
        </div>
	</div>
</div>

<?php get_footer(); ?>