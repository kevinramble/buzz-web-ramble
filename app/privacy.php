<?php
    /* Template Name: Privacy policy */
    
    $webview = isset($_GET['webview']) ? $_GET['webview'] : '';
    if(!$webview || $webview != 'true') :
    // normal, desktop e mobile

    get_header(); ?>

    <section class="l-page-privacy">
        <div class="c-privacy-intro">
            <h1 class="c-privacy-intro__title">Welcome to Buzz’s Privacy Policy</h1>
            <h2 class="c-privacy-intro__subtitle">Last updated: September 5, 2019</h2>
            <p class="c-privacy-intro__text mt-4">This Privacy Policy governs all brands, websites, apps, and other products or services that link to, or contain references to, or made available by Ramble Media Group, its subsidiaries including Buzz and affiliates (“we”, “us” or “our”).  Please read this Privacy Policy carefully as it outlines the ways in which we acquire, secure and use your Data when you access, visit and/or use the Service. This Privacy Policy does not cover information collected on sites, applications, or services linked to from the Service that we do not own, operate or control.</p>
            <p class="c-privacy-intro__text mt-2">By using the Service, or otherwise, you consent to this Privacy Policy. If you do not agree with the terms & conditions of this Privacy Policy, you should not access, visit and/or use the Service</p>
        </div>

        <div class="c-privacy-anchor">
            <a href="#1" class="c-privacy-anchor__item list-indent">1. REASONS FOR COLLECTING USER INFORMATION</a>
            <a href="#2" class="c-privacy-anchor__item list-indent">2. NATURE OF THE INFORMATION WE COLLECT</a>
            <a href="#3" class="c-privacy-anchor__item list-indent">3. OUR USE OF YOUR INFORMATION</a>
            <a href="#4" class="c-privacy-anchor__item list-indent">4. CHANGES TO PRIVACY POLICY</a>
            <a href="#5" class="c-privacy-anchor__item list-indent">5. PROTECTION OF USER INFORMATION</a>
        </div>

        <div id="1" class="c-privacy-block">
            <h2 class="c-privacy-block__title">1. REASONS FOR COLLECTING USER INFORMATION</h2>
            <p class="c-privacy-block__text">Ramble Media Group and/or its subsidiary, Buzz, might collect and combine information you provide to us while using the service, and/or website or applications.  As a general rule we do not share our users’ personal information with third parties without first obtaining consent. By using the Service you agree that we are authorized to collect, use and keep your information for purposes such as:</p>
            <ol type="a" class="c-privacy-block__text list-indent">
                <li>Confirming your identity</li>
                <li>Communicating with you</li>
                <li>Providing potential advertisers and advertisers of our market share in particular geographical locations.  These persons will only be provided with cumulative statistics regarding sales, traffic patterns and general information about our users (location, gender and age).</li>
                <li>Improving and monitoring our products and services</li>
                <li>Assisting us in developing products and services to suit our users’ interests and providing personalized services for our users such as adapting advertisements which appear when you view our website to our users’ interests.</li>
                <li>Creating and provide statistical reports and analysis</li>
                <li>Providing you with notification of changes to our products, services and or the website</li>
            </ol>
        </div>

        <div id="2" class="c-privacy-block">
            <h2 class="c-privacy-block__title">2. NATURE OF THE INFORMATION WE COLLECT</h2>

            <h3 class="mt-4">PERSONAL INFORMATION</h3>
            <p class="c-privacy-block__text">We collect your personal information when you register or sign in to use any of the  interactive features of the service, website and/or applications, to receive information about promotions, jobs, special offers and events.  Such information might include your name, email address, date of birth, occupation, town or city, country, postal code and interests.  Your name, email address, date of birth, occupation, and gender will not be visible to other users of the interactive features of the website unless you authorize us to do so.</p>

            <h3 class="mt-4">OTHER INFORMATION</h3>
            <p class="c-privacy-block__text">We will collect information regarding the nature of the information you prefer to view, your comments, products and services, which are of interest to you.</p>
            <p class="c-privacy-block__text">Information is automatically delivered to us from your computer and web browser when you log on to the website by the placement of a cookie on your computer which delivers information to us. This information is not personal but includes your IP address, the pages you requested on your last visit, operating system and version and Internet settings. You can adjust your computer’s settings to prevent automatic delivery of information.</p>
            <p class="c-privacy-block__text">Advertisements on our website are provided by third party advertisers, such as Google, who may also need to install cookies on your computer to automatically place advertisements which may be of interest to you based on your browsing history. Please see: <a href="http://www.google.com/privacy_ads.html" target="_blank"><u>http://www.google.com/privacy_ads.html</u></a> for more information.</p>
        </div>

        <div id="3" class="c-privacy-block">
            <h2 class="c-privacy-block__title">3. OUR USE OF YOUR INFORMATION</h2>
            <p class="c-privacy-block__text">By using this website you agree that we may share your information with our affiliated companies.</p>
            <ol type="a" class="c-privacy-block__text list-indent">
                <li>We will not disclose, sell, transfer or share your personal information (email address, telephone numbers, and/ or street address) with third parties.  However we reserve the right to disclose your personal information to third parties (including but not limited to our heirs, successors in title or appointees) in the following circumstances:</li>
                    <ol type="1" class="c-privacy-block__text list-indent">
                        <li class="c-privacy-block__text">Where required by any law or regulation or upon the lawful request of a government body</li>
                        <li class="c-privacy-block__text">In furtherance of any criminal investigation</li>
                        <li class="c-privacy-block__text">To prevent unlawful activity</li>
                        <li class="c-privacy-block__text">To defend our legal rights</li>
                        <li class="c-privacy-block__text">To protect ourselves from civil or criminal liability.</li>
                    </ol>

                <li class="c-privacy-block__text">We will respect your email privacy and therefore we will not send unsolicited emails to your email address. We may send you information by email regarding your account or in response to your enquiries.</li>
                <li class="c-privacy-block__text">We will send notices of our promotions or events which maybe of interest to you, if you ask us to.</li>
                <li class="c-privacy-block__text">Please note that persons paying for advertisements or other ecommerce transactions provided by us on the website via credit card will be required to enter their credit card information in the field provided.  We do not retain credit card information.   Ordinary users of the website will not be required to disclose personal information (including credit card information) via email or telephone.</li>
            </ol>
        </div>

        <div id="4" class="c-privacy-block">
            <h2 class="c-privacy-block__title">4. CHANGES TO PRIVACY POLICY</h2>
            <p class="c-privacy-block__text">Notice of any change to our Privacy Policy will be posted on this page.</p>
        </div>

        <div id="5" class="c-privacy-block">
            <h2 class="c-privacy-block__title">5. PROTECTION OF USER INFORMATION</h2>
            <ol type="a" class="c-privacy-block__text list-indent">
                <li class="c-privacy-block__text">Information transmitted via the Internet is at risk of interception.  We make no guarantees as to the safety of your personal information however, in an effort to protect user information we use up to date and trusted security software and technological features to protect users’ information.</li>
                <li class="c-privacy-block__text">We will take the necessary steps to ensure that our contractors are subject to strict contractual obligations including the privacy of our users.</li>
                <li class="c-privacy-block__text">The material that you post on the website can be accessed worldwide. By agreeing to use the website you consent to such transfer of your material for and by way of Internet.</li>
                <li class="c-privacy-block__text">The information we transfer outside of Jamaica is transferred done with the relevant protections required by law in place.</li>
                <li class="c-privacy-block__text">Information or material published via the interactive features on the website is accessible by the public and therefore we urge you not to disclose any personal information.</li>
                <li class="c-privacy-block__text">If there is any information you do not wish for us to collect please do not submit it to us.</li>
            </ol>
            <h3 class="mt-4">Information Collection And Use</h3>
            <p class="c-privacy-block__text">While using our website, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to your name, email address and/or location.</p>

            <h3 class="mt-4">Log Data</h3>
            <p class="c-privacy-block__text">Like many site operators, we collect information that your browser sends whenever you visit our Site ("Log Data").</p>
            <p class="c-privacy-block__text">This Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Site that you visit, the time and date of your visit, the time spent on those pages and other statistics.</p>
            <p class="c-privacy-block__text">In addition, we may use third party services such as Google Analytics that collect, monitor and analyze this information to help us improve our service, website and/or applications</p>

            <h3 class="mt-4">Communications</h3>
            <p class="c-privacy-block__text">We may use your Personal Information to contact you with newsletters, marketing or promotional materials and other information.</p>

            <h3 class="mt-4">Cookies</h3>
            <p class="c-privacy-block__text">Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer's hard drive.</p>
            <p class="c-privacy-block__text">Like many sites, we use "cookies" to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Site.</p>

            <h3 class="mt-4">Security</h3>
            <p class="c-privacy-block__text">The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p>

            <h3 class="mt-4">Changes To This Privacy Policy</h3>
            <p class="c-privacy-block__text">We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the Service after we post any modifications to the Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified Privacy Policy.</p>
            <p class="c-privacy-block__text">If we make any material changes to this Privacy Policy, we will notify you either through the email address you have provided us, or by placing a prominent notice on our website.</p>

            <h3 class="mt-4">Contact Us</h3>
            <p class="c-privacy-block__text">If you have any questions about this Privacy Policy, please <a href="<?php echo get_bloginfo('url'); ?>/contact" title="Contact us">contact us</a>.</p>
        </div>
    </section>

    <?php get_footer(); ?>

<?php elseif($webview && $webview == 'true') :
	// single apenas do app
	include_once('inc/app/header.php'); ?>

    <section class="l-page-privacy">
        <div class="c-privacy-intro">
            <h1 class="c-privacy-intro__title">Welcome to Buzz’s Privacy Policy</h1>
            <h2 class="c-privacy-intro__subtitle">Last updated: September 5, 2019</h2>
            <p class="c-privacy-intro__text mt-4">This Privacy Policy governs all brands, websites, apps, and other products or services that link to, or contain references to, or made available by Ramble Media Group, its subsidiaries including Buzz and affiliates (“we”, “us” or “our”).  Please read this Privacy Policy carefully as it outlines the ways in which we acquire, secure and use your Data when you access, visit and/or use the Service. This Privacy Policy does not cover information collected on sites, applications, or services linked to from the Service that we do not own, operate or control.</p>
            <p class="c-privacy-intro__text mt-2">By using the Service, or otherwise, you consent to this Privacy Policy. If you do not agree with the terms & conditions of this Privacy Policy, you should not access, visit and/or use the Service</p>
        </div>

        <div class="c-privacy-anchor">
            <a href="#1" class="c-privacy-anchor__item list-indent">1. REASONS FOR COLLECTING USER INFORMATION</a>
            <a href="#2" class="c-privacy-anchor__item list-indent">2. NATURE OF THE INFORMATION WE COLLECT</a>
            <a href="#3" class="c-privacy-anchor__item list-indent">3. OUR USE OF YOUR INFORMATION</a>
            <a href="#4" class="c-privacy-anchor__item list-indent">4. CHANGES TO PRIVACY POLICY</a>
            <a href="#5" class="c-privacy-anchor__item list-indent">5. PROTECTION OF USER INFORMATION</a>
        </div>

        <div id="1" class="c-privacy-block">
            <h2 class="c-privacy-block__title">1. REASONS FOR COLLECTING USER INFORMATION</h2>
            <p class="c-privacy-block__text">Ramble Media Group and/or its subsidiary, Buzz, might collect and combine information you provide to us while using the service, and/or website or applications.  As a general rule we do not share our users’ personal information with third parties without first obtaining consent. By using the Service you agree that we are authorized to collect, use and keep your information for purposes such as:</p>
            <ol type="a" class="c-privacy-block__text list-indent">
                <li>Confirming your identity</li>
                <li>Communicating with you</li>
                <li>Providing potential advertisers and advertisers of our market share in particular geographical locations.  These persons will only be provided with cumulative statistics regarding sales, traffic patterns and general information about our users (location, gender and age).</li>
                <li>Improving and monitoring our products and services</li>
                <li>Assisting us in developing products and services to suit our users’ interests and providing personalized services for our users such as adapting advertisements which appear when you view our website to our users’ interests.</li>
                <li>Creating and provide statistical reports and analysis</li>
                <li>Providing you with notification of changes to our products, services and or the website</li>
            </ol>
        </div>

        <div id="2" class="c-privacy-block">
            <h2 class="c-privacy-block__title">2. NATURE OF THE INFORMATION WE COLLECT</h2>

            <h3 class="mt-4">PERSONAL INFORMATION</h3>
            <p class="c-privacy-block__text">We collect your personal information when you register or sign in to use any of the  interactive features of the service, website and/or applications, to receive information about promotions, jobs, special offers and events.  Such information might include your name, email address, date of birth, occupation, town or city, country, postal code and interests.  Your name, email address, date of birth, occupation, and gender will not be visible to other users of the interactive features of the website unless you authorize us to do so.</p>

            <h3 class="mt-4">OTHER INFORMATION</h3>
            <p class="c-privacy-block__text">We will collect information regarding the nature of the information you prefer to view, your comments, products and services, which are of interest to you.</p>
            <p class="c-privacy-block__text">Information is automatically delivered to us from your computer and web browser when you log on to the website by the placement of a cookie on your computer which delivers information to us. This information is not personal but includes your IP address, the pages you requested on your last visit, operating system and version and Internet settings. You can adjust your computer’s settings to prevent automatic delivery of information.</p>
            <p class="c-privacy-block__text">Advertisements on our website are provided by third party advertisers, such as Google, who may also need to install cookies on your computer to automatically place advertisements which may be of interest to you based on your browsing history. Please see: <a href="http://www.google.com/privacy_ads.html" target="_blank"><u>http://www.google.com/privacy_ads.html</u></a> for more information.</p>
        </div>

        <div id="3" class="c-privacy-block">
            <h2 class="c-privacy-block__title">3. OUR USE OF YOUR INFORMATION</h2>
            <p class="c-privacy-block__text">By using this website you agree that we may share your information with our affiliated companies.</p>
            <ol type="a" class="c-privacy-block__text list-indent">
                <li>We will not disclose, sell, transfer or share your personal information (email address, telephone numbers, and/ or street address) with third parties.  However we reserve the right to disclose your personal information to third parties (including but not limited to our heirs, successors in title or appointees) in the following circumstances:</li>
                    <ol type="1" class="c-privacy-block__text list-indent">
                        <li class="c-privacy-block__text">Where required by any law or regulation or upon the lawful request of a government body</li>
                        <li class="c-privacy-block__text">In furtherance of any criminal investigation</li>
                        <li class="c-privacy-block__text">To prevent unlawful activity</li>
                        <li class="c-privacy-block__text">To defend our legal rights</li>
                        <li class="c-privacy-block__text">To protect ourselves from civil or criminal liability.</li>
                    </ol>

                <li class="c-privacy-block__text">We will respect your email privacy and therefore we will not send unsolicited emails to your email address. We may send you information by email regarding your account or in response to your enquiries.</li>
                <li class="c-privacy-block__text">We will send notices of our promotions or events which maybe of interest to you, if you ask us to.</li>
                <li class="c-privacy-block__text">Please note that persons paying for advertisements or other ecommerce transactions provided by us on the website via credit card will be required to enter their credit card information in the field provided.  We do not retain credit card information.   Ordinary users of the website will not be required to disclose personal information (including credit card information) via email or telephone.</li>
            </ol>
        </div>

        <div id="4" class="c-privacy-block">
            <h2 class="c-privacy-block__title">4. CHANGES TO PRIVACY POLICY</h2>
            <p class="c-privacy-block__text">Notice of any change to our Privacy Policy will be posted on this page.</p>
        </div>

        <div id="5" class="c-privacy-block">
            <h2 class="c-privacy-block__title">5. PROTECTION OF USER INFORMATION</h2>
            <ol type="a" class="c-privacy-block__text list-indent">
                <li class="c-privacy-block__text">Information transmitted via the Internet is at risk of interception.  We make no guarantees as to the safety of your personal information however, in an effort to protect user information we use up to date and trusted security software and technological features to protect users’ information.</li>
                <li class="c-privacy-block__text">We will take the necessary steps to ensure that our contractors are subject to strict contractual obligations including the privacy of our users.</li>
                <li class="c-privacy-block__text">The material that you post on the website can be accessed worldwide. By agreeing to use the website you consent to such transfer of your material for and by way of Internet.</li>
                <li class="c-privacy-block__text">The information we transfer outside of Jamaica is transferred done with the relevant protections required by law in place.</li>
                <li class="c-privacy-block__text">Information or material published via the interactive features on the website is accessible by the public and therefore we urge you not to disclose any personal information.</li>
                <li class="c-privacy-block__text">If there is any information you do not wish for us to collect please do not submit it to us.</li>
            </ol>
            <h3 class="mt-4">Information Collection And Use</h3>
            <p class="c-privacy-block__text">While using our website, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to your name, email address and/or location.</p>

            <h3 class="mt-4">Log Data</h3>
            <p class="c-privacy-block__text">Like many site operators, we collect information that your browser sends whenever you visit our Site ("Log Data").</p>
            <p class="c-privacy-block__text">This Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Site that you visit, the time and date of your visit, the time spent on those pages and other statistics.</p>
            <p class="c-privacy-block__text">In addition, we may use third party services such as Google Analytics that collect, monitor and analyze this information to help us improve our service, website and/or applications</p>

            <h3 class="mt-4">Communications</h3>
            <p class="c-privacy-block__text">We may use your Personal Information to contact you with newsletters, marketing or promotional materials and other information.</p>

            <h3 class="mt-4">Cookies</h3>
            <p class="c-privacy-block__text">Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer's hard drive.</p>
            <p class="c-privacy-block__text">Like many sites, we use "cookies" to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Site.</p>

            <h3 class="mt-4">Security</h3>
            <p class="c-privacy-block__text">The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p>

            <h3 class="mt-4">Changes To This Privacy Policy</h3>
            <p class="c-privacy-block__text">We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the Service after we post any modifications to the Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified Privacy Policy.</p>
            <p class="c-privacy-block__text">If we make any material changes to this Privacy Policy, we will notify you either through the email address you have provided us, or by placing a prominent notice on our website.</p>

            <h3 class="mt-4">Contact Us</h3>
            <p class="c-privacy-block__text">If you have any questions about this Privacy Policy, please <a href="<?php echo get_bloginfo('url'); ?>/contact" title="Contact us">contact us</a>.</p>
        </div>
    </section>

    <?php include_once('inc/app/footer.php');
endif; ?>