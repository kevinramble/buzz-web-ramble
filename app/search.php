<?php get_header(); ?>

	<section class="l-page-list">
		<div class="c-list-header">
			<div class="container">
				<div class="row align-items-center pos-relative">
					<div class="col-md-4">
						<h1 class="c-list-header__title js-search-term"><?php echo esc_html(get_search_query(false)); ?></h1>
					</div>

					<span class="js-toggle-filters"></span>
					<div class="col-md-8 d-lg-block js-search-filters js-filters">
						<div class="row">
							<div class="col-9">
								<div class="row mx-n1">
									<div class="col-3 px-1">
										<div class="c-select">
											<select class="js-filter-select" data-type="countries">
												<option value="">All countries</option>
												<?php
													$terms = get_terms(array('taxonomy' => 'countries', 'hide_empty' => 1));
													foreach ($terms as $term) echo '<option value="'. $term->slug . '">'. $term->name .'</option>';
												?>
											</select>
										</div>
									</div>

									<div class="col-3 px-1">
										<div class="c-select">
											<select class="js-filter-select" data-type="cat">
												<option value="">All categories</option>
												<?php
													$categories = get_categories(array('orderby' => 'name', 'order' => 'ASC', 'parent' => 0, 'empty' => 0));
													foreach($categories as $category) echo '<option value="'. $category->slug .'">'. $category->name .'</option>';
												?>
											</select>
										</div>
									</div>

									<div class="col-3 px-1">
										<div class="c-select">
											<select class="js-filter-select" data-type="subcat">
												<option value="">All sub-categories</option>
												<?php
													$terms = get_terms(array('taxonomy' => 'sub-category', 'hide_empty' => 1));
													foreach ($terms as $term) echo '<option value="'. $term->slug . '">'. $term->name .'</option>';
												?>
											</select>
										</div>
									</div>

									<div class="col-3 px-1">
										<div class="c-select">
											<select class="js-filter-select" data-type="type">
												<option value="">Type</option>
												<option value="post">Articles</option>
												<option value="video">Videos</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-3">
								<form action="?" class="c-search-form js-submit-search">
									<input type="search" class="c-search-form__input" placeholder="<?php echo esc_html(get_search_query(false)); ?>">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row l-page-list__results">
			<div class="container">
				<div class="row">
					<div class="col-lg-9">
						<div class="js-posts-list row">
						<?php
							$args = array('s' => get_query_var('s'), 'post_type' => array('post', 'video'), 'posts_per_page' => 9, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged);
							query_posts($args);

							if (have_posts()) :
								while (have_posts()) : the_post();
									loop_genericcard($post->ID);
								endwhile;
							?>

						<?php else : ?>
							<div class="c-search-no-results">
								<h2>We couldn't find anything for <span>"<?php echo esc_html(get_search_query(false)); ?>"</span>.</h2>
								<p>Please check your spelling, try more general, or fewer words and try again!</p>
							</div>
						<?php endif; ?>

						</div>

						<?php // load more ajax
						$wp_query->query_vars['search_orderby_title'] = ''; // necessario pro search
						$load_posts = $wp_query->query_vars; $load_current_page = $wp_query->query_vars['paged']; $load_max_page = $wp_query->max_num_pages;
						loadmore_button($load_posts, $load_current_page, $load_max_page);
						if($wp_query->max_num_pages > 1){ ?>
							<span class="js-loadmore c-bt-load">Load more</span>
						<?php } else { ?>
							<span class="js-loadmore c-bt-load hidden">Load more</span>
						<?php } // end load more ajax

						wp_reset_query(); wp_reset_postdata(); ?>
					</div>
					
					<div class="col-lg-3">
						<section class="ads">
							<div class="c-banner d-none d-lg-block"><!-- DESKTOP -->
								<!-- /21850622224/Buzz_Search_Results_Desktop -->
								<div id='div-gpt-ad-1570539410042-0' style='width: 300px; height: 600px;'>
									<script>
										googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570539410042-0'); });
									</script>
								</div>
							</div>
							
							<div class="c-banner d-block d-lg-none"><!-- MOBILE -->
								<!-- /21850622224/Buzz_Search_Mobile -->
								<div id='div-gpt-ad-1570539377887-0' style='width: 300px; height: 250px;'>
									<script>
										googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570539377887-0'); });
									</script>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>
