<?php
	ini_set('display_errors', 0);
	if(!isset($content_width)) $content_width = 1440;

	// Remove admin bar from site when user is logged in.
	function remove_admin_bar(){
		show_admin_bar(false);
	}
	add_action('after_setup_theme', 'remove_admin_bar');

	// Remove XMLRPC
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	add_filter('xmlrpc_enabled', '__return_false');

	// Remove REST API (wp-json)
	remove_action('wp_head', 'rest_output_link_wp_head', 10);
	remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);

	// Remove WP Emoji
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('admin_print_styles', 'print_emoji_styles');

	// Turn off oEmbed auto discovery.
	remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10); // Don't filter oEmbed results.
	remove_action('wp_head', 'wp_oembed_add_discovery_links'); // Remove oEmbed discovery links.
	remove_action('wp_head', 'wp_oembed_add_host_js'); // Remove oEmbed-specific JavaScript from the front-end and back-end.

	// Change the text at the of the excerpt
	function new_excerpt_more($more){
		global $post;
		return '...';
	}
	add_filter('excerpt_more', 'new_excerpt_more');
	add_filter('excerpt_length', 'my_excerpt_length');
	function my_excerpt_length($len){return 40;}

	// Remove posts and pages
	add_action('admin_menu', 'my_remove_menu_pages');
	function my_remove_menu_pages(){
		//remove_menu_page('edit.php');
		//remove_menu_page('edit.php?post_type=page');
		remove_menu_page('edit-comments.php');
	}

	// Add thumbs
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size(1060, 655, true);
	//add_image_size('##nomeaqui', 1120, 810, true);

	// Get the first image from a post
	function catch_that_image($postID = null){
		if($postID){
			$post = get_post($postID);
		} else {
			global $post, $posts;
		}

		$first_img = '';
		ob_start();
		ob_end_clean();
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
		if($output) $first_img = $matches[1][0];
		if(empty($first_img)) $first_img = get_bloginfo('template_directory') . '/img/content/nothing.png'; // Defines a default image
		return $first_img;
	}
	//Check if thumbnail exists / return placeholder as default
	function thumb_exist($url = NULL){
		if (!$url) return FALSE;

		$noimage = get_template_directory_uri()."/img/layout/thumb_placeholder.jpg";

		$headers = get_headers($url);
		return stripos($headers[0], "200 OK") ? $url : $noimage;
	}

	// Remove sticky posts option (aka hide with css)
	add_action('admin_print_styles', 'hide_sticky_option');
	function hide_sticky_option(){
		global $post_type, $pagenow;
		if('post.php' != $pagenow && 'post-new.php' != $pagenow && 'edit.php' != $pagenow) return; ?>
		<style type="text/css">
			#inspector-checkbox-control-1, #inspector-checkbox-control-1 + label {display:none!important}
			#inspector-checkbox-control-4, #inspector-checkbox-control-4 + label {display:none!important}
			#inspector-checkbox-control-5, #inspector-checkbox-control-5 + label {display:none!important}
		</style> <?php
	}

	// Remove sticky posts from all queries
	add_action('pre_get_posts', 'ignore_sticky_posts');
	function ignore_sticky_posts($query){
		$query->set('ignore_sticky_posts', true);
	}

	// Unlink image from attachment page
	function wpb_imagelink_setup(){
		$image_set = get_option('image_default_link_type');
		if ($image_set !== 'none'){
			update_option('image_default_link_type', 'none');
		}
	}
	add_action('admin_init', 'wpb_imagelink_setup', 10);

	// Change wordpress jquery version
	function modify_jquery_version() {
		if (!is_admin()){
			wp_deregister_script('jquery');
			wp_register_script('jquery',  get_template_directory_uri() . '/scripts/vendor/jquery.js', false, '2.2.4');
			wp_enqueue_script('jquery');
		}
	}
	add_action('wp_enqueue_scripts', 'modify_jquery_version');
	
	// Create pages
	require_once('functions/create-pages/create-pages.php');

	// Global config - home
	require_once('functions/global-config/global-config.php');
	function global_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('global-config-style', get_template_directory_uri() . '/functions/global-config/global-config.css');
		wp_enqueue_script('global-config-js', get_template_directory_uri() . '/functions/global-config/global-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'global_config') add_action('admin_enqueue_scripts', 'global_config_enqueue');

	// Global config - categories
	function global_categories_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('categories-config-style', get_template_directory_uri() . '/functions/global-config/categories/categories-config.css');
		wp_enqueue_script('categories-config-js', get_template_directory_uri() . '/functions/global-config/categories/categories-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'global_config_categories') add_action('admin_enqueue_scripts', 'global_categories_config_enqueue');

	// Global config - videos (play)
   function global_videos_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('video-config-style', get_template_directory_uri() . '/functions/global-config/videos/video-config.css');
		wp_enqueue_script('video-config-js', get_template_directory_uri() . '/functions/global-config/videos/video-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'global_config_videos') add_action('admin_enqueue_scripts', 'global_videos_config_enqueue');

	// Jamaica config - home
	require_once('functions/jamaica-config/jamaica-config.php');
	function jamaica_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('jamaica-config-style', get_template_directory_uri() . '/functions/jamaica-config/jamaica-config.css');
		wp_enqueue_script('jamaica-config-js', get_template_directory_uri() . '/functions/jamaica-config/jamaica-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'jamaica_config') add_action('admin_enqueue_scripts', 'jamaica_config_enqueue');

	// Jamaica config - categories
	function jamaica_categories_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('jamaica-categories-config-style', get_template_directory_uri() . '/functions/jamaica-config/categories/categories-config.css');
		wp_enqueue_script('jamaica-categories-config-js', get_template_directory_uri() . '/functions/jamaica-config/categories/categories-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'jamaica_config_categories') add_action('admin_enqueue_scripts', 'jamaica_categories_config_enqueue');

	// Jamaica config - videos (play)
   function jamaica_videos_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('jamaica-video-config-style', get_template_directory_uri() . '/functions/jamaica-config/videos/video-config.css');
		wp_enqueue_script('jamaica-video-config-js', get_template_directory_uri() . '/functions/jamaica-config/videos/video-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'jamaica_config_videos') add_action('admin_enqueue_scripts', 'jamaica_videos_config_enqueue');

	// St. Lucia config - home
	require_once('functions/stlucia-config/stlucia-config.php');
	function stlucia_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('stlucia-config-style', get_template_directory_uri() . '/functions/stlucia-config/stlucia-config.css');
		wp_enqueue_script('stlucia-config-js', get_template_directory_uri() . '/functions/stlucia-config/stlucia-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'stlucia_config') add_action('admin_enqueue_scripts', 'stlucia_config_enqueue');

	// St. Lucia config - categories
	function stlucia_categories_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('stlucia-categories-config-style', get_template_directory_uri() . '/functions/stlucia-config/categories/categories-config.css');
		wp_enqueue_script('stlucia-categories-config-js', get_template_directory_uri() . '/functions/stlucia-config/categories/categories-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'stlucia_config_categories') add_action('admin_enqueue_scripts', 'stlucia_categories_config_enqueue');

	// St. Lucia config - videos (play)
   function stlucia_videos_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('stlucia-video-config-style', get_template_directory_uri() . '/functions/stlucia-config/videos/video-config.css');
		wp_enqueue_script('stlucia-video-config-js', get_template_directory_uri() . '/functions/stlucia-config/videos/video-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'stlucia_config_videos') add_action('admin_enqueue_scripts', 'stlucia_videos_config_enqueue');

	// Turks and Caicos config - home
	require_once('functions/turksandcaicos-config/turksandcaicos-config.php');
	function turksandcaicos_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('turksandcaicos-config-style', get_template_directory_uri() . '/functions/turksandcaicos-config/turksandcaicos-config.css');
		wp_enqueue_script('turksandcaicos-config-js', get_template_directory_uri() . '/functions/turksandcaicos-config/turksandcaicos-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'turksandcaicos_config') add_action('admin_enqueue_scripts', 'turksandcaicos_config_enqueue');

	// Turks and Caicos config - categories
	function turksandcaicos_categories_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('turksandcaicos-categories-config-style', get_template_directory_uri() . '/functions/turksandcaicos-config/categories/categories-config.css');
		wp_enqueue_script('turksandcaicos-categories-config-js', get_template_directory_uri() . '/functions/turksandcaicos-config/categories/categories-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'turksandcaicos_config_categories') add_action('admin_enqueue_scripts', 'turksandcaicos_categories_config_enqueue');

	// Turks and Caicos config - videos (play)
   function turksandcaicos_videos_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('turksandcaicos-video-config-style', get_template_directory_uri() . '/functions/turksandcaicos-config/videos/video-config.css');
		wp_enqueue_script('turksandcaicos-video-config-js', get_template_directory_uri() . '/functions/turksandcaicos-config/videos/video-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'turksandcaicos_config_videos') add_action('admin_enqueue_scripts', 'turksandcaicos_videos_config_enqueue');

	// Barbados config - home
	require_once('functions/barbados-config/barbados-config.php');
	function barbados_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('barbados-config-style', get_template_directory_uri() . '/functions/barbados-config/barbados-config.css');
		wp_enqueue_script('barbados-config-js', get_template_directory_uri() . '/functions/barbados-config/barbados-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'barbados_config') add_action('admin_enqueue_scripts', 'barbados_config_enqueue');

	// Barbados config - categories
	function barbados_categories_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('barbados-categories-config-style', get_template_directory_uri() . '/functions/barbados-config/categories/categories-config.css');
		wp_enqueue_script('barbados-categories-config-js', get_template_directory_uri() . '/functions/barbados-config/categories/categories-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'barbados_config_categories') add_action('admin_enqueue_scripts', 'barbados_categories_config_enqueue');

	// Barbados config - videos (play)
   function barbados_videos_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('barbados-video-config-style', get_template_directory_uri() . '/functions/barbados-config/videos/video-config.css');
		wp_enqueue_script('barbados-video-config-js', get_template_directory_uri() . '/functions/barbados-config/videos/video-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'barbados_config_videos') add_action('admin_enqueue_scripts', 'barbados_videos_config_enqueue');

	// Bahamas config - home
	require_once('functions/bahamas-config/bahamas-config.php');
	function bahamas_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('bahamas-config-style', get_template_directory_uri() . '/functions/bahamas-config/bahamas-config.css');
		wp_enqueue_script('bahamas-config-js', get_template_directory_uri() . '/functions/bahamas-config/bahamas-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'bahamas_config') add_action('admin_enqueue_scripts', 'bahamas_config_enqueue');

	// Bahamas config - categories
	function bahamas_categories_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('bahamas-categories-config-style', get_template_directory_uri() . '/functions/bahamas-config/categories/categories-config.css');
		wp_enqueue_script('bahamas-categories-config-js', get_template_directory_uri() . '/functions/bahamas-config/categories/categories-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'bahamas_config_categories') add_action('admin_enqueue_scripts', 'bahamas_categories_config_enqueue');

	// Bahamas config - videos (play)
   function bahamas_videos_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('bahamas-video-config-style', get_template_directory_uri() . '/functions/bahamas-config/videos/video-config.css');
		wp_enqueue_script('bahamas-video-config-js', get_template_directory_uri() . '/functions/bahamas-config/videos/video-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'bahamas_config_videos') add_action('admin_enqueue_scripts', 'bahamas_videos_config_enqueue');

	// Cayman config - home
	require_once('functions/cayman-config/cayman-config.php');
	function cayman_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('cayman-config-style', get_template_directory_uri() . '/functions/cayman-config/cayman-config.css');
		wp_enqueue_script('cayman-config-js', get_template_directory_uri() . '/functions/cayman-config/cayman-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'cayman_config') add_action('admin_enqueue_scripts', 'cayman_config_enqueue');

	// Cayman config - categories
	function cayman_categories_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('cayman-categories-config-style', get_template_directory_uri() . '/functions/cayman-config/categories/categories-config.css');
		wp_enqueue_script('cayman-categories-config-js', get_template_directory_uri() . '/functions/cayman-config/categories/categories-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'cayman_config_categories') add_action('admin_enqueue_scripts', 'cayman_categories_config_enqueue');

	// Cayman config - videos (play)
   function cayman_videos_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('cayman-video-config-style', get_template_directory_uri() . '/functions/cayman-config/videos/video-config.css');
		wp_enqueue_script('cayman-video-config-js', get_template_directory_uri() . '/functions/cayman-config/videos/video-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && $_GET['page'] === 'cayman_config_videos') add_action('admin_enqueue_scripts', 'cayman_videos_config_enqueue');



	// App - post, push, config
	require_once('functions/app-post/app-post.php');
	require_once('functions/push-notification/push-notification.php');
	require_once('functions/app-config/app-config.php');
	function app_config_enqueue(){
		wp_enqueue_media();
		wp_enqueue_style('app-config-style', get_template_directory_uri() . '/functions/app-config/app-config.css');
		wp_enqueue_script('app-config-js', get_template_directory_uri() . '/functions/app-config/app-config.js', array('jquery'));
		wp_enqueue_script('jquery-ui-sortable');
	}
	if(isset($_GET['page']) && (strpos($_GET['page'], 'app_config') !== false)){
		add_action('admin_enqueue_scripts', 'app_config_enqueue');
	}

	// -- POST CONFIGURATIONS
	// Gutengerg stuff
	require_once('functions/gutenberg/gutenberg.php');

	// Sponsored post
	require_once('functions/sponsored-post/sponsored-post.php');

	// Videos: custom video post type
	require_once('functions/videos/video-post.php');

	// Create video from post
	require_once('functions/videos/create-video-from-post.php');

	// Jobs: custom job post type
	require_once('functions/jobs/job-post.php');

	// Jobs: Apply for job
	function apply_job_scripts(){
		wp_register_script('apply_job', get_template_directory_uri() . '/functions/jobs/apply-job/apply-job.js', array('jquery'));
		wp_localize_script('apply_job', 'apply_job_params', array('ajaxurl' => site_url() . '/wp-admin/admin-ajax.php'));
		wp_enqueue_script('apply_job');
	}
	add_action('wp_enqueue_scripts', 'apply_job_scripts');
	require_once('functions/jobs/apply-job/apply-job.php');

	// Offers: custom offer post type
	require_once('functions/offers/offer-post.php');

	// Offers: Redeem offers
	function redeem_offer_scripts(){
		wp_register_script('redeem_offer', get_template_directory_uri() . '/functions/offers/redeem-offer/redeem-offer.js', array('jquery'));
		wp_localize_script('redeem_offer', 'redeem_offer_params', array('ajaxurl' => site_url() . '/wp-admin/admin-ajax.php'));
		wp_enqueue_script('redeem_offer');
	}
	add_action('wp_enqueue_scripts', 'redeem_offer_scripts');
	require_once('functions/offers/redeem-offer/redeem-offer.php');

	// Out: custom out post type
	require_once('functions/out/out-post.php');

	// Gallery for posts
	require_once('functions/post-gallery/gallery.php');

	// Helper functions
	require_once('functions/helper-functions/helper-functions.php');
	require_once('functions/helper-functions/country-names.php');
	require_once('functions/helper-functions/get_terms_post_type.php');

	// Criar as categorias
	function build_taxonomies(){
		// Main categories
		$articleTerms = array('Hot', 'Life', 'News');
		foreach($articleTerms as $term) {
			if(!term_exists($term, 'category')){
				wp_insert_term($term, 'category');
			}
		}

		// Main sub-categories
		register_taxonomy(
			'sub-category',
			array('post', 'video', 'app-post'),
			array(
				'hierarchical' => true,
				'label' => 'Sub-category',
				'query_var' => false,
				'rewrite' => array('slug' => 'sub-category', 'with_front' => false),
				'show_in_rest' => true,
				'show_admin_column' => true,
				'capabilities'      => array(
					'assign_terms' => 'edit_posts',
					'edit_terms'   => 'god',
					'manage_terms' => 'god',
				)
			)
		);
		$subTerms = array('Sports', 'Money', 'Music', 'Travel', 'Love', 'Style', 'Wellness', 'Jobs', 'Events', 'Entertainment', 'Politics', 'Cool');
		foreach($subTerms as $term) {
			if(!term_exists($term, 'sub-category')){
				wp_insert_term($term, 'sub-category');
			}
		}

		// Countries
		register_taxonomy(
			'countries',
			array('post', 'video', 'app-post'),
			array(
				'hierarchical' => true,
				'label' => 'Countries',
				'query_var' => false,
				'rewrite' => array('slug' => 'countries', 'with_front' => false),
				'show_in_rest' => true,
				'show_admin_column' => true,
				'capabilities'      => array(
					'assign_terms' => 'edit_posts',
					'edit_terms'   => 'god',
					'manage_terms' => 'god',
				)
			)
		);
		$countriesTerms = array('Anguilla', 'Antigua and Barbuda', 'ABC Islands', 'Barbados', 'Bermuda', 'British Virgin Islands', 'Cayman', 'Dominica', 'FWI', 'Grenada', 'Guyana', 'Haiti', 'Jamaica', 'Montserrat', 'St. Kitts and Nevis', 'St. Lucia', 'St. Martin', 'St. Vincent and the Grenadines', 'Suriname', 'Trinidad and Tobago', 'Turks and Caicos', 'Other');
		foreach($countriesTerms as $term) {
			if(!term_exists($term, 'countries')){
				wp_insert_term($term, 'countries');
			}
		}

		// Offer taxonomy
		register_taxonomy(
			'offer-category',
			'offer',
			array(
				'hierarchical' => true,
				'label' => 'Offer category',
				'query_var' => false,
				'rewrite' => array('slug' => 'offer-category', 'with_front' => false),
				'show_in_rest' => true,
				'show_admin_column' => true,
				'capabilities'      => array(
					'assign_terms' => 'edit_posts',
					'edit_terms'   => 'god',
					'manage_terms' => 'god',
				)
			)
		);
		$offerTerms = array('Household', 'Financial', 'Food', 'Technology', 'Cars', 'Style', 'Travel', 'Services');
		foreach($offerTerms as $term) {
			if(!term_exists($term, 'offer-category')){
				wp_insert_term($term, 'offer-category');
			}
		}

		// Out taxonomy
		register_taxonomy(
			'out-category',
			'out',
			array(
				'hierarchical' => true,
				'label' => 'Out category',
				'query_var' => false,
				'rewrite' => array('slug' => 'out-category', 'with_front' => false),
				'show_in_rest' => true,
				'show_admin_column' => true,
				'capabilities'      => array(
					'assign_terms' => 'edit_posts',
					'edit_terms'   => 'god',
					'manage_terms' => 'god',
				)
			)
		);
		$outTerms = array('Party', 'Sport', 'Business', 'Educational', 'Careers', 'Wellness');
		foreach($outTerms as $term) {
			if(!term_exists($term, 'out-category')){
				wp_insert_term($term, 'out-category');
			}
		}
	}
	add_action('init', 'build_taxonomies', 0);

	// Função de related posts / videos / offers / outs
	function get_related_posts($post_id, $related_count, $args = array()){
		$args = wp_parse_args((array) $args, array(
			'orderby' => 'rand',
			'return'  => 'query',
		));

		$related_args = array(
			'post_type'=> get_post_type($post_id),
			'posts_per_page' => $related_count,
			'post_status' => 'publish',
			'post__not_in' => array( $post_id ),
			'orderby' => $args['orderby'],
			'tax_query' => array()
		);

		$post = get_post($post_id);
		$taxonomies = get_object_taxonomies($post, 'names');
		foreach ($taxonomies as $taxonomy){
			$terms = get_the_terms($post_id, $taxonomy);
			if (empty($terms)) continue;
			$term_list = wp_list_pluck($terms, 'slug');
			$related_args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field' => 'slug',
				'terms' => $term_list
			);
		}

		if(count($related_args['tax_query']) > 1){
			$related_args['tax_query']['relation'] = 'OR';
		}

		if($args['return'] == 'query'){
			return new WP_Query($related_args);
		} else {
			return $related_args;
		}
	}

	// Função de related out
	function get_related_posts_out($post_id, $related_count, $args = array()){
		$args = wp_parse_args((array) $args, array(
			'orderby' => 'rand',
			'return'  => 'query',
		));

		$related_args = array(
			'post_type'=> get_post_type($post_id),
			'posts_per_page' => $related_count,
			'post_status' => 'publish',
			'post__not_in' => array( $post_id ),

			//'orderby' => $args['orderby'],
			'tax_query' => array(),
			'meta_key' => 'out_date', 
			'order' => 'ASC',
			'orderby' => 'meta_value',
			'meta_query' => array('relation' => 'AND', array('key' => 'out_date', 'value' => date('Y-m-d'), 'compare' => '>='))
		);

		$post = get_post($post_id);
		$taxonomies = 'out-category';
		$terms = get_the_terms($post_id, $taxonomies);
		$term_list = wp_list_pluck($terms, 'slug');
		$related_args['tax_query'][] = array(
			'taxonomy' => $taxonomies,
			'field' => 'slug',
			'terms' => $term_list
		);

		if($args['return'] == 'query'){
			 $query = new WP_Query($related_args);
			 return $query;
		} else {
			return $related_args;
		}
	}

	// Redirecionamentos
	function custom_redirects() {
		if(is_post_type_archive('video')){
			wp_redirect(home_url('/play'));
			exit;
		}

		if(is_post_type_archive('offer')){
			wp_redirect(home_url('/offers'));
			exit;
		}

		if(is_post_type_archive('job')){
			wp_redirect(home_url('/jobs'));
			exit;
		}

		if(is_post_type_archive('app-post')){
			wp_redirect(home_url());
			exit;
		}

		if(is_category('news')){
			wp_redirect(home_url('/news'));
			exit;
		}

		if(is_category('hot')){
			wp_redirect(home_url('/hot'));
			exit;
		}

		if(is_category('life')){
			wp_redirect(home_url('/life'));
			exit;
		}

		if(is_singular('poll')){
			wp_redirect(home_url());
			exit;
		}

		if(is_tax('offer-category')){
			wp_redirect(home_url('/offers'));
			exit;
		}

		if(is_tax('out-category')){
			wp_redirect(home_url('/out'));
			exit;
		}

		if(is_tax('location')){
			wp_redirect(home_url());
			exit;
		}

		if(is_post_type_archive('push-notification')){
			wp_redirect(home_url());
			exit;
		}

		if(is_singular('push-notification')){
			wp_redirect(home_url());
			exit;
		}

		if(is_user_logged_in()){
			if(is_page('register')){
				wp_redirect(home_url('/user'));
				exit;
			}

			if(is_page('login')){
				wp_redirect(home_url('/user'));
				exit;
			}
		} else {
			if(is_page('user')){
				wp_redirect(home_url('/login'));
				exit;
			}

			if(is_page('suggest-content')){
				$home_url = get_bloginfo('url');
				wp_redirect(home_url('/login?redirect_to='. $home_url .'/suggest-content/'));
				exit;
			}
		}

		// if search is empty
		if(isset($_GET['s']) && $_GET['s'] == ''){
			wp_redirect(home_url());
			exit;
		}
	}
	add_action('template_redirect', 'custom_redirects');

	// Load more posts
	function load_more_scripts(){
		wp_register_script('loadmore', get_template_directory_uri() . '/functions/load-more/load-more.js', array('jquery'));
		wp_localize_script('loadmore', 'loadmore_params', array('ajaxurl' => site_url() . '/wp-admin/admin-ajax.php'));
		wp_enqueue_script('loadmore');
	}
	add_action('wp_enqueue_scripts', 'load_more_scripts');
	require_once('functions/load-more/load-more.php');

	// Loop functions
	require_once('functions/loop-content/loop-content.php');

	// Filter search results
	function filter_results_scripts(){
		wp_register_script('filter_results', get_template_directory_uri() . '/functions/filter-results/filter-results.js', array('jquery'));
		wp_localize_script('filter_results', 'filter_results_params', array('ajaxurl' => site_url() . '/wp-admin/admin-ajax.php'));
		wp_enqueue_script('filter_results');
	}
	add_action('wp_enqueue_scripts', 'filter_results_scripts');
	require_once('functions/filter-results/filter-results.php');

	// Ultimate member
	require_once('functions/ultimate-member/ultimate-member.php');

	// Favorites
	require_once('functions/favorites/favorites.php');

	// Trending posts
	require_once('functions/trending-posts/trending-posts.php');

	// App API functions
	require_once('api-rest/api-functions.php');

	// Top posts
	require_once('functions/top-posts/top-posts.php');

	// Pra resolver esse erro local, pc da cappen: Notice: ob_end_flush(): failed to send buffer of zlib output compression (1) in ...\wp\wp-includes\functions.php on line 4344
	remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );

	// Deletar as redeemed offers do perfil do usuário
	/*global $wpdb;
	$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}usermeta WHERE meta_key = 'redeemedoffers'" , OBJECT);

	$id = $results[0]->umeta_id;

	$table_name = $wpdb->prefix . 'usermeta';
	$wpdb->update($table_name,
		array('meta_value' => ''),
		array('umeta_id' => $id)
	);*/

	function get_country(){
		$ipAddress = $_SERVER['REMOTE_ADDR'];
		require_once 'IP2Location.php';

		$db = new \IP2Location\Database(get_template_directory().'/databases/IP2LOCATION-LITE-DB1.BIN', \IP2Location\Database::FILE_IO);

		$records = $db->lookup($ipAddress, \IP2Location\Database::ALL);
		switch ($records['countryCode']) {
			case 'JM': $varCountry = "_jamaica" ; break;
			case 'TC': $varCountry = "_turksandcaicos" ; break;
			case 'LC': $varCountry = "_stlucia" ; break;
			//new countries to be added
			case 'BB': $varCountry = "_barbados" ; break;
			case 'BS': $varCountry = "_bahamas" ; break;
			case 'KY': $varCountry = "_cayman" ; break;
			case 'TT': $varCountry = "_trinidad" ; break;

			default: $varCountry = ""; break;
		}

		if(!isset($_COOKIE['site-country'])) {
			if(isset($varCountry)) {
				setcookie('site-country', $varCountry, time()+60*60*24*365, '/');
				$_COOKIE['site-country'] = $varCountry;
			} 
		} else {
			$varCountry = $_COOKIE['site-country'];
		}
		return $varCountry;

	}
?>
