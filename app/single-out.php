<?php $webview = isset($_GET['webview']) ? $_GET['webview'] : '';
if(!$webview || $webview != 'true') :
// single normal, desktop e mobile ?>
	<?php get_header(); ?>

	<section class="l-page-list">
		<div class="c-list-header">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-6">
						<h1 class="c-list-header__title">Out</h1>
					</div>
				</div>
			</div>
		</div>
		<?php if(have_posts()) : while (have_posts()) : the_post();

			$out_country = esc_html(get_post_meta($post->ID, 'out_country', true));
			$out_state = esc_html(get_post_meta($post->ID, 'out_state', true));
			$out_date = esc_html(get_post_meta($post->ID, 'out_date', true));
			$new_out_date = date('l, M d, Y', strtotime($out_date));
			$out_time = esc_html(get_post_meta($post->ID, 'out_time', true));
			$out_description = esc_html(get_post_meta($post->ID, 'out_description', true));
			$out_address = esc_html(get_post_meta($post->ID, 'out_address', true));
			$out_mapurl = esc_html(get_post_meta($post->ID, 'out_mapurl', true));
			$out_category = wp_get_post_terms($post->ID, 'out-category', array('fields' => 'all'));

			if($out_description == ''){
				$out_description = get_the_excerpt();
				$out_description = substr($out_description, 0, 130) . '...';
			} ?>
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<?php
							$gallery = get_post_meta($post->ID, 'post_gallery_id', true);
							if($gallery){ ?>
								<div class="c-carousel-default owl-carousel js-carousel-default">
									<?php foreach ($gallery as $image) {
										$imageURL = wp_get_attachment_url($image);
										echo '<img src="'. $imageURL .'">';
									} ?>
								</div>
							<?php } elseif(has_post_thumbnail()){
								$imageURL = get_the_post_thumbnail_url(); ?>
								<div class="c-carousel-default">
									<img src="<?php echo $imageURL; ?>">
								</div>
							<?php }
						?>
					</div>
					<div class="col-md-6">
						<div class="c-box-info">
							<div class="c-box-info__content">
								<p class="c-card-post__tag"><?php echo $out_category[0]->name; ?></p>
								<h2 class="c-box-info__title"><?php the_title(); ?></h2>
								<p class="c-box-info__location"><?php if($out_state) echo $out_state . ', '; echo $out_country; ?></p>
								<p class="c-box-info__description"><?php echo $out_description; ?></p>

								<div class="row">
									<div class="col-md-6">
										<div class="c-box-info__meta c-box-info__meta--time">
											<p class="c-box-info__meta-title">Date and time</p>
											<p class="c-box-info__meta-description">
												<?php echo $new_out_date; ?>
												<br>
												<?php echo $out_time; ?>
											</p>
										</div>
									</div>
									<div class="col-md-6">
										<div class="c-box-info__meta c-box-info__meta--location">
											<p class="c-box-info__meta-title">Location</p>
											<p class="c-box-info__meta-description">
												<?php echo $out_address; ?>
												<br>
												<?php if($out_mapurl) : ?>
													<a href="<?php echo $out_mapurl; ?>" target="_blank">View Map</a>
												<?php endif; ?>
											</p>
										</div>
									</div>
								</div>

								<div class="c-social c-social--vertical d-none d-md-flex">
									<div class="c-social__book" aria-label="Bookmark"><?php the_favorites_button($post->ID, get_current_blog_id()); ?></div>
									<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="c-social__icon c-social__icon--fb-outline" aria-label="Facebook"></a>
									<a href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank" class="c-social__icon c-social__icon--tw-outline" aria-label="Twitter"></a>
								</div>

								<div class="c-social d-flex d-md-none">
									<div class="c-social__book" aria-label="Bookmark"><?php the_favorites_button($post->ID, get_current_blog_id()); ?></div>
									<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="c-social__icon c-social__icon--fb-outline" aria-label="Facebook"></a>
									<a href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank" class="c-social__icon c-social__icon--tw-outline" aria-label="Twitter"></a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="c-post-content my-4 my-md-5">
					<?php the_content(); ?>
				</div>
			</div>

		<?php endwhile;
			$related = get_related_posts_out(get_the_ID(), 4);
			if($related->have_posts()): ?>
				<div class="c-list-cards">
					<div class="container">
						<h2 class="c-list-cards__title">Related outs</h2>

						<div class="c-list-cards__grid js-posts-list">
							<div class="row">
								<?php while($related->have_posts()) : $related->the_post();
									loop_outlisting($post->ID);
								endwhile; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endif; wp_reset_query(); wp_reset_postdata(); //related ?>
		<?php endif; ?>
	</section>

	<?php get_footer(); ?>

<?php elseif($webview && $webview == 'true') :
	// single apenas do app
	include_once('inc/app/header.php'); ?>

	<div class="container webview mt-3">
		<?php if (have_posts()) : while (have_posts()) : the_post();
			$out_country = esc_html(get_post_meta($post->ID, 'out_country', true));
			$out_state = esc_html(get_post_meta($post->ID, 'out_state', true));
			$out_date = esc_html(get_post_meta($post->ID, 'out_date', true));
			$new_out_date = date('l, M d, Y', strtotime($out_date));
			$out_time = esc_html(get_post_meta($post->ID, 'out_time', true));
			$out_description = esc_html(get_post_meta($post->ID, 'out_description', true));
			$out_address = esc_html(get_post_meta($post->ID, 'out_address', true));
			$out_mapurl = esc_html(get_post_meta($post->ID, 'out_mapurl', true));
			$out_category = wp_get_post_terms($post->ID, 'out-category', array('fields' => 'all'));

			if($out_description == ''){
				$out_description = get_the_excerpt();
				$out_description = substr($out_description, 0, 130) . '...';
			} ?>
			
			<div class="row">
				<div class="col-md-6">
					<?php
						$gallery = get_post_meta($post->ID, 'post_gallery_id', true);
						if($gallery){ ?>
							<div class="c-carousel-default owl-carousel js-carousel-default">
								<?php foreach ($gallery as $image) {
									$imageURL = wp_get_attachment_url($image);
									echo '<img src="'. $imageURL .'">';
								} ?>
							</div>
						<?php } elseif(has_post_thumbnail()){
							$imageURL = get_the_post_thumbnail_url(); ?>
							<div class="c-carousel-default">
								<img src="<?php echo $imageURL; ?>">
							</div>
						<?php }
					?>
				</div>
				<div class="col-md-6">
					<div class="c-box-info">
						<div class="c-box-info__content">
							<p class="c-card-post__tag"><?php echo $out_category[0]->name; ?></p>
							<h2 class="c-box-info__title"><?php the_title(); ?></h2>
							<p class="c-box-info__location"><?php if($out_state) echo $out_state . ', '; echo $out_country; ?></p>
							<p class="c-box-info__description"><?php echo $out_description; ?></p>

							<div class="row">
								<div class="col-md-6">
									<div class="c-box-info__meta c-box-info__meta--time">
										<p class="c-box-info__meta-title">Date and time</p>
										<p class="c-box-info__meta-description">
											<?php echo $new_out_date; ?>
											<br>
											<?php echo $out_time; ?>
										</p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="c-box-info__meta c-box-info__meta--location">
										<p class="c-box-info__meta-title">Location</p>
										<p class="c-box-info__meta-description">
											<?php echo $out_address; ?>
											<br>
											<?php if($out_mapurl) : ?>
												<a href="<?php echo $out_mapurl; ?>" target="_blank">View Map</a>
											<?php endif; ?>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<section class="c-post-content my-4">
				<?php the_content(); ?>
			</section>

		<?php endwhile; endif; ?>
	</div>

	<?php include_once('inc/app/footer.php');
endif; ?>