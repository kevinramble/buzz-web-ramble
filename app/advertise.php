<?php
    /* Template Name: Advertise with us */
    get_header(); ?>

<section class="l-page-advertising">
    <div class="c-contact-intro">
        <h1 class="c-contact-intro__title">Advertise with us</h1>
        <h2 class="c-contact-intro__subtitle">We collaborate with brands in innovative and exciting ways. To  advertise with BUZZ, or to enquire as to how we can help spread your word, send us some details below and our team will be in touch.</h2>
    </div>
    <form action="?" class="c-contact-form js-advertise-form" data-url="<?php bloginfo('template_directory'); ?>" method="post">
        <?php $securitynumber1 = ord(mt_rand(0,9)); $securitynumber2 = ord(mt_rand(0,9)); ?>

        <div class="row">
            <div class="col-md-6">
                <input type="text" class="c-contact-form__input" placeholder="First name" id="adfirstname" name="adfirstname">
            </div>
            <div class="col-md-6">
                <input type="text" class="c-contact-form__input" placeholder="Last name" id="adlastname" name="adlastname">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <input type="email" class="c-contact-form__input" placeholder="E-mail" id="ademail" name="ademail">
            </div>
            <div class="col-md-6">
                <input type="tel" class="c-contact-form__input" placeholder="Phone" id="adphone" name="adphone">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <input type="text" class="c-contact-form__input" placeholder="Company name" id="adcompany" name="adcompany">
            </div>
            <div class="col-md-6">
                <input type="text" class="c-contact-form__input" placeholder="Your position" id="adposition" name="adposition">
            </div>
        </div>
        <textarea class="c-contact-form__textarea" id="admessage" name="admessage" cols="30" rows="10" placeholder="Message"></textarea>
        
        <label class="security-question">
            <span>Prove your humanity:</span>
            <input class="text" placeholder="<?php echo '&#'.$securitynumber1. ' + &#' .$securitynumber2; ?>" name="securityanswer" id="securityanswer">
        </label><!-- .security-question -->
        <input type="hidden" name="securitynumber1" id="securitynumber1" value="<?php echo $securitynumber1; ?>">
        <input type="hidden" name="securitynumber2" id="securitynumber2" value="<?php echo $securitynumber2; ?>">

        <button class="c-contact-form__btn js-send-button">Request information</button>

        <div class="notice"></div>
    </form>
</section>

<div class="modal ad-request-sent-successful">
	<div class="modal__dialog">
		<button class="modal__close" aria-label="Close"></button>

		<h1 class="c-title-headline">Request sent</h1>

        <div class="c-description-headline mt-4">
            <p>We're buzzing with excitement at your request to collaborate! We'll be in touch as soon as we can.</p>

            <a href="<?php echo get_bloginfo('url'); ?>" class="c-button c-button--negative c-button--full mt-4">Back to homepage</a>
        </div>
	</div>
</div>

<?php get_footer(); ?>