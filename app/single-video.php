<?php $webview = isset($_GET['webview']) ? $_GET['webview'] : '';
if(!$webview || $webview != 'true') :
// single normal, desktop e mobile ?>
	<?php get_header(); ?>

	<section class="l-page-video">
		<div class="container">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="c-video">
				<div class="c-video__tools">
					<a href="#0" onclick="window.history.go(-1); return false;" class="c-video__back">Back</a>
				</div>
		
				<div class="carrousel single-video">
					<?php the_content(); ?>
				</div>

				<div class="c-video__info">
					<small class="c-video__date"><?php echo get_the_date('d.m.Y', $post->ID); ?></small>
					<h1 class="c-video__title"><?php the_title(); ?></h1>
					<hr>
				</div>
		
				<?php endwhile;
		
				$related = get_related_posts(get_the_ID(), 4);
				if($related->have_posts()): ?>
					<div class="c-video__list owl-carousel js-videos-carousel">
						<?php while($related->have_posts()) : $related->the_post();
							if(has_post_thumbnail()){
								$image = get_the_post_thumbnail_url();
							} else {
								$image = catch_that_image();
							} ?>
		
							<div class="c-video__list-item">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="c-video__thumb" style="background-image:url('<?php echo $image; ?>')">
									<div class="c-video__thumb__info">
										<span class="c-card-list__bt is-video"></span>
									</div>
								</a>
							</div>
						<?php endwhile; ?>
					</div>
				<?php
					endif; wp_reset_query(); wp_reset_postdata(); // related ?>
			</div>
			<?php endif; ?>
		</div>
	</section>

	<?php get_footer(); ?>

<?php elseif($webview && $webview == 'true') :
	// single apenas do app
	include_once('inc/app/header.php'); ?>

	<div class="container webview">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="c-video">
			<div class="carrousel single-video">
				<?php the_content(); ?>
			</div>

			<div class="c-video__info">
				<small class="c-video__date"><?php echo get_the_date('d.m.Y', $post->ID); ?></small>
				<h1 class="c-video__title"><?php the_title(); ?></h1>
				<hr>
			</div>
	
			<?php endwhile;
	
			$related = get_related_posts(get_the_ID(), 4);
			if($related->have_posts()): ?>
				<div class="c-video__list owl-carousel js-videos-carousel">
					<?php while($related->have_posts()) : $related->the_post();
						if(has_post_thumbnail()){
							$image = get_the_post_thumbnail_url();
						} else {
							$image = catch_that_image();
						} ?>
	
						<div class="c-video__list-item">
							<a href="<?php the_permalink(); ?>?webview=true" title="<?php the_title(); ?>" class="c-video__thumb" style="background-image:url('<?php echo $image; ?>')">
								<div class="c-video__thumb__info">
									<span class="c-card-list__bt is-video"></span>
								</div>
							</a>
						</div>
					<?php endwhile; ?>
				</div>
			<?php
				endif; wp_reset_query(); wp_reset_postdata(); // related ?>
		</div>
		<?php endif; ?>
	</div>

	<?php include_once('inc/app/footer.php');
endif; ?>