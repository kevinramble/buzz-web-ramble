<?php get_header(); ?>

<section class="l-page-form">
	<div class="container">
		<h1 class="c-title-headline">SIGN IN</h1>
			<?php
			if (have_posts()) : while (have_posts()) : the_post();
					the_content();
			endwhile; endif;
			?>
	</div>
</section>

<?php get_footer(); ?>