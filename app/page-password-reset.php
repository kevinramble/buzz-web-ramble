<?php get_header(); ?>

<section class="l-page-form">
		<div class="container">
			<h1 class="c-title-headline">Reset your password?</h1>
			<?php
				if(is_user_logged_in()){
				wp_redirect(home_url('/user'));
				exit;

				} else {
					if (have_posts()) : while (have_posts()) : the_post();
						the_content();
					endwhile; endif;
				}
			?>
    </div>
</section>

<?php get_footer(); ?>