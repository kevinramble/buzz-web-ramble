<?php get_header(); ?>

	<section class="l-page-error">
		<img src="<?php echo get_template_directory_uri(); ?>/img/layout/error-404.png" alt="404" class="c-404">
		<div class="c-error-intro">
			<h1 class="c-error-intro__title">Oh. That's Strange!</h1>
			<p class="c-error-intro__description">We've been buzzing around but cannot find the page you're looking for. Maybe there was a typo, error or it just lost its' buzz	– Don't worry,  you can still browse the hottest stories on our home page.</p>

			<a href="<?php echo get_bloginfo('url'); ?>" title="Go back home" class="c-error-intro__btn">Back home</a>
		</div>
	</section>

<?php get_footer(); ?>