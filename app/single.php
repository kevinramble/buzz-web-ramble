<?php $webview = isset($_GET['webview']) ? $_GET['webview'] : '';
if(!$webview || $webview != 'true') :
// single normal, desktop e mobile ?>
	<?php get_header(); ?>
		<div class="c-social c-social--square d-md-none">
			<div class="c-social__book"><?php the_favorites_button($post->ID, get_current_blog_id()); ?></div>
			<a class="c-social__icon c-social__icon--fb" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"></a>
			<a class="c-social__icon c-social__icon--tw" href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank"></a>
			<a class="c-social__icon c-social__icon--wa" href="https://api.whatsapp.com/send?text=<?php the_permalink(); ?>" target="_blank" data-action="share/whatsapp/share"> <i class="fa fa-whatsapp"></i></a>
		</div>

		<div class="l-page-post">
			<div class="container">
				<div class="row no-gutters py-4">
					<div class="col-xl-8 col-lg-12">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<div class="l-page-post__top">
							<nav class="c-breadcrumb">
								<?php
									foreach((get_the_category()) as $category){
										if ($category->category_parent == 0){
											$parentcategory = $category->name;
											$catslug = $category->slug;
										}
									}

									$subcategory = wp_get_post_terms($post->ID, 'sub-category', array('fields' => 'all'));
									$subcategoryName = '';
									$subcategoryID = '';

									if($subcategory) {
										$subcategoryID = $subcategory[0]->term_id;
										$subcategoryName = $subcategory[0]->name;
									}

									$country = wp_get_post_terms($post->ID, 'countries', array('fields' => 'all'));
									$countries = count($country);
								?>
								<?php 
						//Remove country from breadcrumbs
						//if($country) :
						//for($i = 0; $i < $countries; $i++){
							//$countryName = $country[$i]->name; ?>
							<!-- <span class="c-breadcrumb__item c-breadcrumb__item--country"><?php echo $countryName; ?></span> -->
						<?php //} ?>
					<?php //endif; 
						 if($parentcategory) : ?>
									<a href="<?php echo get_bloginfo('url'); ?>/<?php echo $catslug; ?>" class="c-breadcrumb__item cat-<?php echo $catslug; ?>"><?php echo $parentcategory; ?></a>
								<?php endif; if($subcategory) : ?>
									<a href="<?php echo get_category_link($subcategoryID) ?>" class="c-breadcrumb__item c-breadcrumb__item--subcategory"><?php echo $subcategoryName; ?></a>
								<?php endif; ?>
							</nav>

							<?php
								$sponsored_img_url = esc_html(get_post_meta($post->ID, 'sponsored_img', true));
								$sponsored_url = esc_html(get_post_meta($post->ID, 'sponsored_url', true));

								if($sponsored_img_url) : ?>
									<div class="c-post-sponsored">
										<?php if($sponsored_url) : ?><a href="<?php echo $sponsored_url; ?>" target="_blank" title="<?php echo $sponsored_url; ?>"><?php endif; ?>
											<img src="<?php echo $sponsored_img_url; ?>" alt="Sponsored article" class="c-post-sponsored__img">
										<?php if($sponsored_url) : ?></a><?php endif; ?>
									</div>
							<?php endif; ?>
						</div>
						<?php include_once('inc/post/post-header.php'); ?>
						<?php include_once('inc/post/post-content.php'); ?>

						<div class="d-md-none mb-3">
							<!-- /21850622224/Buzz_News_Above_All_Stories_Mobile -->
							<div id='div-gpt-ad-1570539349933-0' class="mx-auto" style='width: 320px; height: 50px;'>
							<script>
								googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570539349933-0'); });
							</script>
							</div>
						</div>

						<?php if(comments_open() || get_comments_number()){
							comments_template();
						} ?>
					</div>
				<?php endwhile; endif; ?>

					<aside class="col-xl-3 offset-xl-1">
						<section class="ads mb-4">
							<?php if($catslug === 'news'){
								$adSlotDesktop = '1570479897664-0';
								$adSlotMobile = '';

								if($sponsored_img_url) $adSlotDesktop = '1570479945426-0';

							} else if($catslug === 'hot'){
								$adSlotDesktop = '1570479897664-0';
								$adSlotMobile = '1570538852690-0';

								if($sponsored_img_url) $adSlotDesktop = '1570479945426-0';

							} else if($catslug === 'life'){
								$adSlotDesktop = '1570479897664-0';
								$adSlotMobile = '1570539292327-0';

								if($sponsored_img_url) $adSlotDesktop = '1570479945426-0';
							} ?>

							<div class="c-banner d-none d-lg-block"><!-- DESKTOP -->
								<!-- /21850622224/Buzz_Article_RightSideBar_Desktop -->
								<div id='div-gpt-ad-<?php echo $adSlotDesktop; ?>' style='width: 300px; height: 600px;'>
									<script>
									googletag.cmd.push(function() { googletag.display('div-gpt-ad-<?php echo $adSlotDesktop; ?>'); });
									</script>
								</div>
							</div>

							<div class="c-banner d-block d-lg-none"><!-- MOBILE -->
								<!-- /21850622224/Buzz_CAT_Article_Mobile -->
								<div id='div-gpt-ad-<?php echo $adSlotMobile; ?>' style='width: 320px; height: 250px;'>
									<script>
										googletag.cmd.push(function() { googletag.display('div-gpt-ad-<?php echo $adSlotMobile; ?>'); });
									</script>
								</div>
							</div>
						</section>

						<?php include_once('inc/post/post-sidebar.php'); ?>

						<div class="mt-4">
							<div id="taboola-right-rail-thumbnails"></div>
							<script type="text/javascript">
							window._taboola = window._taboola || [];
							_taboola.push({
								mode: 'thumbnails-rr',
								container: 'taboola-right-rail-thumbnails',
								placement: 'Right Rail Thumbnails',
								target_type: 'mix'
							});
							</script>
						</div>
					</aside>

				</div>
			</div>
		</div>

	<?php get_footer(); ?>

<?php elseif($webview && $webview == 'true') :
	// single apenas do app
	include_once('inc/app/header.php'); ?>

	<div class="container webview">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="l-page-post__top mt-3">
				<nav class="c-breadcrumb">
					<?php
						foreach((get_the_category()) as $category){
							if ($category->category_parent == 0){
								$parentcategory = $category->name;
								$catslug = $category->slug;
							}
						}

						$subcategory = wp_get_post_terms($post->ID, 'sub-category', array('fields' => 'all'));
						$subcategoryName = '';
						$subcategoryID = '';

						if($subcategory) {
							$subcategoryID = $subcategory[0]->term_id;
							$subcategoryName = $subcategory[0]->name;
						}

						$country = wp_get_post_terms($post->ID, 'countries', array('fields' => 'all'));
						$countries = count($country);
					?>
					<?php //if($country) :
						//for($i = 0; $i < $countries; $i++){
							//$countryName = $country[$i]->name; ?>
							<!-- <span class="c-breadcrumb__item c-breadcrumb__item--country"><?php echo $countryName; ?></span> -->
						<?php //} ?>
					<?php //endif; 
					if($parentcategory) : ?>
						<span class="c-breadcrumb__item cat-<?php echo $catslug; ?>"><?php echo $parentcategory; ?></span>
					<?php endif; if($subcategory) : ?>
						<span class="c-breadcrumb__item c-breadcrumb__item--subcategory"><?php echo $subcategoryName; ?></span>
					<?php endif; ?>
				</nav>

				<?php
					$sponsored_img_url = esc_html(get_post_meta($post->ID, 'sponsored_img', true));
					$sponsored_url = esc_html(get_post_meta($post->ID, 'sponsored_url', true));

					if($sponsored_img_url) : ?>
						<div class="c-post-sponsored">
							<?php if($sponsored_url) : ?><a href="<?php echo $sponsored_url; ?>" target="_blank" title="<?php echo $sponsored_url; ?>"><?php endif; ?>
								<img src="<?php echo $sponsored_img_url; ?>" alt="Sponsored article" class="c-post-sponsored__img">
							<?php if($sponsored_url) : ?></a><?php endif; ?>
						</div>
				<?php endif; ?>
			</div>

			<header class="c-post-header">
				<h1><?php the_title(); ?></h1>
				<?php if(has_excerpt()) : ?>
					<h4><?php the_excerpt(); ?></h4>
				<?php endif; ?>

				<div class="c-post-author mt-3 mb-3">
					<div class="c-post-author__img" style="background-image:url('<?php echo get_avatar_url(get_the_author_meta('ID')); ?>')"></div>
					<div class="c-post-author__content">
						<p class="c-post-author__txt">By <strong><?php echo get_the_author_meta('display_name'); ?></strong></p>
						<p class="c-post-author__txt"><?php the_time('M d, Y'); ?></p>
					</div>
				</div>
			</header>

			<section class="c-post-content">
				<?php the_content(); ?>
			</section>

		<?php endwhile; endif; ?>
		<?php include_once('inc/app/post-sidebar.php'); ?>
	</div>

	<?php include_once('inc/app/footer.php');
endif; ?>
