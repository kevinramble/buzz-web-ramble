<?php
    /* Template Name: Categories */
    get_header();

    global $post; $currentslug = $post->post_name; $pageTitle = $post->post_title;
    $get_cat = get_category_by_slug($currentslug); $catID = $get_cat->term_id; ?>

    <!-- <div class="c-list-header mb-0">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 d-none d-lg-block">
					<div class="row justify-content-end">
						<div class="col-4">
							<div class="c-select">
								<select class="js-child-cats">
									<option value="">Sub-categories</option>
									<?php
										/*$terms = get_terms(array('taxonomy' => 'sub-category', 'hide_empty' => 1));
										foreach ($terms as $term) echo '<option value="'. get_category_link($term->term_id) .'?category='.$currentslug.'">'. $term->name .'</option>';*/
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->

    <div class="l-page-category-listing cat-<?php echo $currentslug; ?>">
        <?php
        $areaTitle = $pageTitle;
        include_once('inc/category-listing/hero-category-listing.php');

        $layoutOrder = 'layout_order_' . $currentslug . $country;
        for($b = 1; $b <= 4; $b++){
            $optionValue = $layoutOrder('layout_order_'. $currentslug . $country . $b);

            if($optionValue == 1){
                $block = 'inc/category-listing/trending-now.php';
                $areaTitle = "Trending now";
            } elseif($optionValue == 2){
                $block = 'inc/category-listing/full-card.php';
                $areaTitle = '';
            } elseif($optionValue == 3){
                $block = 'inc/category-listing/must-watch.php';
                $areaTitle = "Must Watch";
            } elseif($optionValue == 4){
                $block = 'inc/category-listing/breaking-news.php';
                $areaTitle = "Breaking News";
            }

            include_once($block);
		} ?>

        <?php
            $areaTitle = "All Stories";
            include_once('inc/category-listing/all-stories.php');
        ?>
    </div>

<?php get_footer(); ?>
