<?php get_header(); ?>

<section class="l-page-list">
	<div class="c-list-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6">
					<h1 class="c-list-header__title"><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
    </div>
    
    <div class="l-page-list__results">
		<div class="container">
			<?php if (have_posts()) : while (have_posts()) : the_post();
				the_content();
			endwhile; endif; ?>
        </div>
	</div>
</section>

<?php get_footer(); ?>