<?php
    /* Template Name: Contact */
    get_header(); ?>

<section class="l-page-contact">
    <div class="c-contact-intro">
        <h1 class="c-contact-intro__title">Contact Us</h1>
        <h2 class="c-contact-intro__subtitle">For all general enquiries or specific contacts, please complete the form below. We will try to connect you with the right team, as quickly as possible.</h2>
    </div>
    <form action="?" class="c-contact-form js-contact-form" data-url="<?php bloginfo('template_directory'); ?>" method="post">
        <?php $securitynumber1 = ord(mt_rand(0,9)); $securitynumber2 = ord(mt_rand(0,9)); ?>
        <div class="row">
            <div class="col-md-6">
                <input type="text" class="c-contact-form__input" placeholder="First name" id="contactfirstname" name="contactfirstname">
            </div>
            <div class="col-md-6">
                <input type="text" class="c-contact-form__input" placeholder="Last name" id="contactlastname" name="contactlastname">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <input type="text" class="c-contact-form__input" placeholder="E-mail" id="contactemail" name="contactemail">
            </div>
            <div class="col-md-6">
                <input type="tel" class="c-contact-form__input" placeholder="Phone" id="contactphone" name="contactphone">
            </div>
        </div>
        <div class="c-contact-form__select">
            <select name="contactreason" id="contactreason">
                <option value="">Why are you contacting?</option>
                <option value="Questions">Questions</option>
                <option value="Suggestions">Suggestions</option>
                <option value="Complaints">Complaints</option>
            </select>
        </div>
        <textarea class="c-contact-form__textarea" id="contactmessage" name="contactmessage" cols="30" rows="10" placeholder="Message"></textarea>

        <label class="security-question">
            <span>Prove your humanity:</span>
            <input class="text" placeholder="<?php echo '&#'.$securitynumber1. ' + &#' .$securitynumber2; ?>" name="securityanswer" id="securityanswer">
        </label><!-- .security-question -->
        <input type="hidden" name="securitynumber1" id="securitynumber1" value="<?php echo $securitynumber1; ?>">
        <input type="hidden" name="securitynumber2" id="securitynumber2" value="<?php echo $securitynumber2; ?>">

        <button class="c-contact-form__btn js-send-button">Send your message</button>

        <div class="notice"></div>
    </form>
</section>

<div class="modal contact-sent-successful">
	<div class="modal__dialog">
		<button class="modal__close" aria-label="Close"></button>

		<h1 class="c-title-headline">Message sent</h1>

        <div class="c-description-headline mt-4">
            <p>We're buzzing with excitement at your request! You'll receive an email shortly.</p>

            <a href="<?php echo get_bloginfo('url'); ?>" class="c-button c-button--negative c-button--full mt-4">Back to homepage</a>
        </div>
	</div>
</div>

<?php get_footer(); ?>