<?php get_header(); ?>

<?php
	$countryTitle = single_cat_title('', false);
	$getCountry = get_term_by('name', $countryTitle, 'countries');
	$countryID = $getCountry->term_id;
	$categoryTitle = '';
	$subCategoryTitle = '';
	$tax_query = '';
	$hasCat = '';
	$hasSubCat = '';

	if(isset($_GET['category']) && $_GET['category']){
		$category = $_GET['category'];
		$categoryTitle = ' / ' . ucfirst($category);
		$hasCat = '&category='.$category;

		$tax_query = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'countries',
				'field'    => 'term_id',
				'terms'    => $countryID,
			),
			array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => $category
			)
		);
	}

	if(isset($_GET['sub-category']) && $_GET['sub-category']){
		$subCategory = $_GET['sub-category'];
		$subCategoryTitle = ' / ' . ucfirst($subCategory);
		$hasSubCat = '&sub-category='.$subCategory;

		$tax_query = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'countries',
				'field'    => 'term_id',
				'terms'    => $countryID,
			),
			array(
				'taxonomy' => 'sub-category',
				'field'    => 'slug',
				'terms'    => $subCategory
			)
		);
	}

	if((isset($_GET['category']) && $_GET['category']) && (isset($_GET['sub-category']) && $_GET['sub-category'])){
		$tax_query = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'countries',
				'field'    => 'term_id',
				'terms'    => $countryID,
			),
			array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => $category
			),
			array(
				'taxonomy' => 'sub-category',
				'field'    => 'slug',
				'terms'    => $subCategory
			)
		);
	}
?>

<section class="l-page-list">
	<div class="c-list-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6">
					<h1 class="c-list-header__title">
						<?php
							echo $countryTitle . $categoryTitle . $subCategoryTitle;
						?>
					</h1>
				</div>

				<div class="col-md-6 d-none d-lg-block">
					<div class="row justify-content-end">
						<div class="col-4">
							<div class="c-select">
								<select class="js-child-cats">
									<option value="<?php echo get_bloginfo('url') . '/countries/?' . $hasCat . $hasSubCat; ?>">All countries</option>
									<?php
										$terms = get_terms(array('taxonomy' => 'countries', 'hide_empty' => 1));
										foreach ($terms as $term){
											$active = ($term->name == $countryTitle) ? 'selected="selected"' : '';
											echo '<option '. $active .' value="'. get_category_link($term->term_id) . '?' . $hasCat . $hasSubCat .'">'. $term->name .'</option>';
										}
									?>
								</select>
							</div>
						</div>

						<div class="col-4">
							<div class="c-select">
								<select class="js-child-cats">
									<option value="<?php echo get_category_link($countryID) .'?'. $hasSubCat; ?>">Categories</option>
									<?php
										$terms = get_terms(array('taxonomy' => 'category', 'hide_empty' => 1));
										foreach ($terms as $term){
											$active = ($term->slug == $category) ? 'selected="selected"' : '';
											echo '<option '. $active .' value="'. get_category_link($countryID) .'?category='. $term->slug . $hasSubCat . '">'. $term->name .'</option>';
										}
									?>
								</select>
							</div>
						</div>

						<div class="col-4">
							<div class="c-select">
								<select class="js-child-cats">
									<option value="<?php echo get_category_link($countryID).'?'. $hasCat; ?>">Sub-categories</option>
									<?php
										$terms = get_terms(array('taxonomy' => 'sub-category', 'hide_empty' => 1));
										foreach ($terms as $term){
											$active = ($term->slug == $subCategory) ? 'selected="selected"' : '';
											echo '<option '. $active .' value="'. get_category_link($countryID) . '?' . $hasCat .'&sub-category='.$term->slug.'">'. $term->name .'</option>';
										}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="l-page-list__results">
		<div class="container">
			<div class="row js-posts-list">
				<?php $args = array_merge(
						$wp_query->query, 
						array('tax_query' => $tax_query, 
								'posts_per_page' => 8, 
								'post_type' => array('post', 'video'), 
								'orderby' => 'date', 
								'order' => 'DESC', 
								'paged' => $paged, 
								'childcat' => 1)); query_posts($args); 
								// print_r($args);
				?>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php loop_genericcard($post->ID, '4'); ?>
					<?php endwhile; ?>
				<?php else: ?>
					<h4 class="title-nocontent">Nothing to show.</h4>
				<?php endif; ?>
			</div>

			<?php // load more ajax
			$wp_query->query_vars['search_orderby_title'] = ''; // necessario pro search
			$load_posts = $wp_query->query_vars; $load_current_page = $wp_query->query_vars['paged']; $load_max_page = $wp_query->max_num_pages;
			loadmore_button($load_posts, $load_current_page, $load_max_page);
			if($wp_query->max_num_pages > 1){ ?>
				<span class="js-loadmore c-bt-load">Load more</span>
			<?php } else { ?>
				<span class="js-loadmore c-bt-load hidden">Load more</span>
			<?php } // end load more ajax
				
			wp_reset_query(); wp_reset_postdata(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>