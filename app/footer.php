	</main>
	<?php include_once('inc/newsletter.php'); ?>
	<footer class="l-footer">
		<div class="container">
			<div class="row no-gutters">
				<div class="col-md-8 flex-column d-flex align-items-center justify-content-center mb-3 d-md-block mb-md-0">
					<div class="l-footer__item">
						<img src="<?php echo get_template_directory_uri(); ?>/img/layout/logo-negative.svg" width="114" alt="Buzz" class="c-logo">
					</div>

					<p class="c-copyright d-none d-md-block">Copyright 2016 BUZZ, a subsidiary of Ramble<br> Media Corporation. All rights reserved.</p>

					<div class="l-footer__item">
						<ul class="l-buttons">
							<li class="l-buttons__item">
								<a href="http://facebook.com/werbuzz" target="_blank" class="c-button c-button--icon c-button--negative c-button--circle"><span class="icon icon-facebook"></span></a>
							</li>
							<li class="l-buttons__item">
								<a href="https://twitter.com/werbuzz" target="_blank" class="c-button c-button--icon c-button--negative c-button--circle"><span class="icon icon-twitter"></span></a>
							</li>
							<li class="l-buttons__item">
								<a href="https://www.instagram.com/buzzcaribbean/" target="_blank" class="c-button c-button--icon c-button--negative c-button--circle"><span class="icon icon-instagram"></span></a>
							</li>
							<li class="l-buttons__item">
								<a href="https://www.youtube.com/channel/UCtSyhurnO8mOyJxXXDZ7t0A" target="_blank" class="c-button c-button--icon c-button--negative c-button--circle"><span class="icon icon-youtube-play"></span></a>
							</li>
						</ul>
					</div>

				</div>
				<?php /*<div class="col-6 col-md-4">
					<div class="l-footer__item">
						<h4 class="c-title-footer">Elsewhere</h4>
					</div>

					<ul class="c-logo-list">
						<li class="c-logo-list__item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/layout/earth2.png" alt="Earth 2.0" width="105">
						</li>
						<li class="c-logo-list__item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/layout/hexa.png" alt="Hexa" width="74">
						</li>
						<li class="c-logo-list__item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/layout/fox-hub.png" alt="Fox Hub" width="105">
						</li>
					</ul>
				</div>*/ ?>
				<div class="col-md-4 flex-column d-flex align-items-center justify-content-center">
					<div class="l-footer__item">
						<h4 class="c-title-footer">Information</h4>
					</div>

					<ul class="c-navbar-footer">
						<li class="c-navbar-footer__item">
							<a href="<?php echo get_bloginfo('url'); ?>/about-us/" class="c-navbar-footer__link">About us</a>
						</li>
						<li class="c-navbar-footer__item">
							<a href="<?php echo get_bloginfo('url'); ?>/contact/" class="c-navbar-footer__link">Contact Us</a>
						</li>
						<li class="c-navbar-footer__item">
							<a href="<?php echo get_bloginfo('url'); ?>/advertise-with-us/" class="c-navbar-footer__link">Advertising</a>
						</li>
						<li class="c-navbar-footer__item">
							<?php if (is_user_logged_in()) { ?>
							<a href="<?php echo get_bloginfo('url'); ?>/suggest-content/" class="c-navbar-footer__link">Contribute to Buzz</a>
							<?php } else { ?>
							<a href="<?php echo get_bloginfo('url'); ?>/login?redirect_to=<?php echo get_bloginfo('url'); ?>/suggest-content/" class="c-navbar-footer__link">Suggest content</a>
							<?php } ?>
						</li>
						<li class="c-navbar-footer__item">
							<a href="<?php echo get_bloginfo('url'); ?>/privacy-policy/" class="c-navbar-footer__link">Privacy Policy</a>
						</li>
						<li class="c-navbar-footer__item">
							<a href="<?php echo get_bloginfo('url'); ?>/terms-and-conditions/" class="c-navbar-footer__link">Terms & Conditions</a>
						</li>
					</ul>
				</div>
				<div class="d-md-none w-100 text-center mt-4 mt-md-0">
					<p class="c-copyright">Copyright 2016 BUZZ, a subsidiary of Ramble<br> Media Corporation. All rights reserved.</p>
				</div>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>


	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/scripts/main.js"></script>
	<script type="text/javascript">
		window._taboola = window._taboola || [];
		_taboola.push({flush: true});
	</script>

	</body>

	</html>
