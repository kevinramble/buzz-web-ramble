<?php
    // HERO LAYOUT - 1 POST
    function hero_layout_turksandcaicos_add_options(){
		add_option('hero_layout_turksandcaicos_version', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'hero_layout_turksandcaicos_add_options');
	
	function hero_layout_turksandcaicos_settings(){
		register_setting('hero_layout_turksandcaicos_options', 'hero_layout_turksandcaicos_version');
	}
	add_action('admin_init', 'hero_layout_turksandcaicos_settings');
	
	function hero_layout_turksandcaicos($arg){
		if ($arg == 'hero_layout_turksandcaicos_version') {return get_option('hero_layout_turksandcaicos_version');}
	};
?>