<?php
    // MAIN BANNER - 5 POSTS
    function home_poll_turksandcaicos_add_options(){
		add_option('home_poll_turksandcaicos_status', '', '', 'yes');
		add_option('home_poll_turksandcaicos_shortcode', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'home_poll_turksandcaicos_add_options');
	
	function home_poll_turksandcaicos_settings(){
		register_setting('home_poll_turksandcaicos_options', 'home_poll_turksandcaicos_status');
		register_setting('home_poll_turksandcaicos_options', 'home_poll_turksandcaicos_shortcode');
	}
	add_action('admin_init', 'home_poll_turksandcaicos_settings');
	
	function home_poll_turksandcaicos($arg){
		if ($arg == 'home_poll_turksandcaicos_status') {return get_option('home_poll_turksandcaicos_status');}
		if ($arg == 'home_poll_turksandcaicos_shortcode') {return get_option('home_poll_turksandcaicos_shortcode');}
	};
?>