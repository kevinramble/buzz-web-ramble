<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'main-banner'; ?>
<div class="nav-tab-wrapper secondary">
    <a href="?page=turksandcaicos_config&section=content&tab=main-banner" class="nav-tab <?php echo $active_tab == 'main-banner' ? 'nav-tab-active' : ''; ?>">Hero section</a>
    <a href="?page=turksandcaicos_config&section=content&tab=hot-now" class="nav-tab <?php echo $active_tab == 'hot-now' ? 'nav-tab-active' : ''; ?>">Block 1</a>
    <a href="?page=turksandcaicos_config&section=content&tab=featured-stories" class="nav-tab <?php echo $active_tab == 'featured-stories' ? 'nav-tab-active' : ''; ?>">Block 2</a>
    <a href="?page=turksandcaicos_config&section=content&tab=editors-choice" class="nav-tab <?php echo $active_tab == 'editors-choice' ? 'nav-tab-active' : ''; ?>">Block 3</a>
    <a href="?page=turksandcaicos_config&section=content&tab=categories-block1" class="nav-tab <?php echo $active_tab == 'categories-block1' || $active_tab == 'categories-block2' || $active_tab == 'categories-block3' || $active_tab == 'categories-block4' ? 'nav-tab-active' : ''; ?>">Block 4</a>
    <a href="?page=turksandcaicos_config&section=content&tab=big-block" class="nav-tab <?php echo $active_tab == 'big-block' ? 'nav-tab-active' : ''; ?>">Block 5</a>
    <a href="?page=turksandcaicos_config&section=content&tab=top-ten" class="nav-tab <?php echo $active_tab == 'top-ten' ? 'nav-tab-active' : ''; ?>">Block 8</a>
</div>