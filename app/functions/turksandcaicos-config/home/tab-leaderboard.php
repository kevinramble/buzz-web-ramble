<div class="content-wrapper post-manager">
    <?php settings_fields('home_leaderboard_turksandcaicos_options'); ?>

    <p style="font-weight:bold; font-size:11px;">Leaderboard ad is currently <?php echo get_option('home_leaderboard_turksandcaicos_status') == '' ? '<span style="color:red;">inactive</span>' : '<span style="color:green;">active</span>'; ?>.</p>

    <div class="poll-slot <?php echo get_option('home_leaderboard_turksandcaicos_status') == '' ? 'empty' : ''; ?>">
        <?php $checked = (get_option('home_leaderboard_turksandcaicos_status')) ? 'checked' : ''; ?>
        <label><input type="checkbox" class="checkbox" value="1" name="home_leaderboard_turksandcaicos_status" <?php echo $checked; ?>> Show Leaderboard ad</label>
    </div><!-- .ad-slot -->

    <input class="button-primary" style="display:block;" type="submit" name="Save" value="Save changes">
</div>