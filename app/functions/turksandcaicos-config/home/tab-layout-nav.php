<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'hero-layout'; ?>
<div class="nav-tab-wrapper secondary">
    <a href="?page=turksandcaicos_config&section=layout&tab=hero-layout" class="nav-tab <?php echo $active_tab == 'hero-layout' ? 'nav-tab-active' : ''; ?>">Hero section layout</a>
	<a href="?page=turksandcaicos_config&section=layout&tab=home-order" class="nav-tab <?php echo $active_tab == 'home-order' ? 'nav-tab-active' : ''; ?>">Home order</a>
</div>