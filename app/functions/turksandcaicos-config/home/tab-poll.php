<div class="content-wrapper post-manager">
    <?php settings_fields('home_poll_turksandcaicos_options'); ?>

    <p style="font-weight:bold; font-size:11px;">Poll currently <?php echo get_option('home_poll_turksandcaicos_status') == '' || get_option('home_poll_turksandcaicos_shortcode') == '' ? '<span style="color:red;">inactive</span>' : '<span style="color:green;">active</span>'; ?>.</p>

    <div class="poll-slot <?php echo get_option('home_poll_turksandcaicos_status') == '' || get_option('home_poll_turksandcaicos_shortcode') == '' ? 'empty' : ''; ?>">
        <?php $checked = (get_option('home_poll_turksandcaicos_status')) ? 'checked' : ''; ?>
        <label><input type="checkbox" class="checkbox" value="1" name="home_poll_turksandcaicos_status" <?php echo $checked; ?>> Show poll</label>
        <label class="shortcode">
            <strong>Poll shortcode</strong> <a href="post-new.php?post_type=poll" target="_blank" class="obs">(Create poll)</a><br>
            <input name="home_poll_turksandcaicos_shortcode" id="home_poll_turksandcaicos_shortcode" value="<?php echo esc_html(get_option('home_poll_turksandcaicos_shortcode')); ?>">
        </label>
    </div><!-- .ad-slot -->

    <input class="button-primary" style="display:block;" type="submit" name="Save" value="Save changes">
</div>