<?php
    // BIG BLOCK - 1 POST
    function big_block_turksandcaicos_add_options(){
		add_option('big_block_turksandcaicos_cat', '', '', 'yes');
		add_option('big_block_turksandcaicos_subcat', '', '', 'yes');
		add_option('big_block_turksandcaicos_type', '', '', 'yes');
		add_option('big_block_turksandcaicos_post_id', '', '', 'yes');
		add_option('big_block_turksandcaicos_post_title', '', '', 'yes');
		add_option('big_block_turksandcaicos_title', '', '', 'yes');
		add_option('big_block_turksandcaicos_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'big_block_turksandcaicos_add_options');
	
	function big_block_turksandcaicos_settings(){
		register_setting('big_block_turksandcaicos_options', 'big_block_turksandcaicos_cat');
		register_setting('big_block_turksandcaicos_options', 'big_block_turksandcaicos_subcat');
		register_setting('big_block_turksandcaicos_options', 'big_block_turksandcaicos_type');
		register_setting('big_block_turksandcaicos_options', 'big_block_turksandcaicos_post_id');
		register_setting('big_block_turksandcaicos_options', 'big_block_turksandcaicos_post_title');
		register_setting('big_block_turksandcaicos_options', 'big_block_turksandcaicos_title');
		register_setting('big_block_turksandcaicos_options', 'big_block_turksandcaicos_img');
	}
	add_action('admin_init', 'big_block_turksandcaicos_settings');
	
	function big_block_turksandcaicos($arg){
		if ($arg == 'big_block_turksandcaicos_cat') {return get_option('big_block_turksandcaicos_cat');}
		if ($arg == 'big_block_turksandcaicos_subcat') {return get_option('big_block_turksandcaicos_subcat');}
		if ($arg == 'big_block_turksandcaicos_type') {return get_option('big_block_turksandcaicos_type');}
		if ($arg == 'big_block_turksandcaicos_post_id') {return get_option('big_block_turksandcaicos_post_id');}
		if ($arg == 'big_block_turksandcaicos_post_title') {return get_option('big_block_turksandcaicos_post_title');}
		if ($arg == 'big_block_turksandcaicos_title') {return get_option('big_block_turksandcaicos_title');}
		if ($arg == 'big_block_turksandcaicos_img') {return get_option('big_block_turksandcaicos_img');}
	};
?>