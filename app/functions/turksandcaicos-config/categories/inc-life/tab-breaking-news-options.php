<?php
    // BREAKING NEWS - 8 POSTS
    function breaking_news_life_turksandcaicos_add_options(){
		for($i = 1; $i <= 8; $i++){
			add_option('breaking_news_life_turksandcaicos_cat'.$i, '', '', 'yes');
			add_option('breaking_news_life_turksandcaicos_subcat'.$i, '', '', 'yes');
			add_option('breaking_news_life_turksandcaicos_type'.$i, '', '', 'yes');
			add_option('breaking_news_life_turksandcaicos_post_id'.$i, '', '', 'yes');
			add_option('breaking_news_life_turksandcaicos_post_title'.$i, '', '', 'yes');
			add_option('breaking_news_life_turksandcaicos_title'.$i, '', '', 'yes');
			add_option('breaking_news_life_turksandcaicos_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'breaking_news_life_turksandcaicos_add_options');
	
	function breaking_news_life_turksandcaicos_settings(){
		for($i = 1; $i <= 8; $i++){
			register_setting('breaking_news_life_turksandcaicos_options', 'breaking_news_life_turksandcaicos_cat'.$i);
			register_setting('breaking_news_life_turksandcaicos_options', 'breaking_news_life_turksandcaicos_subcat'.$i);
			register_setting('breaking_news_life_turksandcaicos_options', 'breaking_news_life_turksandcaicos_type'.$i);
			register_setting('breaking_news_life_turksandcaicos_options', 'breaking_news_life_turksandcaicos_post_id'.$i);
			register_setting('breaking_news_life_turksandcaicos_options', 'breaking_news_life_turksandcaicos_post_title'.$i);
			register_setting('breaking_news_life_turksandcaicos_options', 'breaking_news_life_turksandcaicos_title'.$i);
			register_setting('breaking_news_life_turksandcaicos_options', 'breaking_news_life_turksandcaicos_img'.$i);
		}
	}
	add_action('admin_init', 'breaking_news_life_turksandcaicos_settings');
	
	function breaking_news_life_turksandcaicos($arg){
		for($i = 1; $i <= 8; $i++){
			if ($arg == 'breaking_news_life_turksandcaicos_cat'.$i) {return get_option('breaking_news_life_turksandcaicos_cat'.$i);}
			if ($arg == 'breaking_news_life_turksandcaicos_subcat'.$i) {return get_option('breaking_news_life_turksandcaicos_subcat'.$i);}
			if ($arg == 'breaking_news_life_turksandcaicos_type'.$i) {return get_option('breaking_news_life_turksandcaicos_type'.$i);}
			if ($arg == 'breaking_news_life_turksandcaicos_post_id'.$i) {return get_option('breaking_news_life_turksandcaicos_post_id'.$i);}
			if ($arg == 'breaking_news_life_turksandcaicos_post_title'.$i) {return get_option('breaking_news_life_turksandcaicos_post_title'.$i);}
			if ($arg == 'breaking_news_life_turksandcaicos_title'.$i) {return get_option('breaking_news_life_turksandcaicos_title'.$i);}
			if ($arg == 'breaking_news_life_turksandcaicos_img'.$i) {return get_option('breaking_news_life_turksandcaicos_img'.$i);}
		}
	};
?>