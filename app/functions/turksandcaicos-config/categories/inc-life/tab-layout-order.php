<?php settings_fields('layout_order_life_turksandcaicos_options'); ?>

<div class="content-wrapper layout-order" id="layout_order_life_turksandcaicos">
    <h3>Home order</h3>

    <p>Sit eveniet excepturi illo ad quos ut incidunt sapiente. Dicta vel omnis odio ipsa illum, autem cupiditate, aspernatur, non unde odit distinctio. Dolor sit amet consectetur adipisicing elit, fuga eius placeat itaque. Ut nisi dicta nemo animi doloremque dolorum nesciunt pariatur exercitationem quisquam. Distinctio maxime, quidem illum amet ratione.</p>

    <div class="orders">
        <?php for($i = 1; $i <= 4; $i++){
            $optionValue = get_option('layout_order_life_turksandcaicos'. $i);
            if($optionValue == '') update_option('layout_order_life_turksandcaicos'. $i, $i);
        ?>
            <div class="block" data-position="<?php echo $i; ?>">
                <p>Block <strong><?php echo $optionValue; ?></strong></p>
                <img src="<?php echo get_template_directory_uri() . '/functions/turksandcaicos-config/categories/img/block'. $optionValue.'.jpg"'; ?>>
                <input type="hidden" class="input-order" name="layout_order_life_turksandcaicos<?php echo $i; ?>" value="<?php echo $optionValue; ?>">
            </div>
        <?php } //for ?>
    </div>

    <input class="button-primary" type="submit" name="Save" value="Save">
</div>