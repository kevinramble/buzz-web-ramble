<?php
    // MUST WATCH - 9 POSTS
    function must_watch_life_turksandcaicos_add_options(){
		for($i = 1; $i <= 9; $i++){
			add_option('must_watch_life_turksandcaicos_cat'.$i, '', '', 'yes');
			add_option('must_watch_life_turksandcaicos_subcat'.$i, '', '', 'yes');
			add_option('must_watch_life_turksandcaicos_post_id'.$i, '', '', 'yes');
			add_option('must_watch_life_turksandcaicos_post_title'.$i, '', '', 'yes');
			add_option('must_watch_life_turksandcaicos_title'.$i, '', '', 'yes');
			add_option('must_watch_life_turksandcaicos_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'must_watch_life_turksandcaicos_add_options');
	
	function must_watch_life_turksandcaicos_settings(){
		for($i = 1; $i <= 9; $i++){
			register_setting('must_watch_life_turksandcaicos_options', 'must_watch_life_turksandcaicos_cat'.$i);
			register_setting('must_watch_life_turksandcaicos_options', 'must_watch_life_turksandcaicos_subcat'.$i);
			register_setting('must_watch_life_turksandcaicos_options', 'must_watch_life_turksandcaicos_post_id'.$i);
			register_setting('must_watch_life_turksandcaicos_options', 'must_watch_life_turksandcaicos_post_title'.$i);
			register_setting('must_watch_life_turksandcaicos_options', 'must_watch_life_turksandcaicos_title'.$i);
			register_setting('must_watch_life_turksandcaicos_options', 'must_watch_life_turksandcaicos_img'.$i);
		}
	}
	add_action('admin_init', 'must_watch_life_turksandcaicos_settings');
	
	function must_watch_life_turksandcaicos($arg){
		for($i = 1; $i <= 9; $i++){
			if ($arg == 'must_watch_life_turksandcaicos_cat'.$i) {return get_option('must_watch_life_turksandcaicos_cat'.$i);}
			if ($arg == 'must_watch_life_turksandcaicos_subcat'.$i) {return get_option('must_watch_life_turksandcaicos_subcat'.$i);}
			if ($arg == 'must_watch_life_turksandcaicos_post_id'.$i) {return get_option('must_watch_life_turksandcaicos_post_id'.$i);}
			if ($arg == 'must_watch_life_turksandcaicos_post_title'.$i) {return get_option('must_watch_life_turksandcaicos_post_title'.$i);}
			if ($arg == 'must_watch_life_turksandcaicos_title'.$i) {return get_option('must_watch_life_turksandcaicos_title'.$i);}
			if ($arg == 'must_watch_life_turksandcaicos_img'.$i) {return get_option('must_watch_life_turksandcaicos_img'.$i);}
		}
	};
?>