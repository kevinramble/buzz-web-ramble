<?php
    // BIG BLOCK - 1 POST
    function full_block_hot_turksandcaicos_add_options(){
		add_option('full_block_hot_turksandcaicos_cat', '', '', 'yes');
		add_option('full_block_hot_turksandcaicos_subcat', '', '', 'yes');
		add_option('full_block_hot_turksandcaicos_type', '', '', 'yes');
		add_option('full_block_hot_turksandcaicos_post_id', '', '', 'yes');
		add_option('full_block_hot_turksandcaicos_post_title', '', '', 'yes');
		add_option('full_block_hot_turksandcaicos_title', '', '', 'yes');
		add_option('full_block_hot_turksandcaicos_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'full_block_hot_turksandcaicos_add_options');
	
	function full_block_hot_turksandcaicos_settings(){
		register_setting('full_block_hot_turksandcaicos_options', 'full_block_hot_turksandcaicos_cat');
		register_setting('full_block_hot_turksandcaicos_options', 'full_block_hot_turksandcaicos_subcat');
		register_setting('full_block_hot_turksandcaicos_options', 'full_block_hot_turksandcaicos_type');
		register_setting('full_block_hot_turksandcaicos_options', 'full_block_hot_turksandcaicos_post_id');
		register_setting('full_block_hot_turksandcaicos_options', 'full_block_hot_turksandcaicos_post_title');
		register_setting('full_block_hot_turksandcaicos_options', 'full_block_hot_turksandcaicos_title');
		register_setting('full_block_hot_turksandcaicos_options', 'full_block_hot_turksandcaicos_img');
	}
	add_action('admin_init', 'full_block_hot_turksandcaicos_settings');
	
	function full_block_hot_turksandcaicos($arg){
		if ($arg == 'full_block_hot_turksandcaicos_cat') {return get_option('full_block_hot_turksandcaicos_cat');}
		if ($arg == 'full_block_hot_turksandcaicos_subcat') {return get_option('full_block_hot_turksandcaicos_subcat');}
		if ($arg == 'full_block_hot_turksandcaicos_type') {return get_option('full_block_hot_turksandcaicos_type');}
		if ($arg == 'full_block_hot_turksandcaicos_post_id') {return get_option('full_block_hot_turksandcaicos_post_id');}
		if ($arg == 'full_block_hot_turksandcaicos_post_title') {return get_option('full_block_hot_turksandcaicos_post_title');}
		if ($arg == 'full_block_hot_turksandcaicos_title') {return get_option('full_block_hot_turksandcaicos_title');}
		if ($arg == 'full_block_hot_turksandcaicos_img') {return get_option('full_block_hot_turksandcaicos_img');}
	};
?>