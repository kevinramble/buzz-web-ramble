<?php
    // TRENDING NOW - 9 POSTS
    function trending_now_news_bahamas_add_options(){
		for($i = 1; $i <= 9; $i++){
			add_option('trending_now_news_bahamas_cat'.$i, '', '', 'yes');
			add_option('trending_now_news_bahamas_subcat'.$i, '', '', 'yes');
			add_option('trending_now_news_bahamas_type'.$i, '', '', 'yes');
			add_option('trending_now_news_bahamas_post_id'.$i, '', '', 'yes');
			add_option('trending_now_news_bahamas_post_title'.$i, '', '', 'yes');
			add_option('trending_now_news_bahamas_title'.$i, '', '', 'yes');
			add_option('trending_now_news_bahamas_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'trending_now_news_bahamas_add_options');
	
	function trending_now_news_bahamas_settings(){
		for($i = 1; $i <= 9; $i++){
			register_setting('trending_now_news_bahamas_options', 'trending_now_news_bahamas_cat'.$i);
			register_setting('trending_now_news_bahamas_options', 'trending_now_news_bahamas_subcat'.$i);
			register_setting('trending_now_news_bahamas_options', 'trending_now_news_bahamas_type'.$i);
			register_setting('trending_now_news_bahamas_options', 'trending_now_news_bahamas_post_id'.$i);
			register_setting('trending_now_news_bahamas_options', 'trending_now_news_bahamas_post_title'.$i);
			register_setting('trending_now_news_bahamas_options', 'trending_now_news_bahamas_title'.$i);
			register_setting('trending_now_news_bahamas_options', 'trending_now_news_bahamas_img'.$i);
		}
	}
	add_action('admin_init', 'trending_now_news_bahamas_settings');
	
	function trending_now_news_bahamas($arg){
		for($i = 1; $i <= 9; $i++){
			if ($arg == 'trending_now_news_bahamas_cat'.$i) {return get_option('trending_now_news_bahamas_cat'.$i);}
			if ($arg == 'trending_now_news_bahamas_subcat'.$i) {return get_option('trending_now_news_bahamas_subcat'.$i);}
			if ($arg == 'trending_now_news_bahamas_type'.$i) {return get_option('trending_now_news_bahamas_type'.$i);}
			if ($arg == 'trending_now_news_bahamas_post_id'.$i) {return get_option('trending_now_news_bahamas_post_id'.$i);}
			if ($arg == 'trending_now_news_bahamas_post_title'.$i) {return get_option('trending_now_news_bahamas_post_title'.$i);}
			if ($arg == 'trending_now_news_bahamas_title'.$i) {return get_option('trending_now_news_bahamas_title'.$i);}
			if ($arg == 'trending_now_news_bahamas_img'.$i) {return get_option('trending_now_news_bahamas_img'.$i);}
		}
	};
?>