<?php
    // MUST WATCH - 9 POSTS
    function must_watch_hot_bahamas_add_options(){
		for($i = 1; $i <= 9; $i++){
			add_option('must_watch_hot_bahamas_cat'.$i, '', '', 'yes');
			add_option('must_watch_hot_bahamas_subcat'.$i, '', '', 'yes');
			add_option('must_watch_hot_bahamas_post_id'.$i, '', '', 'yes');
			add_option('must_watch_hot_bahamas_post_title'.$i, '', '', 'yes');
			add_option('must_watch_hot_bahamas_title'.$i, '', '', 'yes');
			add_option('must_watch_hot_bahamas_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'must_watch_hot_bahamas_add_options');
	
	function must_watch_hot_bahamas_settings(){
		for($i = 1; $i <= 9; $i++){
			register_setting('must_watch_hot_bahamas_options', 'must_watch_hot_bahamas_cat'.$i);
			register_setting('must_watch_hot_bahamas_options', 'must_watch_hot_bahamas_subcat'.$i);
			register_setting('must_watch_hot_bahamas_options', 'must_watch_hot_bahamas_post_id'.$i);
			register_setting('must_watch_hot_bahamas_options', 'must_watch_hot_bahamas_post_title'.$i);
			register_setting('must_watch_hot_bahamas_options', 'must_watch_hot_bahamas_title'.$i);
			register_setting('must_watch_hot_bahamas_options', 'must_watch_hot_bahamas_img'.$i);
		}
	}
	add_action('admin_init', 'must_watch_hot_bahamas_settings');
	
	function must_watch_hot_bahamas($arg){
		for($i = 1; $i <= 9; $i++){
			if ($arg == 'must_watch_hot_bahamas_cat'.$i) {return get_option('must_watch_hot_bahamas_cat'.$i);}
			if ($arg == 'must_watch_hot_bahamas_subcat'.$i) {return get_option('must_watch_hot_bahamas_subcat'.$i);}
			if ($arg == 'must_watch_hot_bahamas_post_id'.$i) {return get_option('must_watch_hot_bahamas_post_id'.$i);}
			if ($arg == 'must_watch_hot_bahamas_post_title'.$i) {return get_option('must_watch_hot_bahamas_post_title'.$i);}
			if ($arg == 'must_watch_hot_bahamas_title'.$i) {return get_option('must_watch_hot_bahamas_title'.$i);}
			if ($arg == 'must_watch_hot_bahamas_img'.$i) {return get_option('must_watch_hot_bahamas_img'.$i);}
		}
	};
?>