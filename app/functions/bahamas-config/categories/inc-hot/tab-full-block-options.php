<?php
    // BIG BLOCK - 1 POST
    function full_block_hot_bahamas_add_options(){
		add_option('full_block_hot_bahamas_cat', '', '', 'yes');
		add_option('full_block_hot_bahamas_subcat', '', '', 'yes');
		add_option('full_block_hot_bahamas_type', '', '', 'yes');
		add_option('full_block_hot_bahamas_post_id', '', '', 'yes');
		add_option('full_block_hot_bahamas_post_title', '', '', 'yes');
		add_option('full_block_hot_bahamas_title', '', '', 'yes');
		add_option('full_block_hot_bahamas_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'full_block_hot_bahamas_add_options');
	
	function full_block_hot_bahamas_settings(){
		register_setting('full_block_hot_bahamas_options', 'full_block_hot_bahamas_cat');
		register_setting('full_block_hot_bahamas_options', 'full_block_hot_bahamas_subcat');
		register_setting('full_block_hot_bahamas_options', 'full_block_hot_bahamas_type');
		register_setting('full_block_hot_bahamas_options', 'full_block_hot_bahamas_post_id');
		register_setting('full_block_hot_bahamas_options', 'full_block_hot_bahamas_post_title');
		register_setting('full_block_hot_bahamas_options', 'full_block_hot_bahamas_title');
		register_setting('full_block_hot_bahamas_options', 'full_block_hot_bahamas_img');
	}
	add_action('admin_init', 'full_block_hot_bahamas_settings');
	
	function full_block_hot_bahamas($arg){
		if ($arg == 'full_block_hot_bahamas_cat') {return get_option('full_block_hot_bahamas_cat');}
		if ($arg == 'full_block_hot_bahamas_subcat') {return get_option('full_block_hot_bahamas_subcat');}
		if ($arg == 'full_block_hot_bahamas_type') {return get_option('full_block_hot_bahamas_type');}
		if ($arg == 'full_block_hot_bahamas_post_id') {return get_option('full_block_hot_bahamas_post_id');}
		if ($arg == 'full_block_hot_bahamas_post_title') {return get_option('full_block_hot_bahamas_post_title');}
		if ($arg == 'full_block_hot_bahamas_title') {return get_option('full_block_hot_bahamas_title');}
		if ($arg == 'full_block_hot_bahamas_img') {return get_option('full_block_hot_bahamas_img');}
	};
?>