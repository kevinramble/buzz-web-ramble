<?php
    // LAYOUT OUT ORDER - 4 POSTS
    function layout_order_life_bahamas_add_options(){
		for($i = 1; $i <= 4; $i++){
			add_option('layout_order_life_bahamas'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'layout_order_life_bahamas_add_options');
	
	function layout_order_life_bahamas_settings(){
		for($i = 1; $i <= 4; $i++){
			register_setting('layout_order_life_bahamas_options', 'layout_order_life_bahamas'.$i);
		}
	}
	add_action('admin_init', 'layout_order_life_bahamas_settings');
	
	function layout_order_life_bahamas($arg){
		for($i = 1; $i <= 4; $i++){
			if ($arg == 'layout_order_life_bahamas'.$i) {return get_option('layout_order_life_bahamas'.$i);}
		}
	};
?>