<?php
    // MAIN BANNER - 5 POSTS
    function home_poll_bahamas_add_options(){
		add_option('home_poll_bahamas_status', '', '', 'yes');
		add_option('home_poll_bahamas_shortcode', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'home_poll_bahamas_add_options');
	
	function home_poll_bahamas_settings(){
		register_setting('home_poll_bahamas_options', 'home_poll_bahamas_status');
		register_setting('home_poll_bahamas_options', 'home_poll_bahamas_shortcode');
	}
	add_action('admin_init', 'home_poll_bahamas_settings');
	
	function home_poll_bahamas($arg){
		if ($arg == 'home_poll_bahamas_status') {return get_option('home_poll_bahamas_status');}
		if ($arg == 'home_poll_bahamas_shortcode') {return get_option('home_poll_bahamas_shortcode');}
	};
?>