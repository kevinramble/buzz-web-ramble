<?php
    // HOT NOW - 6 POSTS
    function hot_now_bahamas_add_options(){
		for($i = 1; $i <= 6; $i++){
			add_option('hot_now_bahamas_cat'.$i, '', '', 'yes');
			add_option('hot_now_bahamas_subcat'.$i, '', '', 'yes');
			add_option('hot_now_bahamas_type'.$i, '', '', 'yes');
			add_option('hot_now_bahamas_post_id'.$i, '', '', 'yes');
			add_option('hot_now_bahamas_post_title'.$i, '', '', 'yes');
			add_option('hot_now_bahamas_title'.$i, '', '', 'yes');
			add_option('hot_now_bahamas_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'hot_now_bahamas_add_options');
	
	function hot_now_bahamas_settings(){
		for($i = 1; $i <= 6; $i++){
			register_setting('hot_now_bahamas_options', 'hot_now_bahamas_cat'.$i);
			register_setting('hot_now_bahamas_options', 'hot_now_bahamas_subcat'.$i);
			register_setting('hot_now_bahamas_options', 'hot_now_bahamas_type'.$i);
			register_setting('hot_now_bahamas_options', 'hot_now_bahamas_post_id'.$i);
			register_setting('hot_now_bahamas_options', 'hot_now_bahamas_post_title'.$i);
			register_setting('hot_now_bahamas_options', 'hot_now_bahamas_title'.$i);
			register_setting('hot_now_bahamas_options', 'hot_now_bahamas_img'.$i);
		}
	}
	add_action('admin_init', 'hot_now_bahamas_settings');
	
	function hot_now_bahamas($arg){
		for($i = 1; $i <= 6; $i++){
			if ($arg == 'hot_now_bahamas_cat'.$i) {return get_option('hot_now_bahamas_cat'.$i);}
			if ($arg == 'hot_now_bahamas_subcat'.$i) {return get_option('hot_now_bahamas_subcat'.$i);}
			if ($arg == 'hot_now_bahamas_type'.$i) {return get_option('hot_now_bahamas_type'.$i);}
			if ($arg == 'hot_now_bahamas_post_id'.$i) {return get_option('hot_now_bahamas_post_id'.$i);}
			if ($arg == 'hot_now_bahamas_post_title'.$i) {return get_option('hot_now_bahamas_post_title'.$i);}
			if ($arg == 'hot_now_bahamas_title'.$i) {return get_option('hot_now_bahamas_title'.$i);}
			if ($arg == 'hot_now_bahamas_img'.$i) {return get_option('hot_now_bahamas_img'.$i);}
		}
	};
?>