<?php
    // BIG BLOCK - 1 POST
    function big_block_bahamas_add_options(){
		add_option('big_block_bahamas_cat', '', '', 'yes');
		add_option('big_block_bahamas_subcat', '', '', 'yes');
		add_option('big_block_bahamas_type', '', '', 'yes');
		add_option('big_block_bahamas_post_id', '', '', 'yes');
		add_option('big_block_bahamas_post_title', '', '', 'yes');
		add_option('big_block_bahamas_title', '', '', 'yes');
		add_option('big_block_bahamas_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'big_block_bahamas_add_options');
	
	function big_block_bahamas_settings(){
		register_setting('big_block_bahamas_options', 'big_block_bahamas_cat');
		register_setting('big_block_bahamas_options', 'big_block_bahamas_subcat');
		register_setting('big_block_bahamas_options', 'big_block_bahamas_type');
		register_setting('big_block_bahamas_options', 'big_block_bahamas_post_id');
		register_setting('big_block_bahamas_options', 'big_block_bahamas_post_title');
		register_setting('big_block_bahamas_options', 'big_block_bahamas_title');
		register_setting('big_block_bahamas_options', 'big_block_bahamas_img');
	}
	add_action('admin_init', 'big_block_bahamas_settings');
	
	function big_block_bahamas($arg){
		if ($arg == 'big_block_bahamas_cat') {return get_option('big_block_bahamas_cat');}
		if ($arg == 'big_block_bahamas_subcat') {return get_option('big_block_bahamas_subcat');}
		if ($arg == 'big_block_bahamas_type') {return get_option('big_block_bahamas_type');}
		if ($arg == 'big_block_bahamas_post_id') {return get_option('big_block_bahamas_post_id');}
		if ($arg == 'big_block_bahamas_post_title') {return get_option('big_block_bahamas_post_title');}
		if ($arg == 'big_block_bahamas_title') {return get_option('big_block_bahamas_title');}
		if ($arg == 'big_block_bahamas_img') {return get_option('big_block_bahamas_img');}
	};
?>