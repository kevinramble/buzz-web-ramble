<?php
	// CATEGORIES BLOCK - 3 POSTS EACH, 3 TOTAL, ORDER: HOT, LIFE, NEWS
	
	// HOT
    function categories_block1_bahamas_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block1_bahamas_cat'.$i, '', '', 'yes');
			add_option('categories_block1_bahamas_subcat'.$i, '', '', 'yes');
			add_option('categories_block1_bahamas_type'.$i, '', '', 'yes');
			add_option('categories_block1_bahamas_post_id'.$i, '', '', 'yes');
			add_option('categories_block1_bahamas_post_title'.$i, '', '', 'yes');
			add_option('categories_block1_bahamas_title'.$i, '', '', 'yes');
			add_option('categories_block1_bahamas_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block1_bahamas_add_options');
	
	function categories_block1_bahamas_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block1_bahamas_options', 'categories_block1_bahamas_cat'.$i);
			register_setting('categories_block1_bahamas_options', 'categories_block1_bahamas_subcat'.$i);
			register_setting('categories_block1_bahamas_options', 'categories_block1_bahamas_type'.$i);
			register_setting('categories_block1_bahamas_options', 'categories_block1_bahamas_post_id'.$i);
			register_setting('categories_block1_bahamas_options', 'categories_block1_bahamas_post_title'.$i);
			register_setting('categories_block1_bahamas_options', 'categories_block1_bahamas_title'.$i);
			register_setting('categories_block1_bahamas_options', 'categories_block1_bahamas_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block1_bahamas_settings');
	
	function categories_block1_bahamas($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block1_bahamas_cat'.$i) {return get_option('categories_block1_bahamas_cat'.$i);}
			if ($arg == 'categories_block1_bahamas_subcat'.$i) {return get_option('categories_block1_bahamas_subcat'.$i);}
			if ($arg == 'categories_block1_bahamas_type'.$i) {return get_option('categories_block1_bahamas_type'.$i);}
			if ($arg == 'categories_block1_bahamas_post_id'.$i) {return get_option('categories_block1_bahamas_post_id'.$i);}
			if ($arg == 'categories_block1_bahamas_post_title'.$i) {return get_option('categories_block1_bahamas_post_title'.$i);}
			if ($arg == 'categories_block1_bahamas_title'.$i) {return get_option('categories_block1_bahamas_title'.$i);}
			if ($arg == 'categories_block1_bahamas_img'.$i) {return get_option('categories_block1_bahamas_img'.$i);}
		}
	};

	// LIFE
    function categories_block2_bahamas_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block2_bahamas_cat'.$i, '', '', 'yes');
			add_option('categories_block2_bahamas_subcat'.$i, '', '', 'yes');
			add_option('categories_block2_bahamas_type'.$i, '', '', 'yes');
			add_option('categories_block2_bahamas_post_id'.$i, '', '', 'yes');
			add_option('categories_block2_bahamas_post_title'.$i, '', '', 'yes');
			add_option('categories_block2_bahamas_title'.$i, '', '', 'yes');
			add_option('categories_block2_bahamas_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block2_bahamas_add_options');
	
	function categories_block2_bahamas_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block2_bahamas_options', 'categories_block2_bahamas_cat'.$i);
			register_setting('categories_block2_bahamas_options', 'categories_block2_bahamas_subcat'.$i);
			register_setting('categories_block2_bahamas_options', 'categories_block2_bahamas_type'.$i);
			register_setting('categories_block2_bahamas_options', 'categories_block2_bahamas_post_id'.$i);
			register_setting('categories_block2_bahamas_options', 'categories_block2_bahamas_post_title'.$i);
			register_setting('categories_block2_bahamas_options', 'categories_block2_bahamas_title'.$i);
			register_setting('categories_block2_bahamas_options', 'categories_block2_bahamas_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block2_bahamas_settings');
	
	function categories_block2_bahamas($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block2_bahamas_cat'.$i) {return get_option('categories_block2_bahamas_cat'.$i);}
			if ($arg == 'categories_block2_bahamas_subcat'.$i) {return get_option('categories_block2_bahamas_subcat'.$i);}
			if ($arg == 'categories_block2_bahamas_type'.$i) {return get_option('categories_block2_bahamas_type'.$i);}
			if ($arg == 'categories_block2_bahamas_post_id'.$i) {return get_option('categories_block2_bahamas_post_id'.$i);}
			if ($arg == 'categories_block2_bahamas_post_title'.$i) {return get_option('categories_block2_bahamas_post_title'.$i);}
			if ($arg == 'categories_block2_bahamas_title'.$i) {return get_option('categories_block2_bahamas_title'.$i);}
			if ($arg == 'categories_block2_bahamas_img'.$i) {return get_option('categories_block2_bahamas_img'.$i);}
		}
	};

	// NEWS
    function categories_block3_bahamas_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block3_bahamas_cat'.$i, '', '', 'yes');
			add_option('categories_block3_bahamas_subcat'.$i, '', '', 'yes');
			add_option('categories_block3_bahamas_type'.$i, '', '', 'yes');
			add_option('categories_block3_bahamas_post_id'.$i, '', '', 'yes');
			add_option('categories_block3_bahamas_post_title'.$i, '', '', 'yes');
			add_option('categories_block3_bahamas_title'.$i, '', '', 'yes');
			add_option('categories_block3_bahamas_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block3_bahamas_add_options');
	
	function categories_block3_bahamas_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block3_bahamas_options', 'categories_block3_bahamas_cat'.$i);
			register_setting('categories_block3_bahamas_options', 'categories_block3_bahamas_subcat'.$i);
			register_setting('categories_block3_bahamas_options', 'categories_block3_bahamas_type'.$i);
			register_setting('categories_block3_bahamas_options', 'categories_block3_bahamas_post_id'.$i);
			register_setting('categories_block3_bahamas_options', 'categories_block3_bahamas_post_title'.$i);
			register_setting('categories_block3_bahamas_options', 'categories_block3_bahamas_title'.$i);
			register_setting('categories_block3_bahamas_options', 'categories_block3_bahamas_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block3_bahamas_settings');
	
	function categories_block3_bahamas($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block3_bahamas_cat'.$i) {return get_option('categories_block3_bahamas_cat'.$i);}
			if ($arg == 'categories_block3_bahamas_subcat'.$i) {return get_option('categories_block3_bahamas_subcat'.$i);}
			if ($arg == 'categories_block3_bahamas_type'.$i) {return get_option('categories_block3_bahamas_type'.$i);}
			if ($arg == 'categories_block3_bahamas_post_id'.$i) {return get_option('categories_block3_bahamas_post_id'.$i);}
			if ($arg == 'categories_block3_bahamas_post_title'.$i) {return get_option('categories_block3_bahamas_post_title'.$i);}
			if ($arg == 'categories_block3_bahamas_title'.$i) {return get_option('categories_block3_bahamas_title'.$i);}
			if ($arg == 'categories_block3_bahamas_img'.$i) {return get_option('categories_block3_bahamas_img'.$i);}
		}
	};
?>