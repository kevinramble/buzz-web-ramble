<?php require_once('tab-content-nav.php'); ?>

<?php if($active_tab == 'main-banner'){
    // TAB: MAIN BANNER
    require_once('inc-content/tab-main-banner.php');

} elseif($active_tab == 'hot-now'){
    // TAB: HOT NOW
    require_once('inc-content/tab-hot-now.php');

} elseif($active_tab == 'featured-stories'){
    // TAB: FEATURED STORIES 
    require_once('inc-content/tab-featured-stories.php');

} elseif($active_tab == 'editors-choice'){
    // TAB: EDITOR'S CHOICE 
    require_once('inc-content/tab-editors-choice.php');

} elseif($active_tab == 'categories-block1'){
    // TAB: CATEGORIES BLOCK 
    require_once('inc-content/tab-categories-block1.php');

} elseif($active_tab == 'categories-block2'){
    // TAB: CATEGORIES BLOCK 
    require_once('inc-content/tab-categories-block2.php');

} elseif($active_tab == 'categories-block3'){
    // TAB: CATEGORIES BLOCK 
    require_once('inc-content/tab-categories-block3.php');

} elseif($active_tab == 'categories-block4'){
    // TAB: CATEGORIES BLOCK 
    require_once('inc-content/tab-categories-block4.php');

} elseif($active_tab == 'big-block'){
    // TAB: EDITOR'S CHOICE 
    require_once('inc-content/tab-big-block.php');

} elseif($active_tab == 'ten-content-one'){
    // TAB: TEN CONTENT .1
    require_once('inc-content/tab-ten-content-one.php');

} elseif($active_tab == 'ten-content-two'){
    // TAB: TEN CONTENT .2
    require_once('inc-content/tab-ten-content-two.php');

} elseif($active_tab == 'ten-content-three'){
    // TAB: TEN CONTENT .3
    require_once('inc-content/tab-ten-content-three.php');

} elseif($active_tab == 'top-ten'){
    // TAB: EDITOR'S CHOICE 
    require_once('inc-content/tab-top-ten.php');

} elseif($active_tab == 'documentation'){
    // TAB: DOCUMENTATION
    require_once('inc-content/tab-documentation.php');

} ?>