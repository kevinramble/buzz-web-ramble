<?php
    // EDITOR'S CHOICE - 8 POSTS
    function home_order_bahamas_add_options(){
		for($i = 1; $i <= 8; $i++){
			add_option('home_order_bahamas'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'home_order_bahamas_add_options');
	
	function home_order_bahamas_settings(){
		for($i = 1; $i <= 8; $i++){
			register_setting('home_order_bahamas_options', 'home_order_bahamas'.$i);
		}
	}
	add_action('admin_init', 'home_order_bahamas_settings');
	
	function home_order_bahamas($arg){
		for($i = 1; $i <= 8; $i++){
			if ($arg == 'home_order_bahamas'.$i) {return get_option('home_order_bahamas'.$i);}
		}
	};
?>