<?php
    // BIG BLOCK - 1 POST
    function full_block_news_cayman_add_options(){
		add_option('full_block_news_cayman_cat', '', '', 'yes');
		add_option('full_block_news_cayman_subcat', '', '', 'yes');
		add_option('full_block_news_cayman_type', '', '', 'yes');
		add_option('full_block_news_cayman_post_id', '', '', 'yes');
		add_option('full_block_news_cayman_post_title', '', '', 'yes');
		add_option('full_block_news_cayman_title', '', '', 'yes');
		add_option('full_block_news_cayman_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'full_block_news_cayman_add_options');
	
	function full_block_news_cayman_settings(){
		register_setting('full_block_news_cayman_options', 'full_block_news_cayman_cat');
		register_setting('full_block_news_cayman_options', 'full_block_news_cayman_subcat');
		register_setting('full_block_news_cayman_options', 'full_block_news_cayman_type');
		register_setting('full_block_news_cayman_options', 'full_block_news_cayman_post_id');
		register_setting('full_block_news_cayman_options', 'full_block_news_cayman_post_title');
		register_setting('full_block_news_cayman_options', 'full_block_news_cayman_title');
		register_setting('full_block_news_cayman_options', 'full_block_news_cayman_img');
	}
	add_action('admin_init', 'full_block_news_cayman_settings');
	
	function full_block_news_cayman($arg){
		if ($arg == 'full_block_news_cayman_cat') {return get_option('full_block_news_cayman_cat');}
		if ($arg == 'full_block_news_cayman_subcat') {return get_option('full_block_news_cayman_subcat');}
		if ($arg == 'full_block_news_cayman_type') {return get_option('full_block_news_cayman_type');}
		if ($arg == 'full_block_news_cayman_post_id') {return get_option('full_block_news_cayman_post_id');}
		if ($arg == 'full_block_news_cayman_post_title') {return get_option('full_block_news_cayman_post_title');}
		if ($arg == 'full_block_news_cayman_title') {return get_option('full_block_news_cayman_title');}
		if ($arg == 'full_block_news_cayman_img') {return get_option('full_block_news_cayman_img');}
	};
?>