<?php
    // LAYOUT OUT ORDER - 4 POSTS
    function layout_order_news_cayman_add_options(){
		for($i = 1; $i <= 4; $i++){
			add_option('layout_order_news_cayman'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'layout_order_news_cayman_add_options');
	
	function layout_order_news_cayman_settings(){
		for($i = 1; $i <= 4; $i++){
			register_setting('layout_order_news_cayman_options', 'layout_order_news_cayman'.$i);
		}
	}
	add_action('admin_init', 'layout_order_news_cayman_settings');
	
	function layout_order_news_cayman($arg){
		for($i = 1; $i <= 4; $i++){
			if ($arg == 'layout_order_news_cayman'.$i) {return get_option('layout_order_news_cayman'.$i);}
		}
	};
?>