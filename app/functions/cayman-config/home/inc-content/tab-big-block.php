<?php settings_fields('big_block_cayman_options'); ?>

<div class="content-wrapper post-manager" id="big_block_cayman" data-siteurl="<?php echo get_bloginfo('url'); ?>">
    <h3>Block 5</h3>

    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci dolorum aspernatur iusto error magni quasi.</p>

    <div class="slots">
            <?php $i = ''; $empty = (get_option('big_block_cayman_post_id')) ? '' : 'empty'; ?>
            <div class="slot <?php echo $empty; ?>" data-slot="">
                <h4>Slot</h4>

                <span class="remove-content dashicons dashicons-dismiss" title="Remove slot's content"></span>

                <div class="option">
                    <label>
                        <span class="item-title">Category</span>
                        <select class="cat-select" name="big_block_cayman_cat<?php echo $i; ?>">
                            <option value="" <?php if (get_option('big_block_cayman_cat'. $i) == '') echo 'selected="selected"'; ?>>Choose a category</option>
                            <?php
                                $categories = get_categories(array(
                                    'orderby' => 'name',
                                    'order'   => 'ASC',
                                    'parent' => 0,
                                    'empty' => 0
                                ));
                        
                                foreach($categories as $category){
                                    $selected = (get_option('big_block_cayman_cat'. $i) == $category->term_id) ? 'selected="selected"' : '';
                                    echo '<option value="'. $category->term_id .'"'. $selected .'>'. $category->name .'</option>';
                                }
                            ?>
                        </select>
                    </label>

                    <label style="margin-top:15px;">
                        <span class="item-title">Sub-category</span>
                        <select class="subcat-select" name="big_block_cayman_subcat<?php echo $i; ?>">
                            <option value="" <?php if (get_option('big_block_cayman_subcat'. $i) == '') echo 'selected="selected"'; ?>>Choose a sub-category</option>
                            <?php
                                $terms = get_terms(array('taxonomy' => 'sub-category', 'hide_empty' => 1));
                                foreach ($terms as $term){
                                    $selected = (get_option('big_block_cayman_subcat'. $i) == $term->term_id) ? 'selected="selected"' : '';
                                    echo '<option value="'. $term->term_id .'"'. $selected .'>'. $term->name .'</option>';
                                }
                            ?>
                        </select>
                    </label>

                    <label style="margin-top:15px;">
                        <span class="item-title">Type</span>
                        <select class="type-select" name="big_block_cayman_type<?php echo $i; ?>">
                            <option value="post" <?php if(get_option('big_block_cayman_type'. $i) == 'post') echo 'selected="selected"'; ?>>Articles</option>
                            <option value="video" <?php if(get_option('big_block_cayman_type'. $i) == 'video') echo 'selected="selected"'; ?>>Videos</option>
                        </select>
                    </label>
                </div><!-- .option -->

                <div class="option">
                    <span class="item-title">Post</span>
                    <div class="post-info">
                        <?php
                            $currentPostID = get_option('big_block_cayman_post_id');
                            $currentPostTitle = get_option('big_block_cayman_post_title');

                        if(get_option('big_block_cayman_post_id')){
                            echo '<p class="current-selected"><em>Current selected post:</em>'. $currentPostTitle .' <a href="'. get_permalink($currentPostID) .'" class="dashicons dashicons-external" title="Open post in new tab" target="_blank"></a></p>';
                        } ?>
                        <input type="hidden" class="input-post-title" name="big_block_cayman_post_title" value="<?php echo $currentPostTitle; ?>">
                        <input type="hidden" class="input-post-id" name="big_block_cayman_post_id" value="<?php echo $currentPostID; ?>">
                    </div><!-- .post-info -->
                    <a href="#0" class="post-button button-primary">Choose post</a>
                </div><!-- .option -->

                <?php $checked = (get_option('big_block_cayman_title') || get_option('big_block_cayman_img')) ? 'checked' : ''; ?>
                <div class="optional-settings">
                    <input type="checkbox" id="checkbox" class="checkbox-accordion" <?php echo $checked; ?>>
                    <label for="checkbox">Optional settings</label>

                    <div class="option">
                        <label>
                            <span class="item-title">Title</span>
                            <input size="35" class="custom-title" name="big_block_cayman_title" value="<?php echo get_option('big_block_cayman_title'); ?>">
                        </label>
                    </div><!-- .option -->

                    <div class="option">
                        <span class="item-title">Image</span>
                        <div class="used-image">
                            <?php
                            $img = 'big_block_cayman_img';
                            if(get_option($img)){
                                $content  = '<a href="#0" class="delete-image dashicons dashicons-trash" title="Remove image"></a>';
                                $content .= '<img src="'. get_option($img) .'">';
                                $content .= '<input type="hidden" class="custom-img" name="'.$img.'" value="'. get_option($img) .'">';

                                echo $content;
                            } ?>
                        </div>

                        <?php $customImgText = (get_option($img)) ? 'Change custom image' : 'Add custom image' ; ?>
                        <a href="#0" class="upload-button button-primary"><?php echo $customImgText; ?></a>
                    </div><!-- .option -->
                </div><!-- .optional-settings -->
            </div><!-- .slot -->
    </div><!-- .slots -->

    <input class="button-primary" type="submit" name="Save" value="Save changes">
</div>