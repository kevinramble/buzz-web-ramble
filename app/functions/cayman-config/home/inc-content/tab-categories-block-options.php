<?php
	// CATEGORIES BLOCK - 3 POSTS EACH, 3 TOTAL, ORDER: HOT, LIFE, NEWS
	
	// HOT
    function categories_block1_cayman_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block1_cayman_cat'.$i, '', '', 'yes');
			add_option('categories_block1_cayman_subcat'.$i, '', '', 'yes');
			add_option('categories_block1_cayman_type'.$i, '', '', 'yes');
			add_option('categories_block1_cayman_post_id'.$i, '', '', 'yes');
			add_option('categories_block1_cayman_post_title'.$i, '', '', 'yes');
			add_option('categories_block1_cayman_title'.$i, '', '', 'yes');
			add_option('categories_block1_cayman_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block1_cayman_add_options');
	
	function categories_block1_cayman_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block1_cayman_options', 'categories_block1_cayman_cat'.$i);
			register_setting('categories_block1_cayman_options', 'categories_block1_cayman_subcat'.$i);
			register_setting('categories_block1_cayman_options', 'categories_block1_cayman_type'.$i);
			register_setting('categories_block1_cayman_options', 'categories_block1_cayman_post_id'.$i);
			register_setting('categories_block1_cayman_options', 'categories_block1_cayman_post_title'.$i);
			register_setting('categories_block1_cayman_options', 'categories_block1_cayman_title'.$i);
			register_setting('categories_block1_cayman_options', 'categories_block1_cayman_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block1_cayman_settings');
	
	function categories_block1_cayman($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block1_cayman_cat'.$i) {return get_option('categories_block1_cayman_cat'.$i);}
			if ($arg == 'categories_block1_cayman_subcat'.$i) {return get_option('categories_block1_cayman_subcat'.$i);}
			if ($arg == 'categories_block1_cayman_type'.$i) {return get_option('categories_block1_cayman_type'.$i);}
			if ($arg == 'categories_block1_cayman_post_id'.$i) {return get_option('categories_block1_cayman_post_id'.$i);}
			if ($arg == 'categories_block1_cayman_post_title'.$i) {return get_option('categories_block1_cayman_post_title'.$i);}
			if ($arg == 'categories_block1_cayman_title'.$i) {return get_option('categories_block1_cayman_title'.$i);}
			if ($arg == 'categories_block1_cayman_img'.$i) {return get_option('categories_block1_cayman_img'.$i);}
		}
	};

	// LIFE
    function categories_block2_cayman_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block2_cayman_cat'.$i, '', '', 'yes');
			add_option('categories_block2_cayman_subcat'.$i, '', '', 'yes');
			add_option('categories_block2_cayman_type'.$i, '', '', 'yes');
			add_option('categories_block2_cayman_post_id'.$i, '', '', 'yes');
			add_option('categories_block2_cayman_post_title'.$i, '', '', 'yes');
			add_option('categories_block2_cayman_title'.$i, '', '', 'yes');
			add_option('categories_block2_cayman_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block2_cayman_add_options');
	
	function categories_block2_cayman_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block2_cayman_options', 'categories_block2_cayman_cat'.$i);
			register_setting('categories_block2_cayman_options', 'categories_block2_cayman_subcat'.$i);
			register_setting('categories_block2_cayman_options', 'categories_block2_cayman_type'.$i);
			register_setting('categories_block2_cayman_options', 'categories_block2_cayman_post_id'.$i);
			register_setting('categories_block2_cayman_options', 'categories_block2_cayman_post_title'.$i);
			register_setting('categories_block2_cayman_options', 'categories_block2_cayman_title'.$i);
			register_setting('categories_block2_cayman_options', 'categories_block2_cayman_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block2_cayman_settings');
	
	function categories_block2_cayman($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block2_cayman_cat'.$i) {return get_option('categories_block2_cayman_cat'.$i);}
			if ($arg == 'categories_block2_cayman_subcat'.$i) {return get_option('categories_block2_cayman_subcat'.$i);}
			if ($arg == 'categories_block2_cayman_type'.$i) {return get_option('categories_block2_cayman_type'.$i);}
			if ($arg == 'categories_block2_cayman_post_id'.$i) {return get_option('categories_block2_cayman_post_id'.$i);}
			if ($arg == 'categories_block2_cayman_post_title'.$i) {return get_option('categories_block2_cayman_post_title'.$i);}
			if ($arg == 'categories_block2_cayman_title'.$i) {return get_option('categories_block2_cayman_title'.$i);}
			if ($arg == 'categories_block2_cayman_img'.$i) {return get_option('categories_block2_cayman_img'.$i);}
		}
	};

	// NEWS
    function categories_block3_cayman_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block3_cayman_cat'.$i, '', '', 'yes');
			add_option('categories_block3_cayman_subcat'.$i, '', '', 'yes');
			add_option('categories_block3_cayman_type'.$i, '', '', 'yes');
			add_option('categories_block3_cayman_post_id'.$i, '', '', 'yes');
			add_option('categories_block3_cayman_post_title'.$i, '', '', 'yes');
			add_option('categories_block3_cayman_title'.$i, '', '', 'yes');
			add_option('categories_block3_cayman_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block3_cayman_add_options');
	
	function categories_block3_cayman_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block3_cayman_options', 'categories_block3_cayman_cat'.$i);
			register_setting('categories_block3_cayman_options', 'categories_block3_cayman_subcat'.$i);
			register_setting('categories_block3_cayman_options', 'categories_block3_cayman_type'.$i);
			register_setting('categories_block3_cayman_options', 'categories_block3_cayman_post_id'.$i);
			register_setting('categories_block3_cayman_options', 'categories_block3_cayman_post_title'.$i);
			register_setting('categories_block3_cayman_options', 'categories_block3_cayman_title'.$i);
			register_setting('categories_block3_cayman_options', 'categories_block3_cayman_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block3_cayman_settings');
	
	function categories_block3_cayman($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block3_cayman_cat'.$i) {return get_option('categories_block3_cayman_cat'.$i);}
			if ($arg == 'categories_block3_cayman_subcat'.$i) {return get_option('categories_block3_cayman_subcat'.$i);}
			if ($arg == 'categories_block3_cayman_type'.$i) {return get_option('categories_block3_cayman_type'.$i);}
			if ($arg == 'categories_block3_cayman_post_id'.$i) {return get_option('categories_block3_cayman_post_id'.$i);}
			if ($arg == 'categories_block3_cayman_post_title'.$i) {return get_option('categories_block3_cayman_post_title'.$i);}
			if ($arg == 'categories_block3_cayman_title'.$i) {return get_option('categories_block3_cayman_title'.$i);}
			if ($arg == 'categories_block3_cayman_img'.$i) {return get_option('categories_block3_cayman_img'.$i);}
		}
	};
?>