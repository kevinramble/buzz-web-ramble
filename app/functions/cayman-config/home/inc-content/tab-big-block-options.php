<?php
    // BIG BLOCK - 1 POST
    function big_block_cayman_add_options(){
		add_option('big_block_cayman_cat', '', '', 'yes');
		add_option('big_block_cayman_subcat', '', '', 'yes');
		add_option('big_block_cayman_type', '', '', 'yes');
		add_option('big_block_cayman_post_id', '', '', 'yes');
		add_option('big_block_cayman_post_title', '', '', 'yes');
		add_option('big_block_cayman_title', '', '', 'yes');
		add_option('big_block_cayman_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'big_block_cayman_add_options');
	
	function big_block_cayman_settings(){
		register_setting('big_block_cayman_options', 'big_block_cayman_cat');
		register_setting('big_block_cayman_options', 'big_block_cayman_subcat');
		register_setting('big_block_cayman_options', 'big_block_cayman_type');
		register_setting('big_block_cayman_options', 'big_block_cayman_post_id');
		register_setting('big_block_cayman_options', 'big_block_cayman_post_title');
		register_setting('big_block_cayman_options', 'big_block_cayman_title');
		register_setting('big_block_cayman_options', 'big_block_cayman_img');
	}
	add_action('admin_init', 'big_block_cayman_settings');
	
	function big_block_cayman($arg){
		if ($arg == 'big_block_cayman_cat') {return get_option('big_block_cayman_cat');}
		if ($arg == 'big_block_cayman_subcat') {return get_option('big_block_cayman_subcat');}
		if ($arg == 'big_block_cayman_type') {return get_option('big_block_cayman_type');}
		if ($arg == 'big_block_cayman_post_id') {return get_option('big_block_cayman_post_id');}
		if ($arg == 'big_block_cayman_post_title') {return get_option('big_block_cayman_post_title');}
		if ($arg == 'big_block_cayman_title') {return get_option('big_block_cayman_title');}
		if ($arg == 'big_block_cayman_img') {return get_option('big_block_cayman_img');}
	};
?>