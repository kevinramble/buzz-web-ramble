<?php
    // MAIN BANNER - 5 POSTS
    function home_poll_cayman_add_options(){
		add_option('home_poll_cayman_status', '', '', 'yes');
		add_option('home_poll_cayman_shortcode', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'home_poll_cayman_add_options');
	
	function home_poll_cayman_settings(){
		register_setting('home_poll_cayman_options', 'home_poll_cayman_status');
		register_setting('home_poll_cayman_options', 'home_poll_cayman_shortcode');
	}
	add_action('admin_init', 'home_poll_cayman_settings');
	
	function home_poll_cayman($arg){
		if ($arg == 'home_poll_cayman_status') {return get_option('home_poll_cayman_status');}
		if ($arg == 'home_poll_cayman_shortcode') {return get_option('home_poll_cayman_shortcode');}
	};
?>