<?php
    // MAIN BANNER - 5 POSTS
    function home_leaderboard_cayman_add_options(){
		add_option('home_leaderboard_cayman_status', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'home_leaderboard_cayman_add_options');
	
	function home_leaderboard_cayman_settings(){
		register_setting('home_leaderboard_cayman_options', 'home_leaderboard_cayman_status');
	}
	add_action('admin_init', 'home_leaderboard_cayman_settings');
	
	function home_leaderboard_cayman($arg){
		if ($arg == 'home_leaderboard_cayman_status') {return get_option('home_leaderboard_cayman_status');}
	};
?>