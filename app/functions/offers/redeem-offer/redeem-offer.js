jQuery(document).ready(function($) {
	// OFFERS
	window.redeemOffer = function(redeemedOfferID) {
		data = {
			action: 'redeem_offer',
			offerID: redeemedOfferID
		}

		$('.js-offer-modal-content')
			.addClass('js-loading')
			.attr('style', '')
			.html('')

		redeem_offer_ajax()

		function redeem_offer_ajax() {
			$.ajax({
				url: redeem_offer_params.ajaxurl,
				data: data,
				type: 'POST',
				beforeSend: function(xhr) {},
				success: function(data) {
					if (data) {
						var successMessage = '<div class="c-description-headline c-description-headline--highlight mt-4">'
						successMessage += '<p>Thank you! Find your code below:</p>'
						successMessage += '</div>'
						successMessage += '<div class="c-description-headline c-description-headline--small mt-4">'
						successMessage += '<p>Your code</p>'
						successMessage += '</div>'
						successMessage += '<div class="c-reedem mt-3">'
						successMessage += '<p class="c-reedem__code">' + data + '</p>'
						successMessage += '<a href="' + offerCompanyURL + '" target="_blank">'
						successMessage += '<p class="c-reedem__description">Click here to go to</p>'
						successMessage += '<p class="c-reedem__brand">' + offerCompany + '</p>'
						successMessage += '</a>'
						successMessage += '</div>'

						var offerCodeButton = '<div class="c-card-post__code mt-3">'
						offerCodeButton += '<p class="c-card-post__code-number js-box-copy">' + data + '</p>'
						offerCodeButton += '<button class="c-card-post__code-btn js-toggle-copy">Copy</button>'
						offerCodeButton += '</div>'

						$('.js-offer-modal-content')
							.html(successMessage)
							.removeClass('js-loading')
						$('.js-offer-code-container').html(offerCodeButton)
					}
				}
			})
		}
	} // offers filters
})
