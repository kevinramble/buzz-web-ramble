<?php
    // Offer - post type
	function create_offer(){
		register_post_type('offer',
		array(
			'labels' => array(
				'name' => 'Offers',
						'singular_name' => 'Offer',
						'add_new' => 'Add new',
						'add_new_item' => 'Add new offer',
						'edit_item' => 'Edit offer',
						'new_item' => 'New offer',
						'all_items' => 'All offers',
						'view_item' => 'View offer',
						'search_items' => 'Search offers',
						'not_found' => 'No offer found',
						'not_found_in_trash' => 'No offers found in trash',
						'parent_item_colon' => '',
						'menu_name' => 'Offers'
					),
					'public' => true,
					'menu_client' => 5,
					'menu_position' => 4,
					'menu_icon' => 'dashicons-cart',
					'supports' => array('title', 'thumbnail', 'editor', 'author'),
					'show_in_rest' => true,
					'has_archive' => true,
					'rewrite' => array('slug' => 'offer', 'with_front' => false)
				)
			);
	}
	add_action('init', 'create_offer');

	add_action('admin_init', 'offer_admin');
	function offer_admin() {
		add_meta_box('offer_meta_box',
			'Offer details',
			'display_offer_meta_box',
			'offer', 'normal', 'high'
		);
	}

	function display_offer_meta_box($offer_post){
		$offer_country = esc_html(get_post_meta($offer_post->ID, 'offer_country', true));
		$offer_state = esc_html(get_post_meta($offer_post->ID, 'offer_state', true));
		$offer_expiration = esc_html(get_post_meta($offer_post->ID, 'offer_expiration', true));
		$offer_fullprice = esc_html(get_post_meta($offer_post->ID, 'offer_fullprice', true));
		$offer_discount = esc_html(get_post_meta($offer_post->ID, 'offer_discount', true));
		$offer_price = esc_html(get_post_meta($offer_post->ID, 'offer_price', true));
		$offer_description = esc_html(get_post_meta($offer_post->ID, 'offer_description', true));
		$offer_code = esc_html(get_post_meta($offer_post->ID, 'offer_code', true));
		$offer_shortcode = esc_html(get_post_meta($offer_post->ID, 'offer_shortcode', true));
		$offer_company = esc_html(get_post_meta($offer_post->ID, 'offer_company', true));
		$offer_companyurl = esc_html(get_post_meta($offer_post->ID, 'offer_companyurl', true));
	?>
		<p class="label">
			<strong>Location</strong><br>
			<?php if($offer_country != ''){ ?>
				<span class="set-location">
					<input type="hidden" name="offer_country" value="<?php echo $offer_country; ?>">
					<?php /*<input type="hidden" name="offer_state" value="<?php echo $offer_state; ?>">*/ ?>

					<?php echo $offer_country; /*if($offer_state) echo ' / ' . $offer_state;*/ ?>
					<br>
					<a href="#0" class="js-change-location button-primary">Change location</a>
				</span>
			<?php } ?>
		</p>

		<div class="choose-location <?php if($offer_country != '') echo 'hidden'; ?>">
			<select <?php if($offer_country == '') echo 'name="offer_country"'; ?> class="js-offer-country">
				<option value="">Select a country</option>
				<option value="Anguilla">Anguilla</option>
				<option value="Antigua and Barbuda">Antigua and Barbuda</option>
				<option value="ABC Islands">ABC Islands</option>
				<option value="Barbados">Barbados</option>
				<option value="Bermuda">Bermuda</option>
				<option value="British Virgin Islands">British Virgin Islands</option>
				<option value="Cayman Islands">Cayman Islands</option>
				<option value="Dominica">Dominica</option>
				<option value="FWI">FWI</option>
				<option value="Grenada">Grenada</option>
				<option value="Guyana">Guyana</option>
				<option value="Haiti">Haiti</option>
				<option value="Jamaica">Jamaica</option>
				<option value="Montserrat">Montserrat</option>
				<option value="St. Kitts and Nevis">St. Kitts and Nevis</option>
				<option value="St. Lucia">St. Lucia</option>
				<option value="St. Martin">St. Martin</option>
				<option value="St. Vincent and the Grenadines">St. Vincent and the Grenadines</option>
				<option value="Suriname">Suriname</option>
				<option value="Trinidad and Tobago">Trinidad and Tobago</option>
				<option value="Turks and Caicos">Turks and Caicos</option>
			</select>
			
			<br>

			<?php /*<select if($offer_state == '') echo 'name="offer_state"'; ?> disabled="disabled" class="js-offer-state"></select>*/ ?>
		</div>

        <p class="label"><strong>Expiration date</strong><br>
		<input type="text" class="datepicker" name="offer_expiration" value="<?php echo $offer_expiration; ?>"></p>

		<p class="label"><strong>Full price</strong> <span class="obs">(Before discount, only numbers)</span><br>
		<input type="text" class="small" name="offer_fullprice" value="<?php echo $offer_fullprice; ?>"></p>

		<p class="label"><strong>Discount</strong> <span class="obs">(The percentage, only numbers)</span><br>
		<input type="text" class="small" name="offer_discount" value="<?php echo $offer_discount; ?>"></p>

		<?php if($offer_price) : ?>
			<p>Final price, after discount: <strong>$<?php echo $offer_price; ?></strong></p>
		<?php endif; ?>

		<p class="label"><strong>Brief description</strong> <span class="obs">(Max of XXX characters)</span><br>
		<textarea name="offer_description"><?php echo $offer_description; ?></textarea></p>

		<hr>

		<p class="label"><strong>Offer code</strong><br>
		<input type="text" name="offer_code" value="<?php echo $offer_code; ?>"></p>

		<p class="label"><strong>Company's name</strong><br>
		<input type="text" name="offer_company" value="<?php echo $offer_company; ?>"></p>

		<p class="label"><strong>Company's URL</strong> <span class="obs">(Where the user can apply the offer)</span><br>
		<input type="text" name="offer_companyurl" placeholder="http://" value="<?php echo $offer_companyurl; ?>"></p>

		<p class="label"><strong>Poll shortcode</strong> <a href="post-new.php?post_type=poll" target="_blank" class="obs">(Create poll)</a><br>
		<input type="text" class="poll-shortcode" name="offer_shortcode" value="<?php echo $offer_shortcode; ?>"></p>

		<p class="obs">
			<strong>Obs:</strong><br>
			The thumbnail dimensions are XXX by XXX pixels.
		</p>
	<?php
	}

	function add_offer_fields($offer_post_id, $offer_post){
		if($offer_post->post_type == 'offer'){
			if(isset($_POST['offer_country'])){
				update_post_meta($offer_post_id, 'offer_country', $_POST['offer_country']);
			}
			if(isset($_POST['offer_state'])){
				update_post_meta($offer_post_id, 'offer_state', $_POST['offer_state']);
			}
			if(isset($_POST['offer_expiration'])){
				update_post_meta($offer_post_id, 'offer_expiration', $_POST['offer_expiration']);
			}
			if(isset($_POST['offer_fullprice'])){
				update_post_meta($offer_post_id, 'offer_fullprice', $_POST['offer_fullprice']);
			}
			if(isset($_POST['offer_discount'])){
				update_post_meta($offer_post_id, 'offer_discount', $_POST['offer_discount']);
			}
			if(isset($_POST['offer_fullprice']) && isset($_POST['offer_discount'])){
				$discountDecimal = ($_POST['offer_discount'] / 100);
				$discountAmount = (int)($_POST['offer_fullprice'] * $discountDecimal);
				$offerPrice = (int)($_POST['offer_fullprice'] - $discountAmount);
				update_post_meta($offer_post_id, 'offer_price', $offerPrice);
			}
			if(isset($_POST['offer_description'])){
				update_post_meta($offer_post_id, 'offer_description', $_POST['offer_description']);
			}
			if(isset($_POST['offer_code'])){
				update_post_meta($offer_post_id, 'offer_code', $_POST['offer_code']);
			}
			if(isset($_POST['offer_shortcode'])){
				update_post_meta($offer_post_id, 'offer_shortcode', $_POST['offer_shortcode']);
			}
			if(isset($_POST['offer_company'])){
				update_post_meta($offer_post_id, 'offer_company', $_POST['offer_company']);
			}
			if(isset($_POST['offer_companyurl'])){
				update_post_meta($offer_post_id, 'offer_companyurl', $_POST['offer_companyurl']);
			}
		}
	}
	add_action('save_post', 'add_offer_fields', 10, 2);
    
    function offer_post_enqueue($hook){
        global $post_type;
        if($post_type == 'offer'){
			wp_enqueue_style('offer-post-style', get_template_directory_uri() . '/functions/offers/offer-post.css');
			wp_enqueue_script('offer-config-js', get_template_directory_uri() . '/functions/offers/offer-config.js', array('jquery'));
			wp_enqueue_script('jquery-ui-datepicker');
		}
	}
	add_action('admin_enqueue_scripts', 'offer_post_enqueue');

	// Mostrar custom fields na lista de posts
	function add_offers_columns($columns){
		return array_merge($columns, array(
			'location' => 'Location',
			'company' => 'Company',
			'expiration' => 'Expiration date'
		));
	}
	add_filter('manage_offer_posts_columns', 'add_offers_columns');

 	function offer_custom_column($column, $offer_post_id){
		switch($column){
			case 'location':
				if((get_post_meta($offer_post_id, 'offer_country', true) !== '')){
					echo esc_html(get_post_meta($offer_post_id, 'offer_country', true));

					if((get_post_meta($offer_post_id, 'offer_state', true) !== '')){
						echo ' / ' . esc_html(get_post_meta($offer_post_id, 'offer_state', true));
					}
				} else {
					echo '–';
				}
			break;
			case 'company':
				echo $company = ((get_post_meta($offer_post_id, 'offer_company', true) !== '')) ? esc_html(get_post_meta($offer_post_id, 'offer_company', true)) : '–';
			break;
			case 'expiration':
				$offer_expiration = get_post_meta($offer_post_id, 'offer_expiration', true);
				if($offer_expiration){
					$offer_expiration = new DateTime($offer_expiration);
					echo $offer_expiration = $offer_expiration->format('d/m/Y');
				} else {
					echo '–';
				}
			break;
		}
	}
	add_action('manage_offer_posts_custom_column', 'offer_custom_column', 10, 2);

	// ordenar campos no dashboard
	function offer_sortable_columns($columns){
		return array_merge($columns, array(
			'location' => 'offer_country',
			'company' => 'offer_company',
			'expiration' => 'offer_expiration'
		));
	}
	add_filter('manage_edit-offer_sortable_columns', 'offer_sortable_columns');

	function offer_posts_orderby($query){
		if(!is_admin() || ! $query->is_main_query()){
			return;
		}

		if('offer_country' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'offer_country');
		}

		if('offer_company' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'offer_company');
		}

		if('offer_expiration' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'offer_expiration');
		}
	}
	add_action('pre_get_posts', 'offer_posts_orderby');

	// Tirar Quick edit, porque dá erro com o title salvo automaticamente
	function offer_disable_quick_edit($actions = array(), $post = null){
		// Abort if the post type is not "offer"
		if (!is_post_type_archive('offer')){
			return $actions;
		}
		if(isset($actions['inline hide-if-no-js'])){
			unset($actions['inline hide-if-no-js']);
		}
		return $actions;
	}
	add_filter('post_row_actions', 'offer_disable_quick_edit', 10, 2);

	function redeemOfferAfterVote(){ ?>
		<script>
			if($('.js-offer-code-container').length > 0){
				redeemOffer(offerID);
			}
		</script>
	<?php }
	add_action('totalpoll/actions/after/poll/command/vote', 'redeemOfferAfterVote');
?>