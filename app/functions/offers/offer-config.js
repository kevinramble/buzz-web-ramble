jQuery(document).ready(function($) {
	// Expiration date picker
	$('.datepicker').datepicker({
		minDate: 0,
		dateFormat: 'yy-mm-dd'
	})

	// Change location
	$('.js-change-location').click(function() {
		$('.set-location').remove()
		$('.choose-location').removeClass('hidden')
		$('.js-offer-country').attr('name', 'offer_country')
		//$('.js-offer-state').attr('name', 'offer_state')

		return false
	})

	// Validação do Offer Poll
	var pollInput = $('.poll-shortcode'),
		pollVal = pollInput.val(),
		currentVal

	if (pollVal === '') {
		setTimeout(function() {
			$('.components-button.editor-post-publish-panel__toggle').addClass('disabled-poll')
			$('.components-button.editor-post-publish-button').addClass('disabled-poll')

			pollInput.addClass('error')
		}, 500)
	}

	pollInput.on('change keyup', function() {
		currentVal = $(this).val()

		if (!currentVal) {
			$('.components-button.editor-post-publish-panel__toggle').addClass('disabled-poll')
			$('.components-button.editor-post-publish-button').addClass('disabled-poll')

			pollInput.addClass('error')
		} else {
			$('.components-button.editor-post-publish-panel__toggle').removeClass('disabled-poll')
			$('.components-button.editor-post-publish-button').removeClass('disabled-poll')

			pollInput.removeClass('error')
		}
	})
})

// adaptado de: https://www.sitepoint.com/community/t/country-state-city-dropdown-list/2438
var city_states = Object()

//Caribbean
city_states['Anguilla'] = 'The Valley'
city_states['Antigua / Barbuda'] = 'St. John|Barbuda|Redonda|St. George|St. Mary|St. Paul|St. Peter|St. Philip'
city_states['Aruba'] = 'Oranjestad'
city_states['Bahamas'] =
	"Nassau|Acklins/Crooked Islands|Bimini|Cat Island|Exuma|Freeport|Fresh Creek|Governor's Harbour|Green Turtle Cay|Harbour Island|High Rock|Inagua|Kemps Bay|Long Island|Marsh Harbour|Mayaguana|New Providence|Nichollstown/Berry Islands|Ragged Island|Rock Sound|Sandy Point|San Salvador/Rum Cay"
city_states['Barbados'] = 'Bridgetown|Christ Church|St. Andrew|St. George|St. James|St. John|St. Joseph|St. Lucy|St. Michael|St. Peter|St. Philip|St. Thomas'
city_states['Cuba'] = 'Havana|Camaguey|Ciego de Avila|Cienfuegos|Ciudad de La Habana|Granma|Guantanamo|Holguin|Isla de la Juventud|La Habana|Las Tunas|Matanzas|Pinar del Rio|Sancti Spiritus|Santiago de Cuba|Villa Clara'
city_states['Dominica'] = 'Roseau|St. Andrew|St. David|St. George|St. John|St. Joseph|St. Luke|St. Mark|St. Patrick|St. Paul|St. Peter'
city_states['Dominican Republic'] =
	'Santo Domingo|Azua|Baoruco|Barahona|Dajabon|Distrito Nacional|Duarte|Elias Pina|El Seibo|Espaillat|Hato Mayor|Independencia|La Altagracia|La Romana|La Vega|Maria Trinidad Sanchez|Monsenor Nouel|Monte Cristi|Monte Plata|Pedernales|Peravia|Puerto Plata|Salcedo|Samana|Sanchez Ramirez|San Cristobal|San Juan|San Pedro de Macoris|Santiago|Santiago Rodriguez|Valverde'
city_states['Grenada'] = "St. George's|Carriacou/Petit Martinique|St. Andrew|St. David|St. John|St. Mark|St. Patrick"
city_states['Guadeloupe'] = 'Basse-Terre'
city_states['Haiti'] = "Port-au-Prince|Artibonite|Centre|Grand 'Anse|Nord|Nord-Est|Nord-Ouest|Ouest|Sud|Sud-Est"
city_states['Jamaica'] = 'Kingston|Clarendon|Hanover|Manchester|Portland|St. Andrew|St. Ann|St. Catherine|St. Elizabeth|St. James|St. Mary|St. Thomas|Trelawny|Westmoreland'
city_states['Martinique'] = 'Fort-de-France'
city_states['Montserrat'] = 'Brades Estate|Plymouth|St. Anthony|St. Georges|St. Peter'
city_states['Netherlands Antilles'] = 'Willemstad'
city_states['Puerto Rico'] =
	'San Juan|Adjuntas|Aguada|Aguadilla|Aguas Buenas|Aibonito|Anasco|Arecibo|Arroyo|Barceloneta|Barranquitas|Bayamon|Cabo Rojo|Caguas|Camuy|Canovanas|Carolina|Catano|Cayey|Ceiba|Ciales|Cidra|Coamo|Comerio|Corozal|Culebra|Dorado|Fajardo|Florida|Guanica|Guayama|Guayanilla|Guaynabo|Gurabo|Hatillo|Hormigueros|Humacao|Isabela|Jayuya|Juana Diaz|Juncos|Lajas|Lares|Las Marias|Las Piedras|Loiza|Luquillo|Manati|Maricao|Maunabo|Mayaguez|Moca|Morovis|Naguabo|Naranjito|Orocovis|Patillas|Penuelas|Ponce|Quebradillas|Rincon|Rio Grande|Sabana Grande|Salinas|San German|San Lorenzo|San Sebastian|Santa Isabel|Toa Alta|Toa Baja|Trujillo Alto|Utuado|Vega Alta|Vega Baja|Vieques|Villalba|Yabucoa|Yauco'
city_states['St. Barts'] = ''
city_states['St. Kitts / Nevis'] =
	'Basseterre|Christ Church Nichola Town|St. Anne Sandy Point|St. George Basseterre|St. George Gingerland|St. James Windward|St. John Capesterre|St. John Figtree|St. Mary Cayon|St. Paul Capesterre|St. Paul Charlestown|St. Peter Basseterre|St. Thomas Lowland|St. Thomas Middle Island|Trinity Palmetto Point'
city_states['St. Lucia'] = 'Castries|Anse-la-Raye|Castries|Choiseul|Dauphin|Dennery|Gros-Islet|Laborie|Micoud|Praslin|Soufriere|Vieux-Fort'
city_states['St. Martin / Sint Maarten'] = ''
city_states['St. Vincent / Grenadines'] = 'Kingstown|Charlotte|Grenadines|St. Andrew|St. David|St. George|St. Patrick'
city_states['San Andres'] = ''
city_states['Trinidad and Tobago'] = 'Port-of-Spain|Arima|Caroni|Mayaro|Nariva|St. Andrew|St. David|St. George|St. Patrick|San Fernando|Tobago|Victoria'
city_states['Turks and Caicos'] = 'Grand Turk (Cockburn Town)'

function set_city_state(oCountrySel, oCity_StateSel) {
	var city_stateArr
	oCity_StateSel.length = 0
	var country = oCountrySel.options[oCountrySel.selectedIndex].text
	if (city_states[country]) {
		oCity_StateSel.disabled = false
		oCity_StateSel.options[0] = new Option('Select', '')
		city_stateArr = city_states[country].split('|')

		for (var i = 0; i < city_stateArr.length; i++) {
			oCity_StateSel.options[i + 1] = new Option(city_stateArr[i], city_stateArr[i])
		}
	} else {
		oCity_StateSel.disabled = true
	}
}
