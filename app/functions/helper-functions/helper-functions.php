<?php
	function remainingTime($the_expiration){
		$today = date('m/d/Y');
		$today = new DateTime($today);
		$today = strtotime($today->format('Y-m-d'));

		$expiration = new DateTime($the_expiration);
		$expiration = strtotime($expiration->format('Y-m-d'));

		$datediff = $expiration - $today;

		return round($datediff / (60 * 60 * 24));
	}
	
    function get_redeemedoffers(){
		if(is_user_logged_in()){
			global $wpdb;
			$userID = get_current_user_id();
			$allOffers = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}usermeta WHERE meta_key = 'redeemedoffers' AND user_id = ". $userID , OBJECT);

			$redeemedOffers = $allOffers[0]->meta_value;
			$redeemedOffers = unserialize($redeemedOffers);

			return $redeemedOffers;
		} else {
			return false;
		}
	}

	function get_appliedjobs(){
		if(is_user_logged_in()){
			global $wpdb;
			$userID = get_current_user_id();
			$allJobs = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}usermeta WHERE meta_key = 'appliedjobs' AND user_id = ". $userID , OBJECT);

			$appliedJobs = $allJobs[0]->meta_value;
			$appliedJobs = unserialize($appliedJobs);

			return $appliedJobs;
		} else {
			return false;
		}
	}

	function is_appliedjob($jobID){
		$appliedJobs = get_appliedjobs();
		if(!$appliedJobs) $appliedJobs = array();
		if (!in_array($jobID, $appliedJobs)){
			return false;
		} else {
			return true;
		}
	}

	function homePollCustom(){ ?>
		<script>
			if($('.js-home-poll').length > 0){
				$('.totalpoll-buttons').remove();
			}
		</script>
	<?php }
	add_action('totalpoll/actions/before/poll/command/vote', 'homePollCustom');

	// Limit text chars
	function limit_text($text, $limit, $end = null){
		if (str_word_count($text, 0) > $limit) {
			$words = str_word_count($text, 2);
			$pos = array_keys($words);
			$final_text = trim(substr($text, 0, $pos[$limit])) . $end;
		} else {
			$final_text = $text;
		}
		return $final_text;
	}
?>