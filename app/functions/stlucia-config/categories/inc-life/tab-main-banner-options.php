<?php
    // MAIN VIDEO - 3 POSTS
    function main_banner_life_stlucia_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('main_banner_life_stlucia_cat'.$i, '', '', 'yes');
			add_option('main_banner_life_stlucia_subcat'.$i, '', '', 'yes');
			add_option('main_banner_life_stlucia_type'.$i, '', '', 'yes');
			add_option('main_banner_life_stlucia_post_id'.$i, '', '', 'yes');
			add_option('main_banner_life_stlucia_post_title'.$i, '', '', 'yes');
			add_option('main_banner_life_stlucia_title'.$i, '', '', 'yes');
			add_option('main_banner_life_stlucia_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'main_banner_life_stlucia_add_options');
	
	function main_banner_life_stlucia_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('main_banner_life_stlucia_options', 'main_banner_life_stlucia_cat'.$i);
			register_setting('main_banner_life_stlucia_options', 'main_banner_life_stlucia_subcat'.$i);
			register_setting('main_banner_life_stlucia_options', 'main_banner_life_stlucia_type'.$i);
			register_setting('main_banner_life_stlucia_options', 'main_banner_life_stlucia_post_id'.$i);
			register_setting('main_banner_life_stlucia_options', 'main_banner_life_stlucia_post_title'.$i);
			register_setting('main_banner_life_stlucia_options', 'main_banner_life_stlucia_title'.$i);
			register_setting('main_banner_life_stlucia_options', 'main_banner_life_stlucia_img'.$i);
		}
	}
	add_action('admin_init', 'main_banner_life_stlucia_settings');
	
	function main_banner_life_stlucia($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'main_banner_life_stlucia_cat'.$i) {return get_option('main_banner_life_stlucia_cat'.$i);}
			if ($arg == 'main_banner_life_stlucia_subcat'.$i) {return get_option('main_banner_life_stlucia_subcat'.$i);}
			if ($arg == 'main_banner_life_stlucia_type'.$i) {return get_option('main_banner_life_stlucia_type'.$i);}
			if ($arg == 'main_banner_life_stlucia_post_id'.$i) {return get_option('main_banner_life_stlucia_post_id'.$i);}
			if ($arg == 'main_banner_life_stlucia_post_title'.$i) {return get_option('main_banner_life_stlucia_post_title'.$i);}
			if ($arg == 'main_banner_life_stlucia_title'.$i) {return get_option('main_banner_life_stlucia_title'.$i);}
			if ($arg == 'main_banner_life_stlucia_img'.$i) {return get_option('main_banner_life_stlucia_img'.$i);}
		}
	};
?>