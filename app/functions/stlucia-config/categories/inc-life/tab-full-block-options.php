<?php
    // BIG BLOCK - 1 POST
    function full_block_life_stlucia_add_options(){
		add_option('full_block_life_stlucia_cat', '', '', 'yes');
		add_option('full_block_life_stlucia_subcat', '', '', 'yes');
		add_option('full_block_life_stlucia_type', '', '', 'yes');
		add_option('full_block_life_stlucia_post_id', '', '', 'yes');
		add_option('full_block_life_stlucia_post_title', '', '', 'yes');
		add_option('full_block_life_stlucia_title', '', '', 'yes');
		add_option('full_block_life_stlucia_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'full_block_life_stlucia_add_options');
	
	function full_block_life_stlucia_settings(){
		register_setting('full_block_life_stlucia_options', 'full_block_life_stlucia_cat');
		register_setting('full_block_life_stlucia_options', 'full_block_life_stlucia_subcat');
		register_setting('full_block_life_stlucia_options', 'full_block_life_stlucia_type');
		register_setting('full_block_life_stlucia_options', 'full_block_life_stlucia_post_id');
		register_setting('full_block_life_stlucia_options', 'full_block_life_stlucia_post_title');
		register_setting('full_block_life_stlucia_options', 'full_block_life_stlucia_title');
		register_setting('full_block_life_stlucia_options', 'full_block_life_stlucia_img');
	}
	add_action('admin_init', 'full_block_life_stlucia_settings');
	
	function full_block_life_stlucia($arg){
		if ($arg == 'full_block_life_stlucia_cat') {return get_option('full_block_life_stlucia_cat');}
		if ($arg == 'full_block_life_stlucia_subcat') {return get_option('full_block_life_stlucia_subcat');}
		if ($arg == 'full_block_life_stlucia_type') {return get_option('full_block_life_stlucia_type');}
		if ($arg == 'full_block_life_stlucia_post_id') {return get_option('full_block_life_stlucia_post_id');}
		if ($arg == 'full_block_life_stlucia_post_title') {return get_option('full_block_life_stlucia_post_title');}
		if ($arg == 'full_block_life_stlucia_title') {return get_option('full_block_life_stlucia_title');}
		if ($arg == 'full_block_life_stlucia_img') {return get_option('full_block_life_stlucia_img');}
	};
?>