<?php
    // BREAKING NEWS - 8 POSTS
    function breaking_news_life_stlucia_add_options(){
		for($i = 1; $i <= 8; $i++){
			add_option('breaking_news_life_stlucia_cat'.$i, '', '', 'yes');
			add_option('breaking_news_life_stlucia_subcat'.$i, '', '', 'yes');
			add_option('breaking_news_life_stlucia_type'.$i, '', '', 'yes');
			add_option('breaking_news_life_stlucia_post_id'.$i, '', '', 'yes');
			add_option('breaking_news_life_stlucia_post_title'.$i, '', '', 'yes');
			add_option('breaking_news_life_stlucia_title'.$i, '', '', 'yes');
			add_option('breaking_news_life_stlucia_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'breaking_news_life_stlucia_add_options');
	
	function breaking_news_life_stlucia_settings(){
		for($i = 1; $i <= 8; $i++){
			register_setting('breaking_news_life_stlucia_options', 'breaking_news_life_stlucia_cat'.$i);
			register_setting('breaking_news_life_stlucia_options', 'breaking_news_life_stlucia_subcat'.$i);
			register_setting('breaking_news_life_stlucia_options', 'breaking_news_life_stlucia_type'.$i);
			register_setting('breaking_news_life_stlucia_options', 'breaking_news_life_stlucia_post_id'.$i);
			register_setting('breaking_news_life_stlucia_options', 'breaking_news_life_stlucia_post_title'.$i);
			register_setting('breaking_news_life_stlucia_options', 'breaking_news_life_stlucia_title'.$i);
			register_setting('breaking_news_life_stlucia_options', 'breaking_news_life_stlucia_img'.$i);
		}
	}
	add_action('admin_init', 'breaking_news_life_stlucia_settings');
	
	function breaking_news_life_stlucia($arg){
		for($i = 1; $i <= 8; $i++){
			if ($arg == 'breaking_news_life_stlucia_cat'.$i) {return get_option('breaking_news_life_stlucia_cat'.$i);}
			if ($arg == 'breaking_news_life_stlucia_subcat'.$i) {return get_option('breaking_news_life_stlucia_subcat'.$i);}
			if ($arg == 'breaking_news_life_stlucia_type'.$i) {return get_option('breaking_news_life_stlucia_type'.$i);}
			if ($arg == 'breaking_news_life_stlucia_post_id'.$i) {return get_option('breaking_news_life_stlucia_post_id'.$i);}
			if ($arg == 'breaking_news_life_stlucia_post_title'.$i) {return get_option('breaking_news_life_stlucia_post_title'.$i);}
			if ($arg == 'breaking_news_life_stlucia_title'.$i) {return get_option('breaking_news_life_stlucia_title'.$i);}
			if ($arg == 'breaking_news_life_stlucia_img'.$i) {return get_option('breaking_news_life_stlucia_img'.$i);}
		}
	};
?>