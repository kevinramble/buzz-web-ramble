<?php
    // TRENDING NOW - 9 POSTS
    function trending_now_hot_stlucia_add_options(){
		for($i = 1; $i <= 9; $i++){
			add_option('trending_now_hot_stlucia_cat'.$i, '', '', 'yes');
			add_option('trending_now_hot_stlucia_subcat'.$i, '', '', 'yes');
			add_option('trending_now_hot_stlucia_type'.$i, '', '', 'yes');
			add_option('trending_now_hot_stlucia_post_id'.$i, '', '', 'yes');
			add_option('trending_now_hot_stlucia_post_title'.$i, '', '', 'yes');
			add_option('trending_now_hot_stlucia_title'.$i, '', '', 'yes');
			add_option('trending_now_hot_stlucia_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'trending_now_hot_stlucia_add_options');
	
	function trending_now_hot_stlucia_settings(){
		for($i = 1; $i <= 9; $i++){
			register_setting('trending_now_hot_stlucia_options', 'trending_now_hot_stlucia_cat'.$i);
			register_setting('trending_now_hot_stlucia_options', 'trending_now_hot_stlucia_subcat'.$i);
			register_setting('trending_now_hot_stlucia_options', 'trending_now_hot_stlucia_type'.$i);
			register_setting('trending_now_hot_stlucia_options', 'trending_now_hot_stlucia_post_id'.$i);
			register_setting('trending_now_hot_stlucia_options', 'trending_now_hot_stlucia_post_title'.$i);
			register_setting('trending_now_hot_stlucia_options', 'trending_now_hot_stlucia_title'.$i);
			register_setting('trending_now_hot_stlucia_options', 'trending_now_hot_stlucia_img'.$i);
		}
	}
	add_action('admin_init', 'trending_now_hot_stlucia_settings');
	
	function trending_now_hot_stlucia($arg){
		for($i = 1; $i <= 9; $i++){
			if ($arg == 'trending_now_hot_stlucia_cat'.$i) {return get_option('trending_now_hot_stlucia_cat'.$i);}
			if ($arg == 'trending_now_hot_stlucia_subcat'.$i) {return get_option('trending_now_hot_stlucia_subcat'.$i);}
			if ($arg == 'trending_now_hot_stlucia_type'.$i) {return get_option('trending_now_hot_stlucia_type'.$i);}
			if ($arg == 'trending_now_hot_stlucia_post_id'.$i) {return get_option('trending_now_hot_stlucia_post_id'.$i);}
			if ($arg == 'trending_now_hot_stlucia_post_title'.$i) {return get_option('trending_now_hot_stlucia_post_title'.$i);}
			if ($arg == 'trending_now_hot_stlucia_title'.$i) {return get_option('trending_now_hot_stlucia_title'.$i);}
			if ($arg == 'trending_now_hot_stlucia_img'.$i) {return get_option('trending_now_hot_stlucia_img'.$i);}
		}
	};
?>