<?php
    // BIG BLOCK - 1 POST
    function big_block_stlucia_add_options(){
		add_option('big_block_stlucia_cat', '', '', 'yes');
		add_option('big_block_stlucia_subcat', '', '', 'yes');
		add_option('big_block_stlucia_type', '', '', 'yes');
		add_option('big_block_stlucia_post_id', '', '', 'yes');
		add_option('big_block_stlucia_post_title', '', '', 'yes');
		add_option('big_block_stlucia_title', '', '', 'yes');
		add_option('big_block_stlucia_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'big_block_stlucia_add_options');
	
	function big_block_stlucia_settings(){
		register_setting('big_block_stlucia_options', 'big_block_stlucia_cat');
		register_setting('big_block_stlucia_options', 'big_block_stlucia_subcat');
		register_setting('big_block_stlucia_options', 'big_block_stlucia_type');
		register_setting('big_block_stlucia_options', 'big_block_stlucia_post_id');
		register_setting('big_block_stlucia_options', 'big_block_stlucia_post_title');
		register_setting('big_block_stlucia_options', 'big_block_stlucia_title');
		register_setting('big_block_stlucia_options', 'big_block_stlucia_img');
	}
	add_action('admin_init', 'big_block_stlucia_settings');
	
	function big_block_stlucia($arg){
		if ($arg == 'big_block_stlucia_cat') {return get_option('big_block_stlucia_cat');}
		if ($arg == 'big_block_stlucia_subcat') {return get_option('big_block_stlucia_subcat');}
		if ($arg == 'big_block_stlucia_type') {return get_option('big_block_stlucia_type');}
		if ($arg == 'big_block_stlucia_post_id') {return get_option('big_block_stlucia_post_id');}
		if ($arg == 'big_block_stlucia_post_title') {return get_option('big_block_stlucia_post_title');}
		if ($arg == 'big_block_stlucia_title') {return get_option('big_block_stlucia_title');}
		if ($arg == 'big_block_stlucia_img') {return get_option('big_block_stlucia_img');}
	};
?>