<?php
    // TOP TEN - 10 POSTS
    function top_ten_stlucia_add_options(){
		for($i = 1; $i <= 10; $i++){
			add_option('top_ten_stlucia_cat'.$i, '', '', 'yes');
			add_option('top_ten_stlucia_subcat'.$i, '', '', 'yes');
			add_option('top_ten_stlucia_type'.$i, '', '', 'yes');
			add_option('top_ten_stlucia_post_id'.$i, '', '', 'yes');
			add_option('top_ten_stlucia_post_title'.$i, '', '', 'yes');
			add_option('top_ten_stlucia_title'.$i, '', '', 'yes');
			add_option('top_ten_stlucia_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'top_ten_stlucia_add_options');
	
	function top_ten_stlucia_settings(){
		for($i = 1; $i <= 10; $i++){
			register_setting('top_ten_stlucia_options', 'top_ten_stlucia_cat'.$i);
			register_setting('top_ten_stlucia_options', 'top_ten_stlucia_subcat'.$i);
			register_setting('top_ten_stlucia_options', 'top_ten_stlucia_type'.$i);
			register_setting('top_ten_stlucia_options', 'top_ten_stlucia_post_id'.$i);
			register_setting('top_ten_stlucia_options', 'top_ten_stlucia_post_title'.$i);
			register_setting('top_ten_stlucia_options', 'top_ten_stlucia_title'.$i);
			register_setting('top_ten_stlucia_options', 'top_ten_stlucia_img'.$i);
		}
	}
	add_action('admin_init', 'top_ten_stlucia_settings');
	
	function top_ten_stlucia($arg){
		for($i = 1; $i <= 10; $i++){
			if ($arg == 'top_ten_stlucia_cat'.$i) {return get_option('top_ten_stlucia_cat'.$i);}
			if ($arg == 'top_ten_stlucia_subcat'.$i) {return get_option('top_ten_stlucia_subcat'.$i);}
			if ($arg == 'top_ten_stlucia_type'.$i) {return get_option('top_ten_stlucia_type'.$i);}
			if ($arg == 'top_ten_stlucia_post_id'.$i) {return get_option('top_ten_stlucia_post_id'.$i);}
			if ($arg == 'top_ten_stlucia_post_title'.$i) {return get_option('top_ten_stlucia_post_title'.$i);}
			if ($arg == 'top_ten_stlucia_title'.$i) {return get_option('top_ten_stlucia_title'.$i);}
			if ($arg == 'top_ten_stlucia_img'.$i) {return get_option('top_ten_stlucia_img'.$i);}
		}
	};
?>