<?php
	// CATEGORIES BLOCK - 3 POSTS EACH, 3 TOTAL, ORDER: HOT, LIFE, NEWS
	
	// HOT
    function categories_block1_stlucia_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block1_stlucia_cat'.$i, '', '', 'yes');
			add_option('categories_block1_stlucia_subcat'.$i, '', '', 'yes');
			add_option('categories_block1_stlucia_type'.$i, '', '', 'yes');
			add_option('categories_block1_stlucia_post_id'.$i, '', '', 'yes');
			add_option('categories_block1_stlucia_post_title'.$i, '', '', 'yes');
			add_option('categories_block1_stlucia_title'.$i, '', '', 'yes');
			add_option('categories_block1_stlucia_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block1_stlucia_add_options');
	
	function categories_block1_stlucia_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block1_stlucia_options', 'categories_block1_stlucia_cat'.$i);
			register_setting('categories_block1_stlucia_options', 'categories_block1_stlucia_subcat'.$i);
			register_setting('categories_block1_stlucia_options', 'categories_block1_stlucia_type'.$i);
			register_setting('categories_block1_stlucia_options', 'categories_block1_stlucia_post_id'.$i);
			register_setting('categories_block1_stlucia_options', 'categories_block1_stlucia_post_title'.$i);
			register_setting('categories_block1_stlucia_options', 'categories_block1_stlucia_title'.$i);
			register_setting('categories_block1_stlucia_options', 'categories_block1_stlucia_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block1_stlucia_settings');
	
	function categories_block1_stlucia($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block1_stlucia_cat'.$i) {return get_option('categories_block1_stlucia_cat'.$i);}
			if ($arg == 'categories_block1_stlucia_subcat'.$i) {return get_option('categories_block1_stlucia_subcat'.$i);}
			if ($arg == 'categories_block1_stlucia_type'.$i) {return get_option('categories_block1_stlucia_type'.$i);}
			if ($arg == 'categories_block1_stlucia_post_id'.$i) {return get_option('categories_block1_stlucia_post_id'.$i);}
			if ($arg == 'categories_block1_stlucia_post_title'.$i) {return get_option('categories_block1_stlucia_post_title'.$i);}
			if ($arg == 'categories_block1_stlucia_title'.$i) {return get_option('categories_block1_stlucia_title'.$i);}
			if ($arg == 'categories_block1_stlucia_img'.$i) {return get_option('categories_block1_stlucia_img'.$i);}
		}
	};

	// LIFE
    function categories_block2_stlucia_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block2_stlucia_cat'.$i, '', '', 'yes');
			add_option('categories_block2_stlucia_subcat'.$i, '', '', 'yes');
			add_option('categories_block2_stlucia_type'.$i, '', '', 'yes');
			add_option('categories_block2_stlucia_post_id'.$i, '', '', 'yes');
			add_option('categories_block2_stlucia_post_title'.$i, '', '', 'yes');
			add_option('categories_block2_stlucia_title'.$i, '', '', 'yes');
			add_option('categories_block2_stlucia_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block2_stlucia_add_options');
	
	function categories_block2_stlucia_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block2_stlucia_options', 'categories_block2_stlucia_cat'.$i);
			register_setting('categories_block2_stlucia_options', 'categories_block2_stlucia_subcat'.$i);
			register_setting('categories_block2_stlucia_options', 'categories_block2_stlucia_type'.$i);
			register_setting('categories_block2_stlucia_options', 'categories_block2_stlucia_post_id'.$i);
			register_setting('categories_block2_stlucia_options', 'categories_block2_stlucia_post_title'.$i);
			register_setting('categories_block2_stlucia_options', 'categories_block2_stlucia_title'.$i);
			register_setting('categories_block2_stlucia_options', 'categories_block2_stlucia_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block2_stlucia_settings');
	
	function categories_block2_stlucia($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block2_stlucia_cat'.$i) {return get_option('categories_block2_stlucia_cat'.$i);}
			if ($arg == 'categories_block2_stlucia_subcat'.$i) {return get_option('categories_block2_stlucia_subcat'.$i);}
			if ($arg == 'categories_block2_stlucia_type'.$i) {return get_option('categories_block2_stlucia_type'.$i);}
			if ($arg == 'categories_block2_stlucia_post_id'.$i) {return get_option('categories_block2_stlucia_post_id'.$i);}
			if ($arg == 'categories_block2_stlucia_post_title'.$i) {return get_option('categories_block2_stlucia_post_title'.$i);}
			if ($arg == 'categories_block2_stlucia_title'.$i) {return get_option('categories_block2_stlucia_title'.$i);}
			if ($arg == 'categories_block2_stlucia_img'.$i) {return get_option('categories_block2_stlucia_img'.$i);}
		}
	};

	// NEWS
    function categories_block3_stlucia_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block3_stlucia_cat'.$i, '', '', 'yes');
			add_option('categories_block3_stlucia_subcat'.$i, '', '', 'yes');
			add_option('categories_block3_stlucia_type'.$i, '', '', 'yes');
			add_option('categories_block3_stlucia_post_id'.$i, '', '', 'yes');
			add_option('categories_block3_stlucia_post_title'.$i, '', '', 'yes');
			add_option('categories_block3_stlucia_title'.$i, '', '', 'yes');
			add_option('categories_block3_stlucia_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block3_stlucia_add_options');
	
	function categories_block3_stlucia_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block3_stlucia_options', 'categories_block3_stlucia_cat'.$i);
			register_setting('categories_block3_stlucia_options', 'categories_block3_stlucia_subcat'.$i);
			register_setting('categories_block3_stlucia_options', 'categories_block3_stlucia_type'.$i);
			register_setting('categories_block3_stlucia_options', 'categories_block3_stlucia_post_id'.$i);
			register_setting('categories_block3_stlucia_options', 'categories_block3_stlucia_post_title'.$i);
			register_setting('categories_block3_stlucia_options', 'categories_block3_stlucia_title'.$i);
			register_setting('categories_block3_stlucia_options', 'categories_block3_stlucia_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block3_stlucia_settings');
	
	function categories_block3_stlucia($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block3_stlucia_cat'.$i) {return get_option('categories_block3_stlucia_cat'.$i);}
			if ($arg == 'categories_block3_stlucia_subcat'.$i) {return get_option('categories_block3_stlucia_subcat'.$i);}
			if ($arg == 'categories_block3_stlucia_type'.$i) {return get_option('categories_block3_stlucia_type'.$i);}
			if ($arg == 'categories_block3_stlucia_post_id'.$i) {return get_option('categories_block3_stlucia_post_id'.$i);}
			if ($arg == 'categories_block3_stlucia_post_title'.$i) {return get_option('categories_block3_stlucia_post_title'.$i);}
			if ($arg == 'categories_block3_stlucia_title'.$i) {return get_option('categories_block3_stlucia_title'.$i);}
			if ($arg == 'categories_block3_stlucia_img'.$i) {return get_option('categories_block3_stlucia_img'.$i);}
		}
	};
?>