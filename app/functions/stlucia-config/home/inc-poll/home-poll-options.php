<?php
    // MAIN BANNER - 5 POSTS
    function home_poll_stlucia_add_options(){
		add_option('home_poll_stlucia_status', '', '', 'yes');
		add_option('home_poll_stlucia_shortcode', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'home_poll_stlucia_add_options');
	
	function home_poll_stlucia_settings(){
		register_setting('home_poll_stlucia_options', 'home_poll_stlucia_status');
		register_setting('home_poll_stlucia_options', 'home_poll_stlucia_shortcode');
	}
	add_action('admin_init', 'home_poll_stlucia_settings');
	
	function home_poll_stlucia($arg){
		if ($arg == 'home_poll_stlucia_status') {return get_option('home_poll_stlucia_status');}
		if ($arg == 'home_poll_stlucia_shortcode') {return get_option('home_poll_stlucia_shortcode');}
	};
?>