<?php settings_fields('home_order_stlucia_options'); ?>

<div class="content-wrapper" id="home_order_stlucia">
    <h3>Home order</h3>

    <div class="orders">
        <?php for($i = 1; $i <= 8; $i++){
            $optionValue = get_option('home_order_stlucia'. $i);
            if($optionValue == '') update_option('home_order_stlucia'. $i, $i);
        ?>
            <div class="block" data-position="<?php echo $i; ?>">
                <p>Block <strong><?php echo $optionValue; ?></strong></p>
                <img src="<?php echo get_template_directory_uri() . '/functions/stlucia-config/home/inc-layout/img/block'. $optionValue.'.jpg"'; ?>>
                <input type="hidden" class="input-order" name="home_order_stlucia<?php echo $i; ?>" value="<?php echo $optionValue; ?>">
            </div>
        <?php } //for ?>
    </div>

    <input class="button-primary" type="submit" name="Save" value="Save">
</div>