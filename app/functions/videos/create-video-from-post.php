<?php
    function duplicate_video_box() {
        add_meta_box('duplicate-video-metabox', 'Duplicate video', 'duplicate_video_box_callback', 'post', 'side');
    }
    add_action('add_meta_boxes', 'duplicate_video_box', 2);

    function duplicate_video_box_callback($post){
        $is_video_check = esc_html(get_post_meta($post->ID, 'is_video_check', true));
        $is_video = ($is_video_check == 'video') ? 'checked="checked"' : ''; ?>

        <div class="duplicate-video-wrapper" style="padding-top:10px;">
            <input type="checkbox" name="is_video_check" id="is_video_check" class="components-checkbox-control__input" value="video" <?php echo $is_video; ?>>
            <label class="components-checkbox-control__label" for="is_video_check">Set post as <strong>Video</strong></label>
        </div>
    <?php }

    function create_video_post($post_id, $post){
        if(isset($_POST['is_video_check'])){
            update_post_meta($post_id, 'is_video_check', $_POST['is_video_check']);
            
            if($post->post_type == 'post' && $post->post_status == 'publish'){
                if(!(wp_is_post_revision($post_id) || wp_is_post_autosave($post_id))){
                    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
                        return;
                    }
    
                    $times = did_action('save_post');
                    if($times != 1){
                        return;
                    }    
    
                    // varrer conteúdo e retornar apenas os vídeos (blocos do gutenberg)
                    $search = ' wp:embed';
                    $result = explode($search, htmlspecialchars($post->post_content));
    
                    $video_content = '';
                    for ($x = 0; $x < count($result); $x++){
                        $posFinal  = strpos($result[$x], ' /wp:embed');
                        if ($posFinal) {
                            $tag = '<!-- wp:embed '.substr($result[$x],0,$posFinal).' /wp:embed -->';
                            if ((strpos($tag, htmlspecialchars('"providerNameSlug":"youtube"'))) && (strpos($tag, htmlspecialchars('"type":"video"')))){
                                $video_content .= '<!-- wp:embed '.substr($result[$x],0,$posFinal).' /wp:embed -->';
                            }
                        }
                    }
                    // varrer conteúdo
    
                    $video_args = array('post_type' => 'video', 'meta_key' => 'video_parent_post_id', 'meta_value' => $post_id);
                    $video_query = new WP_Query($video_args);
    
                    if($video_query->have_posts()){
                        while($video_query->have_posts()) : $video_query->the_post();
                            // já existe, atualizar -----------------------------------------------------------
    
                            $theID = get_the_ID();
    
                            $update_args = array(
                                'ID' => $theID,
                                'post_author'    => $post->post_author,
                                'post_content'   => htmlspecialchars_decode($video_content),
                                'post_excerpt'   => $post->post_excerpt,
                                'post_name'      => $post->post_name,
                                'post_parent'    => $post->post_parent,
                                'post_status'    => 'publish',
                                'post_title'     => $post->post_title,
                                'post_type'      => 'video'
                            );
                    
                            wp_update_post($update_args);
    
                            $thumbnail_id = get_post_thumbnail_id($post_id);
                            set_post_thumbnail($theID, $thumbnail_id);
                    
                            $taxonomies = get_object_taxonomies($post->post_type);
                            foreach ($taxonomies as $taxonomy) {
                                $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
                                wp_set_object_terms($theID, $post_terms, $taxonomy, false);
                            }
    
                        endwhile;
                    } else {
                        // não existe, criar    -----------------------------------------------------------
    
                        $post = get_post($post_id);
                    
                        if (isset($post) && $post != null) {
                            $args = array(
                                'post_author'    => $post->post_author,
                                'post_content'   => htmlspecialchars_decode($video_content),
                                'post_excerpt'   => $post->post_excerpt,
                                'post_name'      => $post->post_name,
                                'post_parent'    => $post->post_parent,
                                'post_status'    => 'publish',
                                'post_title'     => $post->post_title,
                                'post_type'      => 'video'
                            );
                    
                            $new_post_id = wp_insert_post($args);
    
                            $thumbnail_id = get_post_thumbnail_id($post_id);
                            set_post_thumbnail($new_post_id, $thumbnail_id);
                    
                            $taxonomies = get_object_taxonomies($post->post_type);
                            foreach ($taxonomies as $taxonomy) {
                                $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
                                wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
                            }
    
                            update_post_meta($new_post_id, 'video_parent_post_id', $post_id);
                        } else {
                            wp_die('Post creation failed, could not find original post: ' . $post_id);
                        }
                    }
                }
            }
		} else {
			delete_post_meta($post_id, 'is_video_check');
        }
    }
    add_action('save_post', 'create_video_post', 10, 2);
?>