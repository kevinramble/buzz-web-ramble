<?php
    // Video - post type
	function create_video(){
		register_post_type('video',
		array(
			'labels' => array(
				'name' => 'Videos',
						'singular_name' => 'Video',
						'add_new' => 'Add new',
						'add_new_item' => 'Add new video',
						'edit_item' => 'Edit video',
						'new_item' => 'New video',
						'all_items' => 'All videos',
						'view_item' => 'View video',
						'search_items' => 'Search videos',
						'not_found' => 'No video found',
						'not_found_in_trash' => 'No videos found in trash',
						'parent_item_colon' => '',
						'menu_name' => 'Videos'
					),
					'taxonomies' => array('category'),
					'public' => true,
					'menu_client' => 5,
					'menu_position' => 4,
					'menu_icon' => 'dashicons-video-alt2',
					'supports' => array('title', 'thumbnail', 'editor', 'author'),
					'show_in_rest' => true,
					'has_archive' => true,
					'rewrite' => array('slug' => 'video', 'with_front' => false)
				)
			);
	}
	add_action('init', 'create_video');

	add_action('admin_init', 'video_admin');
	function video_admin() {
		add_meta_box('video_meta_box',
			'Details',
			'display_video_meta_box',
			'video', 'normal', 'high'
		);
	}

	function display_video_meta_box($video_post){
		$video_caption = esc_html(get_post_meta($video_post->ID, 'video_caption', true));
		$video_url = esc_html(get_post_meta($video_post->ID, 'video_url', true));
		$video_timestamp = esc_html(get_post_meta($video_post->ID, 'video_timestamp', true));
		$video_idcode = esc_html(get_post_meta($video_post->ID, 'video_idcode', true));

		$video_parent_post_id = esc_html(get_post_meta($video_post->ID, 'video_parent_post_id', true));
	?>
		<p class="label"><strong>Video <em>parent</em> post:</strong><br>
		<?php if($video_parent_post_id) :
			$parent_post = get_post($video_parent_post_id);
			if($parent_post && $parent_post->post_status == 'publish') : ?>
				"<?php echo get_the_title($video_parent_post_id); ?>"<br>
				<a href="<?php echo admin_url('post.php?post='.$video_parent_post_id.'&action=edit'); ?>" target="_blank">Edit post</a> | 
				<a href="<?php the_permalink($video_parent_post_id); ?>" target="_blank">View post</a>
			<?php else : ?>
				None.
			<?php endif; ?>
		<?php endif; ?>

		<input type="hidden" name="video_parent_post_id" value="<?php echo $video_parent_post_id; ?>"></p>
	<?php
	}

	function add_video_fields($video_post_id, $video_post){
		if($video_post->post_type == 'video'){
			if(isset($_POST['video_post_caption'])){
				update_post_meta($video_post_id, 'video_caption', $_POST['video_post_caption']);
			}
			if(isset($_POST['video_post_url'])){
				update_post_meta($video_post_id, 'video_url', $_POST['video_post_url']);

				// Save video ID code
				$fullURL = $_POST['video_post_url'];
				parse_str(parse_url($fullURL, PHP_URL_QUERY), $video_vars);
				$videoIDcode = $video_vars['v'];

				update_post_meta($video_post_id, 'video_idcode', $videoIDcode);

				// Save video timestamp
				$apiKey = 'AIzaSyCjHldVRg0yQv-nFT3wi3IqK5FwkaZH3wY';
				$apiUrl = 'https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=' . $videoIDcode . '&key=' . $apiKey;

				$data = json_decode(file_get_contents($apiUrl));
				$duration = $data->items[0]->contentDetails->duration;
				if($duration) $convertedTime = convertTime($duration);
				
				update_post_meta($video_post_id, 'video_timestamp', $convertedTime);
			}

			if(isset($_POST['video_parent_post_id'])){
				update_post_meta($video_post_id, 'video_parent_post_id', $_POST['video_parent_post_id']);
			}
		}
	}
    add_action('save_post', 'add_video_fields', 10, 2);
    
    function video_post_enqueue($hook){
        global $post_type;
        if($post_type == 'video'){
			wp_enqueue_style('video-post-style', get_template_directory_uri() . '/functions/videos/video-post.css');
		}
	}
	add_action('admin_enqueue_scripts', 'video_post_enqueue');
	
	// Mostrar custom fields na lista de posts
	function add_video_columns($columns){
		unset($columns['comments']);
		return array_merge($columns, array( 
			'is_video' => 'Parent post'
		));
	}
	add_filter('manage_video_posts_columns', 'add_video_columns');

 	function video_custom_column($column, $video_post_id){
		switch($column){
			case 'is_video':
				$parent_id = esc_html(get_post_meta(get_the_ID(), 'video_parent_post_id', true));
				if($parent_id) {
					$parent_post = get_post($parent_id);
					if($parent_post && $parent_post->post_status == 'publish'){ ?>
						<a href="<?php the_permalink($parent_id); ?>" target="_blank">View</a> / 
						<a href="<?php echo admin_url('post.php?post='.$parent_id.'&action=edit'); ?>" target="_blank">Edit</a>
					<?php } else {
						echo '–';
					}
				}
			break;
		}
	}
	add_action('manage_video_posts_custom_column', 'video_custom_column', 10, 2);
	
	// Função pra lidar com o timer do YouTube
	function convertTime($yt){
		$yt=str_replace(['P','T'],'',$yt);
		foreach(['D','H','M','S'] as $a){
			$pos=strpos($yt,$a);
			if($pos!==false) ${$a}=substr($yt,0,$pos); else { ${$a}=0; continue; }
			$yt=substr($yt,$pos+1);
		}
		if($D>0){
			$M=str_pad($M,2,'0',STR_PAD_LEFT);
			$S=str_pad($S,2,'0',STR_PAD_LEFT);
			return ($H+(24*$D)).":$M:$S"; // add days to hours
		} elseif($H>0){
			$M=str_pad($M,2,'0',STR_PAD_LEFT);
			$S=str_pad($S,2,'0',STR_PAD_LEFT);
			return "$H:$M:$S";
		} else {
			$S=str_pad($S,2,'0',STR_PAD_LEFT);
			return "$M:$S";
		}
	}