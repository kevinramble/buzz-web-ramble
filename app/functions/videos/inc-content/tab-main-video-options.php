<?php
    // MAIN VIDEO - 3 POSTS
    function main_video_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('main_video_cat'.$i, '', '', 'yes');
			add_option('main_video_subcat'.$i, '', '', 'yes');
			add_option('main_video_type'.$i, '', '', 'yes');
			add_option('main_video_post_id'.$i, '', '', 'yes');
			add_option('main_video_post_title'.$i, '', '', 'yes');
			add_option('main_video_title'.$i, '', '', 'yes');
			add_option('main_video_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'main_video_add_options');
	
	function main_video_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('main_video_options', 'main_video_cat'.$i);
			register_setting('main_video_options', 'main_video_subcat'.$i);
			register_setting('main_video_options', 'main_video_type'.$i);
			register_setting('main_video_options', 'main_video_post_id'.$i);
			register_setting('main_video_options', 'main_video_post_title'.$i);
			register_setting('main_video_options', 'main_video_title'.$i);
			register_setting('main_video_options', 'main_video_img'.$i);
		}
	}
	add_action('admin_init', 'main_video_settings');
	
	function main_video($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'main_video_cat'.$i) {return get_option('main_video_cat'.$i);}
			if ($arg == 'main_video_subcat'.$i) {return get_option('main_video_subcat'.$i);}
			if ($arg == 'main_video_type'.$i) {return get_option('main_video_type'.$i);}
			if ($arg == 'main_video_post_id'.$i) {return get_option('main_video_post_id'.$i);}
			if ($arg == 'main_video_post_title'.$i) {return get_option('main_video_post_title'.$i);}
			if ($arg == 'main_video_title'.$i) {return get_option('main_video_title'.$i);}
			if ($arg == 'main_video_img'.$i) {return get_option('main_video_img'.$i);}
		}
	};
?>