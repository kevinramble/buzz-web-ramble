<?php
	function video_config() {
		add_menu_page('Videos config', 'Videos config', 'manage_options', 'video_config', 'video_config_content', 'dashicons-playlist-video', '4.5');
	}
	add_action('admin_menu', 'video_config');

	require_once('inc-content/tab-main-video-options.php');
	require_once('inc-content/tab-watch-next-options.php');

	function video_config_content(){ ?>
		<div class="wrap">
			<h2>Videos page content configurations</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>

						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'main-video'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=video_config&tab=main-video" class="nav-tab <?php echo $active_tab == 'main-video' ? 'nav-tab-active' : ''; ?>">Main banner</a>
							<a href="?page=video_config&tab=watch-next" class="nav-tab <?php echo $active_tab == 'watch-next' ? 'nav-tab-active' : ''; ?>">Watch next</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'main-video'){
								// TAB: MAIN BANNER
								require_once('inc-content/tab-main-video.php');
							
							} elseif($active_tab == 'watch-next'){
								// TAB: WATCH NEXT
								require_once('inc-content/tab-watch-next.php');
							}  ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // video_config_content()
?>