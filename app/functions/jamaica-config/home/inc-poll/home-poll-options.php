<?php
    // MAIN BANNER - 5 POSTS
    function home_poll_jamaica_add_options(){
		add_option('home_poll_jamaica_status', '', '', 'yes');
		add_option('home_poll_jamaica_shortcode', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'home_poll_jamaica_add_options');
	
	function home_poll_jamaica_settings(){
		register_setting('home_poll_jamaica_options', 'home_poll_jamaica_status');
		register_setting('home_poll_jamaica_options', 'home_poll_jamaica_shortcode');
	}
	add_action('admin_init', 'home_poll_jamaica_settings');
	
	function home_poll_jamaica($arg){
		if ($arg == 'home_poll_jamaica_status') {return get_option('home_poll_jamaica_status');}
		if ($arg == 'home_poll_jamaica_shortcode') {return get_option('home_poll_jamaica_shortcode');}
	};
?>