<?php settings_fields('home_order_jamaica_options'); ?>

<div class="content-wrapper" id="home_order_jamaica">
    <h3>Home order</h3>

    <div class="orders">
        <?php for($i = 1; $i <= 8; $i++){
            $optionValue = get_option('home_order_jamaica'. $i);
            if($optionValue == '') update_option('home_order_jamaica'. $i, $i);
        ?>
            <div class="block" data-position="<?php echo $i; ?>">
                <p>Block <strong><?php echo $optionValue; ?></strong></p>
                <img src="<?php echo get_template_directory_uri() . '/functions/jamaica-config/home/inc-layout/img/block'. $optionValue.'.jpg"'; ?>>
                <input type="hidden" class="input-order" name="home_order_jamaica<?php echo $i; ?>" value="<?php echo $optionValue; ?>">
            </div>
        <?php } //for ?>
    </div>

    <input class="button-primary" type="submit" name="Save" value="Save">
</div>