<?php
    // EDITOR'S CHOICE - 8 POSTS
    function home_order_jamaica_add_options(){
		for($i = 1; $i <= 8; $i++){
			add_option('home_order_jamaica'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'home_order_jamaica_add_options');
	
	function home_order_jamaica_settings(){
		for($i = 1; $i <= 8; $i++){
			register_setting('home_order_jamaica_options', 'home_order_jamaica'.$i);
		}
	}
	add_action('admin_init', 'home_order_jamaica_settings');
	
	function home_order_jamaica($arg){
		for($i = 1; $i <= 8; $i++){
			if ($arg == 'home_order_jamaica'.$i) {return get_option('home_order_jamaica'.$i);}
		}
	};
?>