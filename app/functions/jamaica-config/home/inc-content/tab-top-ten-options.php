<?php
    // TOP TEN - 10 POSTS
    function top_ten_jamaica_add_options(){
		for($i = 1; $i <= 10; $i++){
			add_option('top_ten_jamaica_cat'.$i, '', '', 'yes');
			add_option('top_ten_jamaica_subcat'.$i, '', '', 'yes');
			add_option('top_ten_jamaica_type'.$i, '', '', 'yes');
			add_option('top_ten_jamaica_post_id'.$i, '', '', 'yes');
			add_option('top_ten_jamaica_post_title'.$i, '', '', 'yes');
			add_option('top_ten_jamaica_title'.$i, '', '', 'yes');
			add_option('top_ten_jamaica_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'top_ten_jamaica_add_options');
	
	function top_ten_jamaica_settings(){
		for($i = 1; $i <= 10; $i++){
			register_setting('top_ten_jamaica_options', 'top_ten_jamaica_cat'.$i);
			register_setting('top_ten_jamaica_options', 'top_ten_jamaica_subcat'.$i);
			register_setting('top_ten_jamaica_options', 'top_ten_jamaica_type'.$i);
			register_setting('top_ten_jamaica_options', 'top_ten_jamaica_post_id'.$i);
			register_setting('top_ten_jamaica_options', 'top_ten_jamaica_post_title'.$i);
			register_setting('top_ten_jamaica_options', 'top_ten_jamaica_title'.$i);
			register_setting('top_ten_jamaica_options', 'top_ten_jamaica_img'.$i);
		}
	}
	add_action('admin_init', 'top_ten_jamaica_settings');
	
	function top_ten_jamaica($arg){
		for($i = 1; $i <= 10; $i++){
			if ($arg == 'top_ten_jamaica_cat'.$i) {return get_option('top_ten_jamaica_cat'.$i);}
			if ($arg == 'top_ten_jamaica_subcat'.$i) {return get_option('top_ten_jamaica_subcat'.$i);}
			if ($arg == 'top_ten_jamaica_type'.$i) {return get_option('top_ten_jamaica_type'.$i);}
			if ($arg == 'top_ten_jamaica_post_id'.$i) {return get_option('top_ten_jamaica_post_id'.$i);}
			if ($arg == 'top_ten_jamaica_post_title'.$i) {return get_option('top_ten_jamaica_post_title'.$i);}
			if ($arg == 'top_ten_jamaica_title'.$i) {return get_option('top_ten_jamaica_title'.$i);}
			if ($arg == 'top_ten_jamaica_img'.$i) {return get_option('top_ten_jamaica_img'.$i);}
		}
	};
?>