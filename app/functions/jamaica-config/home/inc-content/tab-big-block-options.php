<?php
    // BIG BLOCK - 1 POST
    function big_block_jamaica_add_options(){
		add_option('big_block_jamaica_cat', '', '', 'yes');
		add_option('big_block_jamaica_subcat', '', '', 'yes');
		add_option('big_block_jamaica_type', '', '', 'yes');
		add_option('big_block_jamaica_post_id', '', '', 'yes');
		add_option('big_block_jamaica_post_title', '', '', 'yes');
		add_option('big_block_jamaica_title', '', '', 'yes');
		add_option('big_block_jamaica_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'big_block_jamaica_add_options');
	
	function big_block_jamaica_settings(){
		register_setting('big_block_jamaica_options', 'big_block_jamaica_cat');
		register_setting('big_block_jamaica_options', 'big_block_jamaica_subcat');
		register_setting('big_block_jamaica_options', 'big_block_jamaica_type');
		register_setting('big_block_jamaica_options', 'big_block_jamaica_post_id');
		register_setting('big_block_jamaica_options', 'big_block_jamaica_post_title');
		register_setting('big_block_jamaica_options', 'big_block_jamaica_title');
		register_setting('big_block_jamaica_options', 'big_block_jamaica_img');
	}
	add_action('admin_init', 'big_block_jamaica_settings');
	
	function big_block_jamaica($arg){
		if ($arg == 'big_block_jamaica_cat') {return get_option('big_block_jamaica_cat');}
		if ($arg == 'big_block_jamaica_subcat') {return get_option('big_block_jamaica_subcat');}
		if ($arg == 'big_block_jamaica_type') {return get_option('big_block_jamaica_type');}
		if ($arg == 'big_block_jamaica_post_id') {return get_option('big_block_jamaica_post_id');}
		if ($arg == 'big_block_jamaica_post_title') {return get_option('big_block_jamaica_post_title');}
		if ($arg == 'big_block_jamaica_title') {return get_option('big_block_jamaica_title');}
		if ($arg == 'big_block_jamaica_img') {return get_option('big_block_jamaica_img');}
	};
?>