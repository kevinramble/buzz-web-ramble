<?php
    // EDITOR'S CHOICE - 4 POSTS
    function editors_choice_jamaica_add_options(){
		for($i = 1; $i <= 4; $i++){
			add_option('editors_choice_jamaica_cat'.$i, '', '', 'yes');
			add_option('editors_choice_jamaica_subcat'.$i, '', '', 'yes');
			add_option('editors_choice_jamaica_type'.$i, '', '', 'yes');
			add_option('editors_choice_jamaica_post_id'.$i, '', '', 'yes');
			add_option('editors_choice_jamaica_post_title'.$i, '', '', 'yes');
			add_option('editors_choice_jamaica_title'.$i, '', '', 'yes');
			add_option('editors_choice_jamaica_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'editors_choice_jamaica_add_options');
	
	function editors_choice_jamaica_settings(){
		for($i = 1; $i <= 4; $i++){
			register_setting('editors_choice_jamaica_options', 'editors_choice_jamaica_cat'.$i);
			register_setting('editors_choice_jamaica_options', 'editors_choice_jamaica_subcat'.$i);
			register_setting('editors_choice_jamaica_options', 'editors_choice_jamaica_type'.$i);
			register_setting('editors_choice_jamaica_options', 'editors_choice_jamaica_post_id'.$i);
			register_setting('editors_choice_jamaica_options', 'editors_choice_jamaica_post_title'.$i);
			register_setting('editors_choice_jamaica_options', 'editors_choice_jamaica_title'.$i);
			register_setting('editors_choice_jamaica_options', 'editors_choice_jamaica_img'.$i);
		}
	}
	add_action('admin_init', 'editors_choice_jamaica_settings');
	
	function editors_choice_jamaica($arg){
		for($i = 1; $i <= 4; $i++){
			if ($arg == 'editors_choice_jamaica_cat'.$i) {return get_option('editors_choice_jamaica_cat'.$i);}
			if ($arg == 'editors_choice_jamaica_subcat'.$i) {return get_option('editors_choice_jamaica_subcat'.$i);}
			if ($arg == 'editors_choice_jamaica_type'.$i) {return get_option('editors_choice_jamaica_type'.$i);}
			if ($arg == 'editors_choice_jamaica_post_id'.$i) {return get_option('editors_choice_jamaica_post_id'.$i);}
			if ($arg == 'editors_choice_jamaica_post_title'.$i) {return get_option('editors_choice_jamaica_post_title'.$i);}
			if ($arg == 'editors_choice_jamaica_title'.$i) {return get_option('editors_choice_jamaica_title'.$i);}
			if ($arg == 'editors_choice_jamaica_img'.$i) {return get_option('editors_choice_jamaica_img'.$i);}
		}
	};
?>