jQuery(document).ready(function($) {
	var mediaUploader,
		content,
		selection,
		parent,
		slot,
		postTitle,
		postID,
		ajaxURL,
		siteurl = $('.post-manager').data('siteurl'),
		acceptVideo = $('.post-manager').data('accept-video'),
		optionName = $('.post-manager').attr('id'),
		contentArray = [],
		countryID = $('#poststuff').data('countryid')

	// Media uploader
	$('.upload-button').click(function(e) {
		var $this = $(this)
		parent = $(this).parents('.slot')
		slot = parent.data('slot')

		e.preventDefault()
		if (mediaUploader) {
			mediaUploader.open()
			return
		}

		if (acceptVideo == true) {
			mediaUploader = wp.media.frames.file_frame = wp.media({
				id: 'home-config-uploader',
				title: 'Choose a picture',
				library: { type: ['image', 'video'] },
				button: { text: 'Select' }
			})
		} else {
			mediaUploader = wp.media.frames.file_frame = wp.media({
				id: 'home-config-uploader',
				title: 'Choose a picture',
				library: { type: ['image'] },
				button: { text: 'Select' }
			})
		}

		mediaUploader.on('select', function() {
			selection = mediaUploader
				.state()
				.get('selection')
				.toJSON()

			$.each(selection, function(i, file) {
				console.log('file', file)

				if (file.type == 'image') {
					content = '<a href="#0" class="delete-image dashicons dashicons-trash" title="Remove image"></a>'
					content += '<img src="' + file.url + '" class="added-image">'
					content += '<input type="hidden" class="custom-img" name="' + optionName + '_img' + slot + '" value="' + file.url + '">'
				} else if (file.type == 'video') {
					if (file.subtype == 'mp4') {
						content = '<a href="#0" class="delete-image video dashicons dashicons-trash" title="Remove image"></a>'
						content += '<img src="' + file.thumb.src + '" class="video-thumb">'
						content += '<div class="video-details">'
						content += '<strong>Video:</strong> ' + file.filename + '<br>'
						content += '<strong>Length:</strong> ' + file.fileLength
						content += '<i style="clear:both;display:block;"></i>'
						content += '</div>'

						content += '<input type="hidden" class="custom-img" name="' + optionName + '_img' + slot + '" value="' + file.url + '">'
					} else {
						content = '<em><strong>Video error:</strong> mp4 files only!</em>'
					}
				}

				parent.find('.used-image').html(content)

				$this.html('Change custom image')
			})
		})

		mediaUploader.open()
	}) // upload-button click

	// Remove custom image
	$('.slot').delegate('.delete-image', 'click', function(e) {
		e.preventDefault()

		$(this)
			.parents('.option')
			.find('.upload-button')
			.html('Add custom image')
		$(this)
			.parents('.used-image')
			.html('')
	})

	// Retrieve posts
	$('.post-button').click(function(e) {
		var $this = $(this)
		parent = $(this).parents('.slot')
		catID = parent.find('.cat-select').val()
		subcatID = parent.find('.subcat-select').val()
		type = parent.find('.type-select').val()
		slot = parent.data('slot')
		contentArray = []

		e.preventDefault()

		if (type === 'video') {
			ajaxURL = siteurl + '/wp-json/wp/v2/video/?countries=' + countryID + '&categories=' + catID + '&per_page=100&sub-category=' + subcatID
		} else {
			ajaxURL = siteurl + '/wp-json/wp/v2/posts/?countries=' + countryID + '&categories=' + catID + '&per_page=100&sub-category=' + subcatID
		}

		if (catID) {
			$(this).addClass('loading')

			$.ajax({
				url: ajaxURL,
				success: function(data) {
					var datalength = data.length

					for (var k = 0; k < datalength; k++) {
						postID = data[k].id
						postTitle = data[k].title.rendered

						contentArray += '<option value="' + postID + '">' + postTitle + '</option>'
					}

					parent.find('.post-info').html('<select class="post-select input-post-id" name="' + optionName + '_post_id' + slot + '"><option value="">Select post</option>' + contentArray + '</select>')
					$this.addClass('hidden')

					$('.post-button').removeClass('loading')
				},
				cache: false
			})
		} else {
			$this.before('<span class="post-error">Please choose a category first.</span>')
		}

		if (parent.hasClass('empty')) parent.removeClass('empty')
	}) // post-button click

	$('.slot').delegate('.post-select', 'change', function() {
		var text = $(this)
				.find(':selected')
				.text(),
			parent = $(this).parents('.slot'),
			slot = parent.data('slot')

		parent.find('.input-post-title').remove()
		parent.find('.post-info').append('<input type="hidden" class="input-post-title" name="' + optionName + '_post_title' + slot + '" value="' + text + '">')
	})

	$('.cat-select').each(function() {
		$(this).change(function() {
			parent = $(this).parents('.slot')

			if (parent.hasClass('empty')) parent.removeClass('empty')

			if (parent.find('span.post-error').is(':visible')) {
				parent.find('span.post-error').remove()
			}

			if (parent.find('.post-button').hasClass('hidden')) {
				parent.find('.post-button').removeClass('hidden')
				parent.find('.post-info').html('')
			}

			if (parent.find('.current-selected').is(':visible')) {
				parent.find('.post-info').html('')
			}
		})
	})

	$('.subcat-select').each(function() {
		$(this).change(function() {
			parent = $(this).parents('.slot')

			if (parent.hasClass('empty')) parent.removeClass('empty')

			if (parent.find('span.post-error').is(':visible')) {
				parent.find('span.post-error').remove()
			}

			if (parent.find('.post-button').hasClass('hidden')) {
				parent.find('.post-button').removeClass('hidden')
				parent.find('.post-info').html('')
			}

			if (parent.find('.current-selected').is(':visible')) {
				parent.find('.post-info').html('')
			}
		})
	})

	$('.type-select').each(function() {
		$(this).change(function() {
			parent = $(this).parents('.slot')

			if (parent.hasClass('empty')) parent.removeClass('empty')

			if (parent.find('span.post-error').is(':visible')) {
				parent.find('span.post-error').remove()
			}

			if (parent.find('.post-button').hasClass('hidden')) {
				parent.find('.post-button').removeClass('hidden')
				parent.find('.post-info').html('')
			}

			if (parent.find('.current-selected').is(':visible')) {
				parent.find('.post-info').html('')
			}
		})
	})

	// Remove slot's content
	$('.slot').delegate('.remove-content', 'click', function(e) {
		e.preventDefault()

		parent = $(this).parents('.slot')

		if (!parent.hasClass('empty')) parent.addClass('empty')

		if (parent.find('span.post-error').is(':visible')) {
			parent.find('span.post-error').remove()
		}

		if (parent.find('.post-button').hasClass('hidden')) {
			parent.find('.post-button').removeClass('hidden')
			parent.find('.post-info').html('')
		}

		if (optionName != 'categories_block1' && optionName != 'categories_block2' && optionName != 'categories_block3' && optionName != 'categories_block4') {
			if (parent.find('.cat-select').val() !== '') {
				parent.find('.cat-select').val('')
			}
		}

		if (parent.find('.subcat-select').val() !== '') {
			parent.find('.subcat-select').val('')
		}

		if (parent.find('.type-select').val() !== 'post') {
			parent.find('.type-select').val('post')
		}

		if (parent.find('.current-selected').is(':visible')) {
			parent.find('.post-info').html('')
		}

		// Custom content
		parent.find('.checkbox-accordion').prop('checked', false)
		if (parent.find('.custom-title').val() !== '') {
			parent.find('.custom-title').val('')
		}
		if (parent.find('.used-image').html() !== '') {
			parent.find('.used-image').html('')
			parent.find('.upload-button').html('Add custom image')
		}
	})

	// Sortable slots
	if ($('.slot').length > 1) {
		$('.slots').sortable({
			items: '.slot',
			cursor: 'move',
			scrollSensitivity: 30,
			scrollSpeed: 30,
			tolerance: 'pointer',
			update: function() {
				$(this)
					.find('.slot')
					.each(function() {
						var $this = $(this)
						var oldSlot = parseInt($(this).data('slot'))
						var newSlot = parseInt($(this).index() + 1)

						if (newSlot !== oldSlot) {
							// update info only if slot changed
							$this.data('slot', newSlot) // data slot
							$this.find('h4').html('Slot ' + newSlot) // slot card title
							$this.find('.cat-select').attr('name', optionName + '_cat' + newSlot) // categories select
							$this.find('.subcat-select').attr('name', optionName + '_subcat' + newSlot) // categories select
							$this.find('.type-select').attr('name', optionName + '_type' + newSlot) // categories select
							$this.find('.input-post-title').attr('name', optionName + '_post_title' + newSlot) // post title
							$this.find('.input-post-id').attr('name', optionName + '_post_id' + newSlot) // post id
							$this.find('.custom-title').attr('name', optionName + '_title' + newSlot) // custom title, optional
							$this.find('.custom-img').attr('name', optionName + '_img' + newSlot) // custom img, optional
						}
					})
			}
		}) // sortable
	} // length if

	// Sortable blocks
	if ($('.block').length > 1) {
		$('.orders').sortable({
			items: '.block',
			cursor: 'move',
			axis: 'y',
			scrollSensitivity: 30,
			scrollSpeed: 30,
			tolerance: 'pointer',
			update: function() {
				$(this)
					.find('.block')
					.each(function() {
						var $this = $(this)
						var oldPosition = parseInt($(this).data('position'))
						var newPosition = parseInt($(this).index() + 1)

						if (newPosition !== oldPosition) {
							$this.data('position', newPosition)
							$this.find('.input-order').attr('name', 'home_order_jamaica' + newPosition)
						}
					})
			}
		}) // sortable
	} // length if
})
