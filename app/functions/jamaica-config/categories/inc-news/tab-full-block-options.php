<?php
    // BIG BLOCK - 1 POST
    function full_block_news_jamaica_add_options(){
		add_option('full_block_news_jamaica_cat', '', '', 'yes');
		add_option('full_block_news_jamaica_subcat', '', '', 'yes');
		add_option('full_block_news_jamaica_type', '', '', 'yes');
		add_option('full_block_news_jamaica_post_id', '', '', 'yes');
		add_option('full_block_news_jamaica_post_title', '', '', 'yes');
		add_option('full_block_news_jamaica_title', '', '', 'yes');
		add_option('full_block_news_jamaica_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'full_block_news_jamaica_add_options');
	
	function full_block_news_jamaica_settings(){
		register_setting('full_block_news_jamaica_options', 'full_block_news_jamaica_cat');
		register_setting('full_block_news_jamaica_options', 'full_block_news_jamaica_subcat');
		register_setting('full_block_news_jamaica_options', 'full_block_news_jamaica_type');
		register_setting('full_block_news_jamaica_options', 'full_block_news_jamaica_post_id');
		register_setting('full_block_news_jamaica_options', 'full_block_news_jamaica_post_title');
		register_setting('full_block_news_jamaica_options', 'full_block_news_jamaica_title');
		register_setting('full_block_news_jamaica_options', 'full_block_news_jamaica_img');
	}
	add_action('admin_init', 'full_block_news_jamaica_settings');
	
	function full_block_news_jamaica($arg){
		if ($arg == 'full_block_news_jamaica_cat') {return get_option('full_block_news_jamaica_cat');}
		if ($arg == 'full_block_news_jamaica_subcat') {return get_option('full_block_news_jamaica_subcat');}
		if ($arg == 'full_block_news_jamaica_type') {return get_option('full_block_news_jamaica_type');}
		if ($arg == 'full_block_news_jamaica_post_id') {return get_option('full_block_news_jamaica_post_id');}
		if ($arg == 'full_block_news_jamaica_post_title') {return get_option('full_block_news_jamaica_post_title');}
		if ($arg == 'full_block_news_jamaica_title') {return get_option('full_block_news_jamaica_title');}
		if ($arg == 'full_block_news_jamaica_img') {return get_option('full_block_news_jamaica_img');}
	};
?>