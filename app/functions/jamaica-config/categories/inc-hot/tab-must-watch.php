<?php settings_fields('must_watch_hot_jamaica_options'); ?>

<div class="content-wrapper post-manager" id="must_watch_hot_jamaica" data-video="true" data-siteurl="<?php echo get_bloginfo('url'); ?>">
    <h3>Must watch</h3>

    <p>9 posts in total, at least 3 are required. Just videos.</p>

    <?php $get_cat = get_category_by_slug('hot'); $cat = $get_cat->term_id; ?>

    <div class="slots">
        <?php for($i = 1; $i <= 9; $i++){ ?>
            <?php $empty = (get_option('must_watch_hot_jamaica_post_id'. $i)) ? '' : 'empty'; ?>
            <div class="slot <?php echo $empty; ?>" data-slot="<?php echo $i; ?>">
                <h4>Slot <?php echo $i; ?></h4>

                <span class="remove-content dashicons dashicons-dismiss" title="Remove slot's content"></span>

                <div class="option">
                    <label style="display:none;"> <!-- usado só no js -->
                        <span class="item-title">Category</span>
                        <select class="cat-select" name="must_watch_hot_jamaica_cat<?php echo $i; ?>">
                            <option value="<?php echo $cat; ?>" <?php if(get_option('must_watch_hot_jamaica_cat'. $i) == $cat) echo 'selected="selected"'; ?>>Video: Hot</option>
                        </select>
                    </label>

                    <label style="display:none;"> <!-- usado só no js -->
                        <span class="item-title">Type</span>
                        <select class="type-select" name="must_watch_hot_jamaica_type<?php echo $i; ?>">
                            <option value="video" <?php if(get_option('must_watch_hot_jamaica_type'. $i) == 'video') echo 'selected="selected"'; ?>>Videos</option>
                        </select>
                    </label>

                    <label>
                        <span class="item-title">Sub-category</span>
                        <select class="subcat-select" name="must_watch_hot_jamaica_subcat<?php echo $i; ?>">
                            <option value="" <?php if (get_option('must_watch_hot_jamaica_subcat'. $i) == '') echo 'selected="selected"'; ?>>Choose a sub-category</option>
                            <?php
                                $terms = get_terms(array('taxonomy' => 'sub-category', 'hide_empty' => 1));
                                foreach ($terms as $term){
                                    $selected = (get_option('must_watch_hot_jamaica_subcat'. $i) == $term->term_id) ? 'selected="selected"' : '';
                                    echo '<option value="'. $term->term_id .'"'. $selected .'>'. $term->name .'</option>';
                                }
                            ?>
                        </select>
                    </label>
                </div><!-- .option -->

                <div class="option">
                    <span class="item-title">Post</span>
                    <div class="post-info">
                        <?php
                            $currentPostID = get_option('must_watch_hot_jamaica_post_id'. $i);
                            $currentPostTitle = get_option('must_watch_hot_jamaica_post_title'. $i);

                        if(get_option('must_watch_hot_jamaica_post_id'. $i)){
                            echo '<p class="current-selected"><em>Current selected post:</em>'. $currentPostTitle .' <a href="'. get_permalink($currentPostID) .'" class="dashicons dashicons-external" title="Open post in new tab" target="_blank"></a></p>';
                        } ?>
                        <input type="hidden" class="input-post-title" name="must_watch_hot_jamaica_post_title<?php echo $i; ?>" value="<?php echo $currentPostTitle; ?>">
                        <input type="hidden" class="input-post-id" name="must_watch_hot_jamaica_post_id<?php echo $i; ?>" value="<?php echo $currentPostID; ?>">
                    </div><!-- .post-info -->
                    <a href="#0" class="post-button button-primary">Choose post</a>
                </div><!-- .option -->

                <?php $checked = (get_option('must_watch_hot_jamaica_title'. $i) || get_option('must_watch_hot_jamaica_img'. $i)) ? 'checked' : ''; ?>
                <div class="optional-settings">
                    <input type="checkbox" id="checkbox-<?php echo $i; ?>" class="checkbox-accordion" <?php echo $checked; ?>>
                    <label for="checkbox-<?php echo $i; ?>">Optional settings</label>

                    <div class="option">
                        <label>
                            <span class="item-title">Title</span>
                            <input size="35" class="custom-title" name="must_watch_hot_jamaica_title<?php echo $i; ?>" value="<?php echo get_option('must_watch_hot_jamaica_title'. $i); ?>">
                        </label>
                    </div><!-- .option -->

                    <div class="option">
                        <span class="item-title">Image</span>
                        <div class="used-image">
                            <?php
                            $img = 'must_watch_hot_jamaica_img' . $i;
                            if(get_option($img)){
                                $content  = '<a href="#0" class="delete-image dashicons dashicons-trash" title="Remove image"></a>';
                                $content .= '<img src="'. get_option($img) .'">';
                                $content .= '<input type="hidden" class="custom-img" name="'.$img.'" value="'. get_option($img) .'">';

                                echo $content;
                            } ?>
                        </div>

                        <?php $customImgText = (get_option($img)) ? 'Change custom image' : 'Add custom image' ; ?>
                        <a href="#0" class="upload-button button-primary"><?php echo $customImgText; ?></a>
                    </div><!-- .option -->
                </div><!-- .optional-settings -->
            </div><!-- .slot -->
        <?php } // for ?>
    </div><!-- .slots -->

    <input class="button-primary" type="submit" name="Save" value="Save changes">
</div>