<?php
    // BREAKING NEWS - 8 POSTS
    function breaking_news_hot_jamaica_add_options(){
		for($i = 1; $i <= 8; $i++){
			add_option('breaking_news_hot_jamaica_cat'.$i, '', '', 'yes');
			add_option('breaking_news_hot_jamaica_subcat'.$i, '', '', 'yes');
			add_option('breaking_news_hot_jamaica_type'.$i, '', '', 'yes');
			add_option('breaking_news_hot_jamaica_post_id'.$i, '', '', 'yes');
			add_option('breaking_news_hot_jamaica_post_title'.$i, '', '', 'yes');
			add_option('breaking_news_hot_jamaica_title'.$i, '', '', 'yes');
			add_option('breaking_news_hot_jamaica_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'breaking_news_hot_jamaica_add_options');
	
	function breaking_news_hot_jamaica_settings(){
		for($i = 1; $i <= 8; $i++){
			register_setting('breaking_news_hot_jamaica_options', 'breaking_news_hot_jamaica_cat'.$i);
			register_setting('breaking_news_hot_jamaica_options', 'breaking_news_hot_jamaica_subcat'.$i);
			register_setting('breaking_news_hot_jamaica_options', 'breaking_news_hot_jamaica_type'.$i);
			register_setting('breaking_news_hot_jamaica_options', 'breaking_news_hot_jamaica_post_id'.$i);
			register_setting('breaking_news_hot_jamaica_options', 'breaking_news_hot_jamaica_post_title'.$i);
			register_setting('breaking_news_hot_jamaica_options', 'breaking_news_hot_jamaica_title'.$i);
			register_setting('breaking_news_hot_jamaica_options', 'breaking_news_hot_jamaica_img'.$i);
		}
	}
	add_action('admin_init', 'breaking_news_hot_jamaica_settings');
	
	function breaking_news_hot_jamaica($arg){
		for($i = 1; $i <= 8; $i++){
			if ($arg == 'breaking_news_hot_jamaica_cat'.$i) {return get_option('breaking_news_hot_jamaica_cat'.$i);}
			if ($arg == 'breaking_news_hot_jamaica_subcat'.$i) {return get_option('breaking_news_hot_jamaica_subcat'.$i);}
			if ($arg == 'breaking_news_hot_jamaica_type'.$i) {return get_option('breaking_news_hot_jamaica_type'.$i);}
			if ($arg == 'breaking_news_hot_jamaica_post_id'.$i) {return get_option('breaking_news_hot_jamaica_post_id'.$i);}
			if ($arg == 'breaking_news_hot_jamaica_post_title'.$i) {return get_option('breaking_news_hot_jamaica_post_title'.$i);}
			if ($arg == 'breaking_news_hot_jamaica_title'.$i) {return get_option('breaking_news_hot_jamaica_title'.$i);}
			if ($arg == 'breaking_news_hot_jamaica_img'.$i) {return get_option('breaking_news_hot_jamaica_img'.$i);}
		}
	};
?>