<?php
	// Out - post type
	function create_out(){
		register_post_type('out',
		array(
			'labels' => array(
				'name' => 'Outs',
						'singular_name' => 'Out',
						'add_new' => 'Add new',
						'add_new_item' => 'Add new out',
						'edit_item' => 'Edit out',
						'new_item' => 'New out',
						'all_items' => 'All outs',
						'view_item' => 'View out',
						'search_items' => 'Search outs',
						'not_found' => 'No out found',
						'not_found_in_trash' => 'No outs found in trash',
						'parent_item_colon' => '',
						'menu_name' => 'Outs'
					),
					'public' => true,
					'menu_client' => 5,
					'menu_position' => 4,
					'menu_icon' => 'dashicons-calendar-alt',
					'supports' => array('title', 'thumbnail', 'editor', 'author'),
					'show_in_rest' => true,
					'has_archive' => true,
					'rewrite' => array('slug' => 'out', 'with_front' => false)
				)
			);
	}
	add_action('init', 'create_out');

	add_action('admin_init', 'out_admin');
	function out_admin() {
		add_meta_box('out_meta_box',
			'Out details',
			'display_out_meta_box',
			'out', 'normal', 'high'
		);
	}

	function display_out_meta_box($out_post){
		$out_country = esc_html(get_post_meta($out_post->ID, 'out_country', true));
		$out_state = esc_html(get_post_meta($out_post->ID, 'out_state', true));
		$out_date = esc_html(get_post_meta($out_post->ID, 'out_date', true));
		$out_time = esc_html(get_post_meta($out_post->ID, 'out_time', true));
		$out_address = esc_html(get_post_meta($out_post->ID, 'out_address', true));
		$out_description = esc_html(get_post_meta($out_post->ID, 'out_description', true));
		$out_mapurl = esc_html(get_post_meta($out_post->ID, 'out_mapurl', true));
	?>
        <p class="label"><strong>Event date</strong><br>
		<input type="text" class="datepicker" name="out_date" value="<?php echo $out_date; ?>"></p>

		<p class="label"><strong>Event time</strong><br>
		<input type="text" class="timepicker" name="out_time" value="<?php echo $out_time; ?>"></p>

		<p class="label">
			<strong>Location</strong><br>
			<?php if($out_country != ''){ ?>
				<span class="set-location">
					<input type="hidden" name="out_country" value="<?php echo $out_country; ?>">
					<?php /*<input type="hidden" name="out_state" value="<?php echo $out_state; ?>">*/ ?>

					<?php echo $out_country; //if($out_state) echo ' / ' . $out_state; ?>
					<br>
					<a href="#0" class="js-change-location button-primary">Change location</a>
				</span>
			<?php } ?>
		</p>

		<div class="choose-location <?php if($out_country != '') echo 'hidden'; ?>">
			<select <?php if($out_country == '') echo 'name="out_country"'; ?> class="js-out-country">
				<option value="">Select a country</option>
				<option value="Anguilla">Anguilla</option>
				<option value="Antigua and Barbuda">Antigua and Barbuda</option>
				<option value="ABC Islands">ABC Islands</option>
				<option value="Barbados">Barbados</option>
				<option value="Bermuda">Bermuda</option>
				<option value="British Virgin Islands">British Virgin Islands</option>
				<option value="Cayman Islands">Cayman Islands</option>
				<option value="Dominica">Dominica</option>
				<option value="FWI">FWI</option>
				<option value="Grenada">Grenada</option>
				<option value="Guyana">Guyana</option>
				<option value="Haiti">Haiti</option>
				<option value="Jamaica">Jamaica</option>
				<option value="Montserrat">Montserrat</option>
				<option value="St. Kitts and Nevis">St. Kitts and Nevis</option>
				<option value="St. Lucia">St. Lucia</option>
				<option value="St. Martin">St. Martin</option>
				<option value="St. Vincent and the Grenadines">St. Vincent and the Grenadines</option>
				<option value="Suriname">Suriname</option>
				<option value="Trinidad and Tobago">Trinidad and Tobago</option>
				<option value="Turks and Caicos">Turks and Caicos</option>
			</select>
			
			<br>

			<?php /*<select if($out_state == '') echo 'name="out_state"'; ?> disabled="disabled" class="js-out-state"></select>*/ ?>
		</div>

		<p class="label"><strong>Full address</strong><br>
		<input type="text" name="out_address" value="<?php echo $out_address; ?>"></p>

		<p class="label"><strong>Map URL</strong><br>
		<input type="text" name="out_mapurl" placeholder="http://" value="<?php echo $out_mapurl; ?>"></p>

		<p class="label"><strong>Brief description</strong> <span class="obs">(Max of XXX characters)</span><br>
		<textarea name="out_description"><?php echo $out_description; ?></textarea></p>

		<p class="obs">
			<strong>Obs:</strong><br>
			The thumbnail dimensions are XXX by XXX pixels.
		</p>
	<?php
	}

	function add_out_fields($out_post_id, $out_post){
		if($out_post->post_type == 'out'){
			if(isset($_POST['out_country'])){
				update_post_meta($out_post_id, 'out_country', $_POST['out_country']);
			}
			if(isset($_POST['out_state'])){
				update_post_meta($out_post_id, 'out_state', $_POST['out_state']);
			}
			if(isset($_POST['out_date'])){
				update_post_meta($out_post_id, 'out_date', $_POST['out_date']);
			}
			if(isset($_POST['out_time'])){
				update_post_meta($out_post_id, 'out_time', $_POST['out_time']);
			}
			if(isset($_POST['out_address'])){
				update_post_meta($out_post_id, 'out_address', $_POST['out_address']);
			}
			if(isset($_POST['out_description'])){
				update_post_meta($out_post_id, 'out_description', $_POST['out_description']);
			}
			if(isset($_POST['out_mapurl'])){
				update_post_meta($out_post_id, 'out_mapurl', $_POST['out_mapurl']);
			}
		}
	}
	add_action('save_post', 'add_out_fields', 10, 2);
    
    function out_post_enqueue($hook){
        global $post_type;
        if($post_type == 'out'){
			wp_enqueue_style('out-post-style', get_template_directory_uri() . '/functions/out/out-post.css');
			wp_enqueue_script('out-config-js', get_template_directory_uri() . '/functions/out/out-config.js', array('jquery'));
			wp_enqueue_script('jquery-ui-datepicker');
			wp_enqueue_script('jquery-ui-slider');
			wp_enqueue_style('timepicker-style', get_template_directory_uri() . '/functions/timepicker/timepicker.css');
			wp_enqueue_script('timepicker-js', get_template_directory_uri() . '/functions/timepicker/timepicker.js', array('jquery', 'jquery-ui-datepicker', 'jquery-ui-slider'));
		}
	}
	add_action('admin_enqueue_scripts', 'out_post_enqueue');

	// Mostrar custom fields na lista de posts
	function add_outs_columns($columns){
		return array_merge($columns, array( 
			'location' => 'Location',
			'eventdate' => 'Event date'
		));
	}
	add_filter('manage_out_posts_columns', 'add_outs_columns');

 	function out_custom_column($column, $out_post_id){
		switch($column){
			case 'location':
				if((get_post_meta($out_post_id, 'out_country', true) !== '')){
					echo esc_html(get_post_meta($out_post_id, 'out_country', true));

					if((get_post_meta($out_post_id, 'out_state', true) !== '')){
						echo ' / ' . esc_html(get_post_meta($out_post_id, 'out_state', true));
					}
				} else {
					echo '–';
				}
			break;
			case 'eventdate':
				$event_date = get_post_meta($out_post_id, 'out_date', true);
				if($event_date){
					$event_date = new DateTime($event_date);
					echo $event_date = $event_date->format('d/m/Y');
				} else {
					echo '–';
				}
			break;
		}
	}
	add_action('manage_out_posts_custom_column', 'out_custom_column', 10, 2);

	// ordenar campos no dashboard
	function out_sortable_columns($columns){
		return array_merge($columns, array( 
			'location' => 'out_country',
			'eventdate' => 'out_date'
		));
	}
	add_filter('manage_edit-out_sortable_columns', 'out_sortable_columns');

	function out_posts_orderby($query){
		if(!is_admin() || ! $query->is_main_query()){
			return;
		}

		if('out_country' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'out_country');
		}

		if('out_date' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'out_date');
		}
	}
	add_action('pre_get_posts', 'out_posts_orderby');

	// Tirar Quick edit
	function out_disable_quick_edit($actions = array(), $post = null){
		// Abort if the post type is not "out"
		if (!is_post_type_archive('out')){
			return $actions;
		}
		if(isset($actions['inline hide-if-no-js'])){
			unset($actions['inline hide-if-no-js']);
		}
		return $actions;
	}
	add_filter('post_row_actions', 'out_disable_quick_edit', 10, 2);
?>