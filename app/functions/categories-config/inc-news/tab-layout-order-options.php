<?php
    // LAYOUT OUT ORDER - 4 POSTS
    function layout_order_news_add_options(){
		for($i = 1; $i <= 4; $i++){
			add_option('layout_order_news'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'layout_order_news_add_options');
	
	function layout_order_news_settings(){
		for($i = 1; $i <= 4; $i++){
			register_setting('layout_order_news_options', 'layout_order_news'.$i);
		}
	}
	add_action('admin_init', 'layout_order_news_settings');
	
	function layout_order_news($arg){
		for($i = 1; $i <= 4; $i++){
			if ($arg == 'layout_order_news'.$i) {return get_option('layout_order_news'.$i);}
		}
	};
?>