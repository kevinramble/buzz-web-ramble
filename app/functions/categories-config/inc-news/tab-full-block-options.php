<?php
    // BIG BLOCK - 1 POST
    function full_block_news_add_options(){
		add_option('full_block_news_cat', '', '', 'yes');
		add_option('full_block_news_subcat', '', '', 'yes');
		add_option('full_block_news_type', '', '', 'yes');
		add_option('full_block_news_post_id', '', '', 'yes');
		add_option('full_block_news_post_title', '', '', 'yes');
		add_option('full_block_news_title', '', '', 'yes');
		add_option('full_block_news_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'full_block_news_add_options');
	
	function full_block_news_settings(){
		register_setting('full_block_news_options', 'full_block_news_cat');
		register_setting('full_block_news_options', 'full_block_news_subcat');
		register_setting('full_block_news_options', 'full_block_news_type');
		register_setting('full_block_news_options', 'full_block_news_post_id');
		register_setting('full_block_news_options', 'full_block_news_post_title');
		register_setting('full_block_news_options', 'full_block_news_title');
		register_setting('full_block_news_options', 'full_block_news_img');
	}
	add_action('admin_init', 'full_block_news_settings');
	
	function full_block_news($arg){
		if ($arg == 'full_block_news_cat') {return get_option('full_block_news_cat');}
		if ($arg == 'full_block_news_subcat') {return get_option('full_block_news_subcat');}
		if ($arg == 'full_block_news_type') {return get_option('full_block_news_type');}
		if ($arg == 'full_block_news_post_id') {return get_option('full_block_news_post_id');}
		if ($arg == 'full_block_news_post_title') {return get_option('full_block_news_post_title');}
		if ($arg == 'full_block_news_title') {return get_option('full_block_news_title');}
		if ($arg == 'full_block_news_img') {return get_option('full_block_news_img');}
	};
?>