<?php
    // MAIN VIDEO - 4 POSTS
    function main_banner_hot_add_options(){
		for($i = 1; $i <= 4; $i++){
			add_option('main_banner_hot_cat'.$i, '', '', 'yes');
			add_option('main_banner_hot_subcat'.$i, '', '', 'yes');
			add_option('main_banner_hot_type'.$i, '', '', 'yes');
			add_option('main_banner_hot_post_id'.$i, '', '', 'yes');
			add_option('main_banner_hot_post_title'.$i, '', '', 'yes');
			add_option('main_banner_hot_title'.$i, '', '', 'yes');
			add_option('main_banner_hot_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'main_banner_hot_add_options');
	
	function main_banner_hot_settings(){
		for($i = 1; $i <= 4; $i++){
			register_setting('main_banner_hot_options', 'main_banner_hot_cat'.$i);
			register_setting('main_banner_hot_options', 'main_banner_hot_subcat'.$i);
			register_setting('main_banner_hot_options', 'main_banner_hot_type'.$i);
			register_setting('main_banner_hot_options', 'main_banner_hot_post_id'.$i);
			register_setting('main_banner_hot_options', 'main_banner_hot_post_title'.$i);
			register_setting('main_banner_hot_options', 'main_banner_hot_title'.$i);
			register_setting('main_banner_hot_options', 'main_banner_hot_img'.$i);
		}
	}
	add_action('admin_init', 'main_banner_hot_settings');
	
	function main_banner_hot($arg){
		for($i = 1; $i <= 4; $i++){
			if ($arg == 'main_banner_hot_cat'.$i) {return get_option('main_banner_hot_cat'.$i);}
			if ($arg == 'main_banner_hot_subcat'.$i) {return get_option('main_banner_hot_subcat'.$i);}
			if ($arg == 'main_banner_hot_type'.$i) {return get_option('main_banner_hot_type'.$i);}
			if ($arg == 'main_banner_hot_post_id'.$i) {return get_option('main_banner_hot_post_id'.$i);}
			if ($arg == 'main_banner_hot_post_title'.$i) {return get_option('main_banner_hot_post_title'.$i);}
			if ($arg == 'main_banner_hot_title'.$i) {return get_option('main_banner_hot_title'.$i);}
			if ($arg == 'main_banner_hot_img'.$i) {return get_option('main_banner_hot_img'.$i);}
		}
	};
?>