<?php
	function categories_config() {
		add_menu_page('Categories config', 'Categories config', 'manage_options', 'categories_config', 'categories_config_content', 'dashicons-welcome-widgets-menus', '4.2');
		add_submenu_page('categories_config', 'Categories config – Hot', 'Hot', 'manage_options', 'categories_config', 'categories_config');
		add_submenu_page('categories_config', 'Categories config – Life', 'Life', 'manage_options', 'categories_config_life', 'categories_config_life');
		add_submenu_page('categories_config', 'Categories config – News', 'News', 'manage_options', 'categories_config_news', 'categories_config_news');
	}
	add_action('admin_menu', 'categories_config');

	require_once('inc-hot/tab-main-banner-options.php');
	require_once('inc-hot/tab-trending-now-options.php');
	require_once('inc-hot/tab-full-block-options.php');
	require_once('inc-hot/tab-must-watch-options.php');
	require_once('inc-hot/tab-breaking-news-options.php');
	require_once('inc-hot/tab-layout-order-options.php');

	require_once('inc-life/tab-main-banner-options.php');
	require_once('inc-life/tab-trending-now-options.php');
	require_once('inc-life/tab-full-block-options.php');
	require_once('inc-life/tab-must-watch-options.php');
	require_once('inc-life/tab-breaking-news-options.php');
	require_once('inc-life/tab-layout-order-options.php');

	require_once('inc-news/tab-main-banner-options.php');
	require_once('inc-news/tab-trending-now-options.php');
	require_once('inc-news/tab-full-block-options.php');
	require_once('inc-news/tab-must-watch-options.php');
	require_once('inc-news/tab-breaking-news-options.php');
	require_once('inc-news/tab-layout-order-options.php');

	function categories_config_content(){ ?>
		<div class="wrap">
			<h2>Hot - Categories configurations</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>

						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'main-banner'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=categories_config&tab=main-banner" class="nav-tab <?php echo $active_tab == 'main-banner' ? 'nav-tab-active' : ''; ?>">Main banner</a>
							<a href="?page=categories_config&tab=trending-now" class="nav-tab <?php echo $active_tab == 'trending-now' ? 'nav-tab-active' : ''; ?>">Trending now</a>
							<a href="?page=categories_config&tab=full-block" class="nav-tab <?php echo $active_tab == 'full-block' ? 'nav-tab-active' : ''; ?>">Full block</a>
							<a href="?page=categories_config&tab=must-watch" class="nav-tab <?php echo $active_tab == 'must-watch' ? 'nav-tab-active' : ''; ?>">Must watch</a>
							<a href="?page=categories_config&tab=breaking-news" class="nav-tab <?php echo $active_tab == 'breaking-news' ? 'nav-tab-active' : ''; ?>">Breaking news</a>
							<a href="?page=categories_config&tab=layout-order" class="nav-tab <?php echo $active_tab == 'layout-order' ? 'nav-tab-active' : ''; ?>">Layout order</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'main-banner'){
								// TAB: MAIN BANNER
								require_once('inc-hot/tab-main-banner.php');
							
							} elseif($active_tab == 'trending-now'){
								// TAB: TRENDING NOW
								require_once('inc-hot/tab-trending-now.php');

							} elseif($active_tab == 'full-block'){
								// TAB: FULL BLOCK
								require_once('inc-hot/tab-full-block.php');

							} elseif($active_tab == 'must-watch'){
								// TAB: MUST WATCH
								require_once('inc-hot/tab-must-watch.php');

							} elseif($active_tab == 'breaking-news'){
								// TAB: BREAKING NEWS
								require_once('inc-hot/tab-breaking-news.php');

							} elseif($active_tab == 'layout-order'){
								// TAB: LAYOUT ORDER
								require_once('inc-hot/tab-layout-order.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // categories_config_content()

	function categories_config_life(){ ?>
	<div class="wrap">
		<h2>Life - Categories configurations</h2>
		
		<div id="poststuff">
			<div class="custom-admin-content video-config-options postbox">
				<div class="inside">
					<p class="description">Choose what you would like to edit.</p>

					<?php settings_errors(); ?>

					<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'main-banner'; ?>
					<div class="nav-tab-wrapper">
						<a href="?page=categories_config_life&tab=main-banner" class="nav-tab <?php echo $active_tab == 'main-banner' ? 'nav-tab-active' : ''; ?>">Main banner</a>
						<a href="?page=categories_config_life&tab=trending-now" class="nav-tab <?php echo $active_tab == 'trending-now' ? 'nav-tab-active' : ''; ?>">Trending now</a>
						<a href="?page=categories_config_life&tab=full-block" class="nav-tab <?php echo $active_tab == 'full-block' ? 'nav-tab-active' : ''; ?>">Full block</a>
						<a href="?page=categories_config_life&tab=must-watch" class="nav-tab <?php echo $active_tab == 'must-watch' ? 'nav-tab-active' : ''; ?>">Must watch</a>
						<a href="?page=categories_config_life&tab=breaking-news" class="nav-tab <?php echo $active_tab == 'breaking-news' ? 'nav-tab-active' : ''; ?>">Breaking news</a>
						<a href="?page=categories_config_life&tab=layout-order" class="nav-tab <?php echo $active_tab == 'layout-order' ? 'nav-tab-active' : ''; ?>">Layout order</a>
					</div>

					<form method="post" action="options.php">
						<?php if($active_tab == 'main-banner'){
							// TAB: MAIN BANNER
							require_once('inc-life/tab-main-banner.php');
						
						} elseif($active_tab == 'trending-now'){
							// TAB: TRENDING NOW
							require_once('inc-life/tab-trending-now.php');

						} elseif($active_tab == 'full-block'){
							// TAB: FULL BLOCK
							require_once('inc-life/tab-full-block.php');

						} elseif($active_tab == 'must-watch'){
							// TAB: MUST WATCH
							require_once('inc-life/tab-must-watch.php');

						} elseif($active_tab == 'breaking-news'){
							// TAB: BREAKING NEWS
							require_once('inc-life/tab-breaking-news.php');

						} elseif($active_tab == 'layout-order'){
							// TAB: LAYOUT ORDER
							require_once('inc-life/tab-layout-order.php');

						} ?>
					</form>
				</div><!-- .inside -->
			</div><!-- #video-config-options -->
		</div><!-- #poststuff -->
	</div><!-- .wrap -->
<?php }; // categories_config_content()

function categories_config_news(){ ?>
	<div class="wrap">
		<h2>News - Categories configurations</h2>
		
		<div id="poststuff">
			<div class="custom-admin-content video-config-options postbox">
				<div class="inside">
					<p class="description">Choose what you would like to edit.</p>

					<?php settings_errors(); ?>

					<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'main-banner'; ?>
					<div class="nav-tab-wrapper">
						<a href="?page=categories_config_news&tab=main-banner" class="nav-tab <?php echo $active_tab == 'main-banner' ? 'nav-tab-active' : ''; ?>">Main banner</a>
						<a href="?page=categories_config_news&tab=trending-now" class="nav-tab <?php echo $active_tab == 'trending-now' ? 'nav-tab-active' : ''; ?>">Trending now</a>
						<a href="?page=categories_config_news&tab=full-block" class="nav-tab <?php echo $active_tab == 'full-block' ? 'nav-tab-active' : ''; ?>">Full block</a>
						<a href="?page=categories_config_news&tab=must-watch" class="nav-tab <?php echo $active_tab == 'must-watch' ? 'nav-tab-active' : ''; ?>">Must watch</a>
						<a href="?page=categories_config_news&tab=breaking-news" class="nav-tab <?php echo $active_tab == 'breaking-news' ? 'nav-tab-active' : ''; ?>">Breaking news</a>
						<a href="?page=categories_config_news&tab=layout-order" class="nav-tab <?php echo $active_tab == 'layout-order' ? 'nav-tab-active' : ''; ?>">Layout order</a>
					</div>

					<form method="post" action="options.php">
						<?php if($active_tab == 'main-banner'){
							// TAB: MAIN BANNER
							require_once('inc-news/tab-main-banner.php');
						
						} elseif($active_tab == 'trending-now'){
							// TAB: TRENDING NOW
							require_once('inc-news/tab-trending-now.php');

						} elseif($active_tab == 'full-block'){
							// TAB: FULL BLOCK
							require_once('inc-news/tab-full-block.php');

						} elseif($active_tab == 'must-watch'){
							// TAB: MUST WATCH
							require_once('inc-news/tab-must-watch.php');

						} elseif($active_tab == 'breaking-news'){
							// TAB: BREAKING NEWS
							require_once('inc-news/tab-breaking-news.php');

						} elseif($active_tab == 'layout-order'){
							// TAB: LAYOUT ORDER
							require_once('inc-news/tab-layout-order.php');

						} ?>
					</form>
				</div><!-- .inside -->
			</div><!-- #video-config-options -->
		</div><!-- #poststuff -->
	</div><!-- .wrap -->
<?php }; // categories_config_content() ?>