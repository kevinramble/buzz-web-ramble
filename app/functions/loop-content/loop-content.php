<?php
	// Loops: job / usar na ordem cronológica e no load more
	function loop_joblisting($postID, $layout = null){
		$job_company = esc_html(get_post_meta($postID, 'job_company', true));
		$job_article = esc_html(get_post_meta($postID, 'job_article', true));
		$job_position = esc_html(get_post_meta($postID, 'job_position', true));
		$job_expiration = esc_html(get_post_meta($postID, 'job_expiration', true));
		$job_country = esc_html(get_post_meta($postID, 'job_country', true));
		if(has_post_thumbnail($postID)) $image = get_the_post_thumbnail_url($postID);

		$classlayout = ($layout == 'dark') ? 'c-item-job--outline' : '';
		$classApplied = (is_appliedjob($postID)) ? 'c-item-job--active' : '';
		$pos_article = ($job_article) ? $job_article . ' ' : ''; ?>

		<a href="<?php the_permalink($postID); ?>" class="c-item-job mb-1 mb-md-2 <?php echo $classlayout .' '. $classApplied; ?>">
			<div style="background-image: url('<?php echo $image; ?>')" class="c-item-job__thumb"></div>
			<div class="c-item-job__content">
				<p class="c-item-job__description"><strong><?php echo $job_company; ?></strong> is looking for <?php echo $pos_article; ?><strong><?php echo $job_position; ?></strong> in <?php echo $job_country; ?></p>
				<p class="c-item-job__date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) .' ago'; ?></p>
			</div>
		</a>
	<?php } // loop_joblisting

	// Loops: offer / usar na ordem cronológica e no load more
	function loop_offerlisting($postID, $amout = null){
		$offer_country = esc_html(get_post_meta($postID, 'offer_country', true));
		$offer_state = esc_html(get_post_meta($postID, 'offer_state', true));
		$offer_description = esc_html(get_post_meta($postID, 'offer_description', true));
		$offer_fullprice = esc_html(get_post_meta($postID, 'offer_fullprice', true));
		$offer_discount = esc_html(get_post_meta($postID, 'offer_discount', true));
		$offer_price = esc_html(get_post_meta($postID, 'offer_price', true));
		$offer_expiration = esc_html(get_post_meta($postID, 'offer_expiration', true));
		$offer_category = wp_get_post_terms($postID, 'offer-category', array('fields' => 'all'));
		$offer_company = esc_html(get_post_meta($postID, 'offer_company', true));

		if($offer_description == ''){
			$offer_description = get_the_excerpt($postID);
			$offer_description = substr($offer_description, 0, 130) . '...';
		}

		$expiration = remainingTime($offer_expiration);
		if($expiration < 0){
			$expirationText = 'Deal expired';
		} elseif($expiration == 0) {
			$expirationText = 'Deal ends <strong>today</strong>';
		} elseif($expiration > 0) {
			$expirationText = 'Ends within <strong>'. $expiration .' days</strong>';
		}

		if(has_post_thumbnail($postID)){
			$image = get_the_post_thumbnail_url($postID);
		} else {
			$gallery = get_post_meta($postID, 'post_gallery_id', true);
			if($gallery){
				$image = wp_get_attachment_url($gallery[0]);
			} else {
				$image = get_bloginfo('template_directory') . '/img/content/nothing.png';
			}
		}

		$classAmout = ($amout == '3') ? 'col-lg-4' : 'col-lg-3'; ?>

		<div class="col-md-6 <?php echo $classAmout; ?>">
			<div class="c-card-post">
				<div class="c-card-post__wrap_thumb">
					<a href="<?php the_permalink($postID); ?>" class="c-card-post__thumb" style="background-image:url('<?php echo $image; ?>')"></a>

					<div class="c-card-post__hover">
						<div class="c-social c-social--light">
							<div class="c-social__book" aria-label="Bookmark"><?php the_favorites_button($postID, get_current_blog_id()); ?></div>
							<a href="#0" class="c-social__icon c-social__icon--share js-toggle-share" aria-label="Share"></a>
							<div class="c-social__sublist js-box-share">
								<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink($postID); ?>" target="_blank" class="c-social__icon c-social__icon--fb-bg c-social__icon--small" aria-label="Facebook"></a>
								<a href="http://twitter.com/share?text=<?php echo get_the_title($postID); ?>&url=<?php the_permalink($postID); ?>" target="_blank" class="c-social__icon c-social__icon--tw-bg c-social__icon--small" aria-label="Twitter"></a>
							</div>
						</div>
					</div>
				</div>
				<a href="<?php the_permalink($postID); ?>" class="c-card-post__info">
					<p class="c-card-post__tag"><?php echo $offer_category[0]->name; ?></p>
					<p class="c-card-post__deal"><?php echo $expirationText; ?></p>

					<p class="c-card-post__description"><?php echo get_the_title($postID); ?></p>
					<p class="c-card-post__company"><?php echo $offer_company; ?></p>
					<p class="c-card-post__location"><?php if($offer_state) echo $offer_state . ', '; echo $offer_country; ?></p>
					<p class="c-card-post__offer"><?php echo $offer_description; ?></p>

					<p class="c-card-post__price">
					<?php if($offer_discount > 0){ ?>
						<span class="c-card-post__price-off"><?php echo $offer_discount; ?>% OFF</span>
					<?php } ?>
						<?php /*<span class="c-card-post__price-old">$<?php echo $offer_fullprice; ?></span>
						<span class="c-card-post__price-current">$<?php echo $offer_price; ?></span> */ ?>
					</p>
				</a>
			</div>
		</div>
	<?php } // loop_offerlisting

	function loop_genericcard($postID, $amout = null){
		foreach((get_the_category()) as $category){
			if ($category->category_parent == 0){
				$parentcategory = $category->name;
				$catslug = $category->slug;
			}
		}

		$subcategory = wp_get_post_terms($postID, 'sub-category', array('fields' => 'all'));
		$subcategoryName = '';
		$subcategoryID = '';

		if($subcategory) {
			$subcategoryID = $subcategory[0]->term_id;
			$subcategoryName = $subcategory[0]->name;
		}

		$category = ($subcategoryName) ? $subcategoryName : $parentcategory;

		if(has_post_thumbnail($postID)){
			$image = get_the_post_thumbnail_url($postID);
		} else {
			$image = catch_that_image($postID);
		}

		$classAmout = ($amout == '4') ? 'col-lg-3' : 'col-lg-4'; ?>

		<div class="col-md-6 <?php echo $classAmout; ?>">
			<div class="c-card-post">
				<div class="c-card-post__wrap_thumb <?php echo $is_video = (get_post_type($postID) == 'video') ? 'is-video' : '' ; ?>">
					<?php
					$country = wp_get_post_terms($postID, 'countries', array('fields' => 'all'));
					$countries = count($country);
					
					if($country) : ?>
						<!-- <span class="c-country-tag">
							<?php for($i = 0; $i < $countries; $i++){
								if($i > 0) echo ' / ';
								echo $country[$i]->name;							
							} ?>
						</span> -->
					<?php endif; ?>

					<a href="<?php the_permalink($postID); ?>" class="c-card-post__thumb" style="background-image:url(<?php echo $image; ?>)"></a>

					<div class="c-card-post__hover">
						<div class="c-social c-social--light">
							<?php if(get_post_type($postID) == 'post') : ?>
								<div class="c-social__book" aria-label="Bookmark"><?php the_favorites_button($postID, get_current_blog_id()); ?></div>
							<?php endif; ?>
							<a href="#0" class="c-social__icon c-social__icon--share js-toggle-share" aria-label="Share"></a>
							<div class="c-social__sublist js-box-share">
								<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink($postID); ?>" target="_blank" class="c-social__icon c-social__icon--fb-bg c-social__icon--small" aria-label="Facebook"></a>
								<a href="http://twitter.com/share?text=<?php echo get_the_title($postID); ?>&url=<?php the_permalink($postID); ?>" target="_blank" class="c-social__icon c-social__icon--tw-bg c-social__icon--small" aria-label="Twitter"></a>
							</div>
						</div>
					</div>
				</div>
				<a href="<?php the_permalink($postID); ?>" class="c-card-post__info">
					<span class="c-card-post__tag cat-<?php echo $catslug; ?>"><?php echo $category ?></span>
					<p class="c-card-post__description"><?php echo get_the_title($postID); ?></p>
					<p class="c-card-post__subdescription">
						<?php
							if(get_post_type($postID) == 'post'){
								$excerpt = get_the_excerpt($postID);
								$excerpt = substr($excerpt, 0, 130);

								if(has_excerpt()){
									echo $excerpt;
								} else {
									echo $excerpt . '...';
								}
							}
						?>
					</p>
					<div class="c-card-post__author">
						<p class="c-card-post__author__txt">by <strong><?php echo get_the_author_meta('display_name'); ?></strong></p>
						<p class="c-card-post__author__txt"><?php echo get_the_date('M d, Y'); ?></p>
					</div>
				</a>
			</div>
		</div>
	<?php } //loop_genericcard

	function loop_genericcard_small($postID){
		if(has_post_thumbnail($postID)){
			$image = get_the_post_thumbnail_url($postID);
		} else {
			$image = catch_that_image($postID);
		} ?>
		<div class="col-md-6 col-lg-3">
			<a href="<?php the_permalink($postID); ?>" title="<?php echo get_the_title($postID); ?>" class="c-card-all-stories">
				<div class="c-card-all-stories__wrap_thumb <?php echo $is_video = (get_post_type($postID) == 'video') ? 'is-video' : '' ; ?>">
					<?php
					$country = wp_get_post_terms($postID, 'countries', array('fields' => 'all'));
					$countries = count($country);

					//if($country) : ?>
						<!-- <span class="c-country-tag">
							<?php for($i = 0; $i < $countries; $i++){
								if($i > 0) echo ' / ';
								echo $country[$i]->name;							
							} ?>
						</span> -->
					<?php // endif; ?>

					<div class="c-card-all-stories__thumb" style="background-image:url('<?php echo $image; ?>')"></div>
				</div>
				<div>
					<p class="c-card-all-stories__tag">
						<?php $subcategory = wp_get_post_terms($postID, 'sub-category');
						if($subcategory){
							echo $subcategory[0]->name;
						} else {
							echo get_the_category($postID)[0]->name;
						} ?>
					</p>
					<p class="c-card-all-stories__description"><?php echo get_the_title($postID); ?></p>
					<div class="c-card-all-stories__author">
						<p class="c-card-all-stories__author__txt">by <span><?php echo get_the_author_meta('display_name'); ?></span></p>
						<p class="c-card-all-stories__author__txt"><?php echo get_the_date('M d, Y'); ?></p>
					</div>
				</div>
			</a>
		</div>
	<?php } //loop_genericcard_small

	function loop_redeemedoffer($postID){
		$offer_country = esc_html(get_post_meta($postID, 'offer_country', true));
		$offer_state = esc_html(get_post_meta($postID, 'offer_state', true));
		$offer_description = esc_html(get_post_meta($postID, 'offer_description', true));
		$offer_code = esc_html(get_post_meta($postID, 'offer_code', true));
		$offer_expiration = esc_html(get_post_meta($postID, 'offer_expiration', true));
		$offer_category = wp_get_post_terms($postID, 'offer-category', array('fields' => 'all'));
		$offer_company = esc_html(get_post_meta($postID, 'offer_company', true));

		if($offer_description == ''){
			$offer_description = get_the_excerpt($postID);
			$offer_description = substr($offer_description, 0, 130) . '...';
		}

		$dealExpired = false;
		$expiration = remainingTime($offer_expiration);
		if($expiration < 0){
			$dealExpired = true;
			$expirationText = 'Deal expired';
		} elseif($expiration == 0) {
			$expirationText = 'Deal ends <strong>today</strong>';
		} elseif($expiration > 0) {
			$expirationText = 'Ends within <strong>'. $expiration .' days</strong>';
		}

		if(has_post_thumbnail($postID)){
			$image = get_the_post_thumbnail_url($postID);
		} else {
			$gallery = get_post_meta($postID, 'post_gallery_id', true);
			if($gallery){
				$image = wp_get_attachment_url($gallery[0]);
			} else {
				$image = get_bloginfo('template_directory') . '/img/content/nothing.png';
			}
		} ?>
		<div class="col-md-6 col-lg-3">
			<div class="c-card-post">
				<div class="c-card-post__wrap_thumb">
					<a href="<?php the_permalink($postID); ?>" class="c-card-post__thumb" style="background-image:url('<?php echo $image; ?>')"></a>

					<div class="c-card-post__hover">
						<div class="c-social c-social--light">
							<div class="c-social__book" aria-label="Bookmark"><?php the_favorites_button($postID, get_current_blog_id()); ?></div>
							<a href="#0" class="c-social__icon c-social__icon--share js-toggle-share" aria-label="Share"></a>
							<div class="c-social__sublist js-box-share">
								<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink($postID); ?>" target="_blank" class="c-social__icon c-social__icon--fb-bg c-social__icon--small" aria-label="Facebook"></a>
								<a href="http://twitter.com/share?text=<?php echo get_the_title($postID); ?>&url=<?php the_permalink($postID); ?>" target="_blank" class="c-social__icon c-social__icon--tw-bg c-social__icon--small" aria-label="Twitter"></a>
							</div>
						</div>
					</div>
				</div>
				<div class="c-card-post__info">
					<a href="<?php the_permalink($postID); ?>">
						<p class="c-card-post__tag"><?php echo $offer_category[0]->name; ?></p>
						<p class="c-card-post__deal"><?php echo $expirationText; ?></p>
						<p class="c-card-post__description"><?php echo get_the_title($postID); ?></p>
						<p class="c-card-post__company"><?php echo $offer_company; ?></p>
						<p class="c-card-post__location"><?php if($offer_state) echo $offer_state . ', '; echo $offer_country; ?></p>
					</a>

					<?php if(!$dealExpired) : ?>
						<div class="c-card-post__code">
							<p class="c-card-post__code-number js-box-copy"><?php echo $offer_code; ?></p>
							<button class="c-card-post__code-btn js-toggle-copy">Copy</button>
						</div>
					<?php else : ?>
						<div class="c-card-post__code is-off">
							<p class="c-card-post__code-number"><?php echo $offer_code; ?></p>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php } //loop_redeemedoffer

	// Loops: out / usar na ordem cronológica e no load more
	function loop_outlisting($postID, $amout = null){
		$out_date = esc_html(get_post_meta($postID, 'out_date', true));
		$new_out_date = date('M d', strtotime($out_date));
		$out_country = esc_html(get_post_meta($postID, 'out_country', true));
		$out_state = esc_html(get_post_meta($postID, 'out_state', true));
		$out_description = esc_html(get_post_meta($postID, 'out_description', true));
		$out_category = wp_get_post_terms($postID, 'out-category', array('fields' => 'all'));

		if($out_description == ''){
			$out_description = get_the_excerpt($postID);
			$out_description = substr($out_description, 0, 130) . '...';
		}

		if(has_post_thumbnail($postID)){
			$image = get_the_post_thumbnail_url($postID);
		} else {
			$gallery = get_post_meta($postID, 'post_gallery_id', true);
			if($gallery){
				$image = wp_get_attachment_url($gallery[0]);
			} else {
				$image = get_bloginfo('template_directory') . '/img/content/nothing.png';
			}
		}

		$classAmout = ($amout == '3') ? 'col-lg-4' : 'col-lg-3'; ?>

		<div class="col-md-6 <?php echo $classAmout; ?>">
			<div class="c-card-post">
				<div class="c-card-post__wrap_thumb">
					<a href="<?php the_permalink($postID); ?>"class="c-card-post__thumb" style="background-image:url('<?php echo $image; ?>')"></a>

					<div class="c-card-post__hover">
						<div class="c-social c-social--light">
							<div class="c-social__book" aria-label="Bookmark"><?php the_favorites_button($postID, get_current_blog_id()); ?></div>
							<a href="#0" class="c-social__icon c-social__icon--share js-toggle-share" aria-label="Share"></a>
							<div class="c-social__sublist js-box-share">
								<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink($postID); ?>" target="_blank" class="c-social__icon c-social__icon--fb-bg c-social__icon--small" aria-label="Facebook"></a>
								<a href="http://twitter.com/share?text=<?php echo get_the_title($postID); ?>&url=<?php the_permalink($postID); ?>" target="_blank" class="c-social__icon c-social__icon--tw-bg c-social__icon--small" aria-label="Twitter"></a>
							</div>
						</div>
					</div>
				</div>
				<a href="<?php the_permalink($postID); ?>" class="c-card-post__info">
					<p class="c-card-post__tag"><?php echo $out_category[0]->name; ?></p>
					<p class="c-card-post__date"><?php echo $new_out_date; ?></p>
					<p class="c-card-post__description"><?php echo get_the_title($postID); ?></p>
					<p class="c-card-post__location"><?php if($out_state) echo $out_state . ', '; echo $out_country; ?></p>
					<p class="c-card-post__event"><?php echo $out_description; ?></p>
				</a>
			</div>
		</div>
<?php } // loop_outlisting
?>