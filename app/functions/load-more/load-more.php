<?php
    function loadmore_button($load_posts, $load_current_page, $load_max_page){ ?>
        <script>
            var posts = '<?php echo json_encode($load_posts) ?>',
                current_page = <?php echo $load_current_page ?>,
                max_page = <?php echo $load_max_page ?>
        </script>
    <?php }

    function loadmore_ajax_handler(){
		$args = json_decode(stripslashes($_POST['query']), true);
		$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
		$args['post_status'] = 'publish';
		query_posts($args);
		global $post;

		// post type "post"
		if(($args['post_type'] == '' || $args['post_type'] == 'post') && ($args['childcat'] != 1) && ($args['isfavorites'] != 1) && ($args['s'] == '') && have_posts()) :
			while(have_posts()): the_post();
				loop_genericcard_small($post->ID);
			endwhile;

		// post type "video"
		elseif($args['post_type'] == 'video' && ($args['s'] == '') && have_posts()) :
			while(have_posts()): the_post();
				loop_genericcard_small($post->ID);
			endwhile;

		// post type "job"
		elseif($args['post_type'] == 'job' && have_posts()) :
			while(have_posts()): the_post();
				loop_joblisting($post->ID);
			endwhile;

		// listagem das child categories
		elseif($args['childcat'] == 1 && have_posts()) :
			while(have_posts()): the_post();
				loop_genericcard($post->ID, '4');
			endwhile;

		// post type "offer"
		elseif($args['isfavorites'] != 1 && $args['isoffers'] != 1 && $args['post_type'] == 'offer' && have_posts()) :
			while(have_posts()): the_post();
				loop_offerlisting($post->ID);
			endwhile;

		// post type "offer", but on favorites
		elseif($args['isfavorites'] == 1 && $args['post_type'] == 'offer' && have_posts()) :
			while(have_posts()): the_post();
				loop_offerlisting($post->ID, '3');
			endwhile;

		// post type "offer", but redeemed
		elseif($args['isoffers'] == 1 && $args['post_type'] == 'offer' && have_posts()) :
			while(have_posts()): the_post();
				loop_redeemedoffer($post->ID);
			endwhile;

		// post type "out"
		elseif($args['isfavorites'] != 1 && $args['post_type'] == 'out' && have_posts()) :
			while(have_posts()): the_post();
				loop_outlisting($post->ID);
			endwhile;

		// post type "out", but on favorites
		elseif($args['isfavorites'] == 1 && $args['post_type'] == 'out' && have_posts()) :
			while(have_posts()): the_post();
				loop_outlisting($post->ID, '3');
			endwhile;

		// all SEARCH
		elseif($args['s'] != '' && have_posts()) :
			while(have_posts()): the_post();
				loop_genericcard($post->ID);
			endwhile;

		elseif($args['isfavorites'] == 1 && ($args['post_type'] == '' || $args['post_type'] == 'post') && have_posts()) :
			while(have_posts()): the_post();
				loop_genericcard($post->ID);
			endwhile;

		endif;
		die; // here we exit the script and no wp_reset_query() required!
	}
	add_action('wp_ajax_loadmore', 'loadmore_ajax_handler'); // wp_ajax_{action}
	add_action('wp_ajax_nopriv_loadmore', 'loadmore_ajax_handler'); // wp_ajax_nopriv_{action}
?>