jQuery(document).ready(function($) {
	if ($('.js-loadmore').length > 0) {
		$('.js-loadmore').click(function() {
			if (current_page == 0) current_page = 1

			var button = $(this),
				data = {
					action: 'loadmore',
					query: posts,
					page: current_page
				}

			$.ajax({
				url: loadmore_params.ajaxurl,
				data: data,
				type: 'POST',
				beforeSend: function(xhr) {
					button.text('Loading...').addClass('loading')
				},
				success: function(data) {
					if (data) {
						$('.js-posts-list').append(data)
						button.text('Load more').removeClass('loading')
						current_page++

						if (current_page == max_page) button.removeClass('loading').addClass('hidden') // if last page, remove the button
					} else {
						button.removeClass('loading').addClass('hidden')
					}
				}
			})
		})
	}
})
