<?php
	function home_config() {
		add_menu_page('Home config', 'Home config', 'manage_options', 'home_config', 'home_config_content', 'dashicons-format-gallery', '4');
		add_submenu_page('home_config', 'Home config – Content', 'Content', 'manage_options', 'home_config', 'home_config');
		add_submenu_page('home_config', 'Home config – Layout', 'Layout', 'manage_options', 'home_config_layout', 'home_layout_content');
		add_submenu_page('home_config', 'Home config – Poll', 'Poll', 'manage_options', 'home_config_poll', 'home_poll_content');
		add_submenu_page('home_config', 'Home config – Leaderboard ad', 'Leaderboard ad', 'manage_options', 'home_config_leaderboard', 'home_leaderboard_content');
	}
	add_action('admin_menu', 'home_config');

	require_once('inc-content/tab-main-banner-options.php');
	require_once('inc-content/tab-hot-now-options.php');
	require_once('inc-content/tab-featured-stories-options.php');
	require_once('inc-content/tab-editors-choice-options.php');
	require_once('inc-content/tab-categories-block-options.php');
	require_once('inc-content/tab-big-block-options.php');
	require_once('inc-content/tab-top-ten-options.php');
	require_once('inc-content/tab-ten-content-one-options.php');
	require_once('inc-content/tab-ten-content-two-options.php');
	require_once('inc-content/tab-ten-content-three-options.php');

	require_once('inc-layout/tab-hero-layout-options.php');
	require_once('inc-layout/tab-home-order-options.php');

	require_once('inc-poll/home-poll-options.php');

	require_once('inc-leaderboard/home-leaderboard-options.php');

	function home_config_content(){ ?>
		<div class="wrap">
			<h2>Home content configurations</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content home-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>

						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'main-banner'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=home_config&tab=main-banner" class="nav-tab <?php echo $active_tab == 'main-banner' ? 'nav-tab-active' : ''; ?>">Hero section</a>
							<a href="?page=home_config&tab=hot-now" class="nav-tab <?php echo $active_tab == 'hot-now' ? 'nav-tab-active' : ''; ?>">Block 1</a>
							<a href="?page=home_config&tab=featured-stories" class="nav-tab <?php echo $active_tab == 'featured-stories' ? 'nav-tab-active' : ''; ?>">Block 2</a>
							<a href="?page=home_config&tab=editors-choice" class="nav-tab <?php echo $active_tab == 'editors-choice' ? 'nav-tab-active' : ''; ?>">Block 3</a>
							<a href="?page=home_config&tab=categories-block1" class="nav-tab <?php echo $active_tab == 'categories-block1' || $active_tab == 'categories-block2' || $active_tab == 'categories-block3' || $active_tab == 'categories-block4' ? 'nav-tab-active' : ''; ?>">Block 4</a>
							<a href="?page=home_config&tab=big-block" class="nav-tab <?php echo $active_tab == 'big-block' ? 'nav-tab-active' : ''; ?>">Block 5</a>
							<a href="?page=home_config&tab=top-ten" class="nav-tab <?php echo $active_tab == 'top-ten' ? 'nav-tab-active' : ''; ?>">Block 8</a>
							<a href="?page=home_config&tab=documentation" class="nav-tab <?php echo $active_tab == 'documentation' ? 'nav-tab-active' : ''; ?>">Documentation</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'main-banner'){
								// TAB: MAIN BANNER
								require_once('inc-content/tab-main-banner.php');
							
							} elseif($active_tab == 'hot-now'){
								// TAB: HOT NOW
								require_once('inc-content/tab-hot-now.php');

							} elseif($active_tab == 'featured-stories'){
								// TAB: FEATURED STORIES 
								require_once('inc-content/tab-featured-stories.php');

							} elseif($active_tab == 'editors-choice'){
								// TAB: EDITOR'S CHOICE 
								require_once('inc-content/tab-editors-choice.php');

							} elseif($active_tab == 'categories-block1'){
								// TAB: CATEGORIES BLOCK 
								require_once('inc-content/tab-categories-block1.php');

							} elseif($active_tab == 'categories-block2'){
								// TAB: CATEGORIES BLOCK 
								require_once('inc-content/tab-categories-block2.php');

							} elseif($active_tab == 'categories-block3'){
								// TAB: CATEGORIES BLOCK 
								require_once('inc-content/tab-categories-block3.php');

							} elseif($active_tab == 'categories-block4'){
								// TAB: CATEGORIES BLOCK 
								require_once('inc-content/tab-categories-block4.php');

							} elseif($active_tab == 'big-block'){
								// TAB: EDITOR'S CHOICE 
								require_once('inc-content/tab-big-block.php');

							} elseif($active_tab == 'ten-content-one'){
								// TAB: TEN CONTENT .1
								require_once('inc-content/tab-ten-content-one.php');

							} elseif($active_tab == 'ten-content-two'){
								// TAB: TEN CONTENT .2
								require_once('inc-content/tab-ten-content-two.php');

							} elseif($active_tab == 'ten-content-three'){
								// TAB: TEN CONTENT .3
								require_once('inc-content/tab-ten-content-three.php');

							} elseif($active_tab == 'top-ten'){
								// TAB: EDITOR'S CHOICE 
								require_once('inc-content/tab-top-ten.php');

							} elseif($active_tab == 'documentation'){
								// TAB: DOCUMENTATION
								require_once('inc-content/tab-documentation.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #home-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // home_config_content()

	function home_layout_content(){ ?>
		<div class="wrap">
			<h2>Home layout configurations</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content home-layout-options postbox">
					<div class="inside">
						<p class="description">Choose the order in which the sections appear.</p>

						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'hero-layout'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=home_config_layout&tab=hero-layout" class="nav-tab <?php echo $active_tab == 'hero-layout' ? 'nav-tab-active' : ''; ?>">Hero section layout</a>
							<a href="?page=home_config_layout&tab=home-order" class="nav-tab <?php echo $active_tab == 'home-order' ? 'nav-tab-active' : ''; ?>">Home order</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'hero-layout'){
								// TAB: HERO LAYOUT
								require_once('inc-layout/tab-hero-layout.php');
							
							} elseif($active_tab == 'home-order'){
								// TAB: HOME ORDER
								require_once('inc-layout/tab-home-order.php');

							} ?>
						</form>

					</div><!-- .inside -->
				</div><!-- .home-layout-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php } //home_layout_content

	function home_poll_content(){ ?>
		<div class="wrap">
			<h2>Home poll configurations</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content home-poll-options postbox">
					<div class="inside">
						<?php settings_errors(); ?>

						<form method="post" action="options.php">
							<div class="content-wrapper post-manager">
								<?php settings_fields('home_poll_options'); ?>

								<p style="font-weight:bold; font-size:11px;">Poll currently <?php echo get_option('home_poll_status') == '' || get_option('home_poll_shortcode') == '' ? '<span style="color:red;">inactive</span>' : '<span style="color:green;">active</span>'; ?>.</p>

								<div class="poll-slot <?php echo get_option('home_poll_status') == '' || get_option('home_poll_shortcode') == '' ? 'empty' : ''; ?>">
									<?php $checked = (get_option('home_poll_status')) ? 'checked' : ''; ?>
									<label><input type="checkbox" class="checkbox" value="1" name="home_poll_status" <?php echo $checked; ?>> Show poll</label>
									<label class="shortcode">
										<strong>Poll shortcode</strong> <a href="post-new.php?post_type=poll" target="_blank" class="obs">(Create poll)</a><br>
										<input name="home_poll_shortcode" id="home_poll_shortcode" value="<?php echo esc_html(get_option('home_poll_shortcode')); ?>">
									</label>
								</div><!-- .ad-slot -->

								<input class="button-primary" style="display:block;" type="submit" name="Save" value="Save changes">
							</div>
						</form>
					</div><!-- .inside -->
				</div><!-- .home-poll-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php } //home_poll_content

	function home_leaderboard_content(){ ?>
		<div class="wrap">
			<h2>Leaderboard ad</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content home-poll-options postbox">
					<div class="inside">
						<?php settings_errors(); ?>

						<form method="post" action="options.php">
							<div class="content-wrapper post-manager">
								<?php settings_fields('home_leaderboard_options'); ?>

								<p style="font-weight:bold; font-size:11px;">Leaderboard ad is currently <?php echo get_option('home_leaderboard_status') == '' ? '<span style="color:red;">inactive</span>' : '<span style="color:green;">active</span>'; ?>.</p>

								<div class="poll-slot <?php echo get_option('home_leaderboard_status') == '' ? 'empty' : ''; ?>">
									<?php $checked = (get_option('home_leaderboard_status')) ? 'checked' : ''; ?>
									<label><input type="checkbox" class="checkbox" value="1" name="home_leaderboard_status" <?php echo $checked; ?>> Show Leaderboard ad</label>
								</div><!-- .ad-slot -->

								<input class="button-primary" style="display:block;" type="submit" name="Save" value="Save changes">
							</div>
						</form>
					</div><!-- .inside -->
				</div><!-- .home-poll-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php } // home_leaderboard_content
?>