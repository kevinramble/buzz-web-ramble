<?php
    // MAIN BANNER - 5 POSTS
    function home_leaderboard_add_options(){
		add_option('home_leaderboard_status', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'home_leaderboard_add_options');
	
	function home_leaderboard_settings(){
		register_setting('home_leaderboard_options', 'home_leaderboard_status');
	}
	add_action('admin_init', 'home_leaderboard_settings');
	
	function home_leaderboard($arg){
		if ($arg == 'home_leaderboard_status') {return get_option('home_leaderboard_status');}
	};
?>