<?php settings_fields('hero_layout_options'); ?>

<div class="content-wrapper" id="hero_layout">
    <h3>Hero layout</h3>

    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit eveniet excepturi illo ad quos ut incidunt sapiente.</p>

    <div class="choose-hero">
        <input type="radio" class="radio" id="radio1" name="hero_layout_version" value="1" <?php echo (get_option('hero_layout_version') === "1") ? 'checked="checked"' : ''; ?>>
        <label for="radio1">
            <span>Version 1</span>
            <img src="<?php echo get_template_directory_uri() . '/functions/home-config/inc-layout/img/hero1.jpg'; ?>">
        </label>

        <input type="radio" class="radio" id="radio2" name="hero_layout_version" value="2" <?php echo (get_option('hero_layout_version') === "2") ? 'checked="checked"' : ''; ?>>
        <label for="radio2">
            <span>Version 2</span>
            <img src="<?php echo get_template_directory_uri() . '/functions/home-config/inc-layout/img/hero2.jpg'; ?>">
        </label>
    </div><!-- .choose-hero -->

    <input class="button-primary" type="submit" name="Save" value="Save">
</div>