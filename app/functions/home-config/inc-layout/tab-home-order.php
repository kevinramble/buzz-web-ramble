<?php settings_fields('home_order_options'); ?>

<div class="content-wrapper" id="home_order">
    <h3>Home order</h3>

    <p>Sit eveniet excepturi illo ad quos ut incidunt sapiente. Dicta vel omnis odio ipsa illum, autem cupiditate, aspernatur, non unde odit distinctio. Dolor sit amet consectetur adipisicing elit, fuga eius placeat itaque. Ut nisi dicta nemo animi doloremque dolorum nesciunt pariatur exercitationem quisquam. Distinctio maxime, quidem illum amet ratione.</p>

    <div class="orders">
        <?php for($i = 1; $i <= 8; $i++){
            $optionValue = get_option('home_order'. $i);
            if($optionValue == '') update_option('home_order'. $i, $i);
        ?>
            <div class="block" data-position="<?php echo $i; ?>">
                <!-- <h4>Position <?php echo $i; ?></h4> -->
                <p>Block <strong><?php echo $optionValue; ?></strong></p>
                <img src="<?php echo get_template_directory_uri() . '/functions/home-config/inc-layout/img/block'. $optionValue.'.jpg"'; ?>>
                <input type="hidden" class="input-order" name="home_order<?php echo $i; ?>" value="<?php echo $optionValue; ?>">
            </div>
        <?php } //for ?>
    </div>

    <input class="button-primary" type="submit" name="Save" value="Save">
</div>