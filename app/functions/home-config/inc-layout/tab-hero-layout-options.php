<?php
    // HERO LAYOUT - 1 POST
    function hero_layout_add_options(){
		add_option('hero_layout_version', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'hero_layout_add_options');
	
	function hero_layout_settings(){
		register_setting('hero_layout_options', 'hero_layout_version');
	}
	add_action('admin_init', 'hero_layout_settings');
	
	function hero_layout($arg){
		if ($arg == 'hero_layout_version') {return get_option('hero_layout_version');}
	};
?>