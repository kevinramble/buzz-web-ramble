<?php
    // BIG BLOCK - 1 POST
    function big_block_add_options(){
		add_option('big_block_cat', '', '', 'yes');
		add_option('big_block_subcat', '', '', 'yes');
		add_option('big_block_type', '', '', 'yes');
		add_option('big_block_post_id', '', '', 'yes');
		add_option('big_block_post_title', '', '', 'yes');
		add_option('big_block_title', '', '', 'yes');
		add_option('big_block_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'big_block_add_options');
	
	function big_block_settings(){
		register_setting('big_block_options', 'big_block_cat');
		register_setting('big_block_options', 'big_block_subcat');
		register_setting('big_block_options', 'big_block_type');
		register_setting('big_block_options', 'big_block_post_id');
		register_setting('big_block_options', 'big_block_post_title');
		register_setting('big_block_options', 'big_block_title');
		register_setting('big_block_options', 'big_block_img');
	}
	add_action('admin_init', 'big_block_settings');
	
	function big_block($arg){
		if ($arg == 'big_block_cat') {return get_option('big_block_cat');}
		if ($arg == 'big_block_subcat') {return get_option('big_block_subcat');}
		if ($arg == 'big_block_type') {return get_option('big_block_type');}
		if ($arg == 'big_block_post_id') {return get_option('big_block_post_id');}
		if ($arg == 'big_block_post_title') {return get_option('big_block_post_title');}
		if ($arg == 'big_block_title') {return get_option('big_block_title');}
		if ($arg == 'big_block_img') {return get_option('big_block_img');}
	};
?>