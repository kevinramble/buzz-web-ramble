<?php
    // STORIES BLOCK - 9 POSTS
    function featured_stories_add_options(){
		for($i = 1; $i <= 9; $i++){
			add_option('featured_stories_cat'.$i, '', '', 'yes');
			add_option('featured_stories_subcat'.$i, '', '', 'yes');
			add_option('featured_stories_type'.$i, '', '', 'yes');
			add_option('featured_stories_post_id'.$i, '', '', 'yes');
			add_option('featured_stories_post_title'.$i, '', '', 'yes');
			add_option('featured_stories_title'.$i, '', '', 'yes');
			add_option('featured_stories_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'featured_stories_add_options');
	
	function featured_stories_settings(){
		for($i = 1; $i <= 9; $i++){
			register_setting('featured_stories_options', 'featured_stories_cat'.$i);
			register_setting('featured_stories_options', 'featured_stories_subcat'.$i);
			register_setting('featured_stories_options', 'featured_stories_type'.$i);
			register_setting('featured_stories_options', 'featured_stories_post_id'.$i);
			register_setting('featured_stories_options', 'featured_stories_post_title'.$i);
			register_setting('featured_stories_options', 'featured_stories_title'.$i);
			register_setting('featured_stories_options', 'featured_stories_img'.$i);
		}
	}
	add_action('admin_init', 'featured_stories_settings');
	
	function featured_stories($arg){
		for($i = 1; $i <= 9; $i++){
			if ($arg == 'featured_stories_cat'.$i) {return get_option('featured_stories_cat'.$i);}
			if ($arg == 'featured_stories_subcat'.$i) {return get_option('featured_stories_subcat'.$i);}
			if ($arg == 'featured_stories_type'.$i) {return get_option('featured_stories_type'.$i);}
			if ($arg == 'featured_stories_post_id'.$i) {return get_option('featured_stories_post_id'.$i);}
			if ($arg == 'featured_stories_post_title'.$i) {return get_option('featured_stories_post_title'.$i);}
			if ($arg == 'featured_stories_title'.$i) {return get_option('featured_stories_title'.$i);}
			if ($arg == 'featured_stories_img'.$i) {return get_option('featured_stories_img'.$i);}
		}
	};
?>