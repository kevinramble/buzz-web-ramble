<?php
    // TEN CONTENT .1 - 10 POSTS
    function ten_content_two_add_options(){
		for($i = 1; $i <= 10; $i++){
			add_option('ten_content_two_cat'.$i, '', '', 'yes');
			add_option('ten_content_two_subcat'.$i, '', '', 'yes');
			add_option('ten_content_two_type'.$i, '', '', 'yes');
			add_option('ten_content_two_post_id'.$i, '', '', 'yes');
			add_option('ten_content_two_post_title'.$i, '', '', 'yes');
			add_option('ten_content_two_title'.$i, '', '', 'yes');
			add_option('ten_content_two_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'ten_content_two_add_options');
	
	function ten_content_two_settings(){
		for($i = 1; $i <= 10; $i++){
			register_setting('ten_content_two_options', 'ten_content_two_cat'.$i);
			register_setting('ten_content_two_options', 'ten_content_two_subcat'.$i);
			register_setting('ten_content_two_options', 'ten_content_two_type'.$i);
			register_setting('ten_content_two_options', 'ten_content_two_post_id'.$i);
			register_setting('ten_content_two_options', 'ten_content_two_post_title'.$i);
			register_setting('ten_content_two_options', 'ten_content_two_title'.$i);
			register_setting('ten_content_two_options', 'ten_content_two_img'.$i);
		}
	}
	add_action('admin_init', 'ten_content_two_settings');
	
	function ten_content_two($arg){
		for($i = 1; $i <= 10; $i++){
			if ($arg == 'ten_content_two_cat'.$i) {return get_option('ten_content_two_cat'.$i);}
			if ($arg == 'ten_content_two_subcat'.$i) {return get_option('ten_content_two_subcat'.$i);}
			if ($arg == 'ten_content_two_type'.$i) {return get_option('ten_content_two_type'.$i);}
			if ($arg == 'ten_content_two_post_id'.$i) {return get_option('ten_content_two_post_id'.$i);}
			if ($arg == 'ten_content_two_post_title'.$i) {return get_option('ten_content_two_post_title'.$i);}
			if ($arg == 'ten_content_two_title'.$i) {return get_option('ten_content_two_title'.$i);}
			if ($arg == 'ten_content_two_img'.$i) {return get_option('ten_content_two_img'.$i);}
		}
	};
?>