<?php
    // TEN CONTENT .1 - 10 POSTS
    function ten_content_one_add_options(){
		for($i = 1; $i <= 10; $i++){
			add_option('ten_content_one_cat'.$i, '', '', 'yes');
			add_option('ten_content_one_subcat'.$i, '', '', 'yes');
			add_option('ten_content_one_type'.$i, '', '', 'yes');
			add_option('ten_content_one_post_id'.$i, '', '', 'yes');
			add_option('ten_content_one_post_title'.$i, '', '', 'yes');
			add_option('ten_content_one_title'.$i, '', '', 'yes');
			add_option('ten_content_one_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'ten_content_one_add_options');
	
	function ten_content_one_settings(){
		for($i = 1; $i <= 10; $i++){
			register_setting('ten_content_one_options', 'ten_content_one_cat'.$i);
			register_setting('ten_content_one_options', 'ten_content_one_subcat'.$i);
			register_setting('ten_content_one_options', 'ten_content_one_type'.$i);
			register_setting('ten_content_one_options', 'ten_content_one_post_id'.$i);
			register_setting('ten_content_one_options', 'ten_content_one_post_title'.$i);
			register_setting('ten_content_one_options', 'ten_content_one_title'.$i);
			register_setting('ten_content_one_options', 'ten_content_one_img'.$i);
		}
	}
	add_action('admin_init', 'ten_content_one_settings');
	
	function ten_content_one($arg){
		for($i = 1; $i <= 10; $i++){
			if ($arg == 'ten_content_one_cat'.$i) {return get_option('ten_content_one_cat'.$i);}
			if ($arg == 'ten_content_one_subcat'.$i) {return get_option('ten_content_one_subcat'.$i);}
			if ($arg == 'ten_content_one_type'.$i) {return get_option('ten_content_one_type'.$i);}
			if ($arg == 'ten_content_one_post_id'.$i) {return get_option('ten_content_one_post_id'.$i);}
			if ($arg == 'ten_content_one_post_title'.$i) {return get_option('ten_content_one_post_title'.$i);}
			if ($arg == 'ten_content_one_title'.$i) {return get_option('ten_content_one_title'.$i);}
			if ($arg == 'ten_content_one_img'.$i) {return get_option('ten_content_one_img'.$i);}
		}
	};
?>