<?php
    // MAIN VIDEO - 3 POSTS
    function main_banner_news_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('main_banner_news_cat'.$i, '', '', 'yes');
			add_option('main_banner_news_subcat'.$i, '', '', 'yes');
			add_option('main_banner_news_type'.$i, '', '', 'yes');
			add_option('main_banner_news_post_id'.$i, '', '', 'yes');
			add_option('main_banner_news_post_title'.$i, '', '', 'yes');
			add_option('main_banner_news_title'.$i, '', '', 'yes');
			add_option('main_banner_news_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'main_banner_news_add_options');
	
	function main_banner_news_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('main_banner_news_options', 'main_banner_news_cat'.$i);
			register_setting('main_banner_news_options', 'main_banner_news_subcat'.$i);
			register_setting('main_banner_news_options', 'main_banner_news_type'.$i);
			register_setting('main_banner_news_options', 'main_banner_news_post_id'.$i);
			register_setting('main_banner_news_options', 'main_banner_news_post_title'.$i);
			register_setting('main_banner_news_options', 'main_banner_news_title'.$i);
			register_setting('main_banner_news_options', 'main_banner_news_img'.$i);
		}
	}
	add_action('admin_init', 'main_banner_news_settings');
	
	function main_banner_news($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'main_banner_news_cat'.$i) {return get_option('main_banner_news_cat'.$i);}
			if ($arg == 'main_banner_news_subcat'.$i) {return get_option('main_banner_news_subcat'.$i);}
			if ($arg == 'main_banner_news_type'.$i) {return get_option('main_banner_news_type'.$i);}
			if ($arg == 'main_banner_news_post_id'.$i) {return get_option('main_banner_news_post_id'.$i);}
			if ($arg == 'main_banner_news_post_title'.$i) {return get_option('main_banner_news_post_title'.$i);}
			if ($arg == 'main_banner_news_title'.$i) {return get_option('main_banner_news_title'.$i);}
			if ($arg == 'main_banner_news_img'.$i) {return get_option('main_banner_news_img'.$i);}
		}
	};
?>