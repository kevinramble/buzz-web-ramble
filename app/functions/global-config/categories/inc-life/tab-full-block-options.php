<?php
    // BIG BLOCK - 1 POST
    function full_block_life_add_options(){
		add_option('full_block_life_cat', '', '', 'yes');
		add_option('full_block_life_subcat', '', '', 'yes');
		add_option('full_block_life_type', '', '', 'yes');
		add_option('full_block_life_post_id', '', '', 'yes');
		add_option('full_block_life_post_title', '', '', 'yes');
		add_option('full_block_life_title', '', '', 'yes');
		add_option('full_block_life_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'full_block_life_add_options');
	
	function full_block_life_settings(){
		register_setting('full_block_life_options', 'full_block_life_cat');
		register_setting('full_block_life_options', 'full_block_life_subcat');
		register_setting('full_block_life_options', 'full_block_life_type');
		register_setting('full_block_life_options', 'full_block_life_post_id');
		register_setting('full_block_life_options', 'full_block_life_post_title');
		register_setting('full_block_life_options', 'full_block_life_title');
		register_setting('full_block_life_options', 'full_block_life_img');
	}
	add_action('admin_init', 'full_block_life_settings');
	
	function full_block_life($arg){
		if ($arg == 'full_block_life_cat') {return get_option('full_block_life_cat');}
		if ($arg == 'full_block_life_subcat') {return get_option('full_block_life_subcat');}
		if ($arg == 'full_block_life_type') {return get_option('full_block_life_type');}
		if ($arg == 'full_block_life_post_id') {return get_option('full_block_life_post_id');}
		if ($arg == 'full_block_life_post_title') {return get_option('full_block_life_post_title');}
		if ($arg == 'full_block_life_title') {return get_option('full_block_life_title');}
		if ($arg == 'full_block_life_img') {return get_option('full_block_life_img');}
	};
?>