<?php
    // BREAKING NEWS - 8 POSTS
    function breaking_news_life_add_options(){
		for($i = 1; $i <= 8; $i++){
			add_option('breaking_news_life_cat'.$i, '', '', 'yes');
			add_option('breaking_news_life_subcat'.$i, '', '', 'yes');
			add_option('breaking_news_life_type'.$i, '', '', 'yes');
			add_option('breaking_news_life_post_id'.$i, '', '', 'yes');
			add_option('breaking_news_life_post_title'.$i, '', '', 'yes');
			add_option('breaking_news_life_title'.$i, '', '', 'yes');
			add_option('breaking_news_life_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'breaking_news_life_add_options');
	
	function breaking_news_life_settings(){
		for($i = 1; $i <= 8; $i++){
			register_setting('breaking_news_life_options', 'breaking_news_life_cat'.$i);
			register_setting('breaking_news_life_options', 'breaking_news_life_subcat'.$i);
			register_setting('breaking_news_life_options', 'breaking_news_life_type'.$i);
			register_setting('breaking_news_life_options', 'breaking_news_life_post_id'.$i);
			register_setting('breaking_news_life_options', 'breaking_news_life_post_title'.$i);
			register_setting('breaking_news_life_options', 'breaking_news_life_title'.$i);
			register_setting('breaking_news_life_options', 'breaking_news_life_img'.$i);
		}
	}
	add_action('admin_init', 'breaking_news_life_settings');
	
	function breaking_news_life($arg){
		for($i = 1; $i <= 8; $i++){
			if ($arg == 'breaking_news_life_cat'.$i) {return get_option('breaking_news_life_cat'.$i);}
			if ($arg == 'breaking_news_life_subcat'.$i) {return get_option('breaking_news_life_subcat'.$i);}
			if ($arg == 'breaking_news_life_type'.$i) {return get_option('breaking_news_life_type'.$i);}
			if ($arg == 'breaking_news_life_post_id'.$i) {return get_option('breaking_news_life_post_id'.$i);}
			if ($arg == 'breaking_news_life_post_title'.$i) {return get_option('breaking_news_life_post_title'.$i);}
			if ($arg == 'breaking_news_life_title'.$i) {return get_option('breaking_news_life_title'.$i);}
			if ($arg == 'breaking_news_life_img'.$i) {return get_option('breaking_news_life_img'.$i);}
		}
	};
?>