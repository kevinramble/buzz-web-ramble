<?php
    // MUST WATCH - 9 POSTS
    function must_watch_hot_add_options(){
		for($i = 1; $i <= 9; $i++){
			add_option('must_watch_hot_cat'.$i, '', '', 'yes');
			add_option('must_watch_hot_subcat'.$i, '', '', 'yes');
			add_option('must_watch_hot_post_id'.$i, '', '', 'yes');
			add_option('must_watch_hot_post_title'.$i, '', '', 'yes');
			add_option('must_watch_hot_title'.$i, '', '', 'yes');
			add_option('must_watch_hot_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'must_watch_hot_add_options');
	
	function must_watch_hot_settings(){
		for($i = 1; $i <= 9; $i++){
			register_setting('must_watch_hot_options', 'must_watch_hot_cat'.$i);
			register_setting('must_watch_hot_options', 'must_watch_hot_subcat'.$i);
			register_setting('must_watch_hot_options', 'must_watch_hot_post_id'.$i);
			register_setting('must_watch_hot_options', 'must_watch_hot_post_title'.$i);
			register_setting('must_watch_hot_options', 'must_watch_hot_title'.$i);
			register_setting('must_watch_hot_options', 'must_watch_hot_img'.$i);
		}
	}
	add_action('admin_init', 'must_watch_hot_settings');
	
	function must_watch_hot($arg){
		for($i = 1; $i <= 9; $i++){
			if ($arg == 'must_watch_hot_cat'.$i) {return get_option('must_watch_hot_cat'.$i);}
			if ($arg == 'must_watch_hot_subcat'.$i) {return get_option('must_watch_hot_subcat'.$i);}
			if ($arg == 'must_watch_hot_post_id'.$i) {return get_option('must_watch_hot_post_id'.$i);}
			if ($arg == 'must_watch_hot_post_title'.$i) {return get_option('must_watch_hot_post_title'.$i);}
			if ($arg == 'must_watch_hot_title'.$i) {return get_option('must_watch_hot_title'.$i);}
			if ($arg == 'must_watch_hot_img'.$i) {return get_option('must_watch_hot_img'.$i);}
		}
	};
?>