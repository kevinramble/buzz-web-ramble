<?php
    // EDITOR'S CHOICE - 8 POSTS
    function home_order_add_options(){
		for($i = 1; $i <= 8; $i++){
			add_option('home_order'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'home_order_add_options');
	
	function home_order_settings(){
		for($i = 1; $i <= 8; $i++){
			register_setting('home_order_options', 'home_order'.$i);
		}
	}
	add_action('admin_init', 'home_order_settings');
	
	function home_order($arg){
		for($i = 1; $i <= 8; $i++){
			if ($arg == 'home_order'.$i) {return get_option('home_order'.$i);}
		}
	};
?>