<?php
    // HOT NOW - 6 POSTS
    function hot_now_add_options(){
		for($i = 1; $i <= 6; $i++){
			add_option('hot_now_cat'.$i, '', '', 'yes');
			add_option('hot_now_subcat'.$i, '', '', 'yes');
			add_option('hot_now_type'.$i, '', '', 'yes');
			add_option('hot_now_post_id'.$i, '', '', 'yes');
			add_option('hot_now_post_title'.$i, '', '', 'yes');
			add_option('hot_now_title'.$i, '', '', 'yes');
			add_option('hot_now_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'hot_now_add_options');
	
	function hot_now_settings(){
		for($i = 1; $i <= 6; $i++){
			register_setting('hot_now_options', 'hot_now_cat'.$i);
			register_setting('hot_now_options', 'hot_now_subcat'.$i);
			register_setting('hot_now_options', 'hot_now_type'.$i);
			register_setting('hot_now_options', 'hot_now_post_id'.$i);
			register_setting('hot_now_options', 'hot_now_post_title'.$i);
			register_setting('hot_now_options', 'hot_now_title'.$i);
			register_setting('hot_now_options', 'hot_now_img'.$i);
		}
	}
	add_action('admin_init', 'hot_now_settings');
	
	function hot_now($arg){
		for($i = 1; $i <= 6; $i++){
			if ($arg == 'hot_now_cat'.$i) {return get_option('hot_now_cat'.$i);}
			if ($arg == 'hot_now_subcat'.$i) {return get_option('hot_now_subcat'.$i);}
			if ($arg == 'hot_now_type'.$i) {return get_option('hot_now_type'.$i);}
			if ($arg == 'hot_now_post_id'.$i) {return get_option('hot_now_post_id'.$i);}
			if ($arg == 'hot_now_post_title'.$i) {return get_option('hot_now_post_title'.$i);}
			if ($arg == 'hot_now_title'.$i) {return get_option('hot_now_title'.$i);}
			if ($arg == 'hot_now_img'.$i) {return get_option('hot_now_img'.$i);}
		}
	};
?>