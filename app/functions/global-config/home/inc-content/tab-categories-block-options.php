<?php
	// CATEGORIES BLOCK - 3 POSTS EACH, 3 TOTAL, ORDER: HOT, LIFE, NEWS
	
	// HOT
    function categories_block1_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block1_cat'.$i, '', '', 'yes');
			add_option('categories_block1_subcat'.$i, '', '', 'yes');
			add_option('categories_block1_type'.$i, '', '', 'yes');
			add_option('categories_block1_post_id'.$i, '', '', 'yes');
			add_option('categories_block1_post_title'.$i, '', '', 'yes');
			add_option('categories_block1_title'.$i, '', '', 'yes');
			add_option('categories_block1_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block1_add_options');
	
	function categories_block1_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block1_options', 'categories_block1_cat'.$i);
			register_setting('categories_block1_options', 'categories_block1_subcat'.$i);
			register_setting('categories_block1_options', 'categories_block1_type'.$i);
			register_setting('categories_block1_options', 'categories_block1_post_id'.$i);
			register_setting('categories_block1_options', 'categories_block1_post_title'.$i);
			register_setting('categories_block1_options', 'categories_block1_title'.$i);
			register_setting('categories_block1_options', 'categories_block1_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block1_settings');
	
	function categories_block1($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block1_cat'.$i) {return get_option('categories_block1_cat'.$i);}
			if ($arg == 'categories_block1_subcat'.$i) {return get_option('categories_block1_subcat'.$i);}
			if ($arg == 'categories_block1_type'.$i) {return get_option('categories_block1_type'.$i);}
			if ($arg == 'categories_block1_post_id'.$i) {return get_option('categories_block1_post_id'.$i);}
			if ($arg == 'categories_block1_post_title'.$i) {return get_option('categories_block1_post_title'.$i);}
			if ($arg == 'categories_block1_title'.$i) {return get_option('categories_block1_title'.$i);}
			if ($arg == 'categories_block1_img'.$i) {return get_option('categories_block1_img'.$i);}
		}
	};

	// LIFE
    function categories_block2_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block2_cat'.$i, '', '', 'yes');
			add_option('categories_block2_subcat'.$i, '', '', 'yes');
			add_option('categories_block2_type'.$i, '', '', 'yes');
			add_option('categories_block2_post_id'.$i, '', '', 'yes');
			add_option('categories_block2_post_title'.$i, '', '', 'yes');
			add_option('categories_block2_title'.$i, '', '', 'yes');
			add_option('categories_block2_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block2_add_options');
	
	function categories_block2_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block2_options', 'categories_block2_cat'.$i);
			register_setting('categories_block2_options', 'categories_block2_subcat'.$i);
			register_setting('categories_block2_options', 'categories_block2_type'.$i);
			register_setting('categories_block2_options', 'categories_block2_post_id'.$i);
			register_setting('categories_block2_options', 'categories_block2_post_title'.$i);
			register_setting('categories_block2_options', 'categories_block2_title'.$i);
			register_setting('categories_block2_options', 'categories_block2_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block2_settings');
	
	function categories_block2($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block2_cat'.$i) {return get_option('categories_block2_cat'.$i);}
			if ($arg == 'categories_block2_subcat'.$i) {return get_option('categories_block2_subcat'.$i);}
			if ($arg == 'categories_block2_type'.$i) {return get_option('categories_block2_type'.$i);}
			if ($arg == 'categories_block2_post_id'.$i) {return get_option('categories_block2_post_id'.$i);}
			if ($arg == 'categories_block2_post_title'.$i) {return get_option('categories_block2_post_title'.$i);}
			if ($arg == 'categories_block2_title'.$i) {return get_option('categories_block2_title'.$i);}
			if ($arg == 'categories_block2_img'.$i) {return get_option('categories_block2_img'.$i);}
		}
	};

	// NEWS
    function categories_block3_add_options(){
		for($i = 1; $i <= 3; $i++){
			add_option('categories_block3_cat'.$i, '', '', 'yes');
			add_option('categories_block3_subcat'.$i, '', '', 'yes');
			add_option('categories_block3_type'.$i, '', '', 'yes');
			add_option('categories_block3_post_id'.$i, '', '', 'yes');
			add_option('categories_block3_post_title'.$i, '', '', 'yes');
			add_option('categories_block3_title'.$i, '', '', 'yes');
			add_option('categories_block3_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'categories_block3_add_options');
	
	function categories_block3_settings(){
		for($i = 1; $i <= 3; $i++){
			register_setting('categories_block3_options', 'categories_block3_cat'.$i);
			register_setting('categories_block3_options', 'categories_block3_subcat'.$i);
			register_setting('categories_block3_options', 'categories_block3_type'.$i);
			register_setting('categories_block3_options', 'categories_block3_post_id'.$i);
			register_setting('categories_block3_options', 'categories_block3_post_title'.$i);
			register_setting('categories_block3_options', 'categories_block3_title'.$i);
			register_setting('categories_block3_options', 'categories_block3_img'.$i);
		}
	}
	add_action('admin_init', 'categories_block3_settings');
	
	function categories_block3($arg){
		for($i = 1; $i <= 3; $i++){
			if ($arg == 'categories_block3_cat'.$i) {return get_option('categories_block3_cat'.$i);}
			if ($arg == 'categories_block3_subcat'.$i) {return get_option('categories_block3_subcat'.$i);}
			if ($arg == 'categories_block3_type'.$i) {return get_option('categories_block3_type'.$i);}
			if ($arg == 'categories_block3_post_id'.$i) {return get_option('categories_block3_post_id'.$i);}
			if ($arg == 'categories_block3_post_title'.$i) {return get_option('categories_block3_post_title'.$i);}
			if ($arg == 'categories_block3_title'.$i) {return get_option('categories_block3_title'.$i);}
			if ($arg == 'categories_block3_img'.$i) {return get_option('categories_block3_img'.$i);}
		}
	};
?>