<?php
    // MAIN BANNER - 5 POSTS
    function home_poll_add_options(){
		add_option('home_poll_status', '', '', 'yes');
		add_option('home_poll_shortcode', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'home_poll_add_options');
	
	function home_poll_settings(){
		register_setting('home_poll_options', 'home_poll_status');
		register_setting('home_poll_options', 'home_poll_shortcode');
	}
	add_action('admin_init', 'home_poll_settings');
	
	function home_poll($arg){
		if ($arg == 'home_poll_status') {return get_option('home_poll_status');}
		if ($arg == 'home_poll_shortcode') {return get_option('home_poll_shortcode');}
	};
?>