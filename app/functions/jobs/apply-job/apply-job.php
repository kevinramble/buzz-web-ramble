<?php
    function apply_job_ajax_handler(){
		$jobID = json_decode(stripslashes($_POST['jobID']), true);

		$arr = array($jobID);
		$meta_value = serialize($arr);
		$userID = get_current_user_id();
		$siteID = get_current_blog_id();

		echo $siteID;

		global $wpdb;
		$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}usermeta WHERE meta_key = 'appliedjobs' AND user_id = ". $userID , OBJECT);

		if($results){
			$id = $results[0]->umeta_id;
			$meta_value_new = unserialize($results[0]->meta_value);
			$meta_value_new[] = $jobID;

			$table_name = $wpdb->prefix . 'usermeta';
			$wpdb->update($table_name, 
				array('meta_value' => serialize($meta_value_new)),
				array('umeta_id' => $id)
			);
		} else {
			//não tem nada no banco, cria a primeira vez:
			$table_name = $wpdb->prefix . 'usermeta';
			$wpdb->insert($table_name, array(
				'user_id' => $userID,
				'meta_key' => 'appliedjobs',
				'meta_value' => $meta_value,
				
			));
		}

		die; // here we exit the script and no wp_reset_query() required!
	}
	add_action('wp_ajax_apply_job', 'apply_job_ajax_handler'); // wp_ajax_{action}
	add_action('wp_ajax_nopriv_apply_job', 'apply_job_ajax_handler'); // wp_ajax_nopriv_{action}
?>