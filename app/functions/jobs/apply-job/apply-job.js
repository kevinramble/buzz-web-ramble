jQuery(document).ready(function($) {
	// send for
	if ($('.js-job-form').length > 0) {
		var jobID = ''

		$('.js-open-modal').click(function() {
			jobID = $(this).data('jobid')

			data = {
				action: 'apply_job',
				jobID: jobID
			}
		})

		$('.js-job-form').submit(function() {
			var form = $(this),
				url = form.data('url') + '/inc/send-emails/send-job.php',
				filedata = $('#cv').prop('files')[0],
				formdata = new FormData(this),
				linkedin = $('#joblinkedin'),
				cv = $('#cv'),
				captcha = $('#securityanswer'),
				notice = form.find('.notice'),
				submit = form.find('.js-send-button'),
				error = false

			formdata.append('file', filedata)
			notice.addClass('hidden')

			submit.blur().addClass('js-loading')

			if (cv.val() == '') {
				cv.parent().addClass('js-error')
				error = true
			} else {
				cv.parent().removeClass('js-error')
			}
			if (captcha.val() == '') {
				captcha.addClass('js-error')
				error = true
			} else {
				captcha.removeClass('js-error')
			}

			if (!error) {
				$.ajax({
					type: 'POST',
					url: url,
					data: formdata,
					dataType: 'html',
					contentType: false,
					cache: false,
					processData: false,
					success: function(msg) {
						if (msg == '1') {
							form.get(0).reset()

							$('.js-content-wrapper').addClass('js-sending')
							submit.removeClass('js-loading')
							captcha.removeClass('js-error')

							apply_job_ajax()
						} else if (msg == '2') {
							notice
								.html('Incorrect security answer, please try again.')
								.addClass('js-error')
								.removeClass('js-success')
								.removeClass('hidden')
							submit.removeClass('js-loading')
							captcha.addClass('js-error')
						} else {
							notice
								.html(msg)
								.addClass('js-error')
								.removeClass('js-success')
								.removeClass('hidden')
							submit.removeClass('js-loading')
						}
					}
				})
			} else {
				notice
					.html('Please check the highlighted fields.')
					.addClass('js-error')
					.removeClass('js-success')
					.removeClass('hidden')
				submit.removeClass('js-loading')
			}

			return false
		})

		var successContent = '<h1 class="c-title-headline">Application submitted</h1>'
		successContent += '<div class="c-description-headline mt-4">'
		successContent += "<p>The company will be in touch if you're selected. <strong>Good luck!</strong></p>"
		successContent += '</div>'

		function apply_job_ajax() {
			$.ajax({
				url: apply_job_params.ajaxurl,
				data: data,
				type: 'POST',
				success: function(data) {
					$('.js-content-wrapper')
						.removeClass('js-sending')
						.html(successContent)
					$('.js-apply-job-bt').remove()
				}
			})
		}
	}
})
