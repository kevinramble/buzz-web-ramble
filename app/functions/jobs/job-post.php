<?php
    // Job - post type
	function create_job(){
		register_post_type('job',
		array(
			'labels' => array(
				'name' => 'Jobs',
						'singular_name' => 'Job',
						'add_new' => 'Add new',
						'add_new_item' => 'Add new job',
						'edit_item' => 'Edit job',
						'new_item' => 'New job',
						'all_items' => 'All jobs',
						'view_item' => 'View job',
						'search_items' => 'Search jobs',
						'not_found' => 'No job found',
						'not_found_in_trash' => 'No jobs found in trash',
						'parent_item_colon' => '',
						'menu_name' => 'Jobs'
					),
					'public' => true,
					'menu_client' => 5,
					'menu_position' => 4,
					'menu_icon' => 'dashicons-businessman',
					'supports' => array('thumbnail', 'editor', 'author'),
					'show_in_rest' => true,
					'has_archive' => true,
					'rewrite' => array('slug' => 'job', 'with_front' => false)
				)
			);
	}
	add_action('init', 'create_job');

	add_action('admin_init', 'job_admin');
	function job_admin() {
		add_meta_box('job_meta_box',
			'Job details',
			'display_job_meta_box',
			'job', 'normal', 'high'
		);
	}

	function display_job_meta_box($job_post){
		$job_article = esc_html(get_post_meta($job_post->ID, 'job_article', true));
		$job_country = esc_html(get_post_meta($job_post->ID, 'job_country', true));
		$job_state = esc_html(get_post_meta($job_post->ID, 'job_state', true));
		$job_position = esc_html(get_post_meta($job_post->ID, 'job_position', true));
		$job_type = esc_html(get_post_meta($job_post->ID, 'job_type', true));
		$job_company = esc_html(get_post_meta($job_post->ID, 'job_company', true));
		$job_company_email = esc_html(get_post_meta($job_post->ID, 'job_company_email', true));
		$job_application = esc_html(get_post_meta($job_post->ID, 'job_application', true));
		$job_expiration = esc_html(get_post_meta($job_post->ID, 'job_expiration', true));
	?>

		<p class="label">
			<strong>Location</strong><br>
			<?php if($job_country != ''){ ?>
				<span class="set-location">
					<input type="hidden" name="job_country" value="<?php echo $job_country; ?>">
					<?php /*<input type="hidden" name="job_state" value="echo $job_state; ?>">*/ ?>

					<?php echo $job_country; /*if($job_state) echo ' / ' . $job_state;*/ ?>
					<br>
					<a href="#0" class="js-change-location button-primary">Change location</a>
				</span>
			<?php } ?>
		</p>

		<div class="choose-location <?php if($job_country != '') echo 'hidden'; ?>">
			<select <?php if($job_country == '') echo 'name="job_country"'; ?> class="js-job-country">
				<option value="">Select a country</option>
				<option value="Anguilla">Anguilla</option>
				<option value="Antigua and Barbuda">Antigua and Barbuda</option>
				<option value="ABC Islands">ABC Islands</option>
				<option value="Barbados">Barbados</option>
				<option value="Bermuda">Bermuda</option>
				<option value="British Virgin Islands">British Virgin Islands</option>
				<option value="Cayman Islands">Cayman Islands</option>
				<option value="Dominica">Dominica</option>
				<option value="FWI">FWI</option>
				<option value="Grenada">Grenada</option>
				<option value="Guyana">Guyana</option>
				<option value="Haiti">Haiti</option>
				<option value="Jamaica">Jamaica</option>
				<option value="Montserrat">Montserrat</option>
				<option value="St. Kitts and Nevis">St. Kitts and Nevis</option>
				<option value="St. Lucia">St. Lucia</option>
				<option value="St. Martin">St. Martin</option>
				<option value="St. Vincent and the Grenadines">St. Vincent and the Grenadines</option>
				<option value="Suriname">Suriname</option>
				<option value="Trinidad and Tobago">Trinidad and Tobago</option>
				<option value="Turks and Caicos">Turks and Caicos</option>
			</select>
			
			<br>

			<?php /*<select if($job_state == '') echo 'name="job_state"'; ?> disabled="disabled" class="js-job-state"></select>*/ ?>
		</div>
			
        <p class="label"><strong>Position</strong><br>
		<select name="job_article" class="small">
			<option value="">Article</option>
			<option value="a" <?php if($job_article == 'a') echo 'selected="selected"'; ?>>A</option>
			<option value="an" <?php if($job_article == 'an') echo 'selected="selected"'; ?>>An</option>
		</select>
		<input type="text" name="job_position" value="<?php echo $job_position; ?>"></p>

		<p class="label"><strong>Type</strong><br>
		<select name="job_type">
			<option value="">Select type</option>
			<option value="Full-time" <?php if($job_type == 'Full-time') echo 'selected="selected"'; ?>>Full-time</option>
			<option value="Part-time" <?php if($job_type == 'Part-time') echo 'selected="selected"'; ?>>Part-time</option>
			<option value="Freelance" <?php if($job_type == 'Freelance') echo 'selected="selected"'; ?>>Freelance</option>
			<option value="Internship" <?php if($job_type == 'Internship') echo 'selected="selected"'; ?>>Internship</option>
		</select></p>

		<p class="label"><strong>Company</strong><br>
		<input type="text" name="job_company" value="<?php echo $job_company; ?>"></p>

		<p class="label"><strong>Company's e-mail address</strong> <span class="obs">(To send applications)</span><br>
		<input type="email" name="job_company_email" value="<?php echo $job_company_email; ?>"></p>

		<p class="label"><strong>Application URL</strong><br>
		<input type="text" name="job_application" placeholder="http://" value="<?php echo $job_application; ?>"></p>

		<p class="label"><strong>Expiration date</strong><br>
		<input type="text" class="datepicker" name="job_expiration" value="<?php echo $job_expiration; ?>"></p>

		<p class="obs">
			<strong>Obs:</strong><br>
			If the "Application URL" field is filled, it will have priority over the form;<br>
			The thumbnail dimensions are XXX by XXX pixels.
		</p>
	<?php
	}

	function filter_post_data($data, $postarr){
		if($postarr['post_type'] == 'job'){
			if($_POST['job_company'] && $_POST['job_position']){
				$this_job_company = trim($_POST['job_company']);
				$this_job_position = trim($_POST['job_position']);

				$new_job_title = $this_job_company .', '. $this_job_position;

				$new_job_name = strtolower($new_job_title);
				$new_job_name = preg_replace("/[^a-z0-9_\s-]/", "", $new_job_name);
				$new_job_name = preg_replace("/[\s-]+/", " ", $new_job_name);
				$new_job_name = preg_replace("/[\s_]/", "-", $new_job_name);
				$new_job_name = $new_job_name .'-'. $postarr['ID'];

				$data['post_title'] = $new_job_title;
				$data['post_name'] = $new_job_name;
			}

			return $data;
		} else {
			return $data;
		}
	}
	add_filter('wp_insert_post_data', 'filter_post_data', '99', 2);

	function add_job_fields($job_post_id, $job_post){
		if($job_post->post_type == 'job'){
			if(isset($_POST['job_country'])){
				update_post_meta($job_post_id, 'job_country', $_POST['job_country']);
			}
			if(isset($_POST['job_state'])){
				update_post_meta($job_post_id, 'job_state', $_POST['job_state']);
			}
			if(isset($_POST['job_position'])){
				update_post_meta($job_post_id, 'job_position', $_POST['job_position']);
			}
			if(isset($_POST['job_article'])){
				update_post_meta($job_post_id, 'job_article', $_POST['job_article']);
			}
			if(isset($_POST['job_type'])){
				update_post_meta($job_post_id, 'job_type', $_POST['job_type']);
			}
			if(isset($_POST['job_company'])){
				update_post_meta($job_post_id, 'job_company', $_POST['job_company']);
			}
			if(isset($_POST['job_company_email'])){
				update_post_meta($job_post_id, 'job_company_email', $_POST['job_company_email']);
			}
			if(isset($_POST['job_expiration'])){
				update_post_meta($job_post_id, 'job_expiration', $_POST['job_expiration']);
			}
			if(isset($_POST['job_application'])){
				update_post_meta($job_post_id, 'job_application', $_POST['job_application']);
			}
		}
	}
	add_action('save_post', 'add_job_fields', 10, 2);
    
    function job_post_enqueue($hook){
        global $post_type;
        if($post_type == 'job'){
			wp_enqueue_script('jquery-ui-datepicker');
			wp_enqueue_style('job-post-style', get_template_directory_uri() . '/functions/jobs/job-post.css');
			wp_enqueue_script('job-post-js', get_template_directory_uri() . '/functions/jobs/job-post.js', array('jquery'));
		}
	}
	add_action('admin_enqueue_scripts', 'job_post_enqueue');

	// Mostrar custom fields na lista de posts
	function add_jobs_columns($columns){
		return array_merge($columns, array( 
			'location' => 'Location',
			'company' => 'Company',
			'position' => 'Position',
			'type' => 'Type',
			'expiration' => 'Expiration date'
		));
	}
	add_filter('manage_job_posts_columns', 'add_jobs_columns');

 	function job_custom_column($column, $job_post_id){
		switch($column){
			case 'location':
				if((get_post_meta($job_post_id, 'job_country', true) !== '')){
					echo esc_html(get_post_meta($job_post_id, 'job_country', true));

					if((get_post_meta($job_post_id, 'job_state', true) !== '')){
						echo ' / ' . esc_html(get_post_meta($job_post_id, 'job_state', true));
					}
				} else {
					echo '–';
				}
			break;
			case 'company':
				echo $company = ((get_post_meta($job_post_id, 'job_company', true) !== '')) ? esc_html(get_post_meta($job_post_id, 'job_company', true)) : '–';
			break;
			case 'position':
				echo $position = ((get_post_meta($job_post_id, 'job_position', true) !== '')) ? esc_html(get_post_meta($job_post_id, 'job_position', true)) : '–';
			break;
			case 'type':
				echo $type = ((get_post_meta($job_post_id, 'job_type', true) !== '')) ? esc_html(get_post_meta($job_post_id, 'job_type', true)) : '–';
			break;
			case 'expiration':
				$job_expiration = get_post_meta($job_post_id, 'job_expiration', true);
				if($job_expiration){
					$job_expiration = new DateTime($job_expiration);
					echo $job_expiration = $job_expiration->format('d/m/Y');
				} else {
					echo '–';
				}
			break;
		}
	}
	add_action('manage_job_posts_custom_column', 'job_custom_column', 10, 2);

	// ordenar campos no dashboard
	function job_sortable_columns($columns){
		return array_merge($columns, array( 
			'location' => 'job_country',
			'company' => 'job_company',
			'position' => 'job_position',
			'type' => 'job_type',
			'expiration' => 'job_expiration'
		));
	}
	add_filter('manage_edit-job_sortable_columns', 'job_sortable_columns');

	function job_posts_orderby($query){
		if(!is_admin() || ! $query->is_main_query()){
			return;
		}

		if('job_country' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'job_country');
		}

		if('job_company' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'job_company');
		}

		if('job_position' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'job_position');
		}

		if('job_type' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'job_type');
		}

		if('job_expiration' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'job_expiration');
		}
	}
	add_action('pre_get_posts', 'job_posts_orderby');

	// Tirar Quick edit, porque dá erro com o title salvo automaticamente
	function job_disable_quick_edit($actions = array(), $post = null){
		// Abort if the post type is not "job"
		if (!is_post_type_archive('job')){
			return $actions;
		}
		if(isset( $actions['inline hide-if-no-js'])){
			unset( $actions['inline hide-if-no-js'] );
		}

		return $actions;
	}
	add_filter('post_row_actions', 'job_disable_quick_edit', 10, 2);

	// Adicionar os custom meta fields no json
	add_action('rest_api_init', 'job_posts_meta_field');
	function job_posts_meta_field(){
		register_rest_field('job', 'job_custom_fields', array(
			'get_callback' => 'get_job_meta_for_api',
			'schema' => null,
			)
		);
	}
	function get_job_meta_for_api($object){
		$post_id = $object['id'];
		return get_post_meta( $post_id );
	}
?>