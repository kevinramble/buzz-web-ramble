<?php // Change the Favorites Authentication Modal Content
	add_filter('favorites/authentication_modal_content', 'custom_favorites_auth_content');
	function custom_favorites_auth_content($html){
		$home = get_bloginfo('url');
		$permalink = get_permalink();

		$html = '<div class="modal favorites is-open">';
			$html .= '<div class="modal__dialog">';
				$html .= '<button class="modal__close" data-attribute="data-favorites-modal-close" aria-label="Close"></button>';
				$html .= '<div class="js-content-wrapper">';
					$html .= '<div class="c-badge c-badge--locked"></div>';
					$html .= '<div class="c-description-headline my-3">';
						$html .= '<p>Sign in to save your favourite articles, access special offers and more great stuff!</p>';
					$html .= '</div>';
					$html .= '<a href="'. $home .'/login?redirect_to='. $permalink .'" class="c-button c-button--negative c-button--full mb-2">Click here to login</a>';
					$html .= '<a href="'. $home .'/register?redirect_to='. $permalink .'" class="c-button c-button--secondary c-button--full">Not a member? Register now</a>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';
		$html .= '<script>';
			$html .= 'jQuery(".modal, .modal__close").on("click", function() {';
				$html .= 'jQuery(".simplefavorites-modal-content, .simplefavorites-modal-backdrop").remove();';
			$html .= '})';
		$html .= '</script>';
		return $html;
	}
?>