<?php
	function ads_config() {
		add_menu_page('Ads config', 'Ads config', '10', 'ads_config', 'ads_config_content', 'dashicons-megaphone', '4');
		add_submenu_page('ads_config', 'Ads config – Home', 'Home', '10', 'ads_config', 'ads_config');
		add_submenu_page('ads_config', 'Ads config – ???', '???', '10', 'ads_config_layout', 'ads_layout_content');
		add_submenu_page('ads_config', 'Ads config – Documentation', 'Documentation', '10', 'ads_config_doc', 'ads_docs_content');
	}
	add_action('admin_menu', 'ads_config');

	require_once('inc-content/ads-home-options.php');

	function ads_config_content(){ ?>
		<div class="wrap">
			<h2>Ads configurations</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content ads-config-options postbox">
					<div class="inside">
						<?php settings_errors(); ?>

						<form method="post" action="options.php">
							<div class="content-wrapper post-manager">
								<?php settings_fields('ads_home_options'); ?>

								<h3>Home</h3>

								<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus dolores repudiandae, iure animi asperiores placeat molestiae in fugiat ullam corrupti, est nihil! Quos deserunt accusantium dolorum ipsum alias tenetur placeat?</p>

								<?php for($i = 1; $i <= 5; $i++){
									if($i == 1) : $adname = 'Billboard, 970x250px';
									elseif($i == 2) : $adname = 'Billboard, 970x250px';
									elseif($i == 3) : $adname = 'Billboard, 970x250px';
									elseif($i == 4) : $adname = 'Large leaderboard, 970x90px';
									elseif($i == 5) : $adname = 'Square, 250x250px';
									else : $adname = '';
									endif; ?>
									<div class="ad-slot <?php echo get_option('ads_home_status'. $i) == '' || get_option('ads_home_script'. $i) == '' ? 'empty' : ''; ?>">
										<h4>Ad <?php echo $i; ?> – <?php echo $adname; ?></h4>
										<?php $checked = (get_option('ads_home_status'. $i)) ? 'checked' : ''; ?>
										<label><input type="checkbox" class="checkbox" value="1" name="ads_home_status<?php echo $i; ?>" <?php echo $checked; ?>> Show ad</label>
										<textarea name="ads_home_script<?php echo $i; ?>" id=""><?php echo get_option('ads_home_script'. $i); ?></textarea>
									</div><!-- .ad-slot -->
								<?php } // for ?>

								<input class="button-primary" type="submit" name="Save" value="Save changes">
							</div>
						</form>
					</div><!-- .inside -->
				</div><!-- #home-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // ads_config_content()
?>