<?php
    // MAIN BANNER - 5 POSTS
    function ads_home_add_options(){
		for($i = 1; $i <= 5; $i++){
			add_option('ads_home_status'.$i, '', '', 'yes');
			add_option('ads_home_script'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'ads_home_add_options');
	
	function ads_home_settings(){
		for($i = 1; $i <= 5; $i++){
			register_setting('ads_home_options', 'ads_home_status'.$i);
			register_setting('ads_home_options', 'ads_home_script'.$i);
		}
	}
	add_action('admin_init', 'ads_home_settings');
	
	function ads_home($arg){
		for($i = 1; $i <= 5; $i++){
			if ($arg == 'ads_home_status'.$i) {return get_option('ads_home_status'.$i);}
			if ($arg == 'ads_home_script'.$i) {return get_option('ads_home_script'.$i);}
		}
	};
?>