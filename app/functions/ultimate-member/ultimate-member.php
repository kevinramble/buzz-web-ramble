<?php
	add_action('um_after_account_general', 'showExtraFields', 100);
	function showExtraFields(){
		$custom_fields = [
			"city" => "City",
			"country" => "Country"
		];

		$fields = [];
		foreach ($custom_fields as $key => $value){
			$fields[$key] = array(
				'title' => $value,
				'metakey' => $key,
				'type' => 'text',
				'label' => $value,
				'required' => 1,
				'public' => 1,
				'editable' => 1,
				'validate' => $key
			);
		}

		$id = um_user('ID');
		$fields = apply_filters('um_account_secure_fields', $fields, $id);

		UM()->builtin()->saved_fields = $fields;
		UM()->builtin()->set_custom_fields(); 

		$output = '';
		foreach($fields as $key => $data) $output .= UM()->fields()->edit_field($key, $data);

		echo $output;
	}

	add_action('um_submit_account_errors_hook', 'my_submit_account_errors', 10, 1);
	function my_submit_account_errors($submitted){
		global $ultimatemember;
	}


	add_filter('um_account_tab_general_fields', function($args){
		if(is_string($args)){
			$args = "$args,gender";
		}
		
		add_filter("um_radio_field_options_gender", function($options){
			if(is_array($options)){
				$options = array(
					'Male' => __('Male', 'ultimate-member'),
					'Female' => __('Female', 'ultimate-member')
				);
			}
			
			return $options;
		}, 20);
			
		return $args;
	});

	remove_filter('um_account_content_hook_notifications', 'um_mailchimp_account_tab', 100 );
?>