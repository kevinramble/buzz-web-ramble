<?php
    // App post - post type
	function create_app_post(){
		register_post_type('app-post',
		array(
			'labels' => array(
				'name' => 'App posts',
						'singular_name' => 'App post',
						'add_new' => 'Add new',
						'add_new_item' => 'Add new app post',
						'edit_item' => 'Edit app post',
						'new_item' => 'New app post',
						'all_items' => 'All app posts',
						'view_item' => 'View app post',
						'search_items' => 'Search app posts',
						'not_found' => 'No app post found',
						'not_found_in_trash' => 'No app posts found in trash',
						'parent_item_colon' => '',
						'menu_name' => 'App posts'
                    ),
                    'taxonomies' => array('category'),
					'public' => true,
					'menu_client' => 5,
					'menu_position' => 3,
					'menu_icon' => 'dashicons-smartphone',
					'supports' => array('title', 'thumbnail', 'editor', 'excerpt', 'author'),
					'show_in_rest' => true,
					'has_archive' => true,
					'rewrite' => array('slug' => 'app-post', 'with_front' => false)
				)
			);
	}
	add_action('init', 'create_app_post');
?>