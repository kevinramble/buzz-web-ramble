<?php
    function filter_results_ajax_handler(){
		$args = json_decode(stripslashes($_POST['query']), true);
		$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
		$args['post_status'] = 'publish';
		$args['suppress_filters'] = 0;
		query_posts($args);

		global $post;
		global $wp_query; ?>

		<script>max_page = <?php echo $wp_query->max_num_pages ?></script>

		<?php if($args['post_type'] == 'job' && have_posts()) :
			while(have_posts()): the_post();
				loop_joblisting($post->ID);
			endwhile;

		elseif($args['post_type'] == 'offer' && have_posts()) :
			while(have_posts()): the_post();
				loop_offerlisting($post->ID);
			endwhile;

		elseif($args['post_type'] == 'out' && have_posts()) :
			while(have_posts()): the_post();
				loop_outlisting($post->ID);
			endwhile;

		elseif(($args['post_type'] != 'job' || $args['post_type'] != 'offer' || $args['post_type'] != 'out') && $args['s'] != '' && have_posts()) :
			while(have_posts()): the_post();
				loop_genericcard($post->ID);
			endwhile;

		elseif($args['s'] != '' && !(have_posts())) : ?>
			<div class="c-search-no-results">
				<h2>We couldn't find anything for <span>"<?php echo esc_html(get_search_query(false)); ?>"</span>.</h2>
				<p>Please check your spelling, try more general, or fewer words and try again!</p>
			</div>
			
		<?php else : ?>
			<div class="c-search-no-results">
				<h2>We couldn't find anything for your search.</h2>
			</div>
		<?php endif;
		die; // here we exit the script and no wp_reset_query() required!
	}
	add_action('wp_ajax_filter_results', 'filter_results_ajax_handler'); // wp_ajax_{action}
	add_action('wp_ajax_nopriv_filter_results', 'filter_results_ajax_handler'); // wp_ajax_nopriv_{action}
?>