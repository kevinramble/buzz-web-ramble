jQuery(document).ready(function($) {
	var today = new Date()
	var dd = String(today.getDate()).padStart(2, '0')
	var mm = String(today.getMonth() + 1).padStart(2, '0') // January is 0
	var yyyy = today.getFullYear()
	today = yyyy + '-' + mm + '-' + dd

	// SEARCH FILTERS
	if ($('.js-search-filters').length > 0) {
		var newCat = '',
			newSubCat = '',
			newCountry = '',
			newTerm = '',
			newTaxonomy = '',
			newTaxQuery = {},
			hasCat = false,
			hasSubCat = false,
			hasCountry = false,
			newType = ['post', 'video'],
			filterType = '',
			searchName = $('.js-search-term'),
			originalSearchTerm = searchName.html(),
			newSearchTerm = originalSearchTerm

		// SEARCH INPUT
		$('.js-submit-search').submit(function() {
			newSearchTerm = $(this)
				.find('input')
				.val()
			current_page = 0
			data = {
				action: 'filter_results',
				query: posts,
				page: current_page
			}

			if (!newSearchTerm) newSearchTerm = originalSearchTerm

			var originalURL = window.location.href
			originalURL = originalURL.split('?s=')
			var newURL = originalURL[0] + '?s=' + newSearchTerm
			history.pushState(null, '', newURL)
			document.title = 'Search for "' + newSearchTerm + '" – Buzz'

			searchName.html(newSearchTerm)

			search_ajax()
			return false
		})

		$('.js-filter-select').change(function() {
			current_page = 0
			filterType = $(this).data('type')
			data = {
				action: 'filter_results',
				query: posts,
				page: current_page
			}

			if (filterType == 'cat' && $(this).val()) {
				newCat = $(this).val()
				hasCat = true
			} else if (filterType == 'cat' && !$(this).val()) {
				newCat = ''
				hasCat = false
			}

			if (filterType == 'subcat' && $(this).val()) {
				newSubCat = $(this).val()
				hasSubCat = true
			} else if (filterType == 'subcat' && !$(this).val()) {
				newSubCat = ''
				hasSubCat = false
			}

			if (filterType == 'countries' && $(this).val()) {
				newCountry = $(this).val()
				hasCountry = true
			} else if (filterType == 'countries' && !$(this).val()) {
				newCountry = ''
				hasCountry = false
			}

			if (hasCat && !hasSubCat && !hasCountry) {
				newTaxonomy = 'category'
				newTerm = newCat
				newTaxQuery = {}
			}

			if (!hasCat && hasSubCat && !hasCountry) {
				newTaxonomy = 'sub-category'
				newTerm = newSubCat
				newTaxQuery = {}
			}

			if (!hasCat && !hasSubCat && hasCountry) {
				newTaxonomy = 'countries'
				newTerm = newCountry
				newTaxQuery = {}
			}

			if (!hasCat && !hasSubCat && !hasCountry) {
				newTaxonomy = ''
				newTerm = ''
				newTaxQuery = {}
			}

			if (hasCat && hasSubCat) {
				newTaxonomy = ''
				newTerm = ''
				newTaxQuery = {
					relation: 'AND',
					0: { taxonomy: 'category', field: 'slug', terms: newCat },
					1: { taxonomy: 'sub-category', field: 'slug', terms: newSubCat }
				}
			}

			if (hasCat && hasCountry) {
				newTaxonomy = ''
				newTerm = ''
				newTaxQuery = {
					relation: 'AND',
					0: { taxonomy: 'category', field: 'slug', terms: newCat },
					1: { taxonomy: 'countries', field: 'slug', terms: newCountry }
				}
			}

			if (hasSubCat && hasCountry) {
				newTaxonomy = ''
				newTerm = ''
				newTaxQuery = {
					relation: 'AND',
					0: { taxonomy: 'sub-category', field: 'slug', terms: newSubCat },
					1: { taxonomy: 'countries', field: 'slug', terms: newCountry }
				}
			}

			if (hasCat && hasSubCat && hasCountry) {
				newTaxonomy = ''
				newTerm = ''
				newTaxQuery = {
					relation: 'AND',
					0: { taxonomy: 'category', field: 'slug', terms: newCat },
					1: { taxonomy: 'sub-category', field: 'slug', terms: newSubCat },
					2: { taxonomy: 'countries', field: 'slug', terms: newCountry }
				}
			}

			if (filterType == 'type') {
				if ($(this).val()) {
					newType = $(this).val()
				} else {
					newType = ['post', 'video']
				}
			}

			search_ajax()
		})

		function search_ajax() {
			newQuery = JSON.parse(posts)
			newQuery.taxonomy = newTaxonomy
			newQuery.term = newTerm
			newQuery.tax_query = newTaxQuery
			newQuery.post_type = newType
			newQuery.s = newSearchTerm
			posts = JSON.stringify(newQuery)
			data.query = posts

			$.ajax({
				url: filter_results_params.ajaxurl,
				data: data,
				type: 'POST',
				beforeSend: function(xhr) {
					$('.js-posts-list').addClass('loading')
				},
				success: function(data) {
					$('.js-posts-list').removeClass('loading')
					if (data) {
						$('.js-posts-list').html(data)

						if (max_page > 1) {
							$('.js-loadmore').removeClass('hidden')
						} else {
							$('.js-loadmore').addClass('hidden')
						}
					} else {
						$('.js-posts-list').html('Nothing to show.')
					}
				}
			})
		}
	} // search filters

	// OFFERS FILTERS
	if ($('.js-offer-filters').length > 0) {
		var newCat = '',
			filterType = '',
			newSearchTerm = '',
			newMetaQuery = {},
			hasCat = false,
			hasCountry = false,
			hasState = false

		// SEARCH INPUT
		$('.js-submit-search').submit(function() {
			newSearchTerm = $(this)
				.find('input')
				.val()
			current_page = 0
			data = {
				action: 'filter_results',
				query: posts,
				page: current_page
			}

			offer_ajax()
			return false
		})

		if ($('#offer_country').val() != '') {
			newCountry = $('#offer_country').val()
			hasCountry = true
		}

		$('.js-filter-select').change(function() {
			current_page = 0
			filterType = $(this).data('type')
			data = {
				action: 'filter_results',
				query: posts,
				page: current_page
			}

			if (filterType == 'cat' && $(this).val()) {
				hasCat = true
				newCat = $(this).val()
			} else if (filterType == 'cat' && !$(this).val()) {
				newCat = ''
				hasCat = false
			}

			if (filterType == 'country' && $(this).val()) {
				newCountry = $(this).val()
				hasCountry = true
				newState = ''
				hasState = false
			} else if (filterType == 'country' && !$(this).val()) {
				newCountry = ''
				hasCountry = false
				newState = ''
				hasState = false
			}

			if (filterType == 'state' && $(this).val()) {
				newState = $(this).val()
				hasState = true
				newCountry = $('#offer_country').val()
				hasCountry = true
			} else if (filterType == 'state' && !$(this).val()) {
				newState = ''
				hasState = false
			}

			if (!hasCountry && !hasState) {
				newMetaQuery = {}
			}

			if (hasCountry && !hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'offer_country', value: newCountry }
					}
				}
			}

			if (hasCountry && hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'offer_country', value: newCountry },
						1: { key: 'offer_state', value: newState }
					}
				}
			}

			offer_ajax()
		})

		function offer_ajax() {
			newQuery = JSON.parse(posts)
			newQuery.taxonomy = 'offer-category'
			newQuery.term = newCat
			newQuery.s = newSearchTerm
			newQuery.meta_query = newMetaQuery
			newQuery.meta_key = 'offer_expiration'
			newQuery.meta_value = today
			newQuery.meta_compare = '>='
			posts = JSON.stringify(newQuery)
			data.query = posts

			$.ajax({
				url: filter_results_params.ajaxurl,
				data: data,
				type: 'POST',
				beforeSend: function(xhr) {
					$('.js-posts-list').addClass('loading')
				},
				success: function(data) {
					$('.js-posts-list').removeClass('loading')
					if (data) {
						$('.js-posts-list').html(data)

						if (max_page > 1) {
							$('.js-loadmore').removeClass('hidden')
						} else {
							$('.js-loadmore').addClass('hidden')
						}
					} else {
						$('.js-posts-list').html('Nothing to show.')
					}
				}
			})
		}
	} // offers filters

	// JOB FILTERS
	if ($('.js-job-filters').length > 0) {
		var filterType = '',
			newCat = '',
			newOrder = '',
			newOrderby = '',
			searchTerm = '',
			newMetaQuery = {},
			hasOrder = false,
			hasType = false,
			hasCountry = false,
			hasState = false

		// SEARCH INPUT
		$('.js-submit-search').submit(function() {
			searchTerm = $(this)
				.parent()
				.find('input')
				.val()
			current_page = 0
			data = {
				action: 'filter_results',
				query: posts,
				page: current_page
			}

			job_ajax()
			return false
		})

		if ($('#job_country').val() != '') {
			newCountry = $('#job_country').val()
			hasCountry = true
		}

		// SELECTS
		$('.js-filter-select').change(function() {
			filterType = $(this).data('type')
			current_page = 0
			data = {
				action: 'filter_results',
				query: posts,
				page: current_page
			}

			if (filterType == 'company' && $(this).val()) {
				newOrder = $(this).val()
				newOrderby = 'query_order'
				hasOrder = true
			} else if (filterType == 'company' && !$(this).val()) {
				newOrder = 'DESC'
				newOrderby = 'date'
				hasOrder = false
			}

			if (filterType == 'type' && $(this).val()) {
				newType = $(this).val()
				hasType = true
			} else if (filterType == 'type' && !$(this).val()) {
				newType = ''
				hasType = false
			}

			if (filterType == 'country' && $(this).val()) {
				newCountry = $(this).val()
				hasCountry = true
				newState = ''
				hasState = false
			} else if (filterType == 'country' && !$(this).val()) {
				newCountry = ''
				hasCountry = false
				newState = ''
				hasState = false
			}

			if (filterType == 'state' && $(this).val()) {
				newState = $(this).val()
				hasState = true
				newCountry = $('#job_country').val()
				hasCountry = true
			} else if (filterType == 'state' && !$(this).val()) {
				newState = ''
				hasState = false
			}

			if (!hasOrder && !hasType && !hasCountry && !hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' }
					}
				}
				newOrder = 'DESC'
				newOrderby = 'date'
			}

			if (hasOrder && !hasType && !hasCountry && !hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' }
					},
					query_order: { key: 'job_company' }
				}

				newOrderby = 'query_order'
			}

			if (hasOrder && hasType && !hasCountry && !hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' },
						1: { key: 'job_type', value: newType }
					},
					query_order: { key: 'job_company' }
				}

				newOrderby = 'query_order'
			}

			if (hasOrder && hasType && hasCountry && !hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' },
						1: { key: 'job_type', value: newType },
						2: { key: 'job_country', value: newCountry }
					},
					query_order: { key: 'job_company' }
				}

				newOrderby = 'query_order'
			}

			if (!hasOrder && hasType && !hasCountry && !hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' },
						1: { key: 'job_type', value: newType }
					}
				}

				newOrder = 'DESC'
				newOrderby = 'date'
			}

			if (!hasOrder && hasType && hasCountry && !hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' },
						1: { key: 'job_type', value: newType },
						2: { key: 'job_country', value: newCountry }
					}
				}

				newOrder = 'DESC'
				newOrderby = 'date'
			}

			if (!hasOrder && hasType && hasCountry && hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' },
						1: { key: 'job_type', value: newType },
						2: { key: 'job_country', value: newCountry },
						3: { key: 'job_state', value: newState }
					}
				}

				newOrder = 'DESC'
				newOrderby = 'date'
			}

			if (!hasOrder && !hasType && hasCountry && !hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' },
						1: { key: 'job_country', value: newCountry }
					}
				}

				newOrder = 'DESC'
				newOrderby = 'date'
			}

			if (!hasOrder && !hasType && hasCountry && hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' },
						1: { key: 'job_country', value: newCountry },
						3: { key: 'job_state', value: newState }
					}
				}

				newOrder = 'DESC'
				newOrderby = 'date'
			}

			if (hasOrder && !hasType && hasCountry && !hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' },
						1: { key: 'job_country', value: newCountry }
					},
					query_order: { key: 'job_company' }
				}

				newOrderby = 'query_order'
			}

			if (hasOrder && !hasType && hasCountry && hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' },
						1: { key: 'job_country', value: newCountry },
						3: { key: 'job_state', value: newState }
					},
					query_order: { key: 'job_company' }
				}

				newOrderby = 'query_order'
			}

			if (hasOrder && hasType && hasCountry && hasState) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'job_expiration', value: today, compare: '>=' },
						1: { key: 'job_type', value: newType },
						2: { key: 'job_country', value: newCountry },
						3: { key: 'job_state', value: newState }
					},
					query_order: { key: 'job_company' }
				}

				newOrderby = 'query_order'
			}

			job_ajax()
		})

		function job_ajax() {
			newQuery = JSON.parse(posts)
			newQuery.orderby = newOrderby
			newQuery.order = newOrder
			newQuery.meta_query = newMetaQuery
			newQuery.s = searchTerm

			console.log(newQuery)

			posts = JSON.stringify(newQuery)
			data.query = posts

			$.ajax({
				url: filter_results_params.ajaxurl,
				data: data,
				type: 'POST',
				beforeSend: function(xhr) {
					$('.js-posts-list').addClass('loading')
				},
				success: function(data) {
					$('.js-posts-list').removeClass('loading')
					if (data) {
						$('.js-posts-list').html(data)

						if (max_page > 1) {
							$('.js-loadmore').removeClass('hidden')
						} else {
							$('.js-loadmore').addClass('hidden')
						}
					} else {
						$('.js-posts-list').html('Nothing to show.')
					}
				}
			})
		}
	} // job filters

	// OUT FILTERS
	if ($('.js-out-filters').length > 0) {
		var newCat = '',
			filterType = '',
			newSearchTerm = '',
			newMetaQuery = {},
			newStartDate = '',
			newEndDate = '',
			hasDate = false,
			hasCat = false,
			hasCountry = false,
			hasState = false

		if ($('#out_country').val() != '') {
			newCountry = $('#out_country').val()
			hasCountry = true
		}

		window.runDateRange = function(startDate, endDate) {
			current_page = 0
			data = {
				action: 'filter_results',
				query: posts,
				page: current_page
			}
			newStartDate = startDate
			newEndDate = endDate
			hasDate = true

			if (hasCountry && !hasState && hasDate) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'out_country', value: newCountry },
						1: {
							key: 'out_date',
							value: { newStartDate, newEndDate },
							compare: 'BETWEEN',
							type: 'DATE'
						}
					}
				}
			}

			if (hasCountry && hasState && hasDate) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'out_country', value: newCountry },
						1: { key: 'out_state', value: newState },
						2: {
							key: 'out_date',
							value: { newStartDate, newEndDate },
							compare: 'BETWEEN',
							type: 'DATE'
						}
					}
				}
			}

			if (!hasCountry && !hasState && hasDate) {
				newMetaQuery = {
					query_type: {
						0: {
							key: 'out_date',
							value: { newStartDate, newEndDate },
							compare: 'BETWEEN',
							type: 'DATE'
						}
					}
				}
			}

			out_ajax()
		}

		window.clearDateRange = function() {
			current_page = 0
			data = {
				action: 'filter_results',
				query: posts,
				page: current_page
			}
			hasDate = false

			if (hasCountry && !hasState && !hasDate) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'out_date', value: today, compare: '>=' },
						1: { key: 'out_country', value: newCountry }
					}
				}
			}

			if (hasCountry && hasState && !hasDate) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'out_date', value: today, compare: '>=' },
						1: { key: 'out_country', value: newCountry },
						2: { key: 'out_state', value: newState }
					}
				}
			}

			if (!hasCountry && !hasState && !hasDate) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'out_date', value: today, compare: '>=' }
					}
				}
			}

			out_ajax()
		}

		// SEARCH INPUT
		$('.js-submit-search').submit(function() {
			newSearchTerm = $(this)
				.find('input')
				.val()
			current_page = 0
			data = {
				action: 'filter_results',
				query: posts,
				page: current_page
			}

			out_ajax()
			return false
		})

		$('.js-filter-select').change(function() {
			current_page = 0
			filterType = $(this).data('type')
			data = {
				action: 'filter_results',
				query: posts,
				page: current_page
			}

			if (filterType == 'cat' && $(this).val()) {
				hasCat = true
				newTaxonomy = 'out-category'
				newCat = $(this).val()
			} else if (filterType == 'cat' && !$(this).val()) {
				newCat = ''
				newTaxonomy = ''
				hasCat = false
			}

			if (filterType == 'country' && $(this).val()) {
				newCountry = $(this).val()
				hasCountry = true
				newState = ''
				hasState = false
			} else if (filterType == 'country' && !$(this).val()) {
				newCountry = ''
				hasCountry = false
				newState = ''
				hasState = false
			}

			if (filterType == 'state' && $(this).val()) {
				newState = $(this).val()
				hasState = true
				newCountry = $('#out_country').val()
				hasCountry = true
			} else if (filterType == 'state' && !$(this).val()) {
				newState = ''
				hasState = false
			}

			if (!hasCountry && !hasState && !hasDate) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'out_date', value: today, compare: '>=' }
					}
				}
			}

			if (hasCountry && !hasState && hasDate) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'out_country', value: newCountry },
						1: {
							key: 'out_date',
							value: { newStartDate, newEndDate },
							compare: 'BETWEEN',
							type: 'DATE'
						}
					}
				}
			}

			if (!hasCountry && !hasState && hasDate) {
				newMetaQuery = {
					query_type: {
						0: {
							key: 'out_date',
							value: { newStartDate, newEndDate },
							compare: 'BETWEEN',
							type: 'DATE'
						}
					}
				}
			}

			if (hasCountry && !hasState && !hasDate) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'out_date', value: today, compare: '>=' },
						1: { key: 'out_country', value: newCountry }
					}
				}
			}

			if (hasCountry && hasState && !hasDate) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'out_date', value: today, compare: '>=' },
						1: { key: 'out_country', value: newCountry },
						2: { key: 'out_state', value: newState }
					}
				}
			}

			if (hasCountry && hasState && hasDate) {
				newMetaQuery = {
					relation: 'AND',
					query_type: {
						0: { key: 'out_country', value: newCountry },
						1: { key: 'out_state', value: newState },
						2: {
							key: 'out_date',
							value: { newStartDate, newEndDate },
							compare: 'BETWEEN',
							type: 'DATE'
						}
					}
				}
			}

			out_ajax()
		})

		function out_ajax() {
			newQuery = JSON.parse(posts)
			newQuery.taxonomy = newTaxonomy
			newQuery.term = newCat
			newQuery.s = newSearchTerm
			newQuery.orderby = 'meta_value'
			newQuery.order = 'ASC'
			newQuery.meta_key = 'out_date'
			newQuery.meta_query = newMetaQuery
			posts = JSON.stringify(newQuery)
			data.query = posts

			$.ajax({
				url: filter_results_params.ajaxurl,
				data: data,
				type: 'POST',
				beforeSend: function(xhr) {
					$('.js-posts-list').addClass('loading')
				},
				success: function(data) {
					$('.js-posts-list').removeClass('loading')
					if (data) {
						$('.js-posts-list').html(data)

						if (max_page > 1) {
							$('.js-loadmore').removeClass('hidden')
						} else {
							$('.js-loadmore').addClass('hidden')
						}
					} else {
						$('.js-posts-list').html('Nothing to show.')
					}
				}
			})
		}
	} // outs filters
})
