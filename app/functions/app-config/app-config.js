jQuery(document).ready(function($) {
	var mediaUploader,
		content,
		selection,
		parent,
		slot,
		postTitle,
		postID,
		ajaxURL,
		siteurl = $('.post-manager').data('siteurl'),
		optionName = $('.post-manager').attr('id'),
		contentArray = [],
		countryID = $('#poststuff').data('countryid')

	// Media uploader
	$('.upload-button').click(function(e) {
		var $this = $(this)
		parent = $(this).parents('.slot')
		slot = parent.data('slot')

		e.preventDefault()
		if (mediaUploader) {
			mediaUploader.open()
			return
		}

		mediaUploader = wp.media.frames.file_frame = wp.media({
			id: 'home-config-uploader',
			title: 'Choose a picture',
			library: { type: 'image' },
			button: { text: 'Select' }
		})

		mediaUploader.on('select', function() {
			selection = mediaUploader
				.state()
				.get('selection')
				.toJSON()

			$.each(selection, function(i, img) {
				content = '<a href="#0" class="delete-image dashicons dashicons-trash" title="Remove image"></a>'
				content += '<img src="' + img.url + '" class="added-image">'
				content += '<input type="hidden" class="custom-img" name="' + optionName + '_img' + slot + '" value="' + img.url + '">'

				parent.find('.used-image').html(content)

				$this.html('Change custom image')
			})
		})

		mediaUploader.open()
	}) // upload-button click

	// Remove custom image
	$('.slot').delegate('.delete-image', 'click', function(e) {
		e.preventDefault()

		$(this)
			.parents('.option')
			.find('.upload-button')
			.html('Add custom image')
		$(this)
			.parents('.used-image')
			.html('')
	})

	// Retrieve posts
	$('.post-button').click(function(e) {
		var $this = $(this)
		parent = $(this).parents('.slot')
		catID = parent.find('.cat-select').val()
		subcatID = parent.find('.subcat-select').val()
		type = parent.find('.type-select').val()
		slot = parent.data('slot')
		contentArray = []

		e.preventDefault()

		if (countryID) {
			if (type === 'app-post') {
				ajaxURL = siteurl + '/wp-json/wp/v2/app-post/?countries=' + countryID + '&categories=' + catID + '&per_page=100&sub-category=' + subcatID
			} else if (type === 'video') {
				ajaxURL = siteurl + '/wp-json/wp/v2/video/?countries=' + countryID + '&categories=' + catID + '&per_page=100&sub-category=' + subcatID
			} else {
				ajaxURL = siteurl + '/wp-json/wp/v2/posts/?countries=' + countryID + '&categories=' + catID + '&per_page=100&sub-category=' + subcatID
			}
		} else {
			if (type === 'app-post') {
				ajaxURL = siteurl + '/wp-json/wp/v2/app-post/?categories=' + catID + '&per_page=100&sub-category=' + subcatID
			} else if (type === 'video') {
				ajaxURL = siteurl + '/wp-json/wp/v2/video/?categories=' + catID + '&per_page=100&sub-category=' + subcatID
			} else {
				ajaxURL = siteurl + '/wp-json/wp/v2/posts/?categories=' + catID + '&per_page=100&sub-category=' + subcatID
			}
		}

		if (catID) {
			$(this).addClass('loading')

			$.ajax({
				url: ajaxURL,
				success: function(data) {
					var datalength = data.length

					for (var k = 0; k < datalength; k++) {
						postID = data[k].id
						postTitle = data[k].title.rendered

						contentArray += '<option value="' + postID + '">' + postTitle + '</option>'
					}

					parent.find('.post-info').html('<select class="post-select input-post-id" name="' + optionName + '_post_id' + slot + '"><option value="">Select post</option>' + contentArray + '</select>')
					$this.addClass('hidden')

					$('.post-button').removeClass('loading')
				},
				cache: false
			})
		} else {
			$this.before('<span class="post-error">Please choose a category first.</span>')
		}

		if (parent.hasClass('empty')) parent.removeClass('empty')
	}) // post-button click

	$('.slot').delegate('.post-select', 'change', function() {
		var text = $(this)
				.find(':selected')
				.text(),
			parent = $(this).parents('.slot'),
			slot = parent.data('slot')

		parent.find('.input-post-title').remove()
		parent.find('.post-info').append('<input type="hidden" class="input-post-title" name="' + optionName + '_post_title' + slot + '" value="' + text + '">')
	})

	$('.cat-select').each(function() {
		$(this).change(function() {
			parent = $(this).parents('.slot')

			if (parent.hasClass('empty')) parent.removeClass('empty')

			if (parent.find('span.post-error').is(':visible')) {
				parent.find('span.post-error').remove()
			}

			if (parent.find('.post-button').hasClass('hidden')) {
				parent.find('.post-button').removeClass('hidden')
				parent.find('.post-info').html('')
			}

			if (parent.find('.current-selected').is(':visible')) {
				parent.find('.post-info').html('')
			}
		})
	})

	$('.subcat-select').each(function() {
		$(this).change(function() {
			parent = $(this).parents('.slot')

			if (parent.hasClass('empty')) parent.removeClass('empty')

			if (parent.find('span.post-error').is(':visible')) {
				parent.find('span.post-error').remove()
			}

			if (parent.find('.post-button').hasClass('hidden')) {
				parent.find('.post-button').removeClass('hidden')
				parent.find('.post-info').html('')
			}

			if (parent.find('.current-selected').is(':visible')) {
				parent.find('.post-info').html('')
			}
		})
	})

	$('.type-select').each(function() {
		$(this).change(function() {
			parent = $(this).parents('.slot')

			if (parent.hasClass('empty')) parent.removeClass('empty')

			if (parent.find('span.post-error').is(':visible')) {
				parent.find('span.post-error').remove()
			}

			if (parent.find('.post-button').hasClass('hidden')) {
				parent.find('.post-button').removeClass('hidden')
				parent.find('.post-info').html('')
			}

			if (parent.find('.current-selected').is(':visible')) {
				parent.find('.post-info').html('')
			}
		})
	})

	$('.custom-style').each(function() {
		$(this).change(function() {
			var selectedStyle = $(this).val()
			$(this)
				.parents('.option')
				.find('.item-style')
				.attr('rel', selectedStyle)
		})
	})

	// Remove slot's content
	$('.slot').delegate('.remove-content', 'click', function(e) {
		e.preventDefault()

		parent = $(this).parents('.slot')

		if (!parent.hasClass('empty')) parent.addClass('empty')

		if (parent.find('span.post-error').is(':visible')) {
			parent.find('span.post-error').remove()
		}

		if (parent.find('.post-button').hasClass('hidden')) {
			parent.find('.post-button').removeClass('hidden')
			parent.find('.post-info').html('')
		}

		if (parent.find('.cat-select').val() !== '') {
			parent.find('.cat-select').val('')
		}

		if (parent.find('.subcat-select').val() !== '') {
			parent.find('.subcat-select').val('')
		}

		if (parent.find('.current-selected').is(':visible')) {
			parent.find('.post-info').html('')
		}

		parent.find('.custom-style.dark').attr('checked', 'checked')

		// Custom content
		parent.find('.checkbox-accordion').prop('checked', false)
		if (parent.find('.custom-title').val() !== '') {
			parent.find('.custom-title').val('')
		}
		if (parent.find('.used-image').html() !== '') {
			parent.find('.used-image').html('')
			parent.find('.upload-button').html('Add custom image')
		}
	})

	// Sortable slots
	if ($('.slot').length > 1) {
		$('.slots').sortable({
			items: '.slot',
			cursor: 'move',
			scrollSensitivity: 30,
			scrollSpeed: 30,
			tolerance: 'pointer',
			update: function() {
				$(this)
					.find('.slot')
					.each(function() {
						var $this = $(this),
							oldSlot = parseInt($(this).data('slot')),
							newSlot = parseInt($(this).index() + 1),
							style = $this.find('.item-style').attr('rel')

						if (newSlot !== oldSlot) {
							// update info only if slot changed
							$this.data('slot', newSlot) // data slot
							$this.find('h4').html('Slot ' + newSlot) // slot card title
							$this.find('.cat-select').attr('name', optionName + '_cat' + newSlot) // categories select
							$this.find('.subcat-select').attr('name', optionName + '_subcat' + newSlot) // categories select
							$this.find('.type-select').attr('name', optionName + '_type' + newSlot) // categories select
							$this.find('.input-post-title').attr('name', optionName + '_post_title' + newSlot) // post title
							$this.find('.input-post-id').attr('name', optionName + '_post_id' + newSlot) // post id
							$this.find('.custom-title').attr('name', optionName + '_title' + newSlot) // custom title, optional
							$this.find('.custom-img').attr('name', optionName + '_img' + newSlot) // custom img, optional
							$this.find('.custom-style').attr('name', optionName + '_style' + newSlot) // custom style, optional
							$this.find('.custom-style.' + style).attr('checked', 'checked')
						}
					})
			}
		}) // sortable
	} // length if
})
