<?php
    // BIG BLOCK - 1 POST
    function app_full_block_hot_add_options(){
		add_option('app_full_block_hot_cat', '', '', 'yes');
		add_option('app_full_block_hot_subcat', '', '', 'yes');
		add_option('app_full_block_hot_type', '', '', 'yes');
		add_option('app_full_block_hot_post_id', '', '', 'yes');
		add_option('app_full_block_hot_post_title', '', '', 'yes');
		add_option('app_full_block_hot_title', '', '', 'yes');
		add_option('app_full_block_hot_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'app_full_block_hot_add_options');
	
	function app_full_block_hot_settings(){
		register_setting('app_full_block_hot_options', 'app_full_block_hot_cat');
		register_setting('app_full_block_hot_options', 'app_full_block_hot_subcat');
		register_setting('app_full_block_hot_options', 'app_full_block_hot_type');
		register_setting('app_full_block_hot_options', 'app_full_block_hot_post_id');
		register_setting('app_full_block_hot_options', 'app_full_block_hot_post_title');
		register_setting('app_full_block_hot_options', 'app_full_block_hot_title');
		register_setting('app_full_block_hot_options', 'app_full_block_hot_img');
	}
	add_action('admin_init', 'app_full_block_hot_settings');
	
	function app_full_block_hot($arg){
		if ($arg == 'app_full_block_hot_cat') {return get_option('app_full_block_hot_cat');}
		if ($arg == 'app_full_block_hot_subcat') {return get_option('app_full_block_hot_subcat');}
		if ($arg == 'app_full_block_hot_type') {return get_option('app_full_block_hot_type');}
		if ($arg == 'app_full_block_hot_post_id') {return get_option('app_full_block_hot_post_id');}
		if ($arg == 'app_full_block_hot_post_title') {return get_option('app_full_block_hot_post_title');}
		if ($arg == 'app_full_block_hot_title') {return get_option('app_full_block_hot_title');}
		if ($arg == 'app_full_block_hot_img') {return get_option('app_full_block_hot_img');}
	};
?>