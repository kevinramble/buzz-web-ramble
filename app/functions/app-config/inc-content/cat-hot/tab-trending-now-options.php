<?php
    // TRENDING NOW - 9 POSTS
    function app_trending_now_hot_add_options(){
		for($i = 1; $i <= 9; $i++){
			add_option('app_trending_now_hot_cat'.$i, '', '', 'yes');
			add_option('app_trending_now_hot_subcat'.$i, '', '', 'yes');
			add_option('app_trending_now_hot_type'.$i, '', '', 'yes');
			add_option('app_trending_now_hot_post_id'.$i, '', '', 'yes');
			add_option('app_trending_now_hot_post_title'.$i, '', '', 'yes');
			add_option('app_trending_now_hot_title'.$i, '', '', 'yes');
			add_option('app_trending_now_hot_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'app_trending_now_hot_add_options');
	
	function app_trending_now_hot_settings(){
		for($i = 1; $i <= 9; $i++){
			register_setting('app_trending_now_hot_options', 'app_trending_now_hot_cat'.$i);
			register_setting('app_trending_now_hot_options', 'app_trending_now_hot_subcat'.$i);
			register_setting('app_trending_now_hot_options', 'app_trending_now_hot_type'.$i);
			register_setting('app_trending_now_hot_options', 'app_trending_now_hot_post_id'.$i);
			register_setting('app_trending_now_hot_options', 'app_trending_now_hot_post_title'.$i);
			register_setting('app_trending_now_hot_options', 'app_trending_now_hot_title'.$i);
			register_setting('app_trending_now_hot_options', 'app_trending_now_hot_img'.$i);
		}
	}
	add_action('admin_init', 'app_trending_now_hot_settings');
	
	function app_trending_now_hot($arg){
		for($i = 1; $i <= 9; $i++){
			if ($arg == 'app_trending_now_hot_cat'.$i) {return get_option('app_trending_now_hot_cat'.$i);}
			if ($arg == 'app_trending_now_hot_subcat'.$i) {return get_option('app_trending_now_hot_subcat'.$i);}
			if ($arg == 'app_trending_now_hot_type'.$i) {return get_option('app_trending_now_hot_type'.$i);}
			if ($arg == 'app_trending_now_hot_post_id'.$i) {return get_option('app_trending_now_hot_post_id'.$i);}
			if ($arg == 'app_trending_now_hot_post_title'.$i) {return get_option('app_trending_now_hot_post_title'.$i);}
			if ($arg == 'app_trending_now_hot_title'.$i) {return get_option('app_trending_now_hot_title'.$i);}
			if ($arg == 'app_trending_now_hot_img'.$i) {return get_option('app_trending_now_hot_img'.$i);}
		}
	};
?>