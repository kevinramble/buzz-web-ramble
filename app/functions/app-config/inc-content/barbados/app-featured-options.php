<?php
    // APP FEATURED - 20 POSTS
    function app_featured_barbados_add_options(){
		for($i = 1; $i <= 20; $i++){
			add_option('app_featured_barbados_cat'.$i, '', '', 'yes');
			add_option('app_featured_barbados_subcat'.$i, '', '', 'yes');
			add_option('app_featured_barbados_type'.$i, '', '', 'yes');
			add_option('app_featured_barbados_post_id'.$i, '', '', 'yes');
			add_option('app_featured_barbados_post_title'.$i, '', '', 'yes');
			add_option('app_featured_barbados_title'.$i, '', '', 'yes');
			add_option('app_featured_barbados_img'.$i, '', '', 'yes');
			add_option('app_featured_barbados_style'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'app_featured_barbados_add_options');
	
	function app_featured_barbados_settings(){
		for($i = 1; $i <= 20; $i++){
			register_setting('app_featured_barbados_options', 'app_featured_barbados_cat'.$i);
			register_setting('app_featured_barbados_options', 'app_featured_barbados_subcat'.$i);
			register_setting('app_featured_barbados_options', 'app_featured_barbados_type'.$i);
			register_setting('app_featured_barbados_options', 'app_featured_barbados_post_id'.$i);
			register_setting('app_featured_barbados_options', 'app_featured_barbados_post_title'.$i);
			register_setting('app_featured_barbados_options', 'app_featured_barbados_title'.$i);
			register_setting('app_featured_barbados_options', 'app_featured_barbados_img'.$i);
			register_setting('app_featured_barbados_options', 'app_featured_barbados_style'.$i);
		}
	}
	add_action('admin_init', 'app_featured_barbados_settings');
	
	function app_featured_barbados($arg){
		for($i = 1; $i <= 20; $i++){
			if ($arg == 'app_featured_barbados_cat'.$i) {return get_option('app_featured_barbados_cat'.$i);}
			if ($arg == 'app_featured_barbados_subcat'.$i) {return get_option('app_featured_barbados_subcat'.$i);}
			if ($arg == 'app_featured_barbados_type'.$i) {return get_option('app_featured_barbados_type'.$i);}
			if ($arg == 'app_featured_barbados_post_id'.$i) {return get_option('app_featured_barbados_post_id'.$i);}
			if ($arg == 'app_featured_barbados_post_title'.$i) {return get_option('app_featured_barbados_post_title'.$i);}
			if ($arg == 'app_featured_barbados_title'.$i) {return get_option('app_featured_barbados_title'.$i);}
			if ($arg == 'app_featured_barbados_img'.$i) {return get_option('app_featured_barbados_img'.$i);}
			if ($arg == 'app_featured_barbados_style'.$i) {return get_option('app_featured_barbados_style'.$i);}
		}
	};
?>