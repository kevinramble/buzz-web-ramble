<?php
    // APP BREAKING - 20 POSTS
    function app_breaking_turksandcaicos_add_options(){
		for($i = 1; $i <= 20; $i++){
			add_option('app_breaking_turksandcaicos_cat'.$i, '', '', 'yes');
			add_option('app_breaking_turksandcaicos_subcat'.$i, '', '', 'yes');
			add_option('app_breaking_turksandcaicos_type'.$i, '', '', 'yes');
			add_option('app_breaking_turksandcaicos_post_id'.$i, '', '', 'yes');
			add_option('app_breaking_turksandcaicos_post_title'.$i, '', '', 'yes');
			add_option('app_breaking_turksandcaicos_title'.$i, '', '', 'yes');
			add_option('app_breaking_turksandcaicos_img'.$i, '', '', 'yes');
			add_option('app_breaking_turksandcaicos_style'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'app_breaking_turksandcaicos_add_options');
	
	function app_breaking_turksandcaicos_settings(){
		for($i = 1; $i <= 20; $i++){
			register_setting('app_breaking_turksandcaicos_options', 'app_breaking_turksandcaicos_cat'.$i);
			register_setting('app_breaking_turksandcaicos_options', 'app_breaking_turksandcaicos_subcat'.$i);
			register_setting('app_breaking_turksandcaicos_options', 'app_breaking_turksandcaicos_type'.$i);
			register_setting('app_breaking_turksandcaicos_options', 'app_breaking_turksandcaicos_post_id'.$i);
			register_setting('app_breaking_turksandcaicos_options', 'app_breaking_turksandcaicos_post_title'.$i);
			register_setting('app_breaking_turksandcaicos_options', 'app_breaking_turksandcaicos_title'.$i);
			register_setting('app_breaking_turksandcaicos_options', 'app_breaking_turksandcaicos_img'.$i);
			register_setting('app_breaking_turksandcaicos_options', 'app_breaking_turksandcaicos_style'.$i);
		}
	}
	add_action('admin_init', 'app_breaking_turksandcaicos_settings');
	
	function app_breaking_turksandcaicos($arg){
		for($i = 1; $i <= 20; $i++){
			if ($arg == 'app_breaking_turksandcaicos_cat'.$i) {return get_option('app_breaking_turksandcaicos_cat'.$i);}
			if ($arg == 'app_breaking_turksandcaicos_subcat'.$i) {return get_option('app_breaking_turksandcaicos_subcat'.$i);}
			if ($arg == 'app_breaking_turksandcaicos_type'.$i) {return get_option('app_breaking_turksandcaicos_type'.$i);}
			if ($arg == 'app_breaking_turksandcaicos_post_id'.$i) {return get_option('app_breaking_turksandcaicos_post_id'.$i);}
			if ($arg == 'app_breaking_turksandcaicos_post_title'.$i) {return get_option('app_breaking_turksandcaicos_post_title'.$i);}
			if ($arg == 'app_breaking_turksandcaicos_title'.$i) {return get_option('app_breaking_turksandcaicos_title'.$i);}
			if ($arg == 'app_breaking_turksandcaicos_img'.$i) {return get_option('app_breaking_turksandcaicos_img'.$i);}
			if ($arg == 'app_breaking_turksandcaicos_style'.$i) {return get_option('app_breaking_turksandcaicos_style'.$i);}
		}
	};
?>