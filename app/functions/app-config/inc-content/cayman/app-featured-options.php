<?php
    // APP FEATURED - 20 POSTS
    function app_featured_cayman_add_options(){
		for($i = 1; $i <= 20; $i++){
			add_option('app_featured_cayman_cat'.$i, '', '', 'yes');
			add_option('app_featured_cayman_subcat'.$i, '', '', 'yes');
			add_option('app_featured_cayman_type'.$i, '', '', 'yes');
			add_option('app_featured_cayman_post_id'.$i, '', '', 'yes');
			add_option('app_featured_cayman_post_title'.$i, '', '', 'yes');
			add_option('app_featured_cayman_title'.$i, '', '', 'yes');
			add_option('app_featured_cayman_img'.$i, '', '', 'yes');
			add_option('app_featured_cayman_style'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'app_featured_cayman_add_options');
	
	function app_featured_cayman_settings(){
		for($i = 1; $i <= 20; $i++){
			register_setting('app_featured_cayman_options', 'app_featured_cayman_cat'.$i);
			register_setting('app_featured_cayman_options', 'app_featured_cayman_subcat'.$i);
			register_setting('app_featured_cayman_options', 'app_featured_cayman_type'.$i);
			register_setting('app_featured_cayman_options', 'app_featured_cayman_post_id'.$i);
			register_setting('app_featured_cayman_options', 'app_featured_cayman_post_title'.$i);
			register_setting('app_featured_cayman_options', 'app_featured_cayman_title'.$i);
			register_setting('app_featured_cayman_options', 'app_featured_cayman_img'.$i);
			register_setting('app_featured_cayman_options', 'app_featured_cayman_style'.$i);
		}
	}
	add_action('admin_init', 'app_featured_cayman_settings');
	
	function app_featured_cayman($arg){
		for($i = 1; $i <= 20; $i++){
			if ($arg == 'app_featured_cayman_cat'.$i) {return get_option('app_featured_cayman_cat'.$i);}
			if ($arg == 'app_featured_cayman_subcat'.$i) {return get_option('app_featured_cayman_subcat'.$i);}
			if ($arg == 'app_featured_cayman_type'.$i) {return get_option('app_featured_cayman_type'.$i);}
			if ($arg == 'app_featured_cayman_post_id'.$i) {return get_option('app_featured_cayman_post_id'.$i);}
			if ($arg == 'app_featured_cayman_post_title'.$i) {return get_option('app_featured_cayman_post_title'.$i);}
			if ($arg == 'app_featured_cayman_title'.$i) {return get_option('app_featured_cayman_title'.$i);}
			if ($arg == 'app_featured_cayman_img'.$i) {return get_option('app_featured_cayman_img'.$i);}
			if ($arg == 'app_featured_cayman_style'.$i) {return get_option('app_featured_cayman_style'.$i);}
		}
	};
?>