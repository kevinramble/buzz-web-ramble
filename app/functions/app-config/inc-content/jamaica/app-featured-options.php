<?php
    // APP FEATURED - 20 POSTS
    function app_featured_jamaica_add_options(){
		for($i = 1; $i <= 20; $i++){
			add_option('app_featured_jamaica_cat'.$i, '', '', 'yes');
			add_option('app_featured_jamaica_subcat'.$i, '', '', 'yes');
			add_option('app_featured_jamaica_type'.$i, '', '', 'yes');
			add_option('app_featured_jamaica_post_id'.$i, '', '', 'yes');
			add_option('app_featured_jamaica_post_title'.$i, '', '', 'yes');
			add_option('app_featured_jamaica_title'.$i, '', '', 'yes');
			add_option('app_featured_jamaica_img'.$i, '', '', 'yes');
			add_option('app_featured_jamaica_style'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'app_featured_jamaica_add_options');
	
	function app_featured_jamaica_settings(){
		for($i = 1; $i <= 20; $i++){
			register_setting('app_featured_jamaica_options', 'app_featured_jamaica_cat'.$i);
			register_setting('app_featured_jamaica_options', 'app_featured_jamaica_subcat'.$i);
			register_setting('app_featured_jamaica_options', 'app_featured_jamaica_type'.$i);
			register_setting('app_featured_jamaica_options', 'app_featured_jamaica_post_id'.$i);
			register_setting('app_featured_jamaica_options', 'app_featured_jamaica_post_title'.$i);
			register_setting('app_featured_jamaica_options', 'app_featured_jamaica_title'.$i);
			register_setting('app_featured_jamaica_options', 'app_featured_jamaica_img'.$i);
			register_setting('app_featured_jamaica_options', 'app_featured_jamaica_style'.$i);
		}
	}
	add_action('admin_init', 'app_featured_jamaica_settings');
	
	function app_featured_jamaica($arg){
		for($i = 1; $i <= 20; $i++){
			if ($arg == 'app_featured_jamaica_cat'.$i) {return get_option('app_featured_jamaica_cat'.$i);}
			if ($arg == 'app_featured_jamaica_subcat'.$i) {return get_option('app_featured_jamaica_subcat'.$i);}
			if ($arg == 'app_featured_jamaica_type'.$i) {return get_option('app_featured_jamaica_type'.$i);}
			if ($arg == 'app_featured_jamaica_post_id'.$i) {return get_option('app_featured_jamaica_post_id'.$i);}
			if ($arg == 'app_featured_jamaica_post_title'.$i) {return get_option('app_featured_jamaica_post_title'.$i);}
			if ($arg == 'app_featured_jamaica_title'.$i) {return get_option('app_featured_jamaica_title'.$i);}
			if ($arg == 'app_featured_jamaica_img'.$i) {return get_option('app_featured_jamaica_img'.$i);}
			if ($arg == 'app_featured_jamaica_style'.$i) {return get_option('app_featured_jamaica_style'.$i);}
		}
	};
?>