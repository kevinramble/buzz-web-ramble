<?php
    // APP FEATURED - 20 POSTS
    function app_featured_add_options(){
		for($i = 1; $i <= 20; $i++){
			add_option('app_featured_cat'.$i, '', '', 'yes');
			add_option('app_featured_subcat'.$i, '', '', 'yes');
			add_option('app_featured_type'.$i, '', '', 'yes');
			add_option('app_featured_post_id'.$i, '', '', 'yes');
			add_option('app_featured_post_title'.$i, '', '', 'yes');
			add_option('app_featured_title'.$i, '', '', 'yes');
			add_option('app_featured_img'.$i, '', '', 'yes');
			add_option('app_featured_style'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'app_featured_add_options');
	
	function app_featured_settings(){
		for($i = 1; $i <= 20; $i++){
			register_setting('app_featured_options', 'app_featured_cat'.$i);
			register_setting('app_featured_options', 'app_featured_subcat'.$i);
			register_setting('app_featured_options', 'app_featured_type'.$i);
			register_setting('app_featured_options', 'app_featured_post_id'.$i);
			register_setting('app_featured_options', 'app_featured_post_title'.$i);
			register_setting('app_featured_options', 'app_featured_title'.$i);
			register_setting('app_featured_options', 'app_featured_img'.$i);
			register_setting('app_featured_options', 'app_featured_style'.$i);
		}
	}
	add_action('admin_init', 'app_featured_settings');
	
	function app_featured($arg){
		for($i = 1; $i <= 20; $i++){
			if ($arg == 'app_featured_cat'.$i) {return get_option('app_featured_cat'.$i);}
			if ($arg == 'app_featured_subcat'.$i) {return get_option('app_featured_subcat'.$i);}
			if ($arg == 'app_featured_type'.$i) {return get_option('app_featured_type'.$i);}
			if ($arg == 'app_featured_post_id'.$i) {return get_option('app_featured_post_id'.$i);}
			if ($arg == 'app_featured_post_title'.$i) {return get_option('app_featured_post_title'.$i);}
			if ($arg == 'app_featured_title'.$i) {return get_option('app_featured_title'.$i);}
			if ($arg == 'app_featured_img'.$i) {return get_option('app_featured_img'.$i);}
			if ($arg == 'app_featured_style'.$i) {return get_option('app_featured_style'.$i);}
		}
	};
?>