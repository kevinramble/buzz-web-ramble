<?php
    // MAIN - 4 POSTS
    function app_main_banner_life_add_options(){
		for($i = 1; $i <= 4; $i++){
			add_option('app_main_banner_life_cat'.$i, '', '', 'yes');
			add_option('app_main_banner_life_subcat'.$i, '', '', 'yes');
			add_option('app_main_banner_life_type'.$i, '', '', 'yes');
			add_option('app_main_banner_life_post_id'.$i, '', '', 'yes');
			add_option('app_main_banner_life_post_title'.$i, '', '', 'yes');
			add_option('app_main_banner_life_title'.$i, '', '', 'yes');
			add_option('app_main_banner_life_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'app_main_banner_life_add_options');
	
	function app_main_banner_life_settings(){
		for($i = 1; $i <= 4; $i++){
			register_setting('app_main_banner_life_options', 'app_main_banner_life_cat'.$i);
			register_setting('app_main_banner_life_options', 'app_main_banner_life_subcat'.$i);
			register_setting('app_main_banner_life_options', 'app_main_banner_life_type'.$i);
			register_setting('app_main_banner_life_options', 'app_main_banner_life_post_id'.$i);
			register_setting('app_main_banner_life_options', 'app_main_banner_life_post_title'.$i);
			register_setting('app_main_banner_life_options', 'app_main_banner_life_title'.$i);
			register_setting('app_main_banner_life_options', 'app_main_banner_life_img'.$i);
		}
	}
	add_action('admin_init', 'app_main_banner_life_settings');
	
	function app_main_banner_life($arg){
		for($i = 1; $i <= 4; $i++){
			if ($arg == 'app_main_banner_life_cat'.$i) {return get_option('app_main_banner_life_cat'.$i);}
			if ($arg == 'app_main_banner_life_subcat'.$i) {return get_option('app_main_banner_life_subcat'.$i);}
			if ($arg == 'app_main_banner_life_type'.$i) {return get_option('app_main_banner_life_type'.$i);}
			if ($arg == 'app_main_banner_life_post_id'.$i) {return get_option('app_main_banner_life_post_id'.$i);}
			if ($arg == 'app_main_banner_life_post_title'.$i) {return get_option('app_main_banner_life_post_title'.$i);}
			if ($arg == 'app_main_banner_life_title'.$i) {return get_option('app_main_banner_life_title'.$i);}
			if ($arg == 'app_main_banner_life_img'.$i) {return get_option('app_main_banner_life_img'.$i);}
		}
	};
?>