<?php
	function app_config() {
		add_menu_page('App config', 'App config', 'manage_options', 'app_config', 'app_config_content', 'dashicons-layout', '3.9');
		add_submenu_page('app_config', 'App config – Global', 'Global', 'manage_options', 'app_config', 'app_config');
		add_submenu_page('app_config', 'App config – Jamaica', 'Jamaica', 'manage_options', 'app_config_jamaica', 'app_config_jamaica_content');
		add_submenu_page('app_config', 'App config – St. Lucia', 'St. Lucia', 'manage_options', 'app_config_stlucia', 'app_config_stlucia_content');
		add_submenu_page('app_config', 'App config – Turks and Caicos', 'Turks and Caicos', 'manage_options', 'app_config_turksandcaicos', 'app_config_turksandcaicos_content');
		add_submenu_page('app_config', 'App config – Barbados', 'Barbados', 'manage_options', 'app_config_barbados', 'app_config_barbados_content');
		add_submenu_page('app_config', 'App config – Bahamas', 'Bahamas', 'manage_options', 'app_config_bahamas', 'app_config_bahamas_content');
		add_submenu_page('app_config', 'App config – Cayman', 'Cayman', 'manage_options', 'app_config_cayman', 'app_config_cayman_content');


		add_submenu_page('app_config', 'App config – Hot', 'Hot', 'manage_options', 'app_config_cat_hot', 'app_cat_hot_content');
		add_submenu_page('app_config', 'App config – Life', 'Life', 'manage_options', 'app_config_cat_life', 'app_cat_life_content');
		add_submenu_page('app_config', 'App config – News', 'News', 'manage_options', 'app_config_cat_news', 'app_cat_news_content');
	}
	add_action('admin_menu', 'app_config');

	require_once('inc-content/global/app-featured-options.php');
	require_once('inc-content/global/app-breaking-options.php');

	require_once('inc-content/jamaica/app-featured-options.php');
	require_once('inc-content/jamaica/app-breaking-options.php');

	require_once('inc-content/stlucia/app-featured-options.php');
	require_once('inc-content/stlucia/app-breaking-options.php');

	require_once('inc-content/turksandcaicos/app-featured-options.php');
	require_once('inc-content/turksandcaicos/app-breaking-options.php');

	require_once('inc-content/barbados/app-featured-options.php');
	require_once('inc-content/barbados/app-breaking-options.php');

	require_once('inc-content/bahamas/app-featured-options.php');
	require_once('inc-content/bahamas/app-breaking-options.php');

	require_once('inc-content/cayman/app-featured-options.php');
	require_once('inc-content/cayman/app-breaking-options.php');

	
	require_once('inc-content/cat-hot/tab-main-banner-options.php');
	require_once('inc-content/cat-hot/tab-trending-now-options.php');
	require_once('inc-content/cat-hot/tab-full-block-options.php');
	require_once('inc-content/cat-hot/tab-must-watch-options.php');
	require_once('inc-content/cat-hot/tab-breaking-news-options.php');

	require_once('inc-content/cat-life/tab-main-banner-options.php');
	require_once('inc-content/cat-life/tab-trending-now-options.php');
	require_once('inc-content/cat-life/tab-full-block-options.php');
	require_once('inc-content/cat-life/tab-must-watch-options.php');
	require_once('inc-content/cat-life/tab-breaking-news-options.php');

	require_once('inc-content/cat-news/tab-main-banner-options.php');
	require_once('inc-content/cat-news/tab-trending-now-options.php');
	require_once('inc-content/cat-news/tab-full-block-options.php');
	require_once('inc-content/cat-news/tab-must-watch-options.php');
	require_once('inc-content/cat-news/tab-breaking-news-options.php');

	function app_config_content(){ ?>
		<div class="wrap">
			<h2>Global app configurations</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>
						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'featured'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=app_config&tab=featured" class="nav-tab <?php echo $active_tab == 'featured' ? 'nav-tab-active' : ''; ?>">Featured</a>
							<a href="?page=app_config&tab=breaking" class="nav-tab <?php echo $active_tab == 'breaking' ? 'nav-tab-active' : ''; ?>">Breaking</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'featured'){
								require_once('inc-content/global/app-featured.php');
							
							} elseif($active_tab == 'breaking'){
								require_once('inc-content/global/app-breaking.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // app_config_content()

	function app_config_jamaica_content(){ ?>
		<div class="wrap">
			<h2>Jamaica app configurations</h2>
			
			<?php $getCountry = get_term_by('slug', 'jamaica', 'countries');
			$countryID = $getCountry->term_id; ?>

			<div id="poststuff" data-countryid="<?php echo $countryID; ?>">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>
						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'featured'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=app_config_jamaica&tab=featured" class="nav-tab <?php echo $active_tab == 'featured' ? 'nav-tab-active' : ''; ?>">Featured</a>
							<a href="?page=app_config_jamaica&tab=breaking" class="nav-tab <?php echo $active_tab == 'breaking' ? 'nav-tab-active' : ''; ?>">Breaking</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'featured'){
								require_once('inc-content/jamaica/app-featured.php');
							
							} elseif($active_tab == 'breaking'){
								require_once('inc-content/jamaica/app-breaking.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // app_config_jamaica_content()

	function app_config_stlucia_content(){ ?>
		<div class="wrap">
			<h2>St. Lucia app configurations</h2>
			
			<?php $getCountry = get_term_by('slug', 'st-lucia', 'countries');
			$countryID = $getCountry->term_id; ?>

			<div id="poststuff" data-countryid="<?php echo $countryID; ?>">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>
						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'featured'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=app_config_stlucia&tab=featured" class="nav-tab <?php echo $active_tab == 'featured' ? 'nav-tab-active' : ''; ?>">Featured</a>
							<a href="?page=app_config_stlucia&tab=breaking" class="nav-tab <?php echo $active_tab == 'breaking' ? 'nav-tab-active' : ''; ?>">Breaking</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'featured'){
								require_once('inc-content/stlucia/app-featured.php');
							
							} elseif($active_tab == 'breaking'){
								require_once('inc-content/stlucia/app-breaking.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // app_config_stlucia_content()

	function app_config_turksandcaicos_content(){ ?>
	<div class="wrap">
		<h2>Turks and Caicos app configurations</h2>
		
		<?php $getCountry = get_term_by('slug', 'turks-and-caicos', 'countries');
		$countryID = $getCountry->term_id; ?>

		<div id="poststuff" data-countryid="<?php echo $countryID; ?>">
			<div class="custom-admin-content video-config-options postbox">
				<div class="inside">
					<p class="description">Choose what you would like to edit.</p>
					<?php settings_errors(); ?>

					<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'featured'; ?>
					<div class="nav-tab-wrapper">
						<a href="?page=app_config_turksandcaicos&tab=featured" class="nav-tab <?php echo $active_tab == 'featured' ? 'nav-tab-active' : ''; ?>">Featured</a>
						<a href="?page=app_config_turksandcaicos&tab=breaking" class="nav-tab <?php echo $active_tab == 'breaking' ? 'nav-tab-active' : ''; ?>">Breaking</a>
					</div>

					<form method="post" action="options.php">
						<?php if($active_tab == 'featured'){
							require_once('inc-content/turksandcaicos/app-featured.php');
						
						} elseif($active_tab == 'breaking'){
							require_once('inc-content/turksandcaicos/app-breaking.php');

						} ?>
					</form>
				</div><!-- .inside -->
			</div><!-- #video-config-options -->
		</div><!-- #poststuff -->
	</div><!-- .wrap -->
	<?php }; // app_config_turksandcaicos_content()

	function app_config_barbados_content(){ ?>
		<div class="wrap">
			<h2>Barbados app configurations</h2>
			
			<?php $getCountry = get_term_by('slug', 'barbados', 'countries');
			$countryID = $getCountry->term_id; ?>

			<div id="poststuff" data-countryid="<?php echo $countryID; ?>">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>
						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'featured'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=app_config_barbados&tab=featured" class="nav-tab <?php echo $active_tab == 'featured' ? 'nav-tab-active' : ''; ?>">Featured</a>
							<a href="?page=app_config_barbados&tab=breaking" class="nav-tab <?php echo $active_tab == 'breaking' ? 'nav-tab-active' : ''; ?>">Breaking</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'featured'){
								require_once('inc-content/barbados/app-featured.php');
							
							} elseif($active_tab == 'breaking'){
								require_once('inc-content/barbados/app-breaking.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // app_config_barbados_content()

	function app_config_bahamas_content(){ ?>
		<div class="wrap">
			<h2>Bahamas app configurations</h2>
			
			<?php $getCountry = get_term_by('slug', 'bahamas', 'countries');
			$countryID = $getCountry->term_id; ?>

			<div id="poststuff" data-countryid="<?php echo $countryID; ?>">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>
						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'featured'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=app_config_bahamas&tab=featured" class="nav-tab <?php echo $active_tab == 'featured' ? 'nav-tab-active' : ''; ?>">Featured</a>
							<a href="?page=app_config_bahamas&tab=breaking" class="nav-tab <?php echo $active_tab == 'breaking' ? 'nav-tab-active' : ''; ?>">Breaking</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'featured'){
								require_once('inc-content/bahamas/app-featured.php');
							
							} elseif($active_tab == 'breaking'){
								require_once('inc-content/bahamas/app-breaking.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // app_config_bahamas_content()

	function app_config_cayman_content(){ ?>
		<div class="wrap">
			<h2>Cayman app configurations</h2>
			
			<?php $getCountry = get_term_by('slug', 'cayman', 'countries');
			$countryID = $getCountry->term_id; ?>

			<div id="poststuff" data-countryid="<?php echo $countryID; ?>">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>
						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'featured'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=app_config_cayman&tab=featured" class="nav-tab <?php echo $active_tab == 'featured' ? 'nav-tab-active' : ''; ?>">Featured</a>
							<a href="?page=app_config_cayman&tab=breaking" class="nav-tab <?php echo $active_tab == 'breaking' ? 'nav-tab-active' : ''; ?>">Breaking</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'featured'){
								require_once('inc-content/cayman/app-featured.php');
							
							} elseif($active_tab == 'breaking'){
								require_once('inc-content/cayman/app-breaking.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // app_config_cayman_content()

	function app_breaking_content(){ ?>
		<div class="wrap">
			<h2>App configurations</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<h1 class="mb-neg">Breaking News content</h1>
						<?php settings_errors(); ?>

						<form method="post" action="options.php">
							<?php require_once('inc-content/app-breaking.php'); ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // app_breaking_content()

	function app_cat_hot_content(){ ?>
		<div class="wrap">
			<h2>App configurations</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<h1>Hot: category content</h1>
						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'main-banner'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=app_config_cat_hot&tab=main-banner" class="nav-tab <?php echo $active_tab == 'main-banner' ? 'nav-tab-active' : ''; ?>">Main banner</a>
							<a href="?page=app_config_cat_hot&tab=trending-now" class="nav-tab <?php echo $active_tab == 'trending-now' ? 'nav-tab-active' : ''; ?>">Trending now</a>
							<a href="?page=app_config_cat_hot&tab=full-block" class="nav-tab <?php echo $active_tab == 'full-block' ? 'nav-tab-active' : ''; ?>">Full block</a>
							<a href="?page=app_config_cat_hot&tab=must-watch" class="nav-tab <?php echo $active_tab == 'must-watch' ? 'nav-tab-active' : ''; ?>">Must watch</a>
							<a href="?page=app_config_cat_hot&tab=breaking-news" class="nav-tab <?php echo $active_tab == 'breaking-news' ? 'nav-tab-active' : ''; ?>">Breaking news</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'main-banner'){
								// TAB: MAIN BANNER
								require_once('inc-content/cat-hot/tab-main-banner.php');
							
							} elseif($active_tab == 'trending-now'){
								// TAB: TRENDING NOW
								require_once('inc-content/cat-hot/tab-trending-now.php');

							} elseif($active_tab == 'full-block'){
								// TAB: FULL BLOCK
								require_once('inc-content/cat-hot/tab-full-block.php');

							} elseif($active_tab == 'must-watch'){
								// TAB: MUST WATCH
								require_once('inc-content/cat-hot/tab-must-watch.php');

							} elseif($active_tab == 'breaking-news'){
								// TAB: BREAKING NEWS
								require_once('inc-content/cat-hot/tab-breaking-news.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // app_cat_hot_content()

	function app_cat_life_content(){ ?>
		<div class="wrap">
			<h2>App configurations</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<h1>Life: category content</h1>
						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'main-banner'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=app_config_cat_life&tab=main-banner" class="nav-tab <?php echo $active_tab == 'main-banner' ? 'nav-tab-active' : ''; ?>">Main banner</a>
							<a href="?page=app_config_cat_life&tab=trending-now" class="nav-tab <?php echo $active_tab == 'trending-now' ? 'nav-tab-active' : ''; ?>">Trending now</a>
							<a href="?page=app_config_cat_life&tab=full-block" class="nav-tab <?php echo $active_tab == 'full-block' ? 'nav-tab-active' : ''; ?>">Full block</a>
							<a href="?page=app_config_cat_life&tab=must-watch" class="nav-tab <?php echo $active_tab == 'must-watch' ? 'nav-tab-active' : ''; ?>">Must watch</a>
							<a href="?page=app_config_cat_life&tab=breaking-news" class="nav-tab <?php echo $active_tab == 'breaking-news' ? 'nav-tab-active' : ''; ?>">Breaking news</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'main-banner'){
								// TAB: MAIN BANNER
								require_once('inc-content/cat-life/tab-main-banner.php');
							
							} elseif($active_tab == 'trending-now'){
								// TAB: TRENDING NOW
								require_once('inc-content/cat-life/tab-trending-now.php');

							} elseif($active_tab == 'full-block'){
								// TAB: FULL BLOCK
								require_once('inc-content/cat-life/tab-full-block.php');

							} elseif($active_tab == 'must-watch'){
								// TAB: MUST WATCH
								require_once('inc-content/cat-life/tab-must-watch.php');

							} elseif($active_tab == 'breaking-news'){
								// TAB: BREAKING NEWS
								require_once('inc-content/cat-life/tab-breaking-news.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // app_cat_life_content()

	function app_cat_news_content(){ ?>
		<div class="wrap">
			<h2>App configurations</h2>
			
			<div id="poststuff">
				<div class="custom-admin-content video-config-options postbox">
					<div class="inside">
						<h1>News: category content</h1>
						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'main-banner'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=app_config_cat_news&tab=main-banner" class="nav-tab <?php echo $active_tab == 'main-banner' ? 'nav-tab-active' : ''; ?>">Main banner</a>
							<a href="?page=app_config_cat_news&tab=trending-now" class="nav-tab <?php echo $active_tab == 'trending-now' ? 'nav-tab-active' : ''; ?>">Trending now</a>
							<a href="?page=app_config_cat_news&tab=full-block" class="nav-tab <?php echo $active_tab == 'full-block' ? 'nav-tab-active' : ''; ?>">Full block</a>
							<a href="?page=app_config_cat_news&tab=must-watch" class="nav-tab <?php echo $active_tab == 'must-watch' ? 'nav-tab-active' : ''; ?>">Must watch</a>
							<a href="?page=app_config_cat_news&tab=breaking-news" class="nav-tab <?php echo $active_tab == 'breaking-news' ? 'nav-tab-active' : ''; ?>">Breaking news</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'main-banner'){
								// TAB: MAIN BANNER
								require_once('inc-content/cat-news/tab-main-banner.php');
							
							} elseif($active_tab == 'trending-now'){
								// TAB: TRENDING NOW
								require_once('inc-content/cat-news/tab-trending-now.php');

							} elseif($active_tab == 'full-block'){
								// TAB: FULL BLOCK
								require_once('inc-content/cat-news/tab-full-block.php');

							} elseif($active_tab == 'must-watch'){
								// TAB: MUST WATCH
								require_once('inc-content/cat-news/tab-must-watch.php');

							} elseif($active_tab == 'breaking-news'){
								// TAB: BREAKING NEWS
								require_once('inc-content/cat-news/tab-breaking-news.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #video-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // app_cat_news_content()
?>