var el = wp.element.createElement,
	registerBlockType = wp.blocks.registerBlockType,
	TextControl = wp.components.TextControl

registerBlockType('custom-blocky/related-link', {
	title: 'Related link',
	description: 'Add a related link to the content, with specific layout.',
	category: 'common',
	icon: 'admin-links',
	keywords: ['related', 'link'],

	attributes: {
		linkURL: {
			type: 'url'
		},
		linkText: {
			type: 'text'
		}
	},

	edit: function(props) {
		var linkURL = props.attributes.linkURL
		var linkText = props.attributes.linkText

		return el(
			'div',
			{ className: 'editor-custom-block-related-link' },
			el('p', {}, 'Related link'),
			el(TextControl, {
				type: 'text',
				label: 'Text',
				value: linkText,
				onChange: function(newLinkText) {
					props.setAttributes({ linkText: newLinkText })
				}
			}),
			el(TextControl, {
				type: 'text',
				label: 'URL',
				placeholder: 'http://',
				value: linkURL,
				onChange: function(newLinkURL) {
					props.setAttributes({ linkURL: newLinkURL })
				}
			})
		)
	}, // edit

	save: function(props) {
		var linkURL = props.attributes.linkURL
		var linkText = props.attributes.linkText

		return el(
			'div',
			{ className: 'block-related-link' },
			el(
				'a',
				{
					href: linkURL,
					target: '_blank',
					title: linkText,
					rel: 'noopener noreferrer'
				},
				linkText
			)
		)
	} // save
})
