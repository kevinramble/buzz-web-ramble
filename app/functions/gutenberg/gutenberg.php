<?php
    // Allowed blocks on Gutengerg
	function custom_allowed_block_types($allowed_block_types, $post){
		if($post->post_type == 'job'){
			return array(
				'core/paragraph',
				'core/heading',
				'core/list',
				'core/image',
				'core/embed'
			);
		} elseif($post->post_type == 'offer'){
			return array(
				'core/paragraph',
				'core/heading',
				'core/image',
				'core/embed'
			);
		} elseif($post->post_type == 'out'){
			return array(
				'core/paragraph',
				'core/heading',
				'core/image',
				'core/embed'
			);
		} elseif($post->post_type == 'video'){
			return array(
				'core/embed'
			);
		} elseif($post->post_type == 'post' || $post->post_type == 'app-post'){
			return array(
				'core/paragraph',
				'core/heading',
				'core/list',
				'core/image',
				'core/quote',
				'core/gallery',
				'core/embed',
				'core/shortcode',
				'custom-blocky/related-link',
				'custom-blocky/video-gallery'
			);
		}
	}
	add_filter('allowed_block_types', 'custom_allowed_block_types', 10, 2);

	// Remove default styles
	add_action('wp_print_styles', 'wps_deregister_styles', 100);
	function wps_deregister_styles(){
		wp_dequeue_style('wp-block-library');
	}

	// Add custom blocks
	function custom_blocks(){
		// Scripts.
		wp_register_script('related-link-js', get_template_directory_uri() . '/functions/gutenberg/related-link.js', array('wp-blocks', 'wp-element', 'wp-components'));
		wp_register_script('video-gallery-js', get_template_directory_uri() . '/functions/gutenberg/video-gallery.js', array('wp-blocks', 'wp-element', 'wp-components', 'wp-editor'));
	
		// Styles.
		wp_register_style('gutenberg-css', get_template_directory_uri() . '/functions/gutenberg/gutenberg.css', array('wp-edit-blocks'));

		register_block_type('custom-blocky/related-link', array(
			'editor_script' => 'related-link-js',
			'editor_style' => 'gutenberg-css'
		));

		register_block_type('custom-blocky/video-gallery', array(
			'editor_script' => 'video-gallery-js',
			'editor_style' => 'gutenberg-css'
		));
	} // End function custom_blocks()
	add_action('init', 'custom_blocks');

	// Remove style options + add responsive embeds
	add_theme_support('editor-color-palette');
	add_theme_support('disable-custom-colors');
	add_theme_support('responsive-embeds');
?>