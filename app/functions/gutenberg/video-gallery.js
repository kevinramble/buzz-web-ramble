var el = wp.element.createElement,
	registerBlockType = wp.blocks.registerBlockType,
	InnerBlocks = wp.editor.InnerBlocks

registerBlockType('custom-blocky/video-gallery', {
	title: 'Video gallery',
	description: 'Add a Video gallery to the content, with specific layout.',
	category: 'common',
	icon: 'video-alt2',
	keywords: ['video', 'gallery', 'youtube'],

	edit: function(props) {
		console.log('InnerBlocks', InnerBlocks)

		return el(
			'div',
			{ className: 'editor-custom-block-video-gallery' },
			el('p', {}, 'Video gallery'),

			el(InnerBlocks, {
				allowedBlocks: ['core/embed'],
				template: [['core/embed']]
			})
		)
	}, // edit

	save: function(props) {
		return el(
			'div',
			{ className: 'block-video-gallery' },
			el(InnerBlocks.Content, null)
		)
	} // save
})
