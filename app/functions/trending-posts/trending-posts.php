<?php
    function my_custom_popular_posts_html_list($popular_posts, $instance){
        foreach($popular_posts as $popular_post){
            
            if(has_post_thumbnail($popular_post->id)){
                $image = get_the_post_thumbnail_url($popular_post->id);
            } else {
                $image = catch_that_image($popular_post->id);
            }
            
            $post_type = get_post_type($popular_post->id);
            $video_class = ($post_type == 'video') ? 'is-video' : ''; ?>
            
            <a href="<?php echo get_permalink($popular_post->id); ?>" title="<?php echo $popular_post->title; ?>" class="c-item-list">
                <div class="c-item-list__img <?php echo $video_class; ?>" style="background-image: url('<?php echo $image; ?>');"></div>
                <div class="c-item-list__content">
                    <span class="c-card-entry__tag"><?php $subcategory = wp_get_post_terms($popular_post->id, 'sub-category'); echo $subcategory[0]->name; ?></span>
                    <p class="c-item-list__date"><?php echo get_the_date('d.m.Y', $popular_post->id); ?></p>
                    <p class="c-item-list__description"><?php echo limit_text($popular_post->title, 9, '...'); ?></p>
                </div>
            </a>
                                    
    <?php }
    }
    add_filter('wpp_custom_html', 'my_custom_popular_posts_html_list', 10, 2);
?>