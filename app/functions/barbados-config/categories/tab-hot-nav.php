<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'main-banner'; ?>
<div class="nav-tab-wrapper secondary">
    <a href="?page=barbados_config_categories&section=hot&tab=main-banner" class="nav-tab <?php echo $active_tab == 'main-banner' ? 'nav-tab-active' : ''; ?>">Main banner</a>
    <a href="?page=barbados_config_categories&section=hot&tab=trending-now" class="nav-tab <?php echo $active_tab == 'trending-now' ? 'nav-tab-active' : ''; ?>">Trending now</a>
    <a href="?page=barbados_config_categories&section=hot&tab=full-block" class="nav-tab <?php echo $active_tab == 'full-block' ? 'nav-tab-active' : ''; ?>">Full block</a>
    <a href="?page=barbados_config_categories&section=hot&tab=must-watch" class="nav-tab <?php echo $active_tab == 'must-watch' ? 'nav-tab-active' : ''; ?>">Must watch</a>
    <a href="?page=barbados_config_categories&section=hot&tab=breaking-news" class="nav-tab <?php echo $active_tab == 'breaking-news' ? 'nav-tab-active' : ''; ?>">Breaking news</a>
    <a href="?page=barbados_config_categories&section=hot&tab=layout-order" class="nav-tab <?php echo $active_tab == 'layout-order' ? 'nav-tab-active' : ''; ?>">Layout order</a>
</div>