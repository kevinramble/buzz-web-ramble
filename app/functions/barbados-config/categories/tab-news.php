<?php require_once('tab-news-nav.php'); ?>

<?php if($active_tab == 'main-banner'){
    // TAB: MAIN BANNER
    require_once('inc-news/tab-main-banner.php');

} elseif($active_tab == 'trending-now'){
    // TAB: TRENDING NOW
    require_once('inc-news/tab-trending-now.php');

} elseif($active_tab == 'full-block'){
    // TAB: FULL BLOCK
    require_once('inc-news/tab-full-block.php');

} elseif($active_tab == 'must-watch'){
    // TAB: MUST WATCH
    require_once('inc-news/tab-must-watch.php');

} elseif($active_tab == 'breaking-news'){
    // TAB: BREAKING NEWS
    require_once('inc-news/tab-breaking-news.php');

} elseif($active_tab == 'layout-order'){
    // TAB: LAYOUT ORDER
    require_once('inc-news/tab-layout-order.php');

} ?>