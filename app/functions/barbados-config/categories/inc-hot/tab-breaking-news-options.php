<?php
    // BREAKING NEWS - 8 POSTS
    function breaking_news_hot_barbados_add_options(){
		for($i = 1; $i <= 8; $i++){
			add_option('breaking_news_hot_barbados_cat'.$i, '', '', 'yes');
			add_option('breaking_news_hot_barbados_subcat'.$i, '', '', 'yes');
			add_option('breaking_news_hot_barbados_type'.$i, '', '', 'yes');
			add_option('breaking_news_hot_barbados_post_id'.$i, '', '', 'yes');
			add_option('breaking_news_hot_barbados_post_title'.$i, '', '', 'yes');
			add_option('breaking_news_hot_barbados_title'.$i, '', '', 'yes');
			add_option('breaking_news_hot_barbados_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'breaking_news_hot_barbados_add_options');
	
	function breaking_news_hot_barbados_settings(){
		for($i = 1; $i <= 8; $i++){
			register_setting('breaking_news_hot_barbados_options', 'breaking_news_hot_barbados_cat'.$i);
			register_setting('breaking_news_hot_barbados_options', 'breaking_news_hot_barbados_subcat'.$i);
			register_setting('breaking_news_hot_barbados_options', 'breaking_news_hot_barbados_type'.$i);
			register_setting('breaking_news_hot_barbados_options', 'breaking_news_hot_barbados_post_id'.$i);
			register_setting('breaking_news_hot_barbados_options', 'breaking_news_hot_barbados_post_title'.$i);
			register_setting('breaking_news_hot_barbados_options', 'breaking_news_hot_barbados_title'.$i);
			register_setting('breaking_news_hot_barbados_options', 'breaking_news_hot_barbados_img'.$i);
		}
	}
	add_action('admin_init', 'breaking_news_hot_barbados_settings');
	
	function breaking_news_hot_barbados($arg){
		for($i = 1; $i <= 8; $i++){
			if ($arg == 'breaking_news_hot_barbados_cat'.$i) {return get_option('breaking_news_hot_barbados_cat'.$i);}
			if ($arg == 'breaking_news_hot_barbados_subcat'.$i) {return get_option('breaking_news_hot_barbados_subcat'.$i);}
			if ($arg == 'breaking_news_hot_barbados_type'.$i) {return get_option('breaking_news_hot_barbados_type'.$i);}
			if ($arg == 'breaking_news_hot_barbados_post_id'.$i) {return get_option('breaking_news_hot_barbados_post_id'.$i);}
			if ($arg == 'breaking_news_hot_barbados_post_title'.$i) {return get_option('breaking_news_hot_barbados_post_title'.$i);}
			if ($arg == 'breaking_news_hot_barbados_title'.$i) {return get_option('breaking_news_hot_barbados_title'.$i);}
			if ($arg == 'breaking_news_hot_barbados_img'.$i) {return get_option('breaking_news_hot_barbados_img'.$i);}
		}
	};
?>