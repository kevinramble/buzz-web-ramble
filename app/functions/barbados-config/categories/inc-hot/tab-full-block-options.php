<?php
    // BIG BLOCK - 1 POST
    function full_block_hot_barbados_add_options(){
		add_option('full_block_hot_barbados_cat', '', '', 'yes');
		add_option('full_block_hot_barbados_subcat', '', '', 'yes');
		add_option('full_block_hot_barbados_type', '', '', 'yes');
		add_option('full_block_hot_barbados_post_id', '', '', 'yes');
		add_option('full_block_hot_barbados_post_title', '', '', 'yes');
		add_option('full_block_hot_barbados_title', '', '', 'yes');
		add_option('full_block_hot_barbados_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'full_block_hot_barbados_add_options');
	
	function full_block_hot_barbados_settings(){
		register_setting('full_block_hot_barbados_options', 'full_block_hot_barbados_cat');
		register_setting('full_block_hot_barbados_options', 'full_block_hot_barbados_subcat');
		register_setting('full_block_hot_barbados_options', 'full_block_hot_barbados_type');
		register_setting('full_block_hot_barbados_options', 'full_block_hot_barbados_post_id');
		register_setting('full_block_hot_barbados_options', 'full_block_hot_barbados_post_title');
		register_setting('full_block_hot_barbados_options', 'full_block_hot_barbados_title');
		register_setting('full_block_hot_barbados_options', 'full_block_hot_barbados_img');
	}
	add_action('admin_init', 'full_block_hot_barbados_settings');
	
	function full_block_hot_barbados($arg){
		if ($arg == 'full_block_hot_barbados_cat') {return get_option('full_block_hot_barbados_cat');}
		if ($arg == 'full_block_hot_barbados_subcat') {return get_option('full_block_hot_barbados_subcat');}
		if ($arg == 'full_block_hot_barbados_type') {return get_option('full_block_hot_barbados_type');}
		if ($arg == 'full_block_hot_barbados_post_id') {return get_option('full_block_hot_barbados_post_id');}
		if ($arg == 'full_block_hot_barbados_post_title') {return get_option('full_block_hot_barbados_post_title');}
		if ($arg == 'full_block_hot_barbados_title') {return get_option('full_block_hot_barbados_title');}
		if ($arg == 'full_block_hot_barbados_img') {return get_option('full_block_hot_barbados_img');}
	};
?>