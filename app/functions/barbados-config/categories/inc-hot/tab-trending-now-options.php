<?php
    // TRENDING NOW - 9 POSTS
    function trending_now_hot_barbados_add_options(){
		for($i = 1; $i <= 9; $i++){
			add_option('trending_now_hot_barbados_cat'.$i, '', '', 'yes');
			add_option('trending_now_hot_barbados_subcat'.$i, '', '', 'yes');
			add_option('trending_now_hot_barbados_type'.$i, '', '', 'yes');
			add_option('trending_now_hot_barbados_post_id'.$i, '', '', 'yes');
			add_option('trending_now_hot_barbados_post_title'.$i, '', '', 'yes');
			add_option('trending_now_hot_barbados_title'.$i, '', '', 'yes');
			add_option('trending_now_hot_barbados_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'trending_now_hot_barbados_add_options');
	
	function trending_now_hot_barbados_settings(){
		for($i = 1; $i <= 9; $i++){
			register_setting('trending_now_hot_barbados_options', 'trending_now_hot_barbados_cat'.$i);
			register_setting('trending_now_hot_barbados_options', 'trending_now_hot_barbados_subcat'.$i);
			register_setting('trending_now_hot_barbados_options', 'trending_now_hot_barbados_type'.$i);
			register_setting('trending_now_hot_barbados_options', 'trending_now_hot_barbados_post_id'.$i);
			register_setting('trending_now_hot_barbados_options', 'trending_now_hot_barbados_post_title'.$i);
			register_setting('trending_now_hot_barbados_options', 'trending_now_hot_barbados_title'.$i);
			register_setting('trending_now_hot_barbados_options', 'trending_now_hot_barbados_img'.$i);
		}
	}
	add_action('admin_init', 'trending_now_hot_barbados_settings');
	
	function trending_now_hot_barbados($arg){
		for($i = 1; $i <= 9; $i++){
			if ($arg == 'trending_now_hot_barbados_cat'.$i) {return get_option('trending_now_hot_barbados_cat'.$i);}
			if ($arg == 'trending_now_hot_barbados_subcat'.$i) {return get_option('trending_now_hot_barbados_subcat'.$i);}
			if ($arg == 'trending_now_hot_barbados_type'.$i) {return get_option('trending_now_hot_barbados_type'.$i);}
			if ($arg == 'trending_now_hot_barbados_post_id'.$i) {return get_option('trending_now_hot_barbados_post_id'.$i);}
			if ($arg == 'trending_now_hot_barbados_post_title'.$i) {return get_option('trending_now_hot_barbados_post_title'.$i);}
			if ($arg == 'trending_now_hot_barbados_title'.$i) {return get_option('trending_now_hot_barbados_title'.$i);}
			if ($arg == 'trending_now_hot_barbados_img'.$i) {return get_option('trending_now_hot_barbados_img'.$i);}
		}
	};
?>