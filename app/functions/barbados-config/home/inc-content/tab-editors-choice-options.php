<?php
    // EDITOR'S CHOICE - 4 POSTS
    function editors_choice_barbados_add_options(){
		for($i = 1; $i <= 4; $i++){
			add_option('editors_choice_barbados_cat'.$i, '', '', 'yes');
			add_option('editors_choice_barbados_subcat'.$i, '', '', 'yes');
			add_option('editors_choice_barbados_type'.$i, '', '', 'yes');
			add_option('editors_choice_barbados_post_id'.$i, '', '', 'yes');
			add_option('editors_choice_barbados_post_title'.$i, '', '', 'yes');
			add_option('editors_choice_barbados_title'.$i, '', '', 'yes');
			add_option('editors_choice_barbados_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'editors_choice_barbados_add_options');
	
	function editors_choice_barbados_settings(){
		for($i = 1; $i <= 4; $i++){
			register_setting('editors_choice_barbados_options', 'editors_choice_barbados_cat'.$i);
			register_setting('editors_choice_barbados_options', 'editors_choice_barbados_subcat'.$i);
			register_setting('editors_choice_barbados_options', 'editors_choice_barbados_type'.$i);
			register_setting('editors_choice_barbados_options', 'editors_choice_barbados_post_id'.$i);
			register_setting('editors_choice_barbados_options', 'editors_choice_barbados_post_title'.$i);
			register_setting('editors_choice_barbados_options', 'editors_choice_barbados_title'.$i);
			register_setting('editors_choice_barbados_options', 'editors_choice_barbados_img'.$i);
		}
	}
	add_action('admin_init', 'editors_choice_barbados_settings');
	
	function editors_choice_barbados($arg){
		for($i = 1; $i <= 4; $i++){
			if ($arg == 'editors_choice_barbados_cat'.$i) {return get_option('editors_choice_barbados_cat'.$i);}
			if ($arg == 'editors_choice_barbados_subcat'.$i) {return get_option('editors_choice_barbados_subcat'.$i);}
			if ($arg == 'editors_choice_barbados_type'.$i) {return get_option('editors_choice_barbados_type'.$i);}
			if ($arg == 'editors_choice_barbados_post_id'.$i) {return get_option('editors_choice_barbados_post_id'.$i);}
			if ($arg == 'editors_choice_barbados_post_title'.$i) {return get_option('editors_choice_barbados_post_title'.$i);}
			if ($arg == 'editors_choice_barbados_title'.$i) {return get_option('editors_choice_barbados_title'.$i);}
			if ($arg == 'editors_choice_barbados_img'.$i) {return get_option('editors_choice_barbados_img'.$i);}
		}
	};
?>