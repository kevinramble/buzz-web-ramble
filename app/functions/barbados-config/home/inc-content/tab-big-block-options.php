<?php
    // BIG BLOCK - 1 POST
    function big_block_barbados_add_options(){
		add_option('big_block_barbados_cat', '', '', 'yes');
		add_option('big_block_barbados_subcat', '', '', 'yes');
		add_option('big_block_barbados_type', '', '', 'yes');
		add_option('big_block_barbados_post_id', '', '', 'yes');
		add_option('big_block_barbados_post_title', '', '', 'yes');
		add_option('big_block_barbados_title', '', '', 'yes');
		add_option('big_block_barbados_img', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'big_block_barbados_add_options');
	
	function big_block_barbados_settings(){
		register_setting('big_block_barbados_options', 'big_block_barbados_cat');
		register_setting('big_block_barbados_options', 'big_block_barbados_subcat');
		register_setting('big_block_barbados_options', 'big_block_barbados_type');
		register_setting('big_block_barbados_options', 'big_block_barbados_post_id');
		register_setting('big_block_barbados_options', 'big_block_barbados_post_title');
		register_setting('big_block_barbados_options', 'big_block_barbados_title');
		register_setting('big_block_barbados_options', 'big_block_barbados_img');
	}
	add_action('admin_init', 'big_block_barbados_settings');
	
	function big_block_barbados($arg){
		if ($arg == 'big_block_barbados_cat') {return get_option('big_block_barbados_cat');}
		if ($arg == 'big_block_barbados_subcat') {return get_option('big_block_barbados_subcat');}
		if ($arg == 'big_block_barbados_type') {return get_option('big_block_barbados_type');}
		if ($arg == 'big_block_barbados_post_id') {return get_option('big_block_barbados_post_id');}
		if ($arg == 'big_block_barbados_post_title') {return get_option('big_block_barbados_post_title');}
		if ($arg == 'big_block_barbados_title') {return get_option('big_block_barbados_title');}
		if ($arg == 'big_block_barbados_img') {return get_option('big_block_barbados_img');}
	};
?>