<?php
    // TOP TEN - 10 POSTS
    function top_ten_barbados_add_options(){
		for($i = 1; $i <= 10; $i++){
			add_option('top_ten_barbados_cat'.$i, '', '', 'yes');
			add_option('top_ten_barbados_subcat'.$i, '', '', 'yes');
			add_option('top_ten_barbados_type'.$i, '', '', 'yes');
			add_option('top_ten_barbados_post_id'.$i, '', '', 'yes');
			add_option('top_ten_barbados_post_title'.$i, '', '', 'yes');
			add_option('top_ten_barbados_title'.$i, '', '', 'yes');
			add_option('top_ten_barbados_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'top_ten_barbados_add_options');
	
	function top_ten_barbados_settings(){
		for($i = 1; $i <= 10; $i++){
			register_setting('top_ten_barbados_options', 'top_ten_barbados_cat'.$i);
			register_setting('top_ten_barbados_options', 'top_ten_barbados_subcat'.$i);
			register_setting('top_ten_barbados_options', 'top_ten_barbados_type'.$i);
			register_setting('top_ten_barbados_options', 'top_ten_barbados_post_id'.$i);
			register_setting('top_ten_barbados_options', 'top_ten_barbados_post_title'.$i);
			register_setting('top_ten_barbados_options', 'top_ten_barbados_title'.$i);
			register_setting('top_ten_barbados_options', 'top_ten_barbados_img'.$i);
		}
	}
	add_action('admin_init', 'top_ten_barbados_settings');
	
	function top_ten_barbados($arg){
		for($i = 1; $i <= 10; $i++){
			if ($arg == 'top_ten_barbados_cat'.$i) {return get_option('top_ten_barbados_cat'.$i);}
			if ($arg == 'top_ten_barbados_subcat'.$i) {return get_option('top_ten_barbados_subcat'.$i);}
			if ($arg == 'top_ten_barbados_type'.$i) {return get_option('top_ten_barbados_type'.$i);}
			if ($arg == 'top_ten_barbados_post_id'.$i) {return get_option('top_ten_barbados_post_id'.$i);}
			if ($arg == 'top_ten_barbados_post_title'.$i) {return get_option('top_ten_barbados_post_title'.$i);}
			if ($arg == 'top_ten_barbados_title'.$i) {return get_option('top_ten_barbados_title'.$i);}
			if ($arg == 'top_ten_barbados_img'.$i) {return get_option('top_ten_barbados_img'.$i);}
		}
	};
?>