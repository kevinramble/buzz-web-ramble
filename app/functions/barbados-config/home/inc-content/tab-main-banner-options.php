<?php
    // MAIN BANNER - 5 POSTS
    function main_banner_barbados_add_options(){
		for($i = 1; $i <= 5; $i++){
			add_option('main_banner_barbados_cat'.$i, '', '', 'yes');
			add_option('main_banner_barbados_subcat'.$i, '', '', 'yes');
			add_option('main_banner_barbados_type'.$i, '', '', 'yes');
			add_option('main_banner_barbados_post_id'.$i, '', '', 'yes');
			add_option('main_banner_barbados_post_title'.$i, '', '', 'yes');
			add_option('main_banner_barbados_title'.$i, '', '', 'yes');
			add_option('main_banner_barbados_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'main_banner_barbados_add_options');
	
	function main_banner_barbados_settings(){
		for($i = 1; $i <= 5; $i++){
			register_setting('main_banner_barbados_options', 'main_banner_barbados_cat'.$i);
			register_setting('main_banner_barbados_options', 'main_banner_barbados_subcat'.$i);
			register_setting('main_banner_barbados_options', 'main_banner_barbados_type'.$i);
			register_setting('main_banner_barbados_options', 'main_banner_barbados_post_id'.$i);
			register_setting('main_banner_barbados_options', 'main_banner_barbados_post_title'.$i);
			register_setting('main_banner_barbados_options', 'main_banner_barbados_title'.$i);
			register_setting('main_banner_barbados_options', 'main_banner_barbados_img'.$i);
		}
	}
	add_action('admin_init', 'main_banner_barbados_settings');
	
	function main_banner_barbados($arg){
		for($i = 1; $i <= 5; $i++){
			if ($arg == 'main_banner_barbados_cat'.$i) {return get_option('main_banner_barbados_cat'.$i);}
			if ($arg == 'main_banner_barbados_subcat'.$i) {return get_option('main_banner_barbados_subcat'.$i);}
			if ($arg == 'main_banner_barbados_type'.$i) {return get_option('main_banner_barbados_type'.$i);}
			if ($arg == 'main_banner_barbados_post_id'.$i) {return get_option('main_banner_barbados_post_id'.$i);}
			if ($arg == 'main_banner_barbados_post_title'.$i) {return get_option('main_banner_barbados_post_title'.$i);}
			if ($arg == 'main_banner_barbados_title'.$i) {return get_option('main_banner_barbados_title'.$i);}
			if ($arg == 'main_banner_barbados_img'.$i) {return get_option('main_banner_barbados_img'.$i);}
		}
	};
?>