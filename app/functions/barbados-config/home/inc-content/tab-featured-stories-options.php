<?php
    // STORIES BLOCK - 9 POSTS
    function featured_stories_barbados_add_options(){
		for($i = 1; $i <= 9; $i++){
			add_option('featured_stories_barbados_cat'.$i, '', '', 'yes');
			add_option('featured_stories_barbados_subcat'.$i, '', '', 'yes');
			add_option('featured_stories_barbados_type'.$i, '', '', 'yes');
			add_option('featured_stories_barbados_post_id'.$i, '', '', 'yes');
			add_option('featured_stories_barbados_post_title'.$i, '', '', 'yes');
			add_option('featured_stories_barbados_title'.$i, '', '', 'yes');
			add_option('featured_stories_barbados_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'featured_stories_barbados_add_options');
	
	function featured_stories_barbados_settings(){
		for($i = 1; $i <= 9; $i++){
			register_setting('featured_stories_barbados_options', 'featured_stories_barbados_cat'.$i);
			register_setting('featured_stories_barbados_options', 'featured_stories_barbados_subcat'.$i);
			register_setting('featured_stories_barbados_options', 'featured_stories_barbados_type'.$i);
			register_setting('featured_stories_barbados_options', 'featured_stories_barbados_post_id'.$i);
			register_setting('featured_stories_barbados_options', 'featured_stories_barbados_post_title'.$i);
			register_setting('featured_stories_barbados_options', 'featured_stories_barbados_title'.$i);
			register_setting('featured_stories_barbados_options', 'featured_stories_barbados_img'.$i);
		}
	}
	add_action('admin_init', 'featured_stories_barbados_settings');
	
	function featured_stories_barbados($arg){
		for($i = 1; $i <= 9; $i++){
			if ($arg == 'featured_stories_barbados_cat'.$i) {return get_option('featured_stories_barbados_cat'.$i);}
			if ($arg == 'featured_stories_barbados_subcat'.$i) {return get_option('featured_stories_barbados_subcat'.$i);}
			if ($arg == 'featured_stories_barbados_type'.$i) {return get_option('featured_stories_barbados_type'.$i);}
			if ($arg == 'featured_stories_barbados_post_id'.$i) {return get_option('featured_stories_barbados_post_id'.$i);}
			if ($arg == 'featured_stories_barbados_post_title'.$i) {return get_option('featured_stories_barbados_post_title'.$i);}
			if ($arg == 'featured_stories_barbados_title'.$i) {return get_option('featured_stories_barbados_title'.$i);}
			if ($arg == 'featured_stories_barbados_img'.$i) {return get_option('featured_stories_barbados_img'.$i);}
		}
	};
?>