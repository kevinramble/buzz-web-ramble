<?php
    // MAIN BANNER - 5 POSTS
    function home_poll_barbados_add_options(){
		add_option('home_poll_barbados_status', '', '', 'yes');
		add_option('home_poll_barbados_shortcode', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'home_poll_barbados_add_options');
	
	function home_poll_barbados_settings(){
		register_setting('home_poll_barbados_options', 'home_poll_barbados_status');
		register_setting('home_poll_barbados_options', 'home_poll_barbados_shortcode');
	}
	add_action('admin_init', 'home_poll_barbados_settings');
	
	function home_poll_barbados($arg){
		if ($arg == 'home_poll_barbados_status') {return get_option('home_poll_barbados_status');}
		if ($arg == 'home_poll_barbados_shortcode') {return get_option('home_poll_barbados_shortcode');}
	};
?>