<?php
    // HERO LAYOUT - 1 POST
    function hero_layout_barbados_add_options(){
		add_option('hero_layout_barbados_version', '', '', 'yes');
	}
	register_activation_hook(__FILE__, 'hero_layout_barbados_add_options');
	
	function hero_layout_barbados_settings(){
		register_setting('hero_layout_barbados_options', 'hero_layout_barbados_version');
	}
	add_action('admin_init', 'hero_layout_barbados_settings');
	
	function hero_layout_barbados($arg){
		if ($arg == 'hero_layout_barbados_version') {return get_option('hero_layout_barbados_version');}
	};
?>