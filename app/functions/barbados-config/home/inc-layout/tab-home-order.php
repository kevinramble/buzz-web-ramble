<?php settings_fields('home_order_barbados_options'); ?>

<div class="content-wrapper" id="home_order_barbados">
    <h3>Home order</h3>

    <div class="orders">
        <?php for($i = 1; $i <= 8; $i++){
            $optionValue = get_option('home_order_barbados'. $i);
            if($optionValue == '') update_option('home_order_barbados'. $i, $i);
        ?>
            <div class="block" data-position="<?php echo $i; ?>">
                <p>Block <strong><?php echo $optionValue; ?></strong></p>
                <img src="<?php echo get_template_directory_uri() . '/functions/barbados-config/home/inc-layout/img/block'. $optionValue.'.jpg"'; ?>>
                <input type="hidden" class="input-order" name="home_order_barbados<?php echo $i; ?>" value="<?php echo $optionValue; ?>">
            </div>
        <?php } //for ?>
    </div>

    <input class="button-primary" type="submit" name="Save" value="Save">
</div>