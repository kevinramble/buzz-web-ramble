<?php
    // WATCH NEXT - 9 POSTS
    function watch_next_barbados_add_options(){
		for($i = 1; $i <= 9; $i++){
			add_option('watch_next_barbados_cat'.$i, '', '', 'yes');
			add_option('watch_next_barbados_subcat'.$i, '', '', 'yes');
			add_option('watch_next_barbados_type'.$i, '', '', 'yes');
			add_option('watch_next_barbados_post_id'.$i, '', '', 'yes');
			add_option('watch_next_barbados_post_title'.$i, '', '', 'yes');
			add_option('watch_next_barbados_title'.$i, '', '', 'yes');
			add_option('watch_next_barbados_img'.$i, '', '', 'yes');
		}
	}
	register_activation_hook(__FILE__, 'watch_next_barbados_add_options');
	
	function watch_next_barbados_settings(){
		for($i = 1; $i <= 9; $i++){
			register_setting('watch_next_barbados_options', 'watch_next_barbados_cat'.$i);
			register_setting('watch_next_barbados_options', 'watch_next_barbados_subcat'.$i);
			register_setting('watch_next_barbados_options', 'watch_next_barbados_type'.$i);
			register_setting('watch_next_barbados_options', 'watch_next_barbados_post_id'.$i);
			register_setting('watch_next_barbados_options', 'watch_next_barbados_post_title'.$i);
			register_setting('watch_next_barbados_options', 'watch_next_barbados_title'.$i);
			register_setting('watch_next_barbados_options', 'watch_next_barbados_img'.$i);
		}
	}
	add_action('admin_init', 'watch_next_barbados_settings');
	
	function watch_next_barbados($arg){
		for($i = 1; $i <= 9; $i++){
			if ($arg == 'watch_next_barbados_cat'.$i) {return get_option('watch_next_barbados_cat'.$i);}
			if ($arg == 'watch_next_barbados_subcat'.$i) {return get_option('watch_next_barbados_subcat'.$i);}
			if ($arg == 'watch_next_barbados_type'.$i) {return get_option('watch_next_barbados_type'.$i);}
			if ($arg == 'watch_next_barbados_post_id'.$i) {return get_option('watch_next_barbados_post_id'.$i);}
			if ($arg == 'watch_next_barbados_post_title'.$i) {return get_option('watch_next_barbados_post_title'.$i);}
			if ($arg == 'watch_next_barbados_title'.$i) {return get_option('watch_next_barbados_title'.$i);}
			if ($arg == 'watch_next_barbados_img'.$i) {return get_option('watch_next_barbados_img'.$i);}
		}
	};
?>