<?php
	function barbados_config() {
		add_menu_page('Barbados config', 'Barbados config', 'manage_options', 'barbados_config', 'barbados_config_content', 'dashicons-admin-generic', '4.3');
		add_submenu_page('barbados_config', 'Barbados config – Home', 'Home', 'manage_options', 'barbados_config', 'barbados_config');
		add_submenu_page('barbados_config', 'Barbados config – Categories', 'Categories', 'manage_options', 'barbados_config_categories', 'barbados_config_categories_content');
		add_submenu_page('barbados_config', 'Barbados config – Video', 'Video', 'manage_options', 'barbados_config_videos', 'barbados_config_videos_content');
	}
	add_action('admin_menu', 'barbados_config');

	require_once('home/inc-content/tab-main-banner-options.php');
	require_once('home/inc-content/tab-hot-now-options.php');
	require_once('home/inc-content/tab-featured-stories-options.php');
	require_once('home/inc-content/tab-editors-choice-options.php');
	require_once('home/inc-content/tab-categories-block-options.php');
	require_once('home/inc-content/tab-big-block-options.php');
	require_once('home/inc-content/tab-top-ten-options.php');
	require_once('home/inc-layout/tab-hero-layout-options.php');
	require_once('home/inc-layout/tab-home-order-options.php');
	require_once('home/inc-poll/home-poll-options.php');
	require_once('home/inc-leaderboard/home-leaderboard-options.php');

	require_once('categories/inc-hot/tab-main-banner-options.php');
	require_once('categories/inc-hot/tab-trending-now-options.php');
	require_once('categories/inc-hot/tab-full-block-options.php');
	require_once('categories/inc-hot/tab-must-watch-options.php');
	require_once('categories/inc-hot/tab-breaking-news-options.php');
	require_once('categories/inc-hot/tab-layout-order-options.php');

	require_once('categories/inc-life/tab-main-banner-options.php');
	require_once('categories/inc-life/tab-trending-now-options.php');
	require_once('categories/inc-life/tab-full-block-options.php');
	require_once('categories/inc-life/tab-must-watch-options.php');
	require_once('categories/inc-life/tab-breaking-news-options.php');
	require_once('categories/inc-life/tab-layout-order-options.php');

	require_once('categories/inc-news/tab-main-banner-options.php');
	require_once('categories/inc-news/tab-trending-now-options.php');
	require_once('categories/inc-news/tab-full-block-options.php');
	require_once('categories/inc-news/tab-must-watch-options.php');
	require_once('categories/inc-news/tab-breaking-news-options.php');
	require_once('categories/inc-news/tab-layout-order-options.php');

	require_once('videos/tab-main-video-options.php');
	require_once('videos/tab-watch-next-options.php');

	function barbados_config_content(){ ?>
		<div class="wrap">
			<h2>Barbados home configurations</h2>

			<?php $getCountry = get_term_by('slug', 'barbados', 'countries');
			$countryID = $getCountry->term_id; ?>

			<div id="poststuff" data-countryid="<?php echo $countryID; ?>">
				<div class="custom-admin-content home-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>

						<?php settings_errors(); ?>

						<?php $active_section = isset($_GET['section']) ? $_GET['section'] : 'content'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=barbados_config&section=content&tab=main-banner" class="nav-tab <?php echo $active_section == 'content' ? 'nav-tab-active' : ''; ?>">Content</a>
							<a href="?page=barbados_config&section=layout&tab=hero-layout" class="nav-tab <?php echo $active_section == 'layout' ? 'nav-tab-active' : ''; ?>">Layout</a>
							<a href="?page=barbados_config&section=poll" class="nav-tab <?php echo $active_section == 'poll' ? 'nav-tab-active' : ''; ?>">Poll</a>
							<a href="?page=barbados_config&section=leaderboard" class="nav-tab <?php echo $active_section == 'leaderboard' ? 'nav-tab-active' : ''; ?>">Leaderboard ad</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_section == 'content'){
								// TAB: Content
								require_once('home/tab-content.php');
							
							} elseif($active_section == 'layout'){
								// TAB: Layout
								require_once('home/tab-layout.php');

							} elseif($active_section == 'poll'){
								// TAB: Poll
								require_once('home/tab-poll.php');

							} elseif($active_section == 'leaderboard'){
								// TAB: Leaderboard ad
								require_once('home/tab-leaderboard.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #home-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // barbados_config_content()

	function barbados_config_categories_content(){ ?>
		<div class="wrap">
			<h2>Barbados categories configurations</h2>

			<?php $getCountry = get_term_by('slug', 'barbados', 'countries');
			$countryID = $getCountry->term_id; ?>
			
			<div id="poststuff" data-countryid="<?php echo $countryID; ?>">
				<div class="custom-admin-content home-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>

						<?php settings_errors(); ?>

						<?php $active_section = isset($_GET['section']) ? $_GET['section'] : 'hot'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=barbados_config_categories&section=hot&tab=main-banner" class="nav-tab <?php echo $active_section == 'hot' ? 'nav-tab-active' : ''; ?>">Hot</a>
							<a href="?page=barbados_config_categories&section=life&tab=main-banner" class="nav-tab <?php echo $active_section == 'life' ? 'nav-tab-active' : ''; ?>">Life</a>
							<a href="?page=barbados_config_categories&section=news&tab=main-banner" class="nav-tab <?php echo $active_section == 'news' ? 'nav-tab-active' : ''; ?>">News</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_section == 'hot'){
								// TAB: hot
								require_once('categories/tab-hot.php');
							
							} elseif($active_section == 'life'){
								// TAB: life
								require_once('categories/tab-life.php');

							} elseif($active_section == 'news'){
								// TAB: news
								require_once('categories/tab-news.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #home-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // barbados_config_categories_content()

	function barbados_config_videos_content(){ ?>
		<div class="wrap">
			<h2>Barbados video configurations</h2>

			<?php $getCountry = get_term_by('slug', 'barbados', 'countries');
			$countryID = $getCountry->term_id; ?>
			
			<div id="poststuff" data-countryid="<?php echo $countryID; ?>">
				<div class="custom-admin-content home-config-options postbox">
					<div class="inside">
						<p class="description">Choose what you would like to edit.</p>

						<?php settings_errors(); ?>

						<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'main-banner'; ?>
						<div class="nav-tab-wrapper">
							<a href="?page=barbados_config_videos&tab=main-banner" class="nav-tab <?php echo $active_tab == 'main-banner' ? 'nav-tab-active' : ''; ?>">Main banner</a>
							<a href="?page=barbados_config_videos&tab=watch-next" class="nav-tab <?php echo $active_tab == 'watch-next' ? 'nav-tab-active' : ''; ?>">Watch next</a>
						</div>

						<form method="post" action="options.php">
							<?php if($active_tab == 'main-banner'){
								// TAB: main-banner
								require_once('videos/tab-main-video.php');
							
							} elseif($active_tab == 'watch-next'){
								// TAB: watch-next
								require_once('videos/tab-watch-next.php');

							} ?>
						</form>
					</div><!-- .inside -->
				</div><!-- #home-config-options -->
			</div><!-- #poststuff -->
		</div><!-- .wrap -->
	<?php }; // barbados_config_videos_content()
?>