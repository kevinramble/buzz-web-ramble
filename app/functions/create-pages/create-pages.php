<?php
    function create_buzz_pages(){
        if (is_admin()){
            $my_theme = wp_get_theme();
            if ($my_theme->exists()) $theme_name = esc_html($my_theme);

            if($theme_name == 'Buzz'){
                function create_page($title, $template){
                    $new_page_title = $title;
                    $new_page_content = '';
                    $new_page_template = $template;
                
                    $page_check = get_page_by_title($new_page_title);
                    $new_page = array(
                        'post_type' => 'page',
                        'post_title' => $new_page_title,
                        'post_content' => $new_page_content,
                        'post_status' => 'publish',
                        'post_author' => 1,
                        'post_date' => current_time('Y-m-d H:i:s')
                    );
                    if(!isset($page_check->ID)){
                        $new_page_id = wp_insert_post($new_page);
                        if(!empty($new_page_template)){
                            update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
                        }
                    }
                }
    
                // Buzz
                create_page('Offers', 'offers.php');
                create_page('Play', 'videos.php');
                create_page('Jobs', 'jobs.php');
                create_page('Out', 'out.php');
                create_page('Hot', 'categories.php');
                create_page('News', 'categories.php');
                create_page('Life', 'categories.php');
                create_page('Countries', 'countries.php');
                create_page('Suggest content', 'suggest-content.php');
                create_page('Contact', 'contact.php');
                create_page('Advertise with us', 'advertise.php');
                create_page('About us', 'about.php');
                create_page('Privacy policy', 'privacy.php');
                create_page('Terms and conditions', 'terms.php');
            } // is buzz
        } // is_admin
    }
    add_action('after_setup_theme', 'create_buzz_pages');
?>