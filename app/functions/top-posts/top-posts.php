<?php
    /**
	 * inserts custom 'key' to track the count of articles viewed
	 */
	function buzz_top_posts($post_id) {
		$post_meta_key = 'top_posts';
		//check if post exists
		$count = get_post_meta($post_id, $post_meta_key, true);
		//echo $count;
		//if no result then add new entry
		if ($count == '') {
			$count = 1;
			delete_post_meta($post_id, $post_meta_key);
			add_post_meta($post_id, $post_meta_key, '1');
		} else {
			//else update post view count
			$count++;
			update_post_meta($post_id, $post_meta_key, $count);
		}
	}
	/**
	 * listens and posts the post id to the database to update or insert the 'post id'
	 */
	function buzz_track_posts($post_id) {
		if (!is_single()) 
			return;
		if (empty($post_id)) {
			global $post;
			$post_id = $post->ID;
		}
		buzz_top_posts($post_id);
	}
	add_action('wp_head', 'buzz_track_posts');
?>