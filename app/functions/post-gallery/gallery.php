<?php
	function gallery_metabox_enqueue($hook) {
		if ( 'post.php' == $hook || 'post-new.php' == $hook ) {
			wp_enqueue_script('gallery-metabox', get_template_directory_uri() . '/functions/post-gallery/gallery.js', array('jquery', 'jquery-ui-sortable'));
			wp_enqueue_style('gallery-metabox', get_template_directory_uri() . '/functions/post-gallery/gallery.css');
		}
	}
	add_action('admin_enqueue_scripts', 'gallery_metabox_enqueue');

	function add_gallery_metabox($post_type) {
		$types = array('offer', 'out');
		if (in_array($post_type, $types)) {
			add_meta_box(
			'gallery-metabox',
			'Gallery',
			'gallery_meta_callback',
			$post_type,
			'normal',
			'high'
			);
		}
	}
	add_action('add_meta_boxes', 'add_gallery_metabox');

	function gallery_meta_callback($post) {
		wp_nonce_field( basename(__FILE__), 'gallery_meta_nonce' );
		$ids = get_post_meta($post->ID, 'post_gallery_id', true); ?>

			<table class="form-table">
				<tr>
					<td>
						<a class="gallery-add button button-primary" href="#0" data-uploader-title="Add image(s) to gallery" data-uploader-button-text="Add images">Add images</a>

						<ul id="gallery-metabox-list">
							<?php if($ids) : foreach ($ids as $key => $value) : $image = wp_get_attachment_image_src($value); ?>
								<li>
									<input type="hidden" name="post_gallery_id[<?php echo $key; ?>]" value="<?php echo $value; ?>">
									<img class="image-preview" src="<?php echo $image[0]; ?>">
									<a class="change-image button button-small" href="#0" data-uploader-title="Change image" data-uploader-button-text="Change image">Change image</a>
									<a class="remove-image dashicons dashicons-trash" href="#0"></a>
								</li>
							<?php endforeach; endif; ?>
						</ul>

					</td>
				</tr>
			</table>
	<?php }

	function gallery_meta_save($post_id) {
		if (!isset($_POST['gallery_meta_nonce']) || !wp_verify_nonce($_POST['gallery_meta_nonce'], basename(__FILE__))) return;

		if (!current_user_can('edit_post', $post_id)) return;

		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

		if(isset($_POST['post_gallery_id'])) {
			update_post_meta($post_id, 'post_gallery_id', $_POST['post_gallery_id']);
		} else {
			delete_post_meta($post_id, 'post_gallery_id');
		}
	}
	add_action('save_post', 'gallery_meta_save');
?>