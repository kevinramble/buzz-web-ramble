<?php
    /* Custom metaboxes */
	function sponsored_metabox() {
		add_meta_box('sponsored-meta-box', 'Sponsored', 'sponsored_post_metabox_callback', array('post', 'app-post'));
	}
	add_action('add_meta_boxes', 'sponsored_metabox');

	function sponsored_post_metabox_callback($post){
		$sponsored_img = esc_html(get_post_meta($post->ID, 'sponsored_img', true));
		$sponsored_url = esc_html(get_post_meta($post->ID, 'sponsored_url', true));
	?>
		<p><strong>Sponsor URL</strong><br>
		<input type="text" size="50" name="sponsored_url" id="sponsored_url" placeholder="http://" value="<?php echo $sponsored_url; ?>"></p>

		<div class="sponsor-img">
			<div class="used-image">
				<?php if($sponsored_img){
					$content  = '<a href="#0" class="delete-item dashicons dashicons-trash" title="Remove image"></a>';
					$content .= '<img src="'. $sponsored_img .'">';
					$content .= '<input type="hidden" name="sponsored_img" id="sponsored_img" value="'. $sponsored_img .'">';

					echo $content;
				} ?>
			</div>

			<?php $buttonTitle = ($sponsored_img) ? 'Change sponsor image' : 'Add sponsor image'; ?>
			<a href="#0" class="add-sponsor button-primary" title="Add sponsor image"><?php echo $buttonTitle; ?></a>
		</div><!-- .sponsor-img -->

		<p class="obs">The image must be 151 by 454 pixels.<br>
		The link must have http:// at the beginning.</p>
		
		<a href="#0" title="Remove sponsor" class="remove-sponsor">Click here to remove the Sponsor.</a>
	<?php
	}
	
	function save_sponsored_metabox($post_id){
		if(isset($_POST['sponsored_img'])){
			update_post_meta($post_id, 'sponsored_img', $_POST['sponsored_img']);
		}
		if(isset($_POST['sponsored_url'])){
			update_post_meta($post_id, 'sponsored_url', $_POST['sponsored_url']);
		}
	}
	add_action('save_post', 'save_sponsored_metabox');

	function sponsored_post_enqueue($hook){
		global $post_type;
        if(($hook == 'post-new.php' || $hook == 'post.php') && ($post_type == 'post' || $post_type == 'app-post')){
			wp_enqueue_media();
			wp_enqueue_style('sponsored-post-style', get_template_directory_uri() . '/functions/sponsored-post/sponsored-post.css');
			wp_enqueue_script('sponsored-post-js', get_template_directory_uri() . '/functions/sponsored-post/sponsored-post.js', array('jquery'));
		}
	}
	add_action('admin_enqueue_scripts', 'sponsored_post_enqueue');
	
	// Mostrar custom fields na lista de posts
	function add_spon_columns($columns){
		unset($columns['comments']);
		return array_merge($columns, array( 
			'sponsored' => 'Sponsored',
			'is_video' => 'Marked as Video'
		));
	}
	add_filter('manage_post_posts_columns', 'add_spon_columns');

 	function spon_custom_column($column, $spon_post_id){
		switch($column){
			case 'sponsored':
				echo $spon = ((get_post_meta(get_the_ID(), 'sponsored_img', true) !== '')) ? '&#10003;' : '';
			break;
			case 'is_video':
				$is_video_check = esc_html(get_post_meta(get_the_ID(), 'is_video_check', true));
				echo $is_video = ($is_video_check == 'video') ? '&#10003;' : '';
			break;
		}
	}
	add_action('manage_post_posts_custom_column', 'spon_custom_column', 10, 2);

	// ordenar campos no dashboard
	function post_sortable_columns($columns){
		return array_merge($columns, array( 
			'sponsored' => 'sponsored_img',
			'is_video' => 'is_video'
		));
	}
	add_filter('manage_edit-post_sortable_columns', 'post_sortable_columns');

	function post_posts_orderby($query){
		if(!is_admin() || ! $query->is_main_query()){
			return;
		}

		if('sponsored_img' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'sponsored_img');
		}

		if('is_video' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'is_video_check');
		}
	}
	add_action('pre_get_posts', 'post_posts_orderby');
?>