jQuery(document).ready(function($) {
	var mediaUploader, content, selection, $this

	// Media uploader
	$('.add-sponsor').click(function(e) {
		$this = $(this)

		e.preventDefault()
		if (mediaUploader) {
			mediaUploader.open()
			return
		}

		mediaUploader = wp.media.frames.file_frame = wp.media({
			id: 'home-config-uploader',
			title: 'Choose a picture',
			library: { type: 'image' },
			button: { text: 'Select' }
		})

		mediaUploader.on('select', function() {
			selection = mediaUploader
				.state()
				.get('selection')
				.toJSON()

			$.each(selection, function(i, img) {
				content = '<a href="#0" class="delete-item dashicons dashicons-trash" title="Remove image"></a>'
				content += '<img src="' + img.url + '" class="added-image">'
				content += '<input type="hidden" name="sponsored_img" id="sponsored_img" value="' + img.url + '">'

				$this.siblings('.used-image').html(content)
				$this.text('Change sponsor image')
			})
		})

		mediaUploader.open()
	}) // upload-button click

	$('.sponsor-img').delegate('.delete-item', 'click', function(e) {
		e.preventDefault()

		$(this)
			.parents('.used-image')
			.html('<input type="hidden" name="sponsored_img" id="sponsored_img" value="">')
		$('.add-sponsor').text('Add sponsor image')
	})

	$('.remove-sponsor').click(function(e) {
		e.preventDefault()

		$('.sponsor-img .used-image').html('<input type="hidden" name="sponsored_img" id="sponsored_img" value="">')
		$('#sponsored_url').val('')
		$('.add-sponsor').text('Add sponsor image')
	})
})
