<?php

define("CONTENT_TYPE", "Content-Type: application/json");
define("ENDPOINTSIGNAL", "https://onesignal.com/api/v1/");
define("ONESIGNAL", "dccbc951-82bb-4717-bb6d-31513367f55c");
define("AUTORIZATION_BASIC_ONESIGNAL", "Authorization: Basic ZjM2MDA5ZjItNjkyMS00NTY1LWFjOGYtOGJiYjNkZWZmODUw");

	// Push notification post - post type
	function create_push_notification(){
		register_post_type('push-notification',
		array(
			'labels' => array(
				'name' => 'Push notifications',
						'singular_name' => 'Push notification',
						'add_new' => 'Add new',
						'add_new_item' => 'Add new Push notification',
						'edit_item' => 'Edit Push notification',
						'new_item' => 'New Push notification',
						'all_items' => 'All Push notifications',
						'view_item' => 'View Push notification',
						'search_items' => 'Search Push notifications',
						'not_found' => 'No Push notification found',
						'not_found_in_trash' => 'No Push notifications found in trash',
						'parent_item_colon' => '',
						'menu_name' => 'App push notifications'
                    ),
                    'public' => true,
					'show_in_menu' => true,
					'menu_position' => 3,
					'menu_icon' => 'dashicons-share-alt2',
					'supports' => array('author'),
					'show_in_rest' => true,
					'has_archive' => true,
					'rewrite' => array('slug' => 'push-notification', 'with_front' => false)
				)
			);
	}
	add_action('init', 'create_push_notification');

	add_action('admin_init', 'push_notification_admin');
	function push_notification_admin() {
		add_meta_box('push_notification_meta_box',
			'Push notification details',
			'display_push_notification_meta_box',
			'push-notification', 'normal', 'high'
		);
	}

	function display_push_notification_meta_box($push_notification_post){
		$push_notification_title = esc_html(get_post_meta($push_notification_post->ID, 'push_notification_title', true));
		$push_notification_type = esc_html(get_post_meta($push_notification_post->ID, 'push_notification_type', true));
		$push_notification_content = esc_html(get_post_meta($push_notification_post->ID, 'push_notification_content', true));
	?>
		<?php if($push_notification_post->post_status === 'publish') : ?>
			<p class="already-sent">Notification already sent.</p>
			<script>
				jQuery('.edit-post-status').remove();
				jQuery('#publish').remove();
			</script>
		<?php else : ?>
			<p class="alert"><strong>Attention:</strong> By <u>publishing</u> this notification, you are aware that it is sent to the end users, so make sure of what you're doing.</p>
		<?php endif; ?>

		<p class="label"><strong>Title</strong> <span class="obs">(max 50 characters)</span><br>
		<input type="text" class="title" name="push_notification_title" value="<?php echo $push_notification_title; ?>" maxlength="50"></p>

        <p class="label"><strong>Type</strong><br>
		<select name="push_notification_type" class="type">
			<option value="">Select type</option>
			<option value="breaknews" <?php if($push_notification_type == 'breaknews') echo 'selected="selected"'; ?>>Breaking News</option>
			<option value="promotional" <?php if($push_notification_type == 'promotional') echo 'selected="selected"'; ?>>Promotional Push</option>
			<option value="updates" <?php if($push_notification_type == 'updates') echo 'selected="selected"'; ?>>App Updates</option>
		</select></p>

		<p class="label"><strong>Content</strong> <span class="obs">(max 140 characters)</span><br>
		<textarea name="push_notification_content" maxlength="140" class="content"><?php echo $push_notification_content; ?></textarea></p>
	<?php
	}

	function filter_push_data($data, $postarr){
		if($postarr['post_type'] == 'push-notification'){
			if($_POST['push_notification_title'] && $_POST['push_notification_type']){
				$this_push_notification_title = trim($_POST['push_notification_title']);
				$this_push_notification_type = trim($_POST['push_notification_type']);

				if($this_push_notification_type === 'updates') $this_push_notification_type = 'App Updates';
				if($this_push_notification_type === 'breaknews') $this_push_notification_type = 'Breaking News';
				if($this_push_notification_type === 'promotional') $this_push_notification_type = 'Promotional Push';

				$new_push_notification_title = $this_push_notification_title .', '. $this_push_notification_type;

				$new_push_notification_name = strtolower($new_push_notification_title);
				$new_push_notification_name = preg_replace("/[^a-z0-9_\s-]/", "", $new_push_notification_name);
				$new_push_notification_name = preg_replace("/[\s-]+/", " ", $new_push_notification_name);
				$new_push_notification_name = preg_replace("/[\s_]/", "-", $new_push_notification_name);
				$new_push_notification_name = $new_push_notification_name .'-'. $postarr['ID'];

				$data['post_title'] = $new_push_notification_title;
				$data['post_name'] = $new_push_notification_name;
			}

			return $data;
		} else {
			return $data;
		}
	}
	add_filter('wp_insert_post_data', 'filter_push_data', '99', 2);

	function add_push_notification_fields($push_notification_post_id, $push_notification_post){
		if($push_notification_post->post_type == 'push-notification'){
			if(isset($_POST['push_notification_title'])){
				update_post_meta($push_notification_post_id, 'push_notification_title', $_POST['push_notification_title']);
			}
			if(isset($_POST['push_notification_type'])){
				update_post_meta($push_notification_post_id, 'push_notification_type', $_POST['push_notification_type']);
			}
			if(isset($_POST['push_notification_content'])){
				update_post_meta($push_notification_post_id, 'push_notification_content', $_POST['push_notification_content']);
			}

			if($push_notification_post->post_status === 'publish'){
				run_notification($push_notification_post->ID);
			}
		}
	}
	add_action('save_post', 'add_push_notification_fields', 6, 2);

	function push_notification_post_enqueue($hook){
        global $post_type;
        if($post_type == 'push-notification'){
			wp_enqueue_style('push-notification-style', get_template_directory_uri() . '/functions/push-notification/push-notification.css');
			wp_enqueue_script('push-notification-js', get_template_directory_uri() . '/functions/push-notification/push-notification.js', array('jquery'));
		}
	}
	add_action('admin_enqueue_scripts', 'push_notification_post_enqueue');

	// Mostrar custom fields na lista de posts
	function add_push_notifications_columns($columns){
		return array_merge($columns, array( 
			'type' => 'Type'
		));
	}
	add_filter('manage_push-notification_posts_columns', 'add_push_notifications_columns');

 	function push_notification_custom_column($column, $push_notification_post_id){
		switch($column){
			case 'type':
				if((get_post_meta($push_notification_post_id, 'push_notification_type', true) !== '')){
					$type = esc_html(get_post_meta($push_notification_post_id, 'push_notification_type', true));
					if($type === 'updates') echo 'App Updates';
					if($type === 'breaknews') echo 'Breaking News';
					if($type === 'promotional') echo 'Promotional Push';

					if($type !== 'updates' && $type !== 'breaknews' && $type !== 'promotional') echo $type;
				} else {
					echo '–';
				}
			break;
		}
	}
	add_action('manage_push-notification_posts_custom_column', 'push_notification_custom_column', 10, 2);

	// ordenar campos no dashboard
	function push_notification_sortable_columns($columns){
		return array_merge($columns, array( 
			'type' => 'push_notification_type'
		));
	}
	add_filter('manage_edit-push-notification_sortable_columns', 'push_notification_sortable_columns');

	function push_notification_posts_orderby($query){
		if(!is_admin() || ! $query->is_main_query()){
			return;
		}

		if('push_notification_type' === $query->get('orderby')){
			$query->set('orderby', 'meta_value');
			$query->set('meta_key', 'push_notification_type');
		}
	}
	add_action('pre_get_posts', 'push_notification_posts_orderby');

	// Tirar Quick edit, porque dá erro com o title salvo automaticamente
	function push_notification_disable_quick_edit($actions = array(), $post = null){
		// Abort if the post type is not "push-notification"
		if (!is_post_type_archive('push-notification')){
			return $actions;
		}
		if(isset( $actions['inline hide-if-no-js'])){
			unset( $actions['inline hide-if-no-js'] );
		}

		return $actions;
	}
	add_filter('post_row_actions', 'push_notification_disable_quick_edit', 10, 2);

	function run_notification($postID){
		global $wpdb;

		$notification_title = esc_html(get_post_meta($postID, 'push_notification_title', true));
		$notification_type = esc_html(get_post_meta($postID, 'push_notification_type', true));
		$notification_content = esc_html(get_post_meta($postID, 'push_notification_content', true));
		$testing = $notification_title . ' / ' . $notification_content . ' - ' . $notification_type;
		
		$campo = 'alert_'.$notification_type;

		if ($postID) { 
			
			$users = $wpdb->get_results("SELECT deviceid FROM cp_devices WHERE user_id IN (SELECT user_id FROM wp_usermeta WHERE meta_key = '" . $campo . "' AND meta_value = 1) ");
            $playersID = $objMessage = null;
            if ($users) {
                for($x=0; $x < count($users); $x++) {
					if (!in_array($users[$x]->deviceid, $playersID)) { 
						if (strlen($users[$x]->deviceid) >= 35) {
							$playersID[] = $users[$x]->deviceid;	
						}
					}
                }
			}
			
			if ($playersID) { 
                if (count($playersID) > 0) { 
                    $objMessage = array(
                        'tipo' => $campo,
						'players' => $playersID,
						'title' => $notification_title,
                        'mensagem' => $notification_content);
                    sendPushNotification($objMessage);
                }  
            }
        }
	}

	function sendPushNotification($obj) {         
        $content = array("en" => $obj['mensagem']);
        $headings = array("en" => "BUZZ ".$obj['title']);
        

        switch($obj['tipo']) {
            case "no_activitys" :
                 $fields = array(
                        'app_id' => ONESIGNAL,
                        'include_player_ids' => $obj['players'],
                        'data' => array("foo" => "bar"),
                        'contents' => $content,
                        'headings' => $headings,
                        'small_icon' => 'logo',
                        'large_icon' => 'ic_stat_onesignal_default'
                    );
            break;
            default : 
                $fields = array(
                        'app_id' => ONESIGNAL,
                        'data' => array("foo" => "bar"),
						'include_player_ids' => $obj['players'],
                        'contents' => $content,
                        'headings' => $headings,
                        'small_icon' => 'logo',
                        'large_icon' => 'ic_stat_onesignal_default'
                    );  
            break;
        }

        $fields = json_encode($fields);
		
		if (count($obj['players'])) { 
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, ENDPOINTSIGNAL."notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(CONTENT_TYPE . ", charset=utf-8",
														AUTORIZATION_BASIC_ONESIGNAL));

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

			$response = curl_exec($ch);
			curl_close($ch);
			
			//return $response;
			return $response;

		
		}
	}


?>