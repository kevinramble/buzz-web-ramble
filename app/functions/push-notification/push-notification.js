jQuery(document).ready(function($) {
	if ($('#push_notification_meta_box').length > 0) {
		// Não deixar enviar o formulário apertando enter
		$(window).keydown(function(e) {
			if (e.keyCode === 13) {
				e.preventDefault()
				return false
			}
		})

		var titleInput = $('.title'),
			titleVal = titleInput.val(),
			contentInput = $('.content'),
			contentVal = contentInput.val(),
			typeSelect = $('.type'),
			typeVal = typeSelect.val(),
			hasType = false,
			hasContent = false,
			hasTitle = false

		setTimeout(function() {
			if (titleVal === '') {
				hasTitle = false
				titleInput.addClass('error')
			}
			if (contentVal === '') {
				hasContent = false
				contentInput.addClass('error')
			}
			if (typeVal === '') {
				hasType = false
				typeSelect.addClass('error')
			}

			if (!hasTitle || !hasContent || !hasType) {
				$('#publish').addClass('disabled-submit')
			}
		}, 20)

		titleInput.on('change keyup', function() {
			var currentVal = $(this).val()

			if (!currentVal) {
				hasTitle = false
				titleInput.addClass('error')
			} else {
				hasTitle = true
				titleInput.removeClass('error')
			}

			runValidation()
		})

		contentInput.on('change keyup', function() {
			var currentVal = $(this).val()

			if (!currentVal) {
				hasContent = false
				contentInput.addClass('error')
			} else {
				hasContent = true
				contentInput.removeClass('error')
			}

			runValidation()
		})

		typeSelect.on('change', function() {
			var currentVal = $(this).val()

			if (currentVal) {
				typeSelect.removeClass('error')
				hasType = true

				if (currentVal == 'updates') {
					hasContent = true
					contentInput.removeClass('error').val('Brace yourselves, Buzz just got better. The app is updated.')
				}
			}

			if (!currentVal) {
				hasType = false
				hasContent = false
				contentInput.val('').addClass('error')
				typeSelect.addClass('error')
			}

			runValidation()
		})

		function runValidation() {
			if (!hasTitle || !hasContent || !hasType) {
				$('#publish').addClass('disabled-submit')
			}

			if (hasTitle && hasContent && hasType) {
				$('#publish').removeClass('disabled-submit')
			}
		}
	}
})
