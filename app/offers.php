<?php
    /* Template Name: Offers page */
    get_header(); ?>

	<section class="l-page-list">
		<div class="c-list-header">
			<div class="container">
				<div class="row align-items-center pos-relative">
					<div class="col-md-6">
						<h1 class="c-list-header__title">Offers</h1>
					</div>

					<span class="js-toggle-filters"></span>
					<div class="col-md-6 d-lg-block js-offer-filters js-filters">
						<div class="row">
							<div class="col-6">
								<div class="row">
									<?php /*<div class="col-6 px-1">
										<div class="c-select">
											<select class="js-filter-select" data-type="cat">
												<option value="">All categories</option>
												<?php
													$terms = get_terms(array('taxonomy' => 'offer-category', 'hide_empty' => 1));
													foreach ($terms as $term) echo '<option value="'. $term->slug .'">'. $term->name .'</option>';
												?>
											</select>
										</div>
									</div>*/ ?>

									<div class="col">
										<div class="c-select">
											<select class="js-filter-select" data-type="country" id="offer_country">
												<option value="">All countries</option>
                                                <option value="Anguilla">Anguilla</option>
                                                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                <option value="ABC Islands">ABC Islands</option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Bermuda">Bermuda</option>
                                                <option value="British Virgin Islands">British Virgin Islands</option>
                                                <option value="Cayman Islands">Cayman Islands</option>
                                                <option value="Dominica">Dominica</option>
                                                <option value="FWI">FWI</option>
                                                <option value="Grenada">Grenada</option>
                                                <option value="Guyana">Guyana</option>
                                                <option value="Haiti">Haiti</option>
                                                <option value="Jamaica" <?php if($country == '_jamaica') echo 'selected="selected"'; ?>>Jamaica</option>
                                                <option value="Montserrat">Montserrat</option>
                                                <option value="St. Kitts and Nevis">St. Kitts and Nevis</option>
                                                <option value="St. Lucia" <?php if($country == '_stlucia') echo 'selected="selected"'; ?>>St. Lucia</option>
                                                <option value="St. Martin">St. Martin</option>
                                                <option value="St. Vincent and the Grenadines">St. Vincent and the Grenadines</option>
                                                <option value="Suriname">Suriname</option>
                                                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                <option value="Turks and Caicos" <?php if($country == '_turksandcaicos') echo 'selected="selected"'; ?>>Turks and Caicos</option>
											</select>
										</div>
									</div>
									<?php /*<div class="col-6">
										<div class="c-select">
											<select class="js-filter-select" id="offer_state" disabled="disabled" data-type="state">
												<option value="">State</option>
											</select>
										</div>
									</div>*/ ?>
								</div>
							</div>
							<div class="col-6">
								<form class="c-search-form js-submit-search">
									<input type="search" class="c-search-form__input" placeholder="Search offers">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="l-page-list__results">
			<div class="container">
				<div class="row js-posts-list">
					<?php
						if($country == '_jamaica'){
							$countryQuery = array(
								'key' => 'offer_country',
								'value' => 'Jamaica',
								'compare' => '='
							);
						} elseif($country == '_stlucia'){
							$countryQuery = array(
								'key' => 'offer_country',
								'value' => 'St. Lucia',
								'compare' => '='
							);
						} elseif($country == '_turksandcaicos'){
							$countryQuery = array(
								'key' => 'offer_country',
								'value' => 'Turks and Caicos',
								'compare' => '='
							);
						} else {
							$countryQuery = '';
						}

						$args = array('post_type' => 'offer', 'posts_per_page' => 8, 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged,
						'meta_query' => array('relation' => 'AND', array('key' => 'offer_expiration', 'value' => date('Y-m-d'), 'compare' => '>='), $countryQuery));
						query_posts($args);

						if($wp_query->have_posts()) : ?>
							<?php while ($wp_query->have_posts()) : $wp_query->the_post();
								loop_offerlisting($post->ID);
							endwhile; ?>
					<?php endif; ?>
				</div>

				<?php // load more ajax
				$wp_query->query_vars['search_orderby_title'] = ''; // necessario pro search
				$load_posts = $wp_query->query_vars; $load_current_page = $wp_query->query_vars['paged']; $load_max_page = $wp_query->max_num_pages;
				loadmore_button($load_posts, $load_current_page, $load_max_page);
				if($wp_query->max_num_pages > 1){ ?>
					<span class="js-loadmore c-bt-load">Load more</span>
				<?php } else { ?>
					<span class="js-loadmore c-bt-load hidden">Load more</span>
				<?php } // end load more ajax

				wp_reset_query(); wp_reset_postdata(); ?>
			</div>
		</div>
	</section>

	<?php /*<script src="<?php echo get_template_directory_uri(); ?>/scripts/vendor/country-states.js"></script>
	<?php if($country){ ?>
        <script>set_city_state(offer_country,offer_state)</script>
    <?php }*/ ?>

<?php get_footer(); ?>