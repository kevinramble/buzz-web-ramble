<?php get_header(); ?>

<?php
	$subCatTitle = single_cat_title('', false);
	$getSubCat = get_term_by('name', $subCatTitle, 'sub-category');
	$subCatID = $getSubCat->term_id;
	$categoryTitle = '';
	$countryTitle = '';
	$tax_query = '';
	$hasCat = '';
	$hasCountry = '';

	if(isset($_GET['category']) && $_GET['category']){
		$category = $_GET['category'];
		$categoryTitle = ucfirst($category) . ' / ';
		$hasCat = '?category='.$category;

		$tax_query = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'sub-category',
				'field'    => 'term_id',
				'terms'    => $subCatID,
			),
			array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => $category
			),
		);
	}

	if(isset($_GET['countries']) && $_GET['countries']){
		$country = $_GET['countries'];
		$countryTerm = get_term_by('slug', $country, 'countries');
		$countryTitle = ucfirst($countryTerm->name) . ' / ';
		$hasCountry = '&countries='.$country;

		$tax_query = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'sub-category',
				'field'    => 'term_id',
				'terms'    => $subCatID,
			),
			array(
				'taxonomy' => 'countries',
				'field'    => 'slug',
				'terms'    => $country
			)
		);
	}

	if((isset($_GET['category']) && $_GET['category']) && (isset($_GET['countries']) && $_GET['countries'])){
		$tax_query = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'sub-category',
				'field'    => 'term_id',
				'terms'    => $subCatID,
			),
			array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => $category
			),
			array(
				'taxonomy' => 'countries',
				'field'    => 'slug',
				'terms'    => $country
			)
		);
	}
?>

<section class="l-page-list">
	<div class="c-list-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6">
					<h1 class="c-list-header__title">
						<?php
							echo $countryTitle . $categoryTitle . $subCatTitle;
						?>
					</h1>
				</div>

				<div class="col-md-6 d-none d-lg-block">
					<div class="row justify-content-end">
						<div class="col-4">
							<div class="c-select">
								<select class="js-child-cats">
									<option value="<?php echo get_category_link($subCatID) .'?'. $hasCat; ?>">Countries</option>
									<?php
										$terms = get_terms(array('taxonomy' => 'countries', 'hide_empty' => 1));
										foreach ($terms as $term){
											$active = ($term->slug == $country) ? 'selected="selected"' : '';
											echo '<option '. $active .' value="'. get_category_link($subCatID) . '?' . $hasCat .'&countries='.$term->slug.'">'. $term->name .'</option>';
										}
									?>
								</select>
							</div>
						</div>

						<div class="col-4">
							<div class="c-select">
								<select class="js-child-cats">
									<option value="<?php echo get_category_link($subCatID) .'?'. $hasCountry; ?>">Categories</option>
									<?php
										$terms = get_terms(array('taxonomy' => 'category', 'hide_empty' => 1));
										foreach ($terms as $term){
											$active = ($term->slug == $category) ? 'selected="selected"' : '';
											echo '<option '. $active .' value="'. get_category_link($subCatID) .'?category='. $term->slug . $hasCountry . '">'. $term->name .'</option>';
										}
									?>
								</select>
							</div>
						</div>

						<div class="col-4">
							<div class="c-select">
								<select class="js-child-cats">
									<option value="">Sub-categories</option>
									<?php
										$terms = get_terms(array('taxonomy' => 'sub-category', 'hide_empty' => 1));
										foreach ($terms as $term){
											$active = ($term->name == $subCatTitle) ? 'selected="selected"' : '';
											echo '<option '. $active .' value="'. get_category_link($term->term_id) . '?' . $hasCat . $hasCountry .'">'. $term->name .'</option>';
										}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="l-page-list__results">
		<div class="container">
			<div class="row js-posts-list">
				<?php $args = array_merge($wp_query->query, array('tax_query' => $tax_query, 'posts_per_page' => 8, 'post_type' => array('post', 'video'), 'orderby' => 'date', 'order' => 'DESC', 'paged' => $paged, 'childcat' => 1)); query_posts($args); ?>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php loop_genericcard($post->ID, '4'); ?>
					<?php endwhile; ?>
				<?php else: ?>
					<h4 class="title-nocontent">Nothing to show.</h4>
				<?php endif; ?>
			</div>

			<?php // load more ajax
			$wp_query->query_vars['search_orderby_title'] = ''; // necessario pro search
			$load_posts = $wp_query->query_vars; $load_current_page = $wp_query->query_vars['paged']; $load_max_page = $wp_query->max_num_pages;
			loadmore_button($load_posts, $load_current_page, $load_max_page);
			if($wp_query->max_num_pages > 1){ ?>
				<span class="js-loadmore c-bt-load">Load more</span>
			<?php } else { ?>
				<span class="js-loadmore c-bt-load hidden">Load more</span>
			<?php } // end load more ajax
				
			wp_reset_query(); wp_reset_postdata(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>