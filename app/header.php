<?php
	global $country;

	if(isset($_GET['country'])){
		if($_GET['country'] == 'jamaica' || $_GET['country'] == 'stlucia' || $_GET['country'] == 'turksandcaicos' || $_GET['country'] == 'bahamas'|| $_GET['country'] == 'barbados'|| $_GET['country'] == 'cayman' ){
			$country = '_' . $_GET['country'];
		} else {
			$country = '';
		}

		setcookie('site-country', $country, time()+60*60*24*365, '/');
		$_COOKIE['site-country'] = $country;
	} else {
		$country = get_country();
	}
	//set country to global
	//$country = '';
?>

<!DOCTYPE html>
<html class="no-js" lang="en" xmlns:og="http://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="apple-itunes-app" content="app-id=1478912293">
	<meta name="google-play-app" content="app-id=com.cappen.BUZZ">


	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Assistant:200,400,700">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/styles/main.css?v=3">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/favicons/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/favicons/webmanifest.json">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicons/safari-pinned-tab.svg" color="#000000">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="theme-color" content="#ffffff">

	<script src="<?php echo get_template_directory_uri(); ?>/scripts/modernizr.js"></script>

	<?php wp_head(); ?>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145795056-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-145795056-1');
	</script>

	<?php include_once('inc/shared/ads.php'); ?>

	<script type="text/javascript">
		window._taboola = window._taboola || [];
		_taboola.push({article:'auto'});
		!function (e, f, u, i) {
			if (!document.getElementById(i)){
			e.async = 1;
			e.src = u;
			e.id = i;
			f.parentNode.insertBefore(e, f);
			}
		}(document.createElement('script'),
		document.getElementsByTagName('script')[0],
		'//cdn.taboola.com/libtrc/ramblemedia-network/loader.js',
		'tb_loader_script');
		if(window.performance && typeof window.performance.mark == 'function')
			{window.performance.mark('tbl_ic');}
	</script>
	<!-- Google Tag Manager -->

	<script>
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KNFK8LJ');
	</script>

	<!-- End Google Tag Manager -->
</head>

<body <?php body_class('normal'); ?>>

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KNFK8LJ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

	<?php
		$home_leaderboard_function = 'home_leaderboard' . $country;
		$leaderboard_status = $home_leaderboard_function('home_leaderboard'. $country .'_status');
		if(is_home() && $leaderboard_status == 1) include_once('inc/home/leaderboard-ad.php');
	?>

	<header class="l-header <?php if(is_home() && $leaderboard_status == 1){ if(!($_COOKIE['ad-cookies'])){ echo 'banner-on'; }} ?>">
		<div class="l-header__wrapper">
			<a href="#0" class="c-button-menu d-block d-lg-none js-toggle-menu-mobile"></a>
			<a href="#0" class="c-button c-button--icon d-block d-lg-none order-3 js-toggle-search"><span class="icon icon-search"></span></a>

			<div class="l-header__brand">
				<a href="<?php echo get_bloginfo('url'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/layout/logo.png" alt="Buzz" class="c-logo"></a>
			</div>
			<!-- Remove country picker -->
			<div class="l-header__options d-none d-lg-block">
				<div class="c-select c-select--highlight">
					<select name="country" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
						<option value="?country=global" <?php if($country != '_jamaica' || $country != '_stlucia' || $country != '_turksandcaicos') echo 'selected="selected"'; ?>>Global</option>
						<option value="?country=jamaica" <?php if($country == '_jamaica') echo 'selected="selected"'; ?>>Jamaica</option>
						<option value="?country=stlucia" <?php if($country == '_stlucia') echo 'selected="selected"'; ?>>St. Lucia</option>
						<option value="?country=turksandcaicos" <?php if($country == '_turksandcaicos') echo 'selected="selected"'; ?>>Turks and Caicos</option>
						<!-- New Countries -->
                        <option value="?country=barbados" <?php if($country == '_barbados') echo 'selected="selected"'; ?>>Barbados</option>
                        <option value="?country=bahamas" <?php if($country == '_bahamas') echo 'selected="selected"'; ?>>Bahamas</option>
                        <option value="?country=cayman" <?php if($country == '_cayman') echo 'selected="selected"'; ?>>Cayman</option>
					</select>
				</div>
			</div>
			<!-- End Remove country picker -->
			<div class="l-header__controls d-none d-lg-block">
				<?php include('inc/shared/menu.php'); ?>
			</div>

			<?php include('inc/shared/menu-mobile.php'); ?>
		</div>
		<?php include('inc/shared/search.php'); ?>
	</header>

	<div class="c-cookie-notice js-cookie-bar">
		<p>By continuing to use this site, you give your consent to our use of cookies for analytics, personalization and ads. <a href="<?php echo get_bloginfo('url'); ?>/terms-and-conditions">Read more</a></p>
		<button class="c-cookie-notice__btn js-accept-cookie" aria-label="Close"></button>
	</div>

	<?php
		$class_main = '';

		if (is_page(array('login', 'register', 'password-reset'))) {
			$class_main = 'is-dark is-full is-center';
		}

		if (is_page('jobs')) {
			$class_main = 'is-dark-gray is-full';
		}

		if (is_singular('video')) {
			$class_main = 'is-dark is-full';
		}
	?>

	<main class="l-main <?php echo $class_main; ?>">
