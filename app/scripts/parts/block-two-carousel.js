$(function() {
	if ($('.js-block-two-carousel').length > 0) {
		$blockTwoCarousel = $('.js-block-two-carousel')

		var settings = {
			autoplay: true,
			loop: true,
			nav: false,
			dots: true,
			items: 1,
			smartSpeed: 1000,
			autoplayHoverPause: true
		}

		$blockTwoCarousel.owlCarousel(settings)
	}
})
