$(function() {
	$carouselPost = $('.wp-block-gallery, .block-video-gallery, .carrousel.single-video')
	$carouselPost.addClass('owl-carousel c-carousel-default')

	if ($carouselPost.length > 0) {
		var settings = {
			loop: true,
			items: 1,
			autoHeight: true,
			responsive: {
				768: {
					nav: true,
					dots: false
				}
			}
		}

		$carouselPost.owlCarousel(settings)
	}
})
