$(function() {
	$blocks = $('.js-blocks-carousel')

	if ($blocks.length > 0) {
		var carousel_settings = {
			items: 1,
			smartSpeed: 1000,
			loop: true,
			nav: false,
			dots: true
		}

		function init() {
			var w = $(window).width()

			if (w < 768) {
				$blocks.owlCarousel(carousel_settings)
				$blocks.addClass('owl-carousel')
			} else {
				$blocks.owlCarousel('destroy')
				$blocks.removeClass('owl-carousel')
			}
		}

		var id
		$(window).resize(function() {
			clearTimeout(id)
			id = setTimeout(init, 500)
		})

		init()
	}
})
