import { setCookie } from './cookies'

$(function() {
	// se existir leaderboard:
	if ($('.js-leaderboard-wrapper').length > 0) {
		if ($('.js-leaderboard-wrapper.desktop').is(':visible')) var scrollHeight = 430
		if ($('.js-leaderboard-wrapper.mobile').is(':visible')) var scrollHeight = 130

		$(window).resize(function() {
			if ($('.js-leaderboard-wrapper.desktop').is(':visible')) {
				scrollHeight = 430
			} else if ($('.js-leaderboard-wrapper.mobile').is(':visible')) {
				scrollHeight = 130
			}
		})

		$(document).scroll(function() {
			if ($('.js-leaderboard-wrapper').is(':visible')) {
				if (scrollY <= scrollHeight) {
					$('.l-header').addClass('banner-on')
				} else {
					$('.l-header').removeClass('banner-on')
				}
			} else {
				$('.l-header').removeClass('banner-on')
			}
		})

		$('.js-leaderboard-close').on('click', function() {
			$('.l-header').removeClass('banner-on')
			$('.js-leaderboard-wrapper').hide()

			setCookie('ad-cookies', true, 365)
		})
	}

	// caso não exista o leaderboard, remover classe do header
	if ($('.js-leaderboard-wrapper').length <= 0) {
		$('.l-header').removeClass('banner-on')
	}
})
