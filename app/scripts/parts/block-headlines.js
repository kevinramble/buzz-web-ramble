$(function() {
	var $headlinesFull = $('.js-headlines-full')
	var $headlinesCall = $('.js-headlines-call')
	var $progressBar = $('.js-headlines-progress')

	$headlinesFull
		.on('initialized.owl.carousel', function(e) {
			start()
		})
		.owlCarousel({
			items: 1,
			mouseDrag: false,
			touchDrag: false,
			smartSpeed: 1000,
			loop: true,
			nav: false,
			dots: false
		})
		.on('changed.owl.carousel', function(e) {
			var page = e.relatedTarget.relative(e.item.index)

			setTimeout(function() {
				$headlinesCall.removeClass('is-active')
				$headlinesCall.eq(page).addClass('is-active')
				$headlinesCall.find('.c-headline__progress').removeClass('is-active')
				$headlinesCall
					.eq(page)
					.find('.c-headline__progress')
					.addClass('is-active')
			}, 10)

			moved()
		})

	$headlinesCall.on('click', function() {
		var page = $(this).index()

		$headlinesFull.trigger('to.owl.carousel', page, [800])
	})

	// Reference: http://www.landmarkmlp.com/js-plugin/owl.carousel/demos/progressBar.html
	var time = 8
	var $progressBar, tick, percentTime

	function start() {
		percentTime = 0
		tick = setInterval(interval, 10)
	}

	function interval() {
		percentTime += 1 / time
		$progressBar.css({
			width: percentTime + '%'
		})
		if (percentTime >= 100) {
			$headlinesFull.trigger('next.owl.carousel')
		}
	}

	function moved() {
		clearTimeout(tick)
		start()
	}
})
