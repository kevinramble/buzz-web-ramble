$(function() {
	if ($('.c-newsletter').length > 0) {
		$('.js-newsletter-modal').click(function() {
			var input = $('.c-newsletter .c-newsletter__input'),
				email = input.val(),
				modal = $('.modal.newsletter')

			if (email && validateEmail(email)) {
				$('body').addClass('overflow-hidden')
				modal.addClass('is-open')

				$('.js-email').val(email)
				checkCaptcha()
			} else {
				input.addClass('js-error')
			}

			return false
		})

		$('#mc-embedded-subscribe-form').submit(function() {
			var emailInput = $('#mce-EMAIL'),
				firstNameInput = $('#mce-FNAME'),
				lastNameInput = $('#mce-LNAME'),
				email = emailInput.val(),
				firstName = firstNameInput.val(),
				lastName = lastNameInput.val()

			if (email && validateEmail(email)) {
				emailInput.removeClass('js-error')
			} else {
				emailInput.addClass('js-error')
			}

			if (firstName) {
				firstNameInput.removeClass('js-error')
			} else {
				firstNameInput.addClass('js-error')
			}

			if (lastName) {
				lastNameInput.removeClass('js-error')
			} else {
				lastNameInput.addClass('js-error')
			}
		})

		function checkCaptcha() {
			$('#mc-embedded-subscribe-form').on('keyup keypress', function(e) {
				var keyCode = e.keyCode || e.which
				if (keyCode === 13) {
					e.preventDefault()
					return false
				}
			})

			var captcha = $('.modal.newsletter #securityanswer'),
				securitynumber1 = $('.modal.newsletter #securitynumber1').val(),
				securitynumber2 = $('.modal.newsletter #securitynumber2').val(),
				securityanswer = parseInt(securitynumber1) + parseInt(securitynumber2)

			captcha.on('change keyup', function() {
				currentVal = $(this).val()

				if (currentVal && currentVal == securityanswer) {
					$('#mc-embedded-subscribe').removeClass('inactive')
					captcha.removeClass('js-error')
				} else {
					$('#mc-embedded-subscribe').addClass('inactive')
					captcha.addClass('js-error')
				}
			})
		}
	}
})
