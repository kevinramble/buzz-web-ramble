$(function() {
	$carouselDefault = $('.js-carousel-default')

	if ($carouselDefault.length > 0) {
		var settings = {
			loop: true,
			items: 1,
			autoHeight: true,
			lazyLoad: true,
			responsive: {
				768: {
					nav: true,
					dots: false
				}
			}
		}

		$carouselDefault.owlCarousel(settings)
	}
})
