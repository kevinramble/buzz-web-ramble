$(function() {
	$.smartbanner({
		title: 'Buzz Caribbean',
		author: 'Ramble Media Group',
		daysHidden: 7,
		daysReminder: 30
	})
})
$('body').on('click', '#smartbanner .sb-close', function(e) {
	e.preventDefault()
	$('#smartbanner').slideUp()
})
