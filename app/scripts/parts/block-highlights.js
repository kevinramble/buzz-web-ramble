$(function() {
	var $highlightsFull = $('.js-highlights-full')
	var $highlightsThumb = $('.js-highlights-thumb')
	var $progressBar = $('.js-highlights-progress')
	var time = 12
	var tick, percentTime
	var maskTimeout = 500

	$highlightsFull
		.on('initialized.owl.carousel', function(e) {
			start()
		})
		.owlCarousel({
			items: 1,
			mouseDrag: false,
			touchDrag: false,
			loop: true,
			nav: false,
			dots: false,
			animateIn: 'fadeIn',
			animateOut: 'fadeOut'
		})
		.on('changed.owl.carousel', function(e) {
			var page = e.relatedTarget.relative(e.item.index)
			activeThumb(page)
			moved()
		})

	$highlightsThumb.on('click', function() {
		var page = $(this).index()

		if (!$(this).hasClass('is-active')) {
			$highlightsFull.addClass('is-animated')

			setTimeout(() => {
				$highlightsFull.removeClass('is-animated')
				$highlightsFull.trigger('to.owl.carousel', page, [100])
			}, maskTimeout)
		}

		$highlightsThumb.removeClass('is-active')
		$(this).addClass('is-active')

		activeThumb(page)
	})

	// Reference: http://www.landmarkmlp.com/js-plugin/owl.carousel/demos/progressBar.html
	function start() {
		percentTime = 0
		tick = setInterval(interval, 10)

		$highlightsFull.addClass('is-animated')
		setTimeout(() => {
			$highlightsFull.removeClass('is-animated')
		}, 200)
	}

	function interval() {
		percentTime += 1 / time
		$progressBar.css({
			width: percentTime + '%'
		})
		if (percentTime >= 100) {
			$highlightsFull.addClass('is-animated')

			clearTimeout(tick)

			setTimeout(() => {
				$highlightsFull.trigger('next.owl.carousel')
			}, maskTimeout)
		}
	}

	function moved() {
		clearTimeout(tick)
		start()
	}

	function activeThumb(page) {
		$highlightsThumb.find('h2').removeClass('is-active')
		$highlightsThumb
			.eq(page)
			.find('h2')
			.addClass('is-active')
	}
})
