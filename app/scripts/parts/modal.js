$(function() {
	if ($('.modal').length > 0) {
		$body = $('body')
		$modal = $('.modal')

		function openModal(modalType) {
			$body.addClass('overflow-hidden')
			$('.modal.' + modalType).addClass('is-open')
		}

		function closeModal() {
			$body.removeClass('overflow-hidden')
			$modal.removeClass('is-open')
		}

		$(document).keyup(function(e) {
			if (e.keyCode === 27) closeModal()
		})

		$('.modal, .modal__close').on('click', function() {
			closeModal()
		})

		$('.modal__dialog').on('click', function(e) {
			e.stopPropagation()
		})

		$('.js-open-modal').on('click', function() {
			var modalType = $('.js-open-modal').data('modal')

			if (modalType) openModal(modalType)
			return false
		})
	}
})
