$(function() {
	$storiesCarousel = $('.js-carousel-stories')

	if ($storiesCarousel.length > 0) {
		var settings = {
			autoplay: true,
			loop: true,
			dots: true,
			items: 1,
			smartSpeed: 1000,
			autoplayHoverPause: true,
			responsive: {
				0: {
					nav: false
				},
				768: {
					nav: true
				}
			}
		}

		$storiesCarousel.owlCarousel(settings)
	}
})
