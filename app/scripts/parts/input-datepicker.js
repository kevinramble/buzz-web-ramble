import 'daterangepicker'

$(function() {
	var today = new Date()
	var dd = today.getDate()
	var mm = today.getMonth() + 1 //January is 0
	var yyyy = today.getFullYear()

	if (dd < 10) dd = '0' + dd
	if (mm < 10) mm = '0' + mm
	today = dd + '/' + mm + '/' + yyyy

	$('.js-datepicker-multiple').daterangepicker({
		minDate: today,
		autoUpdateInput: false,
		locale: {
			format: 'DD/MM/YYYY',
			separator: ' - '
		}
	})

	$('.js-datepicker-multiple').on('apply.daterangepicker', function(ev, picker) {
		$(this)
			.find('input')
			.val(picker.startDate.format('DD/MM/YY') + ' - ' + picker.endDate.format('DD/MM/YY'))

		var newStartDate = picker.startDate.format('YYYY-MM-DD'),
			newEndDate = picker.endDate.format('YYYY-MM-DD')
		runDateRange(newStartDate, newEndDate)
	})

	$('.js-datepicker-multiple').on('cancel.daterangepicker', function(ev, picker) {
		$(this)
			.find('input')
			.val('')
		clearDateRange()
	})
})
