$(function() {
	function readFile(obj) {
		var $fileControl = $('*[class*="file-name"]')
		$fileControl.attr('data-title', obj.files[0].name)
	}

	$('.js-input-file').on('change', function() {
		readFile(this)
	})
})
