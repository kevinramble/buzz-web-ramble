const copy = require('copy-text-to-clipboard')

$(function() {
	$('.js-posts-list').on('click', '.js-toggle-share', function(e) {
		e.preventDefault()
		$(this)
			.parent()
			.find('.js-box-share')
			.toggleClass('is-active')
	})

	// Clipboard
	$('body').on('click', '.js-toggle-copy', function() {
		var oldText = $(this).text()

		copy(
			$(this)
				.parent()
				.find('.js-box-copy')
				.text()
		)

		$(this).text('Copied!')
		setTimeout(() => {
			$(this).text(oldText)
		}, 1500)
	})
})
