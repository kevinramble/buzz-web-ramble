$(function() {
	$featuredCarousel = $('.js-featured-carousel')

	if ($featuredCarousel.length > 0) {
		var settings = {
			nav: false,
			dots: true,
			responsive: {
				0: {
					items: 1
				},
				768: {
					items: 3
				}
			}
		}

		$featuredCarousel.owlCarousel(settings)
	}
})
