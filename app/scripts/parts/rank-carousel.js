$(function() {
	if ($('.js-list-carousel').length > 0) {
		$owl = $('.js-list-carousel')

		var carousel_settings = {
			loop: true,
			nav: false,
			dots: false,
			responsive: {
				0: {
					stagePadding: 90,
					margin: 20,
					items: 1
				},
				420: {
					margin: 25,
					stagePadding: 25,
					items: 3
				},
				768: {
					margin: 30,
					stagePadding: 30,
					items: 4
				}
			}
		}

		function init() {
			var w = $(window).width()

			if (w < 992) {
				$owl.owlCarousel(carousel_settings)
				$('.c-list__carousel').addClass('owl-carousel')
			} else {
				$owl.owlCarousel('destroy')
				$('.c-list__carousel').removeClass('owl-carousel')
			}
		}

		var id
		$(window).resize(function() {
			clearTimeout(id)
			id = setTimeout(init, 500)
		})

		init()
	} // length if
})
