$(function() {
	$videosCarousel = $('.js-videos-carousel')

	if ($videosCarousel.length > 0) {
		var settings = {
			responsive: {
				0: {
					items: 2
					// loop: true,
					// autoWidth: true
				},
				768: {
					items: 4
					// loop: true,
					// autoWidth: true,
					// center: true
				},
				1200: {
					items: 4,
					nav: true,
					dots: false
				}
			}
		}

		$videosCarousel.owlCarousel(settings)
	}
})
