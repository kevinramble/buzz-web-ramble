$(function() {
	if ($('.js-advertise-form').length > 0) {
		$('#adphone').mask('9 999 999 9999')
		$('.js-advertise-form').submit(function() {
			var form = $(this),
				url = form.data('url') + '/inc/send-emails/send-advertising.php',
				name = $('#adfirstname'),
				lastname = $('#adlastname'),
				phone = $('#adphone'),
				email = $('#ademail'),
				company = $('#adcompany'),
				position = $('#adposition'),
				message = $('#admessage'),
				captcha = $('#securityanswer'),
				notice = form.find('.notice'),
				modal = $('.modal.ad-request-sent-successful'),
				submit = form.find('.js-send-button'),
				error = false

			notice.addClass('hidden')

			submit.blur().addClass('js-loading')

			if (name.val() == '') {
				name.addClass('js-error')
				error = true
			} else {
				name.removeClass('js-error')
			}
			if (lastname.val() == '') {
				lastname.addClass('js-error')
				error = true
			} else {
				lastname.removeClass('js-error')
			}
			if (phone.val() == '') {
				phone.addClass('js-error')
				error = true
			} else {
				phone.removeClass('js-error')
			}
			if (!validateEmail(email.val())) {
				email.addClass('js-error')
				error = true
			} else {
				email.removeClass('js-error')
			}
			if (company.val() == '') {
				company.addClass('js-error')
				error = true
			} else {
				company.removeClass('js-error')
			}
			if (position.val() == '') {
				position.addClass('js-error')
				error = true
			} else {
				position.removeClass('js-error')
			}
			if (message.val() == '') {
				message.addClass('js-error')
				error = true
			} else {
				message.removeClass('js-error')
			}
			if (captcha.val() == '') {
				captcha.addClass('js-error')
				error = true
			} else {
				captcha.removeClass('js-error')
			}

			if (!error) {
				$.ajax({
					type: 'POST',
					url: url,
					data: form.serialize(),
					success: function(msg) {
						if (msg == '1') {
							form.get(0).reset()
							notice
								.html('')
								.addClass('js-success')
								.removeClass('js-error')
								.addClass('hidden')
							submit.removeClass('js-loading')
							captcha.removeClass('js-error')

							modal.addClass('is-open')
						} else if (msg == '2') {
							notice
								.html('Incorrect security answer, please try again.')
								.addClass('js-error')
								.removeClass('js-success')
								.removeClass('hidden')
							submit.removeClass('js-loading')
							captcha.addClass('js-error')
						} else {
							notice
								.html(msg)
								.addClass('js-error')
								.removeClass('js-success')
								.removeClass('hidden')
							submit.removeClass('js-loading')
						}
					}
				})
			} else {
				notice
					.html('Please check the highlighted fields.')
					.addClass('js-error')
					.removeClass('js-success')
					.removeClass('hidden')
				submit.removeClass('js-loading')
			}

			return false
		})
	}
})
