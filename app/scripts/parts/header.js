import Headroom from 'headroom.js'
window.Headroom = Headroom
import 'headroom.js/dist/jQuery.headroom.js'

$(function() {
	var $boxSearch = $('.js-box-search')

	if ($(window).width() > 768) {
		$('.l-header').headroom({
			offset: 102,
			classes: {
				initial: 'headroom',
				pinned: 'is-pinned',
				unpinned: 'is-unpinned',
				notTop: 'is-not-top',
				notBottom: 'is-not-bottom'
			},
			onPin: function() {
				$boxSearch.removeClass('d-none')
			},
			onUnpin: function() {
				$boxSearch.addClass('d-none')
				$boxSearch.slideUp()
			}
		})
	}

	// Menu mobile
	$('.js-toggle-menu-mobile').on('click', function(e) {
		e.preventDefault()

		$('.l-header').headroom('destroy')

		if ($boxSearch.is(':hidden')) {
			$(this).toggleClass('is-open')
			$('body').toggleClass('overflow-hidden')

			$('.js-box-menu-mobile')
				.stop()
				.slideToggle(600)
		}
	})

	// Search
	$('.js-toggle-search').on('click', function(e) {
		e.preventDefault()

		if ($('.js-box-menu-mobile').is(':hidden')) {
			$boxSearch.stop().slideToggle(function() {
				var $searchInput = $boxSearch.find('input')

				if ($searchInput.is(':visible')) {
					$searchInput.focus()
				} else {
					$searchInput.blur()
				}
			})
		}
	})

	// Mobile dropdown
	$('.js-toggle-drop-mobile').on('click', function(e) {
		e.preventDefault()

		$(this).toggleClass('is-open')

		$(this)
			.parent()
			.find('.js-box-drop-mobile')
			.stop()
			.slideToggle()
	})

	// Desktop dropdown
	$('.js-toggle-dropdown')
		.on('mouseover', function() {
			$(this)
				.find('.js-box-dropdown')
				.show()
		})
		.on('mouseout', function() {
			$(this)
				.find('.js-box-dropdown')
				.hide()
		})
})
