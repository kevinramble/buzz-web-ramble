$(function() {
	if ($('.js-suggest-content-form').length > 0) {
		$('.js-suggest-content-form').submit(function() {
			var form = $(this),
				url = form.data('url') + '/inc/send-emails/send-suggest-content.php',
				articleTitle = $('#article_title'),
				articleCat = $('#article_category'),
				articleContent = $('#article_content'),
				captcha = $('#securityanswer'),
				notice = form.find('.notice'),
				modal = $('.modal.suggestion-successful'),
				submit = form.find('.js-send-button'),
				error = false

			notice.addClass('hidden')

			submit.blur().addClass('js-loading')

			if (articleTitle.val() == '') {
				articleTitle.addClass('js-error')
				error = true
			} else {
				articleTitle.removeClass('js-error')
			}
			if (articleCat.val() == '') {
				articleCat.addClass('js-error')
				error = true
			} else {
				articleCat.removeClass('js-error')
			}
			if (articleContent.val() == '') {
				articleContent.addClass('js-error')
				error = true
			} else {
				articleContent.removeClass('js-error')
			}

			if (captcha.val() == '') {
				captcha.addClass('js-error')
				error = true
			} else {
				captcha.removeClass('js-error')
			}

			if (!error) {
				$.ajax({
					type: 'POST',
					url: url,
					data: form.serialize(),
					success: function(msg) {
						if (msg == '1') {
							form.get(0).reset()
							notice
								.html('')
								.addClass('js-success')
								.removeClass('js-error')
								.addClass('hidden')
							submit.removeClass('js-loading')
							captcha.removeClass('js-error')

							modal.addClass('is-open')
						} else if (msg == '2') {
							notice
								.html('Incorrect security answer, please try again.')
								.addClass('js-error')
								.removeClass('js-success')
								.removeClass('hidden')
							submit.removeClass('js-loading')
							captcha.addClass('js-error')
						} else {
							notice
								.html(msg)
								.addClass('js-error')
								.removeClass('js-success')
								.removeClass('hidden')
							submit.removeClass('js-loading')
						}
					}
				})
			} else {
				notice
					.html('Please check the highlighted fields.')
					.addClass('js-error')
					.removeClass('js-success')
					.removeClass('hidden')
				submit.removeClass('js-loading')
			}

			return false
		})
	}
})
