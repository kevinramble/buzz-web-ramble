$(function() {
	$('.lazy').Lazy({
		enableThrottle: true,
		threshold: 50,
		effect: 'fadeIn',
		effectTime: 1500
	})
})
