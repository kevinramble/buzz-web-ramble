$(function() {
	var $question = $('.js-question-option')
	$question.on('click', function() {
		$question.removeClass('is-selected')
		$(this).addClass('is-selected')

		$('.js-question-input').val($(this).text())
	})
})
