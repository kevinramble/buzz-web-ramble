import { setCookie, getCookie } from './cookies'

function success(position) {
	$.getJSON(
		'https://nominatim.openstreetmap.org/reverse?format=jsonv2',
		{
			lat: position.coords.latitude,
			lon: position.coords.longitude
		},
		function(result) {
			setCookie('site-country', result.address.country_code, 365)
		}
	)

	var cookie = getCookie('site-country')
	if (!cookie || cookie == null) {
		console.log('if cookie: ', getCookie('site-country'))
		location.reload(true)
	} else {
		console.log('else cookie: ', getCookie('site-country'))
	}
}

function error() {
	setCookie('site-country', 'blocked', 365)
	console.log('cookie de erro: ', getCookie('site-country'))
}

if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(success, error)
}
