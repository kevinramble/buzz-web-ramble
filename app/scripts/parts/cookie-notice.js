import { getCookie, setCookie } from './cookies'

$(function() {
	var $cookieBar = $('.js-cookie-bar')

	$cookieBar.addClass('is-loaded')

	if (getCookie('use-cookies')) {
		$cookieBar.addClass('is-closed')
	}

	$('.js-accept-cookie').on('click', function() {
		setCookie('use-cookies', true, 365)
		$cookieBar.addClass('is-closed')
	})
})
