// Dependencies
import 'owl.carousel'
import 'jquery-lazy'

// Parts
import './parts/smartbanner'
import './parts/bannermobile'
import './parts/lazy-loading'
import './parts/header'
import './parts/rank-carousel'
import './parts/featured-carousel'
import './parts/block-two-carousel'
import './parts/stories-carousel'
import './parts/featured-blocks'
import './parts/block-highlights'
import './parts/block-headlines'
import './parts/videos-list'
import './parts/card-post'
import './parts/carousel-default'
import './parts/post-gallery'
import './parts/offer-question'
import './parts/modal'
import './parts/custom-input-file'
import './parts/child-category'
import './parts/validate-email'
import './parts/masked-input'
import './parts/send-suggest-content'
import './parts/send-contact'
import './parts/input-datepicker'
import './parts/send-advertising'
import './parts/toggle-filters'
import './parts/newsletter'
import './parts/cookie-notice'
import './parts/leaderboard-banner'
//import './parts/country'
