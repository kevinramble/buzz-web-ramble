/*! modernizr 3.5.0 (Custom Build) | MIT *
 * https://modernizr.com/download/?-applicationcache-backgroundsize-borderimage-borderradius-boxshadow-cssanimations-csscolumns-cssgradients-csstransforms-csstransforms3d-csstransitions-flexbox-flexboxlegacy-fontface-generatedcontent-hashchange-history-inlinesvg-input-inputtypes-multiplebgs-opacity-rgba-svg-textshadow-webgl-domprefixes-hasevent-mq-prefixes-setclasses-shiv-testallprops-testprop-teststyles !*/
!(function(e, t, n) {
	function r(e, t) {
		return typeof e === t
	}
	function o() {
		var e, t, n, o, i, a, s
		for (var l in x)
			if (x.hasOwnProperty(l)) {
				if (
					((e = []),
					(t = x[l]),
					t.name &&
						(e.push(t.name.toLowerCase()),
						t.options &&
							t.options.aliases &&
							t.options.aliases.length))
				)
					for (n = 0; n < t.options.aliases.length; n++)
						e.push(t.options.aliases[n].toLowerCase())
				for (
					o = r(t.fn, 'function') ? t.fn() : t.fn, i = 0;
					i < e.length;
					i++
				)
					(a = e[i]),
						(s = a.split('.')),
						1 === s.length
							? (Modernizr[s[0]] = o)
							: (!Modernizr[s[0]] ||
									Modernizr[s[0]] instanceof Boolean ||
									(Modernizr[s[0]] = new Boolean(
										Modernizr[s[0]]
									)),
							  (Modernizr[s[0]][s[1]] = o)),
						b.push((o ? '' : 'no-') + s.join('-'))
			}
	}
	function i(e) {
		var t = C.className,
			n = Modernizr._config.classPrefix || ''
		if ((w && (t = t.baseVal), Modernizr._config.enableJSClass)) {
			var r = new RegExp('(^|\\s)' + n + 'no-js(\\s|$)')
			t = t.replace(r, '$1' + n + 'js$2')
		}
		Modernizr._config.enableClasses &&
			((t += ' ' + n + e.join(' ' + n)),
			w ? (C.className.baseVal = t) : (C.className = t))
	}
	function a() {
		return 'function' != typeof t.createElement
			? t.createElement(arguments[0])
			: w
			? t.createElementNS.call(
					t,
					'http://www.w3.org/2000/svg',
					arguments[0]
			  )
			: t.createElement.apply(t, arguments)
	}
	function s() {
		var e = t.body
		return e || ((e = a(w ? 'svg' : 'body')), (e.fake = !0)), e
	}
	function l(e, n, r, o) {
		var i,
			l,
			u,
			d,
			c = 'modernizr',
			f = a('div'),
			p = s()
		if (parseInt(r, 10))
			for (; r--; )
				(u = a('div')),
					(u.id = o ? o[r] : c + (r + 1)),
					f.appendChild(u)
		return (
			(i = a('style')),
			(i.type = 'text/css'),
			(i.id = 's' + c),
			(p.fake ? p : f).appendChild(i),
			p.appendChild(f),
			i.styleSheet
				? (i.styleSheet.cssText = e)
				: i.appendChild(t.createTextNode(e)),
			(f.id = c),
			p.fake &&
				((p.style.background = ''),
				(p.style.overflow = 'hidden'),
				(d = C.style.overflow),
				(C.style.overflow = 'hidden'),
				C.appendChild(p)),
			(l = n(f, e)),
			p.fake
				? (p.parentNode.removeChild(p),
				  (C.style.overflow = d),
				  C.offsetHeight)
				: f.parentNode.removeChild(f),
			!!l
		)
	}
	function u(e, t) {
		return !!~('' + e).indexOf(t)
	}
	function d(e) {
		return e
			.replace(/([a-z])-([a-z])/g, function(e, t, n) {
				return t + n.toUpperCase()
			})
			.replace(/^-/, '')
	}
	function c(e) {
		return e
			.replace(/([A-Z])/g, function(e, t) {
				return '-' + t.toLowerCase()
			})
			.replace(/^ms-/, '-ms-')
	}
	function f(t, n, r) {
		var o
		if ('getComputedStyle' in e) {
			o = getComputedStyle.call(e, t, n)
			var i = e.console
			if (null !== o) r && (o = o.getPropertyValue(r))
			else if (i) {
				var a = i.error ? 'error' : 'log'
				i[a].call(
					i,
					'getComputedStyle returning null, its possible modernizr test results are inaccurate'
				)
			}
		} else o = !n && t.currentStyle && t.currentStyle[r]
		return o
	}
	function p(t, r) {
		var o = t.length
		if ('CSS' in e && 'supports' in e.CSS) {
			for (; o--; ) if (e.CSS.supports(c(t[o]), r)) return !0
			return !1
		}
		if ('CSSSupportsRule' in e) {
			for (var i = []; o--; ) i.push('(' + c(t[o]) + ':' + r + ')')
			return (
				(i = i.join(' or ')),
				l(
					'@supports (' +
						i +
						') { #modernizr { position: absolute; } }',
					function(e) {
						return 'absolute' == f(e, null, 'position')
					}
				)
			)
		}
		return n
	}
	function m(e, t, o, i) {
		function s() {
			c && (delete D.style, delete D.modElem)
		}
		if (((i = r(i, 'undefined') ? !1 : i), !r(o, 'undefined'))) {
			var l = p(e, o)
			if (!r(l, 'undefined')) return l
		}
		for (
			var c, f, m, h, g, v = ['modernizr', 'tspan', 'samp'];
			!D.style && v.length;

		)
			(c = !0), (D.modElem = a(v.shift())), (D.style = D.modElem.style)
		for (m = e.length, f = 0; m > f; f++)
			if (
				((h = e[f]),
				(g = D.style[h]),
				u(h, '-') && (h = d(h)),
				D.style[h] !== n)
			) {
				if (i || r(o, 'undefined')) return s(), 'pfx' == t ? h : !0
				try {
					D.style[h] = o
				} catch (y) {}
				if (D.style[h] != g) return s(), 'pfx' == t ? h : !0
			}
		return s(), !1
	}
	function h(e, t) {
		return function() {
			return e.apply(t, arguments)
		}
	}
	function g(e, t, n) {
		var o
		for (var i in e)
			if (e[i] in t)
				return n === !1
					? e[i]
					: ((o = t[e[i]]), r(o, 'function') ? h(o, n || t) : o)
		return !1
	}
	function v(e, t, n, o, i) {
		var a = e.charAt(0).toUpperCase() + e.slice(1),
			s = (e + ' ' + B.join(a + ' ') + a).split(' ')
		return r(t, 'string') || r(t, 'undefined')
			? m(s, t, o, i)
			: ((s = (e + ' ' + E.join(a + ' ') + a).split(' ')), g(s, t, n))
	}
	function y(e, t, r) {
		return v(e, n, n, t, r)
	}
	var b = [],
		x = [],
		S = {
			_version: '3.5.0',
			_config: {
				classPrefix: '',
				enableClasses: !0,
				enableJSClass: !0,
				usePrefixes: !0
			},
			_q: [],
			on: function(e, t) {
				var n = this
				setTimeout(function() {
					t(n[e])
				}, 0)
			},
			addTest: function(e, t, n) {
				x.push({ name: e, fn: t, options: n })
			},
			addAsyncTest: function(e) {
				x.push({ name: null, fn: e })
			}
		},
		Modernizr = function() {}
	;(Modernizr.prototype = S), (Modernizr = new Modernizr())
	var C = t.documentElement,
		w = 'svg' === C.nodeName.toLowerCase(),
		T = 'Moz O ms Webkit',
		E = S._config.usePrefixes ? T.toLowerCase().split(' ') : []
	S._domPrefixes = E
	var k = S._config.usePrefixes
		? ' -webkit- -moz- -o- -ms- '.split(' ')
		: ['', '']
	S._prefixes = k
	w ||
		!(function(e, t) {
			function n(e, t) {
				var n = e.createElement('p'),
					r = e.getElementsByTagName('head')[0] || e.documentElement
				return (
					(n.innerHTML = 'x<style>' + t + '</style>'),
					r.insertBefore(n.lastChild, r.firstChild)
				)
			}
			function r() {
				var e = b.elements
				return 'string' == typeof e ? e.split(' ') : e
			}
			function o(e, t) {
				var n = b.elements
				'string' != typeof n && (n = n.join(' ')),
					'string' != typeof e && (e = e.join(' ')),
					(b.elements = n + ' ' + e),
					u(t)
			}
			function i(e) {
				var t = y[e[g]]
				return t || ((t = {}), v++, (e[g] = v), (y[v] = t)), t
			}
			function a(e, n, r) {
				if ((n || (n = t), c)) return n.createElement(e)
				r || (r = i(n))
				var o
				return (
					(o = r.cache[e]
						? r.cache[e].cloneNode()
						: h.test(e)
						? (r.cache[e] = r.createElem(e)).cloneNode()
						: r.createElem(e)),
					!o.canHaveChildren || m.test(e) || o.tagUrn
						? o
						: r.frag.appendChild(o)
				)
			}
			function s(e, n) {
				if ((e || (e = t), c)) return e.createDocumentFragment()
				n = n || i(e)
				for (
					var o = n.frag.cloneNode(), a = 0, s = r(), l = s.length;
					l > a;
					a++
				)
					o.createElement(s[a])
				return o
			}
			function l(e, t) {
				t.cache ||
					((t.cache = {}),
					(t.createElem = e.createElement),
					(t.createFrag = e.createDocumentFragment),
					(t.frag = t.createFrag())),
					(e.createElement = function(n) {
						return b.shivMethods ? a(n, e, t) : t.createElem(n)
					}),
					(e.createDocumentFragment = Function(
						'h,f',
						'return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(' +
							r()
								.join()
								.replace(/[\w\-:]+/g, function(e) {
									return (
										t.createElem(e),
										t.frag.createElement(e),
										'c("' + e + '")'
									)
								}) +
							');return n}'
					)(b, t.frag))
			}
			function u(e) {
				e || (e = t)
				var r = i(e)
				return (
					!b.shivCSS ||
						d ||
						r.hasCSS ||
						(r.hasCSS = !!n(
							e,
							'article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}'
						)),
					c || l(e, r),
					e
				)
			}
			var d,
				c,
				f = '3.7.3',
				p = e.html5 || {},
				m = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
				h = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
				g = '_html5shiv',
				v = 0,
				y = {}
			!(function() {
				try {
					var e = t.createElement('a')
					;(e.innerHTML = '<xyz></xyz>'),
						(d = 'hidden' in e),
						(c =
							1 == e.childNodes.length ||
							(function() {
								t.createElement('a')
								var e = t.createDocumentFragment()
								return (
									'undefined' == typeof e.cloneNode ||
									'undefined' ==
										typeof e.createDocumentFragment ||
									'undefined' == typeof e.createElement
								)
							})())
				} catch (n) {
					;(d = !0), (c = !0)
				}
			})()
			var b = {
				elements:
					p.elements ||
					'abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output picture progress section summary template time video',
				version: f,
				shivCSS: p.shivCSS !== !1,
				supportsUnknownElements: c,
				shivMethods: p.shivMethods !== !1,
				type: 'default',
				shivDocument: u,
				createElement: a,
				createDocumentFragment: s,
				addElements: o
			}
			;(e.html5 = b),
				u(t),
				'object' == typeof module &&
					module.exports &&
					(module.exports = b)
		})('undefined' != typeof e ? e : this, t),
		Modernizr.addTest('applicationcache', 'applicationCache' in e),
		Modernizr.addTest('history', function() {
			var t = navigator.userAgent
			return (-1 === t.indexOf('Android 2.') &&
				-1 === t.indexOf('Android 4.0')) ||
				-1 === t.indexOf('Mobile Safari') ||
				-1 !== t.indexOf('Chrome') ||
				-1 !== t.indexOf('Windows Phone') ||
				'file:' === location.protocol
				? e.history && 'pushState' in e.history
				: !1
		}),
		Modernizr.addTest(
			'svg',
			!!t.createElementNS &&
				!!t.createElementNS('http://www.w3.org/2000/svg', 'svg')
					.createSVGRect
		)
	var _ = (function() {
		function e(e, t) {
			var o
			return e
				? ((t && 'string' != typeof t) || (t = a(t || 'div')),
				  (e = 'on' + e),
				  (o = e in t),
				  !o &&
						r &&
						(t.setAttribute || (t = a('div')),
						t.setAttribute(e, ''),
						(o = 'function' == typeof t[e]),
						t[e] !== n && (t[e] = n),
						t.removeAttribute(e)),
				  o)
				: !1
		}
		var r = !('onblur' in t.documentElement)
		return e
	})()
	;(S.hasEvent = _),
		Modernizr.addTest('hashchange', function() {
			return _('hashchange', e) === !1
				? !1
				: t.documentMode === n || t.documentMode > 7
		}),
		Modernizr.addTest('webgl', function() {
			var t = a('canvas'),
				n =
					'probablySupportsContext' in t
						? 'probablySupportsContext'
						: 'supportsContext'
			return n in t
				? t[n]('webgl') || t[n]('experimental-webgl')
				: 'WebGLRenderingContext' in e
		}),
		Modernizr.addTest('cssgradients', function() {
			for (
				var e,
					t = 'background-image:',
					n =
						'gradient(linear,left top,right bottom,from(#9f9),to(white));',
					r = '',
					o = 0,
					i = k.length - 1;
				i > o;
				o++
			)
				(e = 0 === o ? 'to ' : ''),
					(r +=
						t +
						k[o] +
						'linear-gradient(' +
						e +
						'left top, #9f9, white);')
			Modernizr._config.usePrefixes && (r += t + '-webkit-' + n)
			var s = a('a'),
				l = s.style
			return (
				(l.cssText = r),
				('' + l.backgroundImage).indexOf('gradient') > -1
			)
		}),
		Modernizr.addTest('multiplebgs', function() {
			var e = a('a').style
			return (
				(e.cssText =
					'background:url(https://),url(https://),red url(https://)'),
				/(url\s*\(.*?){3}/.test(e.background)
			)
		}),
		Modernizr.addTest('opacity', function() {
			var e = a('a').style
			return (
				(e.cssText = k.join('opacity:.55;')), /^0.55$/.test(e.opacity)
			)
		}),
		Modernizr.addTest('rgba', function() {
			var e = a('a').style
			return (
				(e.cssText = 'background-color:rgba(150,255,150,.5)'),
				('' + e.backgroundColor).indexOf('rgba') > -1
			)
		}),
		Modernizr.addTest('inlinesvg', function() {
			var e = a('div')
			return (
				(e.innerHTML = '<svg/>'),
				'http://www.w3.org/2000/svg' ==
					('undefined' != typeof SVGRect &&
						e.firstChild &&
						e.firstChild.namespaceURI)
			)
		})
	var z = a('input'),
		A = 'autocomplete autofocus list placeholder max min multiple pattern required step'.split(
			' '
		),
		N = {}
	Modernizr.input = (function(t) {
		for (var n = 0, r = t.length; r > n; n++) N[t[n]] = !!(t[n] in z)
		return (
			N.list && (N.list = !(!a('datalist') || !e.HTMLDataListElement)), N
		)
	})(A)
	var P = 'search tel url email datetime date month week time datetime-local number range color'.split(
			' '
		),
		M = {}
	Modernizr.inputtypes = (function(e) {
		for (var r, o, i, a = e.length, s = '1)', l = 0; a > l; l++)
			z.setAttribute('type', (r = e[l])),
				(i = 'text' !== z.type && 'style' in z),
				i &&
					((z.value = s),
					(z.style.cssText = 'position:absolute;visibility:hidden;'),
					/^range$/.test(r) && z.style.WebkitAppearance !== n
						? (C.appendChild(z),
						  (o = t.defaultView),
						  (i =
								o.getComputedStyle &&
								'textfield' !==
									o.getComputedStyle(z, null)
										.WebkitAppearance &&
								0 !== z.offsetHeight),
						  C.removeChild(z))
						: /^(search|tel)$/.test(r) ||
						  (i = /^(url|email)$/.test(r)
								? z.checkValidity && z.checkValidity() === !1
								: z.value != s)),
				(M[e[l]] = !!i)
		return M
	})(P)
	var R = 'CSS' in e && 'supports' in e.CSS,
		j = 'supportsCSS' in e
	Modernizr.addTest('supports', R || j)
	var F = (function() {
		var t = e.matchMedia || e.msMatchMedia
		return t
			? function(e) {
					var n = t(e)
					return (n && n.matches) || !1
			  }
			: function(t) {
					var n = !1
					return (
						l(
							'@media ' +
								t +
								' { #modernizr { position: absolute; } }',
							function(t) {
								n =
									'absolute' ==
									(e.getComputedStyle
										? e.getComputedStyle(t, null)
										: t.currentStyle
									).position
							}
						),
						n
					)
			  }
	})()
	S.mq = F
	var O = (S.testStyles = l)
	O(
		'#modernizr{font:0/0 a}#modernizr:after{content:":)";visibility:hidden;font:7px/1 a}',
		function(e) {
			Modernizr.addTest('generatedcontent', e.offsetHeight >= 6)
		}
	)
	var L = (function() {
		var e = navigator.userAgent,
			t = e.match(/w(eb)?osbrowser/gi),
			n =
				e.match(/windows phone/gi) &&
				e.match(/iemobile\/([0-9])+/gi) &&
				parseFloat(RegExp.$1) >= 9
		return t || n
	})()
	L
		? Modernizr.addTest('fontface', !1)
		: O('@font-face {font-family:"font";src:url("https://")}', function(
				e,
				n
		  ) {
				var r = t.getElementById('smodernizr'),
					o = r.sheet || r.styleSheet,
					i = o
						? o.cssRules && o.cssRules[0]
							? o.cssRules[0].cssText
							: o.cssText || ''
						: '',
					a = /src/i.test(i) && 0 === i.indexOf(n.split(' ')[0])
				Modernizr.addTest('fontface', a)
		  })
	var B = S._config.usePrefixes ? T.split(' ') : []
	S._cssomPrefixes = B
	var $ = { elem: a('modernizr') }
	Modernizr._q.push(function() {
		delete $.elem
	})
	var D = { style: $.elem.style }
	Modernizr._q.unshift(function() {
		delete D.style
	})
	var H = (S.testProp = function(e, t, r) {
		return m([e], n, t, r)
	})
	Modernizr.addTest('textshadow', H('textShadow', '1px 1px')),
		(S.testAllProps = v),
		(S.testAllProps = y),
		Modernizr.addTest('cssanimations', y('animationName', 'a', !0)),
		Modernizr.addTest('borderimage', y('borderImage', 'url() 1', !0)),
		Modernizr.addTest('backgroundsize', y('backgroundSize', '100%', !0)),
		Modernizr.addTest('borderradius', y('borderRadius', '0px', !0)),
		(function() {
			Modernizr.addTest('csscolumns', function() {
				var e = !1,
					t = y('columnCount')
				try {
					;(e = !!t), e && (e = new Boolean(e))
				} catch (n) {}
				return e
			})
			for (
				var e,
					t,
					n = [
						'Width',
						'Span',
						'Fill',
						'Gap',
						'Rule',
						'RuleColor',
						'RuleStyle',
						'RuleWidth',
						'BreakBefore',
						'BreakAfter',
						'BreakInside'
					],
					r = 0;
				r < n.length;
				r++
			)
				(e = n[r].toLowerCase()),
					(t = y('column' + n[r])),
					('breakbefore' === e ||
						'breakafter' === e ||
						'breakinside' == e) &&
						(t = t || y(n[r])),
					Modernizr.addTest('csscolumns.' + e, t)
		})(),
		Modernizr.addTest('boxshadow', y('boxShadow', '1px 1px', !0)),
		Modernizr.addTest('flexbox', y('flexBasis', '1px', !0)),
		Modernizr.addTest('flexboxlegacy', y('boxDirection', 'reverse', !0)),
		Modernizr.addTest('csstransforms', function() {
			return (
				-1 === navigator.userAgent.indexOf('Android 2.') &&
				y('transform', 'scale(1)', !0)
			)
		}),
		Modernizr.addTest('csstransforms3d', function() {
			var e = !!y('perspective', '1px', !0),
				t = Modernizr._config.usePrefixes
			if (e && (!t || 'webkitPerspective' in C.style)) {
				var n,
					r = '#modernizr{width:0;height:0}'
				Modernizr.supports
					? (n = '@supports (perspective: 1px)')
					: ((n = '@media (transform-3d)'),
					  t && (n += ',(-webkit-transform-3d)')),
					(n +=
						'{#modernizr{width:7px;height:18px;margin:0;padding:0;border:0}}'),
					O(r + n, function(t) {
						e = 7 === t.offsetWidth && 18 === t.offsetHeight
					})
			}
			return e
		}),
		Modernizr.addTest('csstransitions', y('transition', 'all', !0)),
		o(),
		i(b),
		delete S.addTest,
		delete S.addAsyncTest
	for (var V = 0; V < Modernizr._q.length; V++) Modernizr._q[V]()
	e.Modernizr = Modernizr
})(window, document)
