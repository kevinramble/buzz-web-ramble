<?php

class Email
{
    // private static $name = 'BUZZ.';
    // private static $host = 'smtp.mailtrap.io';
    // private static $user = '604f75cf8e2fc8';
    // private static $pass = 'bdb55990f0c394';
    // private static $port = 2525;
    
    // private static $host = 'server10.servhost.com.br';
    // private static $user = 'contact@buzz-caribbean.com';
    // private static $pass = 'rx2qtnx9navw';
    // private static $protocol = 'tls';
    // private static $port = 587;

    // private static $name = 'Teste';
    // private static $host = '67.23.229.249';
    // private static $user = 'noreply@gatorade5v5.com';
    // private static $pass = 'Cdas231@009!';
    // private static $protocol = 'tls';
    // private static $port = 587;

    // ------------------------------------------------------------

    /**
     * Welcome.
     *
     * @param  string $nome
     * @param  string $email
     */
    public static function welcome($name = '', $email = '')
    {
        $title = 'Welcome to Buzz Caribbean';
        $subject =  '=?UTF-8?B?'.base64_encode($title).'?=';
        $titleContent = '<div style="font-family:Averta Light, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;">
                            <span class="title" style="font-family: Tusker Grotesk, sans-serif; font-weight: bold; color: #000000;">
                                Thank you for signing up!<br> Your account is now active.
                            </span>
                         </div>';
        $content = '<div style="font-family:Averta Light, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;">
                      <span class="description" style="font-size: 14px; font-weight: 300; line-height: 1.71; color: #bfbfbf;">
                          We r so happy you have signed up to our mailing list because your inbox is about to get hotter!<br>
                          Here at Buzz, we love to entertain, inspire and share. And so, we promise to only send you the latest updates, hottest events and special offers.<br>
                          We’ll be in touch soon. In the meanwhile, check out the latest buzz
                      </span>
                    </div>';
        
        $button = '<td align="center" bgcolor="#000000" role="presentation" style="border:none;border-radius:0;cursor:auto;height:40px;mso-padding-alt:0;background:#000000;" valign="middle">
                    <a href="https://buzz-caribbean.com/" rel="button" style="display:inline-block;width:256px;background:#000000;color:#ffffff;font-family:Tusker Grotesk, sans-serif;font-size:16px;font-weight:bold;line-height:120%;margin:0;text-decoration:none;text-transform:uppercase;padding:0;mso-padding-alt:0px;border-radius:0;" target="_blank">
                      <span>Start buzzing</span>
                    </a>
                  </td>';
        $body =  self::geraEmail($titleContent, $content);
        
        return self::envia($email, $subject, $body);
    }

    /**
     * Nova senha.
     *
     * @param  string $nome
     * @param  string $email
     * @param  string $token
     */
    public static function newPassword($name = '', $email = '', $password = '')
    {
        $title = 'New password';
        $subject =  '=?UTF-8?B?'.base64_encode($title).'?=';
        $titleContent = '<div style="font-family:Averta Light, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;">
                          <span class="title" style="font-family: Tusker Grotesk, sans-serif; font-weight: bold; color: #000000;">
                              Lost your password? No problem! Here is the new password '. $password . '
                          </span>
                      </div>';
        // $content =  '<div style="font-family:Averta Light, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;">
        //               <span class="description" style="font-size: 14px; font-weight: 300; line-height: 1.71; color: #bfbfbf;"> 
                          
        //               </span>
        //             </div>';
        $content = '';
        $button = '<td align="center" bgcolor="#000000" role="presentation" style="border:none;border-radius:0;cursor:auto;height:40px;mso-padding-alt:0;background:#000000;" valign="middle">
                    <a href="https://buzz-caribbean.com/" rel="button" style="display:inline-block;width:256px;background:#000000;color:#ffffff;font-family:Tusker Grotesk, sans-serif;font-size:16px;font-weight:bold;line-height:120%;margin:0;text-decoration:none;text-transform:uppercase;padding:0;mso-padding-alt:0px;border-radius:0;" target="_blank">
                      <span>Start buzzing</span>
                    </a>
                  </td>';
        $body =  self::geraEmail($titleContent, $content);
        return self::envia($email, $subject, $body);
    }

 
    /**
     * Edit senha.
     *
     * @param  string $nome
     * @param  string $email
     * @param  string $token
     */
    public static function editPassword($name = '', $email = '')
    {
        $title = 'Edit password';
        $subject =  '=?UTF-8?B?'.base64_encode($title).'?=';
        $titleContent = '<div style="font-family: Averta Light, sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #000000;">
                        <span class="title" style="font-family: Tusker Grotesk, sans-serif; font-weight: bold; color: #000000;">
                            You recently changed the password associated with your account.
                        </span>
                    </div>';
        $content = '';
        $button = '<td align="center" bgcolor="#000000" role="presentation" style="border:none;border-radius:0;cursor:auto;height:40px;mso-padding-alt:0;background:#000000;" valign="middle">
                      <a href="https://buzz-caribbean.com/" rel="button" style="display:inline-block;width:256px;background:#000000;color:#ffffff;font-family:Tusker Grotesk, sans-serif;font-size:16px;font-weight:bold;line-height:120%;margin:0;text-decoration:none;text-transform:uppercase;padding:0;mso-padding-alt:0px;border-radius:0;" target="_blank">
                        <span>Start buzzing</span>
                      </a>
                    </td>';
        $body = self::geraEmail($titleContent, $content);
        
        return self::envia($email, $subject, $body);
    }

 

    /**
     * Envia o email.
     *
     * @param  string $email
     * @param  string $subject
     * @param  string $body
     * @return boolean
     */
    public static function envia($email = '', $subject = '', $body = '')
    {

        require_once ABSPATH . WPINC . '/class-phpmailer.php'; 
        require_once ABSPATH . WPINC . '/class-smtp.php';
        $mail = new PHPMailer;
        
        $mail->CharSet = 'UTF-8';
        $mail->setFrom('contact@buzz-caribbean.com', 'Contact Buzz');

        $mail->Body = $body;
        $mail->Subject = $subject;

        $mail->IsHTML(true);

        //mailtrap, temp:
        $mail->isSMTP();
        // $mail->Host = 'smtp.mailtrap.io';
        // $mail->SMTPAuth = true;
        // $mail->Port = 2525;
        // $mail->Username = '604f75cf8e2fc8';
        // $mail->Password = 'bdb55990f0c394';
        
        $mail->Host = 'server10.servhost.com.br';
        $mail->SMTPAuth = true;
        $mail->Port = 587;
        $mail->Username = 'contact@buzz-caribbean.com';
        $mail->Password = 'rx2qtnx9navw';
        $mail->SMTPSecure = 'tls';
        
        $mail->ClearAllRecipients();

        $mail->addAddress($email, 'Buzz');
        
        
        if (!$mail->send()) {
            return false;
        }
        return true;
    }


    public static function geraEmail($title="", $content="") {
        $button = '';
        return $txtEmail = '
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
          <title>
            Buzz
          </title>
          <!--[if !mso]><!-- -->
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <!--<![endif]-->
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <style type="text/css">
            #outlook a { padding:0; }
            body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
            table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
            img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
            p { display:block;margin:13px 0; }
          </style>
          <!--[if mso]>
          <xml>
          <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
          </o:OfficeDocumentSettings>
          </xml>
          <![endif]-->
          <!--[if lte mso 11]>
          <style type="text/css">
            .outlook-group-fix { width:100% !important; }
          </style>
          <![endif]-->
  
  
      <style type="text/css">
        @media only screen and (min-width:480px) {
          .mj-column-px-470 { width:470px !important; max-width: 470px; }
  .mj-column-per-100 { width:100% !important; max-width: 100%; }
        }
      </style>
  
  
          <style type="text/css">
  
  
  
      @media only screen and (max-width:480px) {
        table.full-width-mobile { width: 100% !important; }
        td.full-width-mobile { width: auto !important; }
      }
  
          </style>
          <style type="text/css">@import url("https://emails.buzz-caribbean.com/fonts/fonts.css");.title {
      font-size: 32px;
      line-height: 1.5;
    }
  
    /* Utilities */
    .desktop-hide {
      display: none;
    }
  
    @media only screen and (max-device-width: 469px) {
      .title {
        font-size: 30px;
      }
  
      /* Utilities */
      .mobile-hide {
        display: none;
      }
  
      .desktop-hide {
        display: block;
      }
    }</style>
          <!-- Responsive -->
  <!--[if mso]>
      <style type="text/css">
        a,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        h6,
        body,
        table,
        td,
        div,
        span,
        p {
          font-family: Arial, Helvetica, sans-serif !important;
        }
      </style>
    <![endif]-->
        </head>
        <body style="background-color:#ffffff;">
  
  
        <div style="background-color:#ffffff;">
  
        <table align="center" class="header" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background-color: #000; width: 100%;" width="100%" bgcolor="#000">
          <tbody>
            <tr>
              <td>
  
  
        <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" class="header-outlook" style="width:600px;" width="600"
        >
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
  
  
        <div style="margin:0px auto;max-width:600px;">
  
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:0;text-align:center;">
                  <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
  
              <tr>
                <td
                   class="" width="600px"
                >
            <![endif]-->
  
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td>
  
  
        <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
        >
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
  
  
        <div style="margin:0px auto;max-width:600px;">
  
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:40px 0;text-align:center;">
                  <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
  
          <tr>
  
              <td
                 class="" style="width:470px;"
              >
            <![endif]-->
  
        <div class="mj-column-px-470 outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
          <!--[if mso | IE]>
          <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
            <tr>
  
                <td
                   style="vertical-align:top;width:470px;"
                >
                <![endif]-->
  
        <div class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
  
        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
  
              <tr>
                <td align="center" class="logo" style="font-size:0px;padding:0;word-break:break-word;">
  
        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
          <tbody>
            <tr>
              <td style="width:110px;">
  
        <img alt="Logo" height="auto" src="https://emails.buzz-caribbean.com/img/logo.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="110">
  
              </td>
            </tr>
          </tbody>
        </table>
  
                </td>
              </tr>
  
        </table>
  
        </div>
  
                <!--[if mso | IE]>
                </td>
  
            </tr>
            </table>
          <![endif]-->
        </div>
  
            <!--[if mso | IE]>
              </td>
  
          </tr>
  
                    </table>
                  <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
  
        </div>
  
  
        <!--[if mso | IE]>
            </td>
          </tr>
        </table>
        <![endif]-->
  
  
              </td>
            </tr>
          </tbody>
        </table>
  
            <!--[if mso | IE]>
                </td>
              </tr>
  
                    </table>
                  <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
  
        </div>
  
  
        <!--[if mso | IE]>
            </td>
          </tr>
        </table>
        <![endif]-->
  
  
              </td>
            </tr>
          </tbody>
        </table>
  
        <table align="center" class="hero" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td>
  
  
        <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" class="hero-outlook" style="width:600px;" width="600"
        >
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
  
  
        <div style="margin:0px auto;max-width:600px;">
  
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:0 20px;text-align:center;">
                  <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
  
              <tr>
                <td
                   class="" width="600px"
                >
            <![endif]-->
  
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td>
  
  
        <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:560px;" width="560"
        >
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
  
  
        <div style="margin:0px auto;max-width:560px;">
  
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:50px 0 40px;text-align:center;">
                  <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
  
          <tr>
  
              <td
                 class="" style="width:470px;"
              >
            <![endif]-->
  
        <div class="mj-column-px-470 outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
          <!--[if mso | IE]>
          <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
            <tr>
  
                <td
                   style="vertical-align:top;width:470px;"
                >
                <![endif]-->
  
        <div class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
  
        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
  
              <tr>
                <td align="center" style="font-size:0px;padding:0;word-break:break-word;">
  
        <div style="font-family:Averta Light, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;"><span class="title" style="font-family: Tusker Grotesk, sans-serif; font-weight: bold; color: #000000;">'.$title.'</span></div>
  
                </td>
              </tr>
  
              <tr>
                <td style="background:white;font-size:0px;word-break:break-word;">
  
  
      <!--[if mso | IE]>
  
          <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="50" style="vertical-align:top;height:50px;">
  
      <![endif]-->
  
        <div style="height:50px;">
          &nbsp;
        </div>
  
      <!--[if mso | IE]>
  
          </td></tr></table>
  
      <![endif]-->
  
  
                </td>
              </tr>
  
              <tr>
                <td align="center" style="font-size:0px;padding:0;word-break:break-word;">
  
        <div style="font-family:Averta Light, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;"><span class="description" style="font-size: 14px; font-weight: 300; line-height: 1.71; color: #bfbfbf;">'.$content.'</span></div>
  
                </td>
              </tr>
  
        </table>
  
        </div>
  
                <!--[if mso | IE]>
                </td>
  
            </tr>
            </table>
          <![endif]-->
        </div>
  
            <!--[if mso | IE]>
              </td>
  
          </tr>
  
                    </table>
                  <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
  
        </div>
  
  
        <!--[if mso | IE]>
            </td>
          </tr>
        </table>
        <![endif]-->
  
  
              </td>
            </tr>
          </tbody>
        </table>
  
            <!--[if mso | IE]>
                </td>
              </tr>
  
              <tr>
                <td
                   class="" width="600px"
                >
  
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:560px;" width="560"
        >
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
  
  
        <div style="margin:0px auto;max-width:560px;">
  
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:0 0 60px;text-align:center;">
                  <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
  
          <tr>
  
              <td
                 class="" style="width:470px;"
              >
            <![endif]-->
  
        <div class="mj-column-px-470 outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
          <!--[if mso | IE]>
          <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
            <tr>
  
                <td
                   style="vertical-align:top;width:470px;"
                >
                <![endif]-->
  
        <div class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
  
        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
  
              <tr>
                <td align="center" vertical-align="middle" style="font-size:0px;padding:0;word-break:break-word;">
  
       
  
                </td>
              </tr>
  
        </table>
  
        </div>
  
                <!--[if mso | IE]>
                </td>
  
            </tr>
            </table>
          <![endif]-->
        </div>
  
            <!--[if mso | IE]>
              </td>
  
          </tr>
  
                    </table>
                  <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
  
        </div>
  
  
        <!--[if mso | IE]>
            </td>
          </tr>
        </table>
  
                </td>
              </tr>
  
                    </table>
                  <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
  
        </div>
  
  
        <!--[if mso | IE]>
            </td>
          </tr>
        </table>
        <![endif]-->
  
  
              </td>
            </tr>
          </tbody>
        </table>
  
  
        <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" class="hero-outlook" style="width:600px;" width="600"
        >
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
  
  
        <div class="hero" style="background:#ececec;background-color:#ececec;margin:0px auto;max-width:600px;">
  
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ececec;background-color:#ececec;width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:30px 0;text-align:center;">
                  <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
  
              <tr>
                <td
                   class="" width="600px"
                >
            <![endif]-->
  
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td>
  
  
        <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
        >
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
  
  
        <div style="margin:0px auto;max-width:600px;">
  
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:0 30px;text-align:center;">
                  <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
  
          <tr>
  
              <td
                 class="" style="width:470px;"
              >
            <![endif]-->
  
        <div class="mj-column-px-470 outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
          <!--[if mso | IE]>
          <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
            <tr>
  
                <td
                   style="vertical-align:top;width:470px;"
                >
                <![endif]-->
  
        <div class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
  
        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
  
              <tr>
                <td align="center" style="font-size:0px;padding:0;word-break:break-word;">
  
        <div style="font-family:Averta Light, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;"><span class="description" style="font-size: 14px; font-weight: 300; line-height: 1.71; color: #bfbfbf;">If you have any problems, please contact us at<br>
              <a href="mailto:contact@buzz-caribbean.com" class="text-link" style="color: #4a90e2;">contact@buzz-caribbean.com</a></span></div>
  
                </td>
              </tr>
  
              <tr>
                <td style="background:#ececec;font-size:0px;word-break:break-word;">
  
  
      <!--[if mso | IE]>
  
          <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="20" style="vertical-align:top;height:20px;">
  
      <![endif]-->
  
        <div style="height:20px;">
          &nbsp;
        </div>
  
      <!--[if mso | IE]>
  
          </td></tr></table>
  
      <![endif]-->
  
  
                </td>
              </tr>
  
        </table>
  
        </div>
  
                <!--[if mso | IE]>
                </td>
  
            </tr>
            </table>
          <![endif]-->
        </div>
  
            <!--[if mso | IE]>
              </td>
  
          </tr>
  
                    </table>
                  <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
  
        </div>
  
  
        <!--[if mso | IE]>
            </td>
          </tr>
        </table>
        <![endif]-->
  
  
              </td>
            </tr>
          </tbody>
        </table>
  
            <!--[if mso | IE]>
                </td>
              </tr>
            <![endif]-->
          <!-- Desktop -->
            <!--[if mso | IE]>
              <tr>
                <td
                   class="" width="600px"
                >
            <![endif]-->
  
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td>
  
  
        <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
        >
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
  
  
        <div style="margin:0px auto;max-width:600px;">
  
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:0;text-align:center;">
                  <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
  
          <tr>
  
              <td
                 class="" style="width:470px;"
              >
            <![endif]-->
  
        <div class="mj-column-px-470 outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
          <!--[if mso | IE]>
          <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
            <tr>
  
                <td
                   style="vertical-align:top;width:470px;"
                >
                <![endif]-->
  
        <div class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
  
        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
  
              <tr>
                <td align="left" style="font-size:0px;padding:0;word-break:break-word;">
  
        <table cellpadding="0" cellspacing="0" width="100%" border="0" style="color:#000000;font-family:Averta Light, sans-serif;font-size:13px;line-height:80px;table-layout:auto;width:100%;border:none;">
          <tr class="mobile-hide">
              <td align="center">
                <table cellpadding="0" cellspacing="0" padding="0">
                  <tr>
                    <td width="30" class="social-space" style="padding: 0 5px;">
                      <a href="https://www.facebook.com/werbuzz" class="block" style="display: block;"><img class="block" width="30" src="https://emails.buzz-caribbean.com/img/facebook.png" alt="Facebook" style="display: block;"></a>
                    </td>
                    <td width="30" class="social-space" style="padding: 0 5px;">
                      <a href="https://www.instagram.com/buzzcaribbean/" class="block" style="display: block;"><img class="block" width="30" src="https://emails.buzz-caribbean.com/img/instagram.png" alt="Instagram" style="display: block;"></a>
                    </td>
                    <td width="30" class="social-space" style="padding: 0 5px;">
                      <a href="https://www.youtube.com/user/thebuzzcaribbean" class="block" style="display: block;"><img class="block" width="30" src="https://emails.buzz-caribbean.com/img/youtube.png" alt="Youtube" style="display: block;"></a>
                    </td>
                    <td width="30" class="social-space" style="padding: 0 5px;">
                      <a href="https://twitter.com/werbuzz" class="block" style="display: block;"><img class="block" width="30" src="https://emails.buzz-caribbean.com/img/twitter.png" alt="Twitter" style="display: block;"></a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
        </table>
  
                </td>
              </tr>
  
        </table>
  
        </div>
  
                <!--[if mso | IE]>
                </td>
  
            </tr>
            </table>
          <![endif]-->
        </div>
  
            <!--[if mso | IE]>
              </td>
  
          </tr>
  
                    </table>
                  <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
  
        </div>
  
  
        <!--[if mso | IE]>
            </td>
          </tr>
        </table>
        <![endif]-->
  
  
              </td>
            </tr>
          </tbody>
        </table>
  
            <!--[if mso | IE]>
                </td>
              </tr>
            <![endif]-->
          <!-- Mobile -->
            <!--[if mso | IE]>
              <tr>
                <td
                   class="" width="600px"
                >
            <![endif]-->
  
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td>
  
  
        <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
        >
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
  
  
        <div style="margin:0px auto;max-width:600px;">
  
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:0;text-align:center;">
                  <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
  
          <tr>
  
              <td
                 class="" style="width:470px;"
              >
            <![endif]-->
  
        <div class="mj-column-px-470 outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
          <!--[if mso | IE]>
          <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
            <tr>
  
                <td
                   style="vertical-align:top;width:470px;"
                >
                <![endif]-->
  
        <div class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
  
        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
  
              <tr>
                <td align="center" style="font-size:0px;padding:0;word-break:break-word;">
  
        <table cellpadding="0" cellspacing="0" width="100%" border="0" style="color:#000000;font-family:Averta Light, sans-serif;font-size:13px;line-height:22px;table-layout:auto;width:100%;border:none;">
            <tr class="desktop-hide">
              <td height="20px"></td>
            </tr>
            <tr align="center">
              <td>
                <table>
                  <tr class="desktop-hide">
                    <td width="30" class="social-space" style="padding: 0 5px;">
                      <a href="https://www.facebook.com/werbuzz"><img width="30" src="https://emails.buzz-caribbean.com/img/facebook.png" alt="Facebook"></a>
                    </td>
                    <td width="30" class="social-space" style="padding: 0 5px;">
                      <a href="https://www.instagram.com/buzzcaribbean/"><img width="30" src="https://emails.buzz-caribbean.com/img/instagram.png" alt="Instagram"></a>
                    </td>
                    <td width="30" class="social-space" style="padding: 0 5px;">
                      <a href="https://www.youtube.com/user/thebuzzcaribbean"><img width="30" src="https://emails.buzz-caribbean.com/img/youtube.png" alt="Youtube"></a>
                    </td>
                    <td width="30" class="social-space" style="padding: 0 5px;">
                      <a href="https://twitter.com/werbuzz"><img width="30" src="https://emails.buzz-caribbean.com/img/twitter.png" alt="Twitter"></a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
        </table>
  
                </td>
              </tr>
  
        </table>
  
        </div>
  
                <!--[if mso | IE]>
                </td>
  
            </tr>
            </table>
          <![endif]-->
        </div>
  
            <!--[if mso | IE]>
              </td>
  
          </tr>
  
                    </table>
                  <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
  
        </div>
  
  
        <!--[if mso | IE]>
            </td>
          </tr>
        </table>
        <![endif]-->
  
  
              </td>
            </tr>
          </tbody>
        </table>
  
            <!--[if mso | IE]>
                </td>
              </tr>
  
                    </table>
                  <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
  
        </div>
  
  
        <!--[if mso | IE]>
            </td>
          </tr>
        </table>
        <![endif]-->
  
  
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td>
  
  
        <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
        >
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
  
  
        <div style="margin:0px auto;max-width:600px;">
  
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:0 20px;text-align:center;">
                  <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
  
              <tr>
                <td
                   class="" width="600px"
                >
            <![endif]-->
  
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
          <tbody>
            <tr>
              <td>
  
  
        <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:560px;" width="560"
        >
          <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
  
  
        <div style="margin:0px auto;max-width:560px;">
  
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
              <tr>
                <td style="direction:ltr;font-size:0px;padding:30px 0 50px;text-align:center;">
                  <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
  
          <tr>
  
              <td
                 class="" style="vertical-align:top;width:470px;"
              >
            <![endif]-->
  
        <div class="mj-column-px-470 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
  
        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
  
              <tr>
                <td align="center" style="font-size:0px;padding:0;word-break:break-word;">
  
        <div style="font-family:Averta Light, sans-serif;font-size:10px;font-weight:300;line-height:20px;text-align:center;color:#979797;"><p>
            You are receiving this message because you or your organization has been identified by us or our third-party partners as someone who would be interested in receiving information about BUZZ. If this is not the case, you can opt out
            of receiving future emails by clicking <a href="#" class="text-link" style="color: #4a90e2;">unsubscribe</a>. For more information about how we process data, please see our <a href="https://buzz-caribbean.com/privacy-policy/" class="text-link" style="color: #4a90e2;">Privacy Policy</a>.
          </p></div>
  
                </td>
              </tr>
  
        </table>
  
        </div>
  
            <!--[if mso | IE]>
              </td>
  
          </tr>
  
                    </table>
                  <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
  
        </div>
  
  
        <!--[if mso | IE]>
            </td>
          </tr>
        </table>
        <![endif]-->
  
  
              </td>
            </tr>
          </tbody>
        </table>
  
            <!--[if mso | IE]>
                </td>
              </tr>
  
                    </table>
                  <![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
  
        </div>
  
  
        <!--[if mso | IE]>
            </td>
          </tr>
        </table>
        <![endif]-->
  
  
              </td>
            </tr>
          </tbody>
        </table>
  
        </div>
  
        </body>
      </html>
  ';

    } 
}

