<?php



// WP-JSON SEARCH POST
add_action( 'rest_api_init', 'search_post' );

function search_post() {
	register_rest_route('wp/v2', 'app/search/', array(
		'methods' => 'GET',
		'callback' => 'app_search_post',
	));
}

function app_search_post($request){
	require_once('Jwt.php');
	
	global $wpdb;
	$favorit = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();


	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];
		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
	}

	$search = ( isset($request["s"]) ) ? sanitize_text_field($request["s"]) : false ;
	$search = (string)$search;

    if (get_country_header()) { 
        $country = get_slug_country();
    } else { 
        $objCountry = app_get_country_app();
        $country = $objCountry->data->country['code'];
    }
	switch ($country) {
		case '_jamaica': $country_name = 'Jamaica'; break;
		case '_stlucia': $country_name = 'St. Lucia'; break;
		case '_turksandcaicos': $country_name = 'Turks and Caicos'; break;
		//new countries to be added
		case 'BB': $varCountry = "_barbados" ; break;
		case 'BS': $varCountry = "_bahamas" ; break;
		case 'KY': $varCountry = "_cayman" ; break;
		case 'TT': $varCountry = "_trinidad" ; break;
		default: $country_name = ''; break;
	}

	if (@$country_name && @$country_name != '') { 
		$tax_query = array(
			array (
				'taxonomy' => 'countries',   // taxonomy name
				'field' => 'name',           // term_id, slug or name
				'terms' => $country_name
			)
		);
	} else {
		$tax_query = null;
	}	
 
	$args = array(
		's' => $search,
		'post_type' => array('post', 'app-post', 'offer', 'job', 'out'),
		'post_status' =>  'publish',
		'posts_per_page' => -1,
		'orderby'        => 'post_date',
		'order'          => 'DESC'
		//'tax_query' => $tax_query
	);
	

	$query = new WP_Query( $args );
	$posts = $query->posts;
	
	// $posts = $wpdb->get_results( "SELECT *
	// 								FROM $wpdb->posts LEFT JOIN wp_term_relationships ON $wpdb->posts.ID = wp_term_relationships.object_id
	// 							   WHERE (($wpdb->posts.post_title LIKE '%$search%') 
	// 										 OR ($wpdb->posts.post_content LIKE '%$search%')
	// 										 OR ($wpdb->posts.post_excerpt LIKE '%$search%'))
	// 								 AND $wpdb->posts.post_status =  'publish'
	// 								 AND ($wpdb->posts.post_type =   
	// 								 	  OR $wpdb->posts.post_type =  'app-post' 
	// 									  OR $wpdb->posts.post_type =  'offer' 
	// 									  OR $wpdb->posts.post_type =  'job'  
	// 									  OR $wpdb->posts.post_type =  'out') 	  
	// 							ORDER BY $wpdb->posts.post_date DESC");
	//$posts = get_posts($args);
	

	foreach($posts as $post) {
		$sub = get_the_terms( $post->ID, 'sub-category' );
		$cat = get_the_terms( $post->ID, 'category' );

		$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
		$color = getCor(@$cat[0]->name);

		$guid = get_url_base($post->ID);

		if(has_post_thumbnail($post->ID)){
            $image = get_the_post_thumbnail_url($post->ID);
        } else {
            $image = catch_that_image($post->ID);
		} 
		
		$thumb = '';
        if (!strpos($image, "nothing.png")) { 
            if ($image) { 
                $imagem = preg_replace("/([-]\d{3,4}x\d{3}[.])/", ".", $image);
                $thumbnail = ($imagem) ? explode(".", $imagem) : '';
                $ext = end($thumbnail);
                $thumb = str_replace(".".$ext,"-150x150.".$ext, $imagem);
            }
        } else {
            $image = "";
        }  
        
		if ((strtolower(@$cat[0]->name) == 'news') && (strtolower($post->post_type) == 'post')){ 
			$dadosNews[] = array(
				"ID" => $post->ID,
				"post_date" => $post->post_date,
				"post_date_gmt" => $post->post_date_gmt,
				"postTitle" => $post->post_title,
				"type" => $post->post_type,
				"title" => $post->post_title,
				"favorit" => $favorit,
				"post_excerpt" => $post->post_excerpt,
				"post_status" => $post->post_status,
				"post_modified" => $post->post_modified,
				"guid" => $guid,
				"post_type" => $post->post_type,
				"categoty" => @$cat[0]->name,
				"subcategory" => @$sub[0]->name,
				"image" => $imagem, 
				"thumb" => $thumb, 
				"color" => $color 
			);	
		} 

		if ((strtolower(@$cat[0]->name) == 'hot') && (strtolower($post->post_type) == 'post')){ 
			$dadosHot[] = array(
				"ID" => $post->ID,
				"post_date" => $post->post_date,
				"post_date_gmt" => $post->post_date_gmt,
				"postTitle" => $post->post_title,
				"type" => $post->post_type,
				"title" => $post->post_title,
				"favorit" => $favorit,
				"post_excerpt" => $post->post_excerpt,
				"post_status" => $post->post_status,
				"post_modified" => $post->post_modified,
				"guid" => $guid,
				"post_type" => $post->post_type,
				"categoty" => @$cat[0]->name,
				"subcategory" => @$sub[0]->name,
				"image" => $imagem, 
				"thumb" => $thumb, 
				"color" => $color 
			);	
		} 

		if ((strtolower(@$cat[0]->name) == 'life') && (strtolower($post->post_type) == 'post')){ 
			$dadosLife[] = array(
				"ID" => $post->ID,
				"post_date" => $post->post_date,
				"post_date_gmt" => $post->post_date_gmt,
				"postTitle" => $post->post_title,
				"type" => $post->post_type,
				"title" => $post->post_title,
				"favorit" => $favorit,
				"post_excerpt" => $post->post_excerpt,
				"post_status" => $post->post_status,
				"post_modified" => $post->post_modified,
				"guid" => $guid,
				"post_type" => $post->post_type,
				"categoty" => @$cat[0]->name,
				"subcategory" => @$sub[0]->name,
				"image" => $imagem, 
				"thumb" => $thumb, 
				"color" => $color
			);	
		} 

		if (strtolower($post->post_type) == 'offer'){ 
			
			$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
			if ($postmeta) {
				$fullprice = '';
				for ($i=0; $i< count($postmeta); $i++) {
					
					$fullprice = ($postmeta[$i]->meta_key == 'offer_fullprice') ? $postmeta[$i]->meta_value : $fullprice;
					$discount = ($postmeta[$i]->meta_key == 'offer_discount') ? $postmeta[$i]->meta_value : $discount;
					$description = ($postmeta[$i]->meta_key == 'offer_description') ? $postmeta[$i]->meta_value : $description;
					$price = ($postmeta[$i]->meta_key == 'offer_price') ? $postmeta[$i]->meta_value : $price;
					$company = ($postmeta[$i]->meta_key == 'offer_company') ? $postmeta[$i]->meta_value : $company;
					$place = ($postmeta[$i]->meta_key == 'offer_place') ? $postmeta[$i]->meta_value : $place;
					$offer_expiration = ($postmeta[$i]->meta_key == 'offer_expiration') ? $postmeta[$i]->meta_value : $offer_expiration;
					$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
					$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
					
				}
			}
			$sub = get_the_terms( $post->ID, 'sub-category' );
			$cat = get_the_terms( $post->ID, 'category' );
			$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
			$offer_taked = (@in_array($post->ID, $arrOffers_user));
			$guid = get_url_base( $post->ID);
			$category = get_the_terms( $post->ID, 'offer-category');
			$place = get_the_terms( $post->ID, 'countries');

			//data expirada
			if (strtotime(date("Y-m-d")) <= strtotime(date("Y-m-d", strtotime($offer_expiration)))) {
				
				$date1= date("Y-m-d", strtotime($offer_expiration));
				$date2= date("Y-m-d") ;
				
				$data1 = new DateTime( $date1 );
				$data2 = new DateTime( $date2 );

				$intervalo = $data2->diff( $data1 );
				
				$expired = 'end within '. $intervalo->d .' days';
			} else {
				$expired = 'deal expired';
			}
			$dadosOffer[] = array(
				"ID" => $post->ID,
				"post_date" => $post->post_date,
				"post_date_gmt" => $post->post_date_gmt,
				"postTitle" => $post->post_title,
				"type" => $post->post_type,
				"title" => $post->post_title,
				"favorit" => $favorit,
				"post_excerpt" => $post->post_excerpt,
				"post_status" => $post->post_status,
				"post_modified" => $post->post_modified,
				"guid" => str_replace("&webview=true", "",$guid),
				"post_type" => $post->post_type,
				"categoty" => @$category[0]->name,
				"subcategory" => @$sub[0]->name,
				"color" => $color,
				"guid" => $guid,
				"fullprice" => $fullprice,
				"discount" => $discount,
				"price" => $price,
				"company" => $company,
				"place" => @$place[0]->name,
				"expired" => $expired,
				"offer_expiration" => $offer_expiration,
				"thumb" => @$thumb[0]->guid,
				"offer_redeemed" => $offer_taked 
			);	
		} 
		
		
		if (strtolower($post->post_type) == 'job'){ 
			
			$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
			if ($postmeta) {
				$fullprice = '';
				for ($i=0; $i< count($postmeta); $i++) {
					//$description = ($postmeta[$i]->meta_key == 'job_description') ? $postmeta[$i]->meta_value : $description;
					$position = ($postmeta[$i]->meta_key == 'job_position') ? $postmeta[$i]->meta_value : $position;
					$type = ($postmeta[$i]->meta_key == 'job_type') ? $postmeta[$i]->meta_value : $type;
					$company = ($postmeta[$i]->meta_key == 'job_company') ? $postmeta[$i]->meta_value : $company;
					$application = ($postmeta[$i]->meta_key == 'job_application') ? $postmeta[$i]->meta_value : $application;
					$company_email = ($postmeta[$i]->meta_key == 'job_company_email') ? $postmeta[$i]->meta_value : $company_email;
					$job_expiration = ($postmeta[$i]->meta_key == 'job_expiration') ? $postmeta[$i]->meta_value : $job_expiration;
					$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
					$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
				}
			}
			$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
			$job_applied = (@in_array($post->ID, $arrjobs_user));
			$guid = get_url_base( $post->ID);
			$place = get_the_terms( $post->ID, 'countries');

			$imgThumb = '';
			if  (@$thumb[0]->guid) {
				$thumbpartes = explode(".",@$thumb[0]->guid);
				$imgThumb = str_replace(".".end($thumbpartes), "-150x150.".end($thumbpartes), @$thumb[0]->guid);
			}

			//data expirada
			$expired = false;
			if (strtotime(date("Y-m-d")) <= strtotime(date("Y-m-d", strtotime($job_expiration)))) {
				
				$date1= date("Y-m-d", strtotime($job_expiration));
				$date2= date("Y-m-d") ;
				
				$data1 = new DateTime( $date1 );
				$data2 = new DateTime( $date2 );

				$intervalo = $data2->diff( $data1 );
				
				$expired = false;
			} else {
				$expired = true;
			}
			
			if (!$expired) {
				$dadosJob[] = array(
					"ID" => $post->ID,
					"post_date" => $post->post_date,
					"description" =>  $post->post_content,
					"post_title" => $post->post_title,
					"post_modified" => $post->post_modified,
					"guid" => $guid,
					"post_type" => $post->post_type,
					"favorit" => $favorit,
					"position" => $position,
					"guid" => $guid,
					"type" => $type,
					"application" => $application,
					"company" => $company,
					"company_email" => $company_email,
					"place" => @$place[0]->name,
					"job_expiration" => $job_expiration,
					"image" => @$thumb[0]->guid,
					"thumb" => $imgThumb,
					"job_applied" => $job_applied);
			}
	
		} 

		if (strtolower($post->post_type) == 'out'){ 
			
			$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
			if ($postmeta) {
				$fullprice = '';
				for ($i=0; $i< count($postmeta); $i++) {
			
				
					$mapurl = ($postmeta[$i]->meta_key == 'out_mapurl') ? $postmeta[$i]->meta_value : $mapurl;
					$address = ($postmeta[$i]->meta_key == 'out_address') ? $postmeta[$i]->meta_value : $address;
					$description = ($postmeta[$i]->meta_key == 'out_description') ? $postmeta[$i]->meta_value : $description;
					$place = ($postmeta[$i]->meta_key == 'out_place') ? $postmeta[$i]->meta_value : $place;
					$date = ($postmeta[$i]->meta_key == 'out_date') ? $postmeta[$i]->meta_value : $date;
					$time = ($postmeta[$i]->meta_key == 'out_time') ? $postmeta[$i]->meta_value : $time;
					$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
					$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
					
				}
			}
			$category = get_the_terms( $post->ID, 'out-category');
			$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
			$guid = get_url_base( $post->ID);
			$place = get_the_terms( $post->ID, 'countries');

			
			$dadosOut[] = array(
				"ID" => $post->ID,
				"post_date" => $post->post_date,
				"post_date_gmt" => $post->post_date_gmt,
				"post_title" => $post->post_title,
				"type" => $post->post_type,
				"title" => $post->post_title,
				"favorit" => $favorit,
				"post_excerpt" => $post->post_excerpt,
				"post_status" => $post->post_status,
				"post_modified" => $post->post_modified,
				"post_type" => $post->post_type,
				"categoty" => @$category[0]->name,
				//"subcategory" => @$sub[0]->name,
				"color" => $color,
				"guid" => $guid,
				"place" => @$place[0]->name,
				"thumb" => @$thumb[0]->guid,
				
			);	
		} 
	}

	$dados = array(
		"hot" => $dadosHot,
		"news" => $dadosNews,
		"life" => $dadosLife,
		"outs" => $dadosOut,
		"jobs" => $dadosJob,
		"offers" => $dadosOffer,
	);
	
	$obj = new \stdClass();
	$obj->categories = $dados;
	
	$response = new WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);
	return $response;

}	

// WP-JSON SEARCH POST
add_action( 'rest_api_init', 'search_autocomplete' );

function search_autocomplete() {
	register_rest_route('wp/v2', 'app/autocomplete/', array(
		'methods' => 'GET',
		'callback' => 'app_search_autocomplete',
	));
}

function app_search_autocomplete($request){
	require_once('Jwt.php');
	
	global $wpdb;
	$favorit = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();
	
	$search = ( isset($request["s"]) ) ? sanitize_text_field($request["s"]) : false ;
	$search = (string)$search;

	$posts = $wpdb->get_results( "SELECT *
									FROM $wpdb->posts
								   WHERE ($wpdb->posts.post_title LIKE '%$search%') 
									 AND $wpdb->posts.post_status =  'publish'
									 AND ($wpdb->posts.post_type =  'post' OR $wpdb->posts.post_type =  'app-post' OR $wpdb->posts.post_type =  'offer') 
								ORDER BY $wpdb->posts.post_date DESC");
	
	foreach ($posts as $post) {
		$dados[] = array("title" => $post->post_title);	
	}

	
	$obj = new \stdClass();
	$obj->posts = $dados;
	
	$response = new WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);
	return $response;

}	