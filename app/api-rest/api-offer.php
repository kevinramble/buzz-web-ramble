<?php


// WP-JSON POSTS  OFFERS
add_action( 'rest_api_init', 'posts_offer' );

function posts_offer() {
	register_rest_route('wp/v2', 'app/posts/offers/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_offers',
	));
}

function app_posts_offers($request){
	
	require_once('Jwt.php');

	global $wpdb;
	$favorit = $offer_taked = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];
		
		$offers_user = $wpdb->get_results( "SELECT meta_value as offers_user FROM wp_usermeta WHERE meta_key = 'redeemedoffers' AND user_id = ".$user->uid );
		$arrOffers_user = unserialize($offers_user[0]->offers_user);

		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
		
	}

    if (get_country_header()) { 
        $country = get_slug_country();
    } else { 
        $objCountry = app_get_country_app();
        $country = $objCountry->data->country['code'];
    }
	if($country == '_jamaica'){
		$countryQuery = array(
			'key' => 'offer_country',
			'value' => 'Jamaica',
			'compare' => '='
		);
	} elseif($country == '_stlucia'){
		$countryQuery = array(
			'key' => 'offer_country',
			'value' => 'St. Lucia',
			'compare' => '='
		);
	} elseif($country == '_turksandcaicos'){
		$countryQuery = array(
			'key' => 'offer_country',
			'value' => 'Turks and Caicos',
			'compare' => '='
		);
	} elseif($country == '_barbados'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Barbados',
			'compare' => '='
		);
	} elseif($country == '_bahamas'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Bahamas',
			'compare' => '='
		);
	} elseif($country == '_cayman'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Cayman',
			'compare' => '='
		);
	} else {
		$countryQuery = '';
	}

	$args = array(
		'post_type' => "offer",
		'post_status' =>  'publish',
		'nopaging ' => true,
		'posts_per_page' => -1,
		'orderby'        => 'post_date',
		'order'          => 'DESC'
		/*'meta_query' => array('relation' => 'AND', 
							  array('key' => 'offer_expiration', 
									'value' => date('Y-m-d'), 
									'compare' => '>='), 
							$countryQuery)*/
	);

	$posts = get_posts($args);
	
	if (empty($posts)) {
		return new WP_Error( 'empty_offer', 'there is no post in this category', array('status' => 404) );
	}
	//print_r($posts);die();

	$dados = null;

	foreach($posts as $post) {
	
		$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
		if ($postmeta) {
			
			$fullprice = $discount = $price = $code = $company = $place = $offer_expiration = $description = $_thumbnail_id = '';
			for ($i=0; $i< count($postmeta); $i++) {
				
				$fullprice = ($postmeta[$i]->meta_key == 'offer_fullprice') ? $postmeta[$i]->meta_value : $fullprice;
				$discount = ($postmeta[$i]->meta_key == 'offer_discount') ? $postmeta[$i]->meta_value : $discount;
				$description = ($postmeta[$i]->meta_key == 'offer_description') ? $postmeta[$i]->meta_value : $description;
				$price = ($postmeta[$i]->meta_key == 'offer_price') ? $postmeta[$i]->meta_value : $price;
				$company = ($postmeta[$i]->meta_key == 'offer_company') ? $postmeta[$i]->meta_value : $company;
				$place = ($postmeta[$i]->meta_key == 'offer_country') ? $postmeta[$i]->meta_value : $place;
				$offer_expiration = ($postmeta[$i]->meta_key == 'offer_expiration') ?  $postmeta[$i]->meta_value : $offer_expiration;
				$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
				$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
				
			}
		}
		
		$sub = get_the_terms( $post->ID, 'sub-category' );
		$cat = get_the_terms( $post->ID, 'category' );
		$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
		$offer_taked = (@in_array($post->ID, $arrOffers_user));
		$guid = get_url_base( $post->ID);
		$category = get_the_terms( $post->ID, 'offer-category');
		$date_expired = 0;
		
		//data expirada
		$date_expired = remainingTime($offer_expiration);
		if ($date_expired >= 0) {
			$expired =  'end within '. $date_expired .' days';
		} else {
			$expired = 'deal expired';
		}
		
		$dados[] = array(
				"ID" => $post->ID,
				"post_date" => $post->post_date,
				"description" => $description,
				"post_title" => $post->post_title,
				"post_modified" => $post->post_modified,
				"guid" => $guid,
				"post_type" => $post->post_type,
				"category" => @$category[0]->name,
				"sub_category" => @$sub[0]->name,
				"favorit" => $favorit,
				"guid" => $guid,
				"fullprice" => $fullprice,
				"discount" => $discount,
				"price" => $price,
				"company" => $company,
				"place" => $place,
				"expired" => $expired,
				"offer_expiration" => $offer_expiration,
				"image" => @$thumb[0]->guid,
				"thumb" => @$thumb[0]->guid,
				"offer_redeemed" => $offer_taked);
				

	}

	$obj = new \stdClass();
	$obj->posts = $dados;
	
	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;

}	

// WP-JSON POSTS  OFFERS ID
add_action( 'rest_api_init', 'posts_offer_id' );

function posts_offer_id() {
	register_rest_route('wp/v2', 'app/posts/offer/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_offers_id',
	));
}

function app_posts_offers_id($request){
	// $teste = unserialize("a:5:{i:0;i:171;i:1;i:243;i:2;i:202;i:3;i:203;i:4;i:266;}");
	// echo "<pre>";print_r($teste);die();
	require_once('Jwt.php');

	global $wpdb;
	$favorit = $offer_taked = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];

		$offers_user = $wpdb->get_results( "SELECT meta_value as offers_user FROM wp_usermeta WHERE meta_key = 'redeemedoffers' AND user_id = ".$user->uid );
		$arrOffers_user = unserialize($offers_user[0]->offers_user);

		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
	}

	$args = array(
        'ID' => $request['post_id'],
		'post_type' => "offer",
	);

	$post =  get_post( $request['post_id'] );
	if (empty($post)) {
		return new WP_Error( 'empty_offer', 'there is no post in this category', array('status' => 404) );
	}
            
	$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
	if ($postmeta) {
		$fullprice = $discount = $price = $code = $company = $place = $offer_expiration = $description = $_thumbnail_id = '';
		for ($i=0; $i< count($postmeta); $i++) {
			if($postmeta[$i]->meta_key == 'offer_shortcode') {
				$poll =  ($postmeta[$i]->meta_value) ? $postmeta[$i]->meta_value : '';
				$totalpoll_id =  str_replace('"]','',str_replace('[totalpoll id="','',$poll));
				
				if ($totalpoll_id) {
					$totallpoll = get_post($totalpoll_id);
					$content = json_decode($totallpoll->post_content);
					$enquete = $content->questions[0];
				}
			}
			$fullprice = ($postmeta[$i]->meta_key == 'offer_fullprice') ? $postmeta[$i]->meta_value : $fullprice;
			$discount = ($postmeta[$i]->meta_key == 'offer_discount') ? $postmeta[$i]->meta_value : $discount;
			$price = ($postmeta[$i]->meta_key == 'offer_price') ? $postmeta[$i]->meta_value : $price;
			$code = ($postmeta[$i]->meta_key == 'offer_code') ? $postmeta[$i]->meta_value : $code;
			$company = ($postmeta[$i]->meta_key == 'offer_company') ? $postmeta[$i]->meta_value : $company;
			$place = ($postmeta[$i]->meta_key == 'offer_country') ? $postmeta[$i]->meta_value : $place;
			$offer_expiration = ($postmeta[$i]->meta_key == 'offer_expiration') ? $postmeta[$i]->meta_value : $offer_expiration;
			$description = ($postmeta[$i]->meta_key == 'offer_description') ? $postmeta[$i]->meta_value : $description;
			$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
			$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
			
		}
	}

	
	$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
	$offer_taked = (@in_array($post->ID, $arrOffers_user));
	$guid = get_url_base($post->ID);

	$category = get_the_terms( $post->ID, 'offer-category');

	//data expirada
	$date_expired = remainingTime($offer_expiration);

	if ($date_expired >= 0) {
		$expired =  'end within '. $date_expired .' days';
	} else {
		$expired = 'deal expired';
	}
	
	$dados = array(
		"ID" => $post->ID,
		"post_date" => $post->post_date,
		"description" => $description,
		"post_title" => $post->post_title,
		"post_modified" => $post->post_modified,
		"guid" => $post->guid,
		"post_type" => $post->post_type,
		"category" => @$category[0]->name,
		"favorit" => $favorit,
		"poll_id" => $totalpoll_id,
		"poll" => $enquete,
		"fullprice" => $fullprice,
		"discount" => $discount,
		"price" => $price,
		"code" => $code,
		"company" => $company,
		"place" => $place,
		"expired" => $expired,
		"offer_expiration" => $offer_expiration,
		"image" => @$thumb[0]->guid,
		"thumb" => @$thumb[0]->guid,
		"offer_redeemed" => $offer_taked
	);

	$obj = new \stdClass();
	$obj->posts = $dados;
	
	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;

}	


// WP-JSON VOTAR
add_action( 'rest_api_init', 'vote_add' );

function vote_add() {
	register_rest_route('wp/v2', 'app/posts/offer-vote/', array(
		'methods' => 'POST',
		'callback' => 'app_vote',
	));
}

function app_vote($request){
	require_once('Jwt.php');
	global $wpdb;
	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		}
	} else {
		$response['code'] = 401;
		$response['message'] = __( 'Invalid Token', "wp-rest-user");
		return new WP_REST_Response($response, 401);
	} 
	
	$user = $tokenValido['user'];

	$choice = $request['choice'];
	$pollId = $request['pollID'];
	$offerID = $request['offerID'];
	$detail = json_encode(array("choices" => array($request['choice_label'])));
	$arr = array($offerID);
	$meta_value = serialize($arr);

	$userID = $user->uid;
	//$siteID = get_current_blog_id();

	global $wpdb;
	$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}usermeta WHERE meta_key = 'redeemedoffers' AND user_id = ". $userID , OBJECT);

	if($results){
		
		$id = $results[0]->umeta_id;
		$meta_value_new = unserialize($results[0]->meta_value);
		$meta_value_new[] = $offerID;
		
		$table_name = $wpdb->prefix . 'usermeta';
		$insert = $wpdb->update($table_name, 
						array('meta_value' => serialize($meta_value_new)),
						array('umeta_id' => $id)
					);
	} else {

		//não tem nada no banco, cria a primeira vez:
		$table_name = $wpdb->prefix . 'usermeta';
		$insert = $insert = $wpdb->insert($table_name, array(
			'user_id' => $userID,
			'meta_key' => 'redeemedoffers',
			'meta_value' => $meta_value
			
		));
		
	}

	if (!$insert) {
		$response['code'] = 401;
		$response['message'] = __( 'Error trying to take the offer.', "wp-rest-user");
		return new WP_REST_Response($response, 401);
	}

	$offer_code = esc_html(get_post_meta($offerID, 'offer_code', true));
	$offer_place = esc_html(get_post_meta($offerID, 'offer_place', true));
	$offer_expiration = esc_html(get_post_meta($offerID, 'offer_expiration', true));
	$offer_company = esc_html(get_post_meta($offerID, 'offer_company', true));
	$offer_company_url = esc_html(get_post_meta($offerID, 'offer_company_url', true));
	//return $offer_code;

	// Vereificar se o id da questao ja existe na tabela
	
	$results = $wpdb->get_results( "SELECT * FROM wp_totalpoll_votes WHERE choice_uid LIKE '".$choice."' ");
	
	//se existir 
	$table_name = $wpdb->prefix . 'totalpoll_votes';
	if ($results){
		$votes = ($results[0]->votes) + 1;
		$insert = $wpdb->query(" UPDATE ".$table_name." SET poll_id = ".$pollId. ",
															 votes = ".$votes .",
															 last_vote_at = NOW()
													   WHERE choice_uid LIKE '%".$choice."%' " );		 
			
	} else {

		$insert = $wpdb->insert($table_name, 
			array('poll_id' => $pollId,
				  'votes' => 1,
				  'last_vote_at' => date("Y-m-d H:m:i"),
				  'choice_uid' => $choice));
	}

	if ($insert) { 

		//gerar log
		$table_name = $wpdb->prefix . 'totalpoll_log';
		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		$insert = $wpdb->insert($table_name, 
								array(
									'ip' =>$ip,
									'useragent' => $_SERVER['HTTP_USER_AGENT'],
									'user_id' => $userID,
									'poll_id' => $pollId,
									'choices' => json_encode(array($choice)),
									'action' => "vote",
									'status' => "accepted",
									'details' => $detail,
									'date' => date("Y-m-d H:m:i")));

		favorite_offer($offerID, $userID);
		
		$dados = array("offer_code" => $offer_code,
						"offer_place" => $offer_place,
						"offer_expiration" => $offer_expiration,
						"offer_company" => $offer_company,
						"offer_company_url" => $offer_company_url);
		$obj = new \stdClass();
		$obj->posts = $dados;
		
		$response = new \WP_REST_Response();
		$response->set_data($obj);
		$response->set_status(200);
		
		return $response;	
	} else {
		$response['code'] = 401;
		$response['message'] = __( 'Error trying to take the offer.', "wp-rest-user");
		return new WP_REST_Response($response, 401);
	}

}	


// WP-JSON SEARCH POST
add_action( 'rest_api_init', 'search_offer' );

function search_offer() {
	register_rest_route('wp/v2', 'app/search-offer/', array(
		'methods' => 'GET',
		'callback' => 'app_search_offer',
	));
}

function app_search_offer($request){
	require_once('Jwt.php');
	
	global $wpdb;
	$favorit = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();


	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];
		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
	}

    if (get_country_header()) { 
        $country = get_slug_country();
    } else { 
        $objCountry = app_get_country_app();
        $country = $objCountry->data->country['code'];
    }
	if($country == '_jamaica'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Jamaica',
			'compare' => '='
		);
	} elseif($country == '_stlucia'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'St. Lucia',
			'compare' => '='
		);
	} elseif($country == '_turksandcaicos'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Turks and Caicos',
			'compare' => '='
		);
	} else {
		$countryQuery = '';
	}

	$args = array(
		'post_type' => 'offer',
		'tax_query' => array(
			array (
				'taxonomy' => 'offer-category',   // taxonomy name
				'field' => 'term_id',           // term_id, slug or name
				'terms' => $request['category_id'] 
			)
		),
		'post_status' =>  'publish',
		'nopaging ' => true,
		'posts_per_page' => -1,
		'orderby'        => 'post_date',
		'order'          => 'DESC',
		'meta_query'	 => $countryQuery 
	);
	
	$posts = get_posts($args);
	if (empty($posts)) {
		return new WP_Error( 'empty_sub-category', 'there is no post in this category', array('status' => 404) );
	}


	// $search = ( isset($request["s"]) ) ? sanitize_text_field($request["s"]) : false ;
	// $search = (string)$search;

	// $posts = $wpdb->get_results( "SELECT *
	// 								FROM $wpdb->posts
	// 							   WHERE (($wpdb->posts.post_title LIKE '%$search%') 
	// 								  OR ($wpdb->posts.post_content LIKE '%$search%')
	// 								  OR ($wpdb->posts.post_excerpt LIKE '%$search%'))
	// 								 AND $wpdb->posts.post_status =  'publish'
	// 								 AND $wpdb->posts.post_type =  'offer'
	// 							ORDER BY $wpdb->posts.post_date DESC");
	
	if ($posts) { 
		foreach ($posts as $post) {
			$sub = get_the_terms( $post->ID, 'sub-category' );
			$cat = get_the_terms( $post->ID, 'category' );

			$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
			$color = getCor(@$cat[0]->name);

			$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
	
			if ($postmeta) {
				$fullprice = '';
				for ($i=0; $i< count($postmeta); $i++) {
					if($postmeta[$i]->meta_key == 'offer_shortcode') {
						$poll =  ($postmeta[$i]->meta_value) ? $postmeta[$i]->meta_value : '';
						$totalpoll_id =  str_replace('"]','',str_replace('[totalpoll id="','',$poll));
						
						if ($totalpoll_id) {
							$totallpoll = get_post($totalpoll_id);
							$content = json_decode($totallpoll->post_content);
							$enquete = $content->questions[0];
						}
					}
			
					$fullprice = ($postmeta[$i]->meta_key == 'offer_fullprice') ? $postmeta[$i]->meta_value : $fullprice;
					$discount = ($postmeta[$i]->meta_key == 'offer_discount') ? $postmeta[$i]->meta_value : $discount;
					$price = ($postmeta[$i]->meta_key == 'offer_price') ? $postmeta[$i]->meta_value : $price;
					$code = ($postmeta[$i]->meta_key == 'offer_code') ? $postmeta[$i]->meta_value : $code;
					$company = ($postmeta[$i]->meta_key == 'offer_company') ? $postmeta[$i]->meta_value : $company;
					$place = ($postmeta[$i]->meta_key == 'offer_country') ? $postmeta[$i]->meta_value : $place;
					$offer_expiration = ($postmeta[$i]->meta_key == 'offer_expiration') ? $postmeta[$i]->meta_value : $offer_expiration;
					$description = ($postmeta[$i]->meta_key == 'offer_description') ? $postmeta[$i]->meta_value : $description;
					$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
					$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
					
				}
			}

			$offer_taked = (@in_array($post->ID, $arrOffers_user));
			$guid = get_url_base($post->ID);

			$category = get_the_terms( $post->ID, 'offer-category');
			//$place = get_the_terms( $post->ID, 'location');

			//data expirada
			if (strtotime(date("Y-m-d")) <= strtotime(date("Y-m-d", strtotime($offer_expiration)))) {
				
				$date1= date("Y-m-d", strtotime($offer_expiration));
				$date2= date("Y-m-d") ;
				
				$data1 = new DateTime( $date1 );
				$data2 = new DateTime( $date2 );

				$intervalo = $data2->diff( $data1 );
				
				$expired = 'end within '. $intervalo->d .' days';
			} else {
				$expired = 'deal expired';
			}
			
			$dados[] = array(
				"ID" => $post->ID,
				"post_date" => $post->post_date,
				"description" => $description,
				"post_title" => $post->post_title,
				"post_modified" => $post->post_modified,
				"guid" => $post->guid,
				"post_type" => $post->post_type,
				"category" => @$category[0]->name,
				"favorit" => $favorit,
				"poll_id" => $totalpoll_id,
				"poll" => $enquete,
				"fullprice" => $fullprice,
				"discount" => $discount,
				"price" => $price,
				"code" => $code,
				"company" => $company,
				"place" => @$place,
				"expired" => $expired,
				"offer_expiration" => $offer_expiration,
				"thumb" => @$thumb[0]->guid,
				"offer_redeemed" => $offer_taked
			);
		}
	} else {
		$dados = null;
	}

	$obj = new \stdClass();
	$obj->posts = $dados;
	
	$response = new WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);
	return $response;

}	


function favorite_offer($p, $userID) {
	global $wpdb;
	$results = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$userID );
	
	if ($results) { 
		
		$favorites = unserialize($results[0]->favorites);
		$favoritesPost = $favorites[0]['posts'];
		$favoritesGroups = $favorites[0]['groups'][0]['posts'];
		
	} else {
		
		$favorites = array(array("site_id" => 1,
							"posts" => array(),
							"groups" => array(
								array("group_id" => 1,
										"site_id" => 1,
										"group_name" => "Default List",
										"posts" => array()	
										)
							)
				  ));
	

		
		
		$favoritesPost = $favorites[0]['posts'];
		$favoritesGroups = $favorites[0]['groups'][0]['posts'];
	}
	
	
	if (is_array($favoritesPost)) { 
		if (in_array($p, $favoritesPost)) {
			
			$msg = array('msg' => 'dislike');
			$key = @array_search($p, $favoritesPost);
			if($key!==false){
				unset($favoritesPost[$key]);
				unset($favoritesGroups[$key]);
			} 
		} else {
			$msg = array('msg' => "like");
			$favoritesPost[] = $p;
			$favoritesGroups[] = $p;
		}
		
	} else {
		$msg = array('msg' => "like");
		$favoritesPost = $p;
		$favoritesGroups = $p;
	}
	
	$favorites[0]['posts'] = $favoritesPost;
	$favorites[0]['groups'][0]['posts'] = $favoritesGroups;
	
	
	$result = $wpdb->query("SELECT * FROM wp_usermeta 
							WHERE meta_key = 'simplefavorites' 
								AND user_id=".$userID);
	if (!$result) { 
		
		add_user_meta($user->uid, 'simplefavorites', unserialize(serialize($favorites)));
	} else { 
		$results[0]->favorites = serialize($favorites);
		$wpdb->query("UPDATE wp_usermeta 
						SET meta_value= '".$results[0]->favorites."' 
						WHERE meta_key = 'simplefavorites' 
						AND user_id=".$userID);
	}	

	return true;

}