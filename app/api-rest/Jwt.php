<?php
//namespace App\Models;
//use App\Models\User;
//use Illuminate\Database\Eloquent\Model;

class Jwt {

    public $id;
    public $name;
    public $email;
    private $senha = "F#CW-]5++h23Tcap";

    public $jwt_string = '';

    /**
     * Método construtor.
     */
    public function __construct()
    {
    }

    /*
    * Gerar token e salvar no banco
    */
    public function generateCode($obj){
        
        $issuedAt = time();
        $expire = $issuedAt + (120 * 86400);
        
        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT'
        ];
        $header = json_encode($header);
        $header = base64_encode($header);

        $payload = [
            'iat'  => $issuedAt,            // timestamp de geracao do token
            'iss'  => 'http://'.$_SERVER['SERVER_NAME'].'/',  // dominio, pode ser usado para descartar tokens de outros dominios
            'exp'  => $expire,              // expiracao do token
            'nbf'  => $issuedAt - 1,
            'data' => ['name'  => $obj->user_login,
                        'uid'   => $obj->ID,
                       'email' => $obj->user_email]        // token nao eh valido Antes de
        ];

        
        $payload = json_encode($payload);
        $payload = base64_encode($payload);

        $signature = hash_hmac('sha256',"$header.$payload",$this->senha,true);
        $signature = base64_encode($signature);

        $this->jwt_string = "$header.$payload.$signature";
        return array("token" => $this->jwt_string, "expire_at" => date("m/d/Y H:i:s", $expire)) ;
    }


    /*
    * Gerar token e salvar no banco
    */
    public function encode(Jwt $obj){
        
        $issuedAt = time();
        $expire = $issuedAt + 1440;

        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT'
        ];
        $header = json_encode($header);
        $header = base64_encode($header);

        $payload = [
            'iat'  => $issuedAt,            // timestamp de geracao do token
            'iss'  => 'http://'.$_SERVER['SERVER_NAME'].'/',  // dominio, pode ser usado para descartar tokens de outros dominios
            'exp'  => $expire,              // expiracao do token
            'nbf'  => $issuedAt - 1,        // token nao eh valido Antes de
            'data' => ['name'  => $obj->name,
                    //    'uid'   => $obj->id,
                       'email' => $obj->email]
        ];
        $payload = json_encode($payload);
        $payload = base64_encode($payload);

        $signature = hash_hmac('sha256',"$header.$payload",$this->senha,true);
        $signature = base64_encode($signature);

        $this->jwt_string = "$signature";
        FabricaDAO::fabricar(static::className())->inserir($jwt_string, $obj->id, $expire, $issuedAt);
        return $this->jwt_string;
    }

    /*
    * Verificar se token é válido e se não expirou.
    * Se válido, continua o fluxo, se inválido ou expirado, atualiza o banco para active = 0 e redireciona para o login
    */
    public  function validateCode($token){
        
        //$tokenuser = User::where("token", "=", $token)->first();
       
        if ($token) {
           
            $part = explode(".",$token);
            $header = $part[0];
            $payload = $part[1];
            $signature = $part[2];
            
            
            $valid = hash_hmac('sha256',"$header.$payload",$this->senha,true);
            $valid = base64_encode($valid);

            if($signature == $valid){
                $payload = base64_decode($payload);
                $payload = json_decode($payload);
                $tokenuser = $payload->data;

                $timeatual = time();
                $timetoken = $payload->exp;

                if ($timetoken < $timeatual) {
                    $dados = array("callback" => false, "message" => "expired");
                } else {
                    $dados = array("callback" => true, "message" => "valid", "user" => $tokenuser);
                }
            }else{
                $dados = array("callback" => false, "message" => "Invalid Token" );
            }

        } else {
            $dados = array("callback" => false, "message" => "Invalid Token" );
        }
        return $dados;
    }

    /*
    * Gerar token para esqueci a senha
    */
    public function generateToken(Jwt $obj){
        
        $issuedAt = time();
        $expire = $issuedAt + 1440;
        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT'
        ];
        $header = json_encode($header);
        $header = base64_encode($header);

        $payload = [
            'iat'  => $issuedAt,            // timestamp de geracao do token
            'exp'  => $expire,              // expiracao do token
            'iss'  => 'http://'.$_SERVER['SERVER_NAME'].'/',  // dominio, pode ser usado para descartar tokens de outros dominios
            'nbf'  => $issuedAt - 1,        // token nao eh valido Antes de
            'data' => ['uid' => $obj->id]
        ];
        $payload = json_encode($payload);
        $payload = base64_encode($payload);

        $signature = hash_hmac('sha256',"$header.$payload",$this->senha,true);
        $signature = base64_encode($signature);

        $token = $signature;
        Usuario::mudarToken($obj->id, $token);
        return $token;
    }

    /*
    * Validar token do esqueci a senha
    * Se valido, usuario segue o fluxi indo para uma pagina onde ele informa a nova senha
    * Se invalido ou expirado, usuário deverá regerar novo token
    */
    public function validarToken($token){
        
        $token = base64_decode($token);
        $part = explode(".",$token);
        $header = $part[0];
        $payload = $part[1];
        $signature = $part[2];
        
        $valid = hash_hmac('sha256',"$header.$payload",$this->senha,true);
        $valid = base64_encode($valid);

        if($signature == $valid){
        $payload = base64_decode($payload);
        $payload = json_decode($payload);

            $timeatual = time();
            $timetoken = $payload->exp;

            if ($timetoken < $timeatual) {
                echo "expirou. precisa redefinir a senha novamente.";die();
            } else {
                echo "valido";die();
            }
        }else{
            echo "invalido";die();
        }
    }

    public function getTokenHeader(){
  
        $headers = self::getallheaders();
        $accessToken = $headers['Access-Token'];  
        if ( $accessToken) {
            return  $accessToken;
        }    

        return false;
    }

    
    function getallheaders() 
    { 
        $headers = array (); 
        foreach ($_SERVER as $name => $value) 
        { 
            if (substr($name, 0, 5) == 'HTTP_') 
            { 
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
            } 
        } 
        return $headers; 
    } 


}