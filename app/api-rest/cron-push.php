<?php 
add_action( 'rest_api_init', 'register_cron_push' );
/**
 * add device
 *
 * @param  WP_REST_Request $request Full details about the request.
 * @return array $args.
 **/

function register_cron_push() {
    register_rest_route('wp/v2', 'cron_push', array(
        'methods' => 'POST',
        'callback' => 'cron_push_notification',
    ));
}

function cron_push_notification($request){
    // Definindo a localidade;
    date_default_timezone_set('America/Chicago');

    $where = date('Y-m-d');  
    global $wpdb;  

    //one-week
    //consultar os usuários que completaram 1 semana sem registrar journals
    $sql = get_sql($where, 5);
    $users = $wpdb->get_results($sql);
    $playersID = $objMessage = null;
    if ($users) {
        for($x=0; $x < count($users); $x++) {
            $playersID[] = $users[$x]->deviceid;
        }
    }

    if ($playersID) { 
        if (count($playersID) > 0) { 
            $objMessage = array(
                'tipo' => 'no_activitys',
                'tempo' => 'one_week',
                'title' => '',
                'players' => $playersID,
                'mensagem' => "It’s been a while since you checked in. Don’t miss out on latest Buzz news, features and discounts");
            sendPushNotification($objMessage);
        }
    }

    
    // //three_weeks':
    // //consultar os usuários que completaram 3 semanas sem registrar journals
    $sql = get_sql($where, 30);
    $users = $wpdb->get_results($sql);
    $playersID = $objMessage = null;
    if ($users) {
        for($x=0; $x < count($users); $x++) {
            $playersID[] = $users[$x]->deviceid;
        }
    }
    
    if ($playersID) { 
        if (count($playersID) > 0) { 
            $objMessage = array(
                'tipo' => 'no_activitys',
                'tempo' => 'three_week',
                'title' => '',
                'players' => $playersID,
                'mensagem' => "Hey, where'd you go? Catch up on the latest Buzz");
            sendPushNotification($objMessage);
        }
    }

    print_r($objMessage);

}


function get_sql($where = '', $days = 0){
	 $txtSql = "SELECT user_id, 
                    DATE_ADD(DATE(updated_at), INTERVAL $days DAY) as date_created, 
                    deviceid, 
                    updated_at
            FROM cp_devices
           WHERE  DATE_ADD(DATE(updated_at), INTERVAL $days DAY) = '".$where."'
           AND user_id IN (SELECT user_id FROM wp_usermeta WHERE meta_key = 'alert_general' AND meta_value = 1) " ;

    return $txtSql;
}
?>