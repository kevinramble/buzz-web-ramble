<?php

// WP-JSON POSTS BY CATEGORIES
	add_action( 'rest_api_init', 'posts_explore' );

	function posts_explore() {
		register_rest_route('wp/v2', 'app/posts/explore/', array(
			'methods' => 'GET',
			'callback' => 'app_posts_explore',
		));
	}

	function app_posts_explore($request){
		require_once('Jwt.php');
		/*
		* 4 - NEWS
		* 3 - LIFE
		* 2 - HOT
		*/
		
		global $wpdb;
		$favorit = false; 

		//validar token
        $jwt = new Jwt();
        $token = $jwt->getTokenHeader();

		if ($token) { 
			$tokenValido = $jwt->validateCode($token);

			if (!$tokenValido['callback']) {
				$response['code'] = 401;
				$response['message'] = __( $tokenValido['message'], "wp-rest-user");
				return new WP_REST_Response($response, 401);
			} 

			$user = $tokenValido['user'];
			$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
			$favorites = unserialize($favorites[0]->favorites);
			$arrFavorites = $favorites[0]['posts']; 
		}
		require_once("inc/categorys/categorys-main.php");
		require_once("inc/categorys/categorys-trending.php");
		//require_once("inc/categorys/categorys-must-watch.php");
		require_once("inc/categorys/categorys-all-stories.php");
		require_once("inc/categorys/categorys-breaking-news.php");
		$dadosMustWatch = null;
		$all = array(
			"main" => $dadosMain,
			"trending_now" => $dadosTrending,
			"all_astories" => $dadosAllStories,
			"must_watch" => $dadosMustWatch,
			"breaking_news" => $dadosBreakingNews,
		);
		
		$obj = new \stdClass();
		$obj->posts = $all;
		
		$response = new WP_REST_Response();
		$response->set_data($obj);
		$response->set_status(200);
		return $response;
		
	}	


	//LIST CATEGORIES
	add_action( 'rest_api_init', 'categories_list' );

	function categories_list() {
		register_rest_route('wp/v2', 'app/categories-list/', array(
			'methods' => 'GET',
			'callback' => 'app_categories_list',
		));
	}

	function app_categories_list($request){
		require_once('Jwt.php');
		
		global $wpdb;

		$terms = get_terms(array('taxonomy' => 'sub-category', 'hide_empty' => 1));
		foreach ($terms as $term)  { 
		
			$all[] = array(
				"id" => $term->term_id,
				"name" => $term->name
			);
		}

		$obj = new \stdClass();
		$obj->categories = $all;
		
		$response = new WP_REST_Response();
		$response->set_data($obj);
		$response->set_status(200);
		return $response;
	
	}	

	
