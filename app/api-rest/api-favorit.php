<?php


// WP-JSON FAVOTIS LIST 
add_action( 'rest_api_init', 'favorite_list' );

function favorite_list() {
	register_rest_route('wp/v2', 'app/favorites/', array(
		'methods' => 'GET',
		'callback' => 'app_favorite_list',
	));
}

function app_favorite_list($request){
	require_once('Jwt.php');
	global $wpdb;
	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	$tokenValido = $jwt->validateCode($token);

	if (!$tokenValido['callback']) {
		$response['code'] = 401;
		$response['message'] = __( $tokenValido['message'], "wp-rest-user");
		return new WP_REST_Response($response, 401);
	} 
	
	$user = $tokenValido['user'];

	// Create the WP_User_Query object
	$results = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );

	$favorites = unserialize($results[0]->favorites);
	$favoritesPost = $favorites[0]['posts'];
	
	if ($favoritesPost) {
		foreach($favoritesPost as $chave => $valor) { 
			$arr .= $valor. ",";
		}
		$arrFavorits = substr($arr, 0, strlen($arr) - 1); 
	}
		
	$posts = $wpdb->get_results( "SELECT * FROM wp_posts WHERE ID IN (". $arrFavorits.")" );
	
	if ($posts) { 
		foreach ($posts as $post) {

			if ($post->post_type != 'offer') { 
				$sub = get_the_terms( $post->ID, 'sub-category' );
				$cat = get_the_terms( $post->ID, 'category' );
			} else {
				$cat = get_the_terms( $post->ID, 'offer-category');
				$place = get_the_terms( $post->ID, 'location');
			}

			$favorit = (@in_array($post->ID, $favoritesPost)) ? true : false;
			$color = getCor(@$cat[0]->name);

			if(has_post_thumbnail($post->ID)){
                $image = get_the_post_thumbnail_url($post->ID);
            } else {
                $image = catch_that_image($post->ID);
			}
			
			$thumb = '';
			if (!strpos($image, "nothing.png")) { 
				if ($image) { 
					$imagem = preg_replace("/([-]\d{3,4}x\d{3}[.])/", ".", $image);
					$thumbnail = ($imagem) ? explode(".", $imagem) : '';
					$ext = end($thumbnail);
					$thumb = str_replace(".".$ext,"-150x150.".$ext, $imagem);
				}
			} else {
				$image = "";
			}

			$guid = ($post->post_type == 'offer') ? str_replace("&webview=true", "", get_url_base($post->ID)) :  get_url_base($post->ID);

			if ($post->post_type != "offer") { 
				$dados[] = array(
					"ID" => $post->ID,
					"type" => $post->post_type,
					"post_type" => $post->post_type,
					"post_title" => $post->post_title,
					"postTitle" => $post->post_title,
					"image" => $image,
					"thumb" => $thumb,
					"authorID" => $post->post_author,
					"guid" => $guid,
					"favorit" => true,
					"category" => @$cat[0]->name,
					"subcategory" => @$sub[0]->name
				);	
			}
		}
	} else {
		$dados=null;
	}
	
	
	$obj = new \stdClass();
	$obj->posts = $dados;
		
	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;

}		


// WP-JSON FAVOTIS LIST 
add_action( 'rest_api_init', 'favorite_list_offer' );

function favorite_list_offer() {
	register_rest_route('wp/v2', 'app/favorites-offer/', array(
		'methods' => 'GET',
		'callback' => 'app_favorite_list_offer',
	));
}

function app_favorite_list_offer($request){
	require_once('Jwt.php');
	global $wpdb;
	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	$tokenValido = $jwt->validateCode($token);

	if (!$tokenValido['callback']) {
		$response['code'] = 401;
		$response['message'] = __( $tokenValido['message'], "wp-rest-user");
		return new WP_REST_Response($response, 401);
	} 
	
	$user = $tokenValido['user'];

	// Create the WP_User_Query object
	$results = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );

	$favorites = unserialize($results[0]->favorites);
	$favoritesPost = $favorites[0]['posts'];
	
	if ($favoritesPost) {
		foreach($favoritesPost as $chave => $valor) { 
			$arr .= $valor. ",";
		}
		$arrFavorits = substr($arr, 0, strlen($arr) - 1); 
	}
		
	$posts = $wpdb->get_results( "SELECT * FROM wp_posts WHERE ID IN (". $arrFavorits.")" );
	
	if ($posts) { 
		foreach ($posts as $post) {
			if ($post->post_type == "offer") { 
				$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
				if ($postmeta) {
					$fullprice = '';
					for ($i=0; $i< count($postmeta); $i++) {
						if($postmeta[$i]->meta_key == 'offer_shortcode') {
							$poll =  ($postmeta[$i]->meta_value) ? $postmeta[$i]->meta_value : '';
							$totalpoll_id =  str_replace('"]','',str_replace('[totalpoll id="','',$poll));
							
							if ($totalpoll_id) {
								$totallpoll = get_post($totalpoll_id);
								$content = json_decode($totallpoll->post_content);
								$enquete = $content->questions[0];
							}
						}
						$fullprice = ($postmeta[$i]->meta_key == 'offer_fullprice') ? $postmeta[$i]->meta_value : $fullprice;
						$discount = ($postmeta[$i]->meta_key == 'offer_discount') ? $postmeta[$i]->meta_value : $discount;
						$price = ($postmeta[$i]->meta_key == 'offer_price') ? $postmeta[$i]->meta_value : $price;
						$code = ($postmeta[$i]->meta_key == 'offer_code') ? $postmeta[$i]->meta_value : $code;
						$company = ($postmeta[$i]->meta_key == 'offer_company') ? $postmeta[$i]->meta_value : $company;
						$place = ($postmeta[$i]->meta_key == 'offer_place') ? $postmeta[$i]->meta_value : $place;
						$offer_expiration = ($postmeta[$i]->meta_key == 'offer_expiration') ? $postmeta[$i]->meta_value : $offer_expiration;
						$description = ($postmeta[$i]->meta_key == 'offer_description') ? $postmeta[$i]->meta_value : $description;
						$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
						$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
						
					}
				}

				
				$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
				$offer_taked = (@in_array($post->ID, $arrOffers_user));
				$guid = ($post->post_type == 'offer') ? str_replace("&webview=true", "", get_url_base($post->ID)) :  get_url_base($post->ID);

				$cat = get_the_terms( $post->ID, 'offer-category');
				$place = get_the_terms( $post->ID, 'location');

				//data expirada
				if (strtotime(date("Y-m-d")) <= strtotime(date("Y-m-d", strtotime($offer_expiration)))) {
					
					$date1= date("Y-m-d", strtotime($offer_expiration));
					$date2= date("Y-m-d") ;
					
					$data1 = new DateTime( $date1 );
					$data2 = new DateTime( $date2 );

					$intervalo = $data2->diff( $data1 );
					
					$expired = 'end within '. $intervalo->d .' days';
				} else {
					$expired = 'deal expired';
				}

			
				$dados[] = array(
					"ID" => $post->ID,
					"type" => $post->post_type,
					"post_type" => $post->post_type,
					"post_title" => $post->post_title,
					"postTitle" => $post->post_title,
					"image" => $image,
					"thumb" => $thumb,
					"authorID" => $post->post_author,
					"guid" => $guid,
					"favorit" => true,
					"category" => @$cat[0]->name,
					"subcategory" => @$sub[0]->name,
					"place" => @$place[0]->name,
					"expired" => $expired,
					"offer_expiration" => $offer_expiration,
					"image" => @$thumb[0]->guid,
					"thumb" => @$thumb[0]->guid,
					"offer_redeemed" => $offer_taked
						
				);	
			}
		}
	} else {
		$dados = null;
	}
	
	
	$obj = new \stdClass();
	$obj->posts = $dados;
		
	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;

}		





// WP-JSON FAVORITAR 
add_action( 'rest_api_init', 'favorite_add' );

function favorite_add() {
	register_rest_route('wp/v2', 'app/favorite/', array(
		'methods' => 'POST',
		'callback' => 'app_favorite',
	));
}

function app_favorite($request){
	require_once('Jwt.php');
	global $wpdb;
	
	//$teste = unserialize('a:1:{i:0;a:3:{s:7:"site_id";i:1;s:5:"posts";a:1:{i:0;i:302;}s:6:"groups";a:1:{i:0;a:4:{s:8:"group_id";i:1;s:7:"site_id";i:1;s:10:"group_name";s:12:"Default List";s:5:"posts";a:1:{i:0;i:302;}}}}}');
	//$teste = unserialize('a:1:{i:0;a:3:{s:7:"site_id";i:1;s:5:"posts";a:2:{i:0;i:73;i:1;s:2:"21";}s:6:"groups";a:1:{i:0;a:4:{s:8:"group_id";i:1;s:7:"site_id";i:1;s:10:"group_name";s:12:"Default List";s:5:"posts";a:2:{i:0;i:73;i:1;s:2:"21";}}}}}');
	//print_r($teste);die();
	
	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	$tokenValido = $jwt->validateCode($token);

	if (!$tokenValido['callback']) {
		$response['code'] = 401;
		$response['message'] = __( $tokenValido['message'], "wp-rest-user");
		return new WP_REST_Response($response, 401);
	} 
	
	$user = $tokenValido['user'];


	$url = urldecode($request['url']);
	$query = (parse_url($url, PHP_URL_QUERY));
	$parsestr = parse_str($query);

	if (!$p) {
		$partes = explode( "/", $url);
		if ( end($partes) == "?webview=true") { 
			$slug = $partes[count($partes) - 2];
		} else {
			$slug = end($partes);
		}
	
		$result = $wpdb->get_results( "SELECT *  FROM wp_posts WHERE post_name LIKE '%".$slug."%' ");
	
		if ($result) {
			$p = $result[0]->ID;
		} else {
			$response['code'] = 401;
			$response['message'] = __( 'Article not found', "wp-rest-post");
			return new WP_REST_Response($response, 401);
		}
	}

	// Create the WP_User_Query object
	$results = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
	if ($results) { 
		
		$favorites = unserialize($results[0]->favorites);
		$favoritesPost = $favorites[0]['posts'];
		$favoritesGroups = $favorites[0]['groups'][0]['posts'];
		
	} else {
		
		$favorites = array(array("site_id" => 1,
							"posts" => array(),
							"groups" => array(
								array("group_id" => 1,
										"site_id" => 1,
										"group_name" => "Default List",
										"posts" => array()	
										)
							)
				  ));
	

		
		
		$favoritesPost = $favorites[0]['posts'];
		$favoritesGroups = $favorites[0]['groups'][0]['posts'];
	}
	
	
	if (is_array($favoritesPost)) { 
		if (in_array($p, $favoritesPost)) {
			
			$msg = array('msg' => 'dislike');
			$key = @array_search($p, $favoritesPost);
			if($key!==false){
				unset($favoritesPost[$key]);
				unset($favoritesGroups[$key]);
			} 
		} else {
			$msg = array('msg' => "like");
			$favoritesPost[] = $p;
			$favoritesGroups[] = $p;
		}
	} else {
		$msg = array('msg' => "like");
		$favoritesPost = $p;
		$favoritesGroups = $p;
	}
	
	$favorites[0]['posts'] = $favoritesPost;
	$favorites[0]['groups'][0]['posts'] = $favoritesGroups;
	
	
	$result = $wpdb->query("SELECT * FROM wp_usermeta 
							WHERE meta_key = 'simplefavorites' 
								AND user_id=".$user->uid);
	if (!$result) { 
		
		add_user_meta($user->uid, 'simplefavorites', unserialize(serialize($favorites)));
	} else { 
		$results[0]->favorites = serialize($favorites);
		$wpdb->query("UPDATE wp_usermeta 
						SET meta_value= '".$results[0]->favorites."' 
						WHERE meta_key = 'simplefavorites' 
						AND user_id=".$user->uid);
	}	

	$response = new \WP_REST_Response();
	$response->set_data($msg);
	$response->set_status(200);
	return $response;

}		

