<?php

// WP-JSON POSTS  jobS
add_action( 'rest_api_init', 'posts_job' );

function posts_job() {
	register_rest_route('wp/v2', 'app/posts/jobs/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_jobs',
	));
}

function app_posts_jobs($request){
	require_once('Jwt.php');

	global $wpdb;
	$favorit = $job_applied = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];
		
		$jobs_user = $wpdb->get_results( "SELECT meta_value as jobs_user FROM wp_usermeta WHERE meta_key = 'appliedjobs' AND user_id = ".$user->uid );
		$arrjobs_user = unserialize($jobs_user[0]->jobs_user);

		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
		
	}

    if (get_country_header()) { 
        $country = get_slug_country();
    } else { 
        $objCountry = app_get_country_app();
        $country = $objCountry->data->country['code'];
    }
	if($country == '_jamaica'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Jamaica',
			'compare' => '='
		);
	} elseif($country == '_stlucia'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'St. Lucia',
			'compare' => '='
		);
	} elseif($country == '_turksandcaicos'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Turks and Caicos',
			'compare' => '='
		);
	} elseif($country == '_barbados'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Barbados',
			'compare' => '='
		);
	} elseif($country == '_bahamas'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Bahamas',
			'compare' => '='
		);
	} elseif($country == '_cayman'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Cayman',
			'compare' => '='
		);
	} else {
		$countryQuery = '';
	}
	
	$args = array(
		'post_type' => 'job',
		'post_status' =>  'publish',
		'nopaging ' => true,
		'posts_per_page' => -1,
		'orderby'        => 'post_date',
		'order'          => 'DESC'
		/*'meta_query' => array('relation' => 'AND', 
							array('key' => 'job_expiration', 
									'value' => date('Y-m-d'), 
									'compare' => '>='), 
							$countryQuery)*/
	);
		
	
	$posts = get_posts($args);
	if (empty($posts)) {
		return new WP_Error( 'empty_job', 'there is no post in this category', array('status' => 404) );
	}
	
	foreach($posts as $post) {
		$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
		if ($postmeta) {
			$job_expiration = $job_country = $position = $type = $company = $company_email = $_thumbnail_id = '';
			for ($i=0; $i< count($postmeta); $i++) {
				
				$description = ($postmeta[$i]->meta_key == 'job_description') ? $postmeta[$i]->meta_value : $description;
				$position = ($postmeta[$i]->meta_key == 'job_position') ? $postmeta[$i]->meta_value : $position;
				$type = ($postmeta[$i]->meta_key == 'job_type') ? $postmeta[$i]->meta_value : $type;
				$company = ($postmeta[$i]->meta_key == 'job_company') ? $postmeta[$i]->meta_value : $company;
				$application = ($postmeta[$i]->meta_key == 'job_application') ? $postmeta[$i]->meta_value : $application;
				$company_email = ($postmeta[$i]->meta_key == 'job_company_email') ? $postmeta[$i]->meta_value : $company_email;
				$job_expiration = ($postmeta[$i]->meta_key == 'job_expiration') ? $postmeta[$i]->meta_value : $job_expiration;
				$job_country = ($postmeta[$i]->meta_key == 'job_country') ? $postmeta[$i]->meta_value : $job_country;
				$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
				$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
				
			}
		}
		
		$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
		$job_applied = (@in_array($post->ID, $arrjobs_user));
		$guid = get_url_base( $post->ID);
		//$place = get_the_terms( $post->ID, 'location');

		$imgThumb = '';
		if  (@$thumb[0]->guid) {
			$thumbpartes = explode(".",@$thumb[0]->guid);
			$imgThumb = str_replace(".".end($thumbpartes), "-150x150.".end($thumbpartes), @$thumb[0]->guid);
		}

		
		
		if (!$expired) {
			$dados[] = array(
				"ID" => $post->ID,
				"post_date" => $post->post_date,
				"description" =>  $post->post_content,
				"post_title" => $post->post_title,
				"post_modified" => $post->post_modified,
				"guid" => $guid,
				"post_type" => $post->post_type,
				"favorit" => $favorit,
				"position" => $position,
				"guid" => $guid,
				"type" => $type,
				"application" => $application,
				"company" => $company,
				"company_email" => $company_email,
				"place" => $job_country,
				"job_expiration" => $job_expiration,
				"image" => @$thumb[0]->guid,
				"thumb" => $imgThumb,
				"job_applied" => $job_applied);
			}

	}

	$obj = new \stdClass();
	$obj->posts = $dados;
	
	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;

}	

// WP-JSON POSTS  jobS ID
add_action( 'rest_api_init', 'posts_job_id' );

function posts_job_id() {
	register_rest_route('wp/v2', 'app/posts/job/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_jobs_id',
	));
}

function app_posts_jobs_id($request){
	require_once('Jwt.php');

	global $wpdb;
	$favorit = $job_applied = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];

		$jobs_user = $wpdb->get_results( "SELECT meta_value as jobs_user FROM wp_usermeta WHERE meta_key = 'appliedjobs' AND user_id = ".$user->uid );
		$arrjobs_user = unserialize($jobs_user[0]->jobs_user);

		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
	}

	$args = array(
        'ID' => $request['post_id'],
		'post_type' => "job",
	);

	$post =  get_post( $request['post_id'] );
	if (empty($post)) {
		return new WP_Error( 'empty_job', 'there is no post in this category', array('status' => 404) );
	}
            
	$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
	if ($postmeta) {
		$job_expiration = $position = $type = $company = $company_email = $_thumbnail_id = '';
		for ($i=0; $i< count($postmeta); $i++) {
			
			$description = ($postmeta[$i]->meta_key == 'job_description') ? $postmeta[$i]->meta_value : $description;
			$position = ($postmeta[$i]->meta_key == 'job_position') ? $postmeta[$i]->meta_value : $position;
			$type = ($postmeta[$i]->meta_key == 'job_type') ? $postmeta[$i]->meta_value : $type;
			$application = ($postmeta[$i]->meta_key == 'job_application') ? $postmeta[$i]->meta_value : $application;
			$company = ($postmeta[$i]->meta_key == 'job_company') ? $postmeta[$i]->meta_value : $company;
			$company_email = ($postmeta[$i]->meta_key == 'job_company_email') ? $postmeta[$i]->meta_value : $company_email;
			$job_expiration = ($postmeta[$i]->meta_key == 'job_expiration') ? $postmeta[$i]->meta_value : $job_expiration;
			$job_country = ($postmeta[$i]->meta_key == 'job_country') ? $postmeta[$i]->meta_value : $job_country;
			$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
			$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
			
		}
	}

	$imgThumb = '';
	if  (@$thumb[0]->guid) {
		$thumbpartes = explode(".",@$thumb[0]->guid);
		$imgThumb = str_replace(".".end($thumbpartes), "-150x150.".end($thumbpartes), @$thumb[0]->guid);
	}
	
	$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
	$job_applied = (@in_array($post->ID, $arrjobs_user));
	$guid = get_url_base($post->ID);

	//$place = get_the_terms( $post->ID, 'location');

	//data expirada
	if (strtotime(date("Y-m-d")) <= strtotime(date("Y-m-d", strtotime($job_expiration)))) {
		
		$date1= date("Y-m-d", strtotime($job_expiration));
		$date2= date("Y-m-d") ;
		
		$data1 = new DateTime( $date1 );
		$data2 = new DateTime( $date2 );

		$intervalo = $data2->diff( $data1 );
		
		$expired = false;
	} else {
		$expired = true;
	}
	
	$dados = array(
		"ID" => $post->ID,
		"post_date" => $post->post_date,
		"description" => $description,
		"post_title" => $post->post_title,
		"post_modified" => $post->post_modified,
		"guid" => $guid,
		"post_type" => $post->post_type,
		"category" => @$category[0]->name,
		"favorit" => $favorit,
		"position" => $position,
		"guid" => $guid,
		"type" => $type,
		"application" => $application,
		"company" => $company,
		"company_email" => $company_email,
		"place" => @$job_country,
		"job_expiration" => $job_expiration,
		"image" => @$thumb[0]->guid,
		"thumb" => $imgThumb,
		"job_applied" => $job_applied);

	$obj = new \stdClass();
	$obj->posts = $dados;
	
	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;

}	



// WP-JSON VOTAR
add_action( 'rest_api_init', 'job_applied' );

function job_applied() {
	register_rest_route('wp/v2', 'app/posts/job-applied/', array(
		'methods' => 'POST',
		'callback' => 'app_job_applied',
	));
}

function app_job_applied($request){
	require_once('Jwt.php');
	require_once ABSPATH . WPINC . '/class-phpmailer.php'; 
	require_once ABSPATH . WPINC . '/class-smtp.php';
	global $wpdb;
	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();
	
	// $obj = new \stdClass();
	// $obj->posts = $request;
		
	// $response = new \WP_REST_Response();
	// $response->set_data($request['jobID']);
	// $response->set_status(200);
	// return $response;	

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		}
	} else {
		$response['code'] = 401;
		$response['message'] = __( 'Invalid Token', "wp-rest-user");
		return new WP_REST_Response($response, 401);
	} 
	
	$user = $tokenValido['user'];

	$jobID = $request['jobID'];
	$arr = array($jobID);
	$meta_value = serialize($arr);

	$userID = $user->uid;
	//$siteID = get_current_blog_id();

	global $wpdb;
	$results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}usermeta WHERE meta_key = 'appliedjobs' AND user_id = ". $userID , OBJECT);

	if($results){
		
		$id = $results[0]->umeta_id;
		$meta_value_new = unserialize($results[0]->meta_value);
		$meta_value_new[] = $jobID;
		
		$table_name = $wpdb->prefix . 'usermeta';
		$insert = $wpdb->update($table_name, 
						array('meta_value' => serialize($meta_value_new)),
						array('umeta_id' => $id)
					);
	} else {

		//não tem nada no banco, cria a primeira vez:
		$table_name = $wpdb->prefix . 'usermeta';
		$insert = $insert = $wpdb->insert($table_name, array(
			'user_id' => $userID,
			'meta_key' => 'appliedjobs',
			'meta_value' => $meta_value
			
		));
		
	}

	if (!$insert) {
		$response['code'] = 401;
		$response['message'] = __( 'Error trying to take the job.', "wp-rest-user");
		return new WP_REST_Response($response, 401);
	}

	
	favorite_job($jobID, $userID);
	

	
	if(isset($request['securityanswer']) && trim(stripslashes($request['securityanswer'])) != null){
		$securitynumber1 = (int) $request['securitynumber1'];
		$securitynumber2 = (int) $request['securitynumber2'];
		if($request['securityanswer'] == ($securitynumber1+$securitynumber2)){
	
			foreach($request as $field => $value){$$field = $value;}
	
			$user_name = $request['user_name'];
			$user_email = $request['user_email'];
			$joblinkedin = $request['joblinkedin'];
			$company = $request['company'];
			$company_email = $request['company_email'];

			$content = "<b style='font-size:110%;'>Name: </b>$user_name	<br>
			<b style='font-size:110%;'>E-mail: </b>$user_email<br>
			<b style='font-size:110%;'>LinkedIn: </b>$joblinkedin<br>
			<b style='font-size:110%;'>Applied job info: </b>$job_info<br><br>
			<b style='font-size:110%;'>Temp:</b><br>
			<b style='font-size:110%;'>Company name: </b>$company<br>
			<b style='font-size:110%;'>Company e-mail: </b>$company_email<br>";
	
			$mail = new PHPMailer;
			
			$mail->CharSet = 'UTF-8';
			$mail->setFrom($user_email, $user_name);
			$mail->addReplyTo($user_email, $user_name);
	
			$mail->Body = $content;
			$mail->Subject = 'Buzz: application for job - ' . $user_name;
	
			$mail->IsHTML(true);
	
			if($request['cv']){
				
				$upload_dir = wp_upload_dir();
				$temp_upload_path = $upload_dir['basedir'].'/temp';
				if(! file_exists($temp_upload_path)){
					if(! mkdir($temp_upload_path)){
						die(__('can not create a directory to save the temp files under uploads directory .', 'smpush-plugin-lang'));
					}
				}

				$target_path = '';

				$data = $request['cv'];
				
				$decoded_file = base64_decode($data); // decode the file
				$mime_type = finfo_buffer(finfo_open(), $decoded_file, FILEINFO_MIME_TYPE); // extract mime type
				$extension = mime2ext($mime_type);
				
				$nameFile = uniqid().'.'.$extension;
				$target_path = $temp_upload_path.'/'.$nameFile;

				file_put_contents($target_path, base64_decode($data));
				$mail->addAttachment($target_path);
				
			
			}
			
			
			$mail->isSMTP();

			$mail->Host = 'server10.servhost.com.br';
			$mail->SMTPAuth = true;
			$mail->Port = 587;
			$mail->Username = 'contact@buzz-caribbean.com';
			$mail->Password = 'rx2qtnx9navw';
			$mail->SMTPSecure = 'tls';
	
			$mail->addAddress($company_email, $company);
	
			if(!$mail->send()){
				$response['code'] = 401;
				$response['message'] = __( 'The message could not be sent. Mailer Error: ' . $mail->ErrorInfo);
				return new WP_REST_Response($response, 401);
			} else {
				$response['code'] = 200;
				$response['message'] = __( 'Mail send');
				if ($target_path) { 
					unlink($target_path);
				}
				return new WP_REST_Response($response, 200);
			}
		} else {
			$response['code'] = 401;
			$response['message'] = __( 'Security answer incorrect!');
			return new WP_REST_Response($response, 401);
		}
	 }

	//return $response;	
	
}	


/*
// WP-JSON SEARCH POST
add_action( 'rest_api_init', 'search_job' );

function search_job() {
	register_rest_route('wp/v2', 'app/search-job/', array(
		'methods' => 'GET',
		'callback' => 'app_search_job',
	));
}

function app_search_job($request){
	require_once('Jwt.php');
	
	global $wpdb;
	$favorit = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();


	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];
		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
	}

	$args = array(
		'post_type' => 'job',
		'tax_query' => array(
			array (
				'taxonomy' => 'job-category',   // taxonomy name
				'field' => 'term_id',           // term_id, slug or name
				'terms' => $request['category_id'] 
			),
			array (
				'taxonomy' => 'location',   // taxonomy name
				'field' => 'term_id',           // term_id, slug or name
				'terms' => $request['location'] 
			)
		),
		'post_status' =>  'publish',
		'nopaging ' => true,
		'posts_per_page' => -1,
		'orderby'        => 'post_date',
		'order'          => 'DESC',
	);
	
	$posts = get_posts($args);
	if (empty($posts)) {
		return new WP_Error( 'empty_sub-category', 'there is no post in this category', array('status' => 404) );
	}


	// $search = ( isset($request["s"]) ) ? sanitize_text_field($request["s"]) : false ;
	// $search = (string)$search;

	// $posts = $wpdb->get_results( "SELECT *
	// 								FROM $wpdb->posts
	// 							   WHERE (($wpdb->posts.post_title LIKE '%$search%') 
	// 								  OR ($wpdb->posts.post_content LIKE '%$search%')
	// 								  OR ($wpdb->posts.post_excerpt LIKE '%$search%'))
	// 								 AND $wpdb->posts.post_status =  'publish'
	// 								 AND $wpdb->posts.post_type =  'job'
	// 							ORDER BY $wpdb->posts.post_date DESC");
	
	if ($posts) { 
		foreach ($posts as $post) {
			$sub = get_the_terms( $post->ID, 'sub-category' );
			$cat = get_the_terms( $post->ID, 'category' );

			$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
			$color = getCor(@$cat[0]->name);

			$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
	
			if ($postmeta) {
				$fullprice = '';
				for ($i=0; $i< count($postmeta); $i++) {
					if($postmeta[$i]->meta_key == 'job_shortcode') {
						$poll =  ($postmeta[$i]->meta_value) ? $postmeta[$i]->meta_value : '';
						$totalpoll_id =  str_replace('"]','',str_replace('[totalpoll id="','',$poll));
						
						if ($totalpoll_id) {
							$totallpoll = get_post($totalpoll_id);
							$content = json_decode($totallpoll->post_content);
							$enquete = $content->questions[0];
						}
					}
			
					$fullprice = ($postmeta[$i]->meta_key == 'job_fullprice') ? $postmeta[$i]->meta_value : $fullprice;
					$discount = ($postmeta[$i]->meta_key == 'job_discount') ? $postmeta[$i]->meta_value : $discount;
					$price = ($postmeta[$i]->meta_key == 'job_price') ? $postmeta[$i]->meta_value : $price;
					$code = ($postmeta[$i]->meta_key == 'job_code') ? $postmeta[$i]->meta_value : $code;
					$company = ($postmeta[$i]->meta_key == 'job_company') ? $postmeta[$i]->meta_value : $company;
					// $place = ($postmeta[$i]->meta_key == 'job_place') ? $postmeta[$i]->meta_value : $place;
					$job_expiration = ($postmeta[$i]->meta_key == 'job_expiration') ? $postmeta[$i]->meta_value : $job_expiration;
					$description = ($postmeta[$i]->meta_key == 'job_description') ? $postmeta[$i]->meta_value : $description;
					$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
					$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
					
				}
			}

			$job_applied = (@in_array($post->ID, $arrjobs_user));
			$guid = get_url_base($post->ID);

			$category = get_the_terms( $post->ID, 'job-category');
			$place = get_the_terms( $post->ID, 'location');

			//data expirada
			if (strtotime(date("Y-m-d")) <= strtotime(date("Y-m-d", strtotime($job_expiration)))) {
				
				$date1= date("Y-m-d", strtotime($job_expiration));
				$date2= date("Y-m-d") ;
				
				$data1 = new DateTime( $date1 );
				$data2 = new DateTime( $date2 );

				$intervalo = $data2->diff( $data1 );
				
				$expired = 'end within '. $intervalo->d .' days';
			} else {
				$expired = 'deal expired';
			}
			
			$dados[] = array(
				"ID" => $post->ID,
				"post_date" => $post->post_date,
				"description" => $description,
				"post_title" => $post->post_title,
				"post_modified" => $post->post_modified,
				"guid" => $post->guid,
				"post_type" => $post->post_type,
				"category" => @$category[0]->name,
				"favorit" => $favorit,
				"poll_id" => $totalpoll_id,
				"poll" => $enquete,
				"fullprice" => $fullprice,
				"discount" => $discount,
				"price" => $price,
				"code" => $code,
				"company" => $company,
				"place" => @$place[0]->name,
				"expired" => $expired,
				"job_expiration" => $job_expiration,
				"thumb" => @$thumb[0]->guid,
				"job_redeemed" => $job_applied
			);
		}
	} else {
		$dados = null;
	}

	$obj = new \stdClass();
	$obj->posts = $dados;
	
	$response = new WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);
	return $response;

}	

*/
function favorite_job($p, $userID) {
	global $wpdb;
	$results = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$userID );
	
	if ($results) { 
		
		$favorites = unserialize($results[0]->favorites);
		$favoritesPost = $favorites[0]['posts'];
		$favoritesGroups = $favorites[0]['groups'][0]['posts'];
		
	} else {
		
		$favorites = array(array("site_id" => 1,
							"posts" => array(),
							"groups" => array(
								array("group_id" => 1,
										"site_id" => 1,
										"group_name" => "Default List",
										"posts" => array()	
										)
							)
				  ));
	

		
		
		$favoritesPost = $favorites[0]['posts'];
		$favoritesGroups = $favorites[0]['groups'][0]['posts'];
	}
	
	
	if (is_array($favoritesPost)) { 
		if (in_array($p, $favoritesPost)) {
			
			$msg = array('msg' => 'dislike');
			$key = @array_search($p, $favoritesPost);
			if($key!==false){
				unset($favoritesPost[$key]);
				unset($favoritesGroups[$key]);
			} 
		} else {
			$msg = array('msg' => "like");
			$favoritesPost[] = $p;
			$favoritesGroups[] = $p;
		}
		
	} else {
		$msg = array('msg' => "like");
		$favoritesPost = $p;
		$favoritesGroups = $p;
	}
	
	$favorites[0]['posts'] = $favoritesPost;
	$favorites[0]['groups'][0]['posts'] = $favoritesGroups;
	
	
	$result = $wpdb->query("SELECT * FROM wp_usermeta 
							WHERE meta_key = 'simplefavorites' 
								AND user_id=".$userID);
	if (!$result) { 
		
		add_user_meta($userID, 'simplefavorites', unserialize(serialize($favorites)));
	} else { 
		$results[0]->favorites = serialize($favorites);
		$wpdb->query("UPDATE wp_usermeta 
						SET meta_value= '".$results[0]->favorites."' 
						WHERE meta_key = 'simplefavorites' 
						AND user_id=".$userID);
	}	

	return true;

}

