<?php
ini_set('display_errors', 0);
define("URL_BASE", "http://".$_SERVER['HTTP_HOST']);
//define("URL_BASE", "http://localhost/buzz/web/");

// WP-JSON USER
	add_action('rest_api_init', 'wp_rest_user_endpoints'); 
	/**
	 * Register a new user
	 *
	 * @param  WP_REST_Request $request Full details about the request.
	 * @return array $args.
	 **/
	function wp_rest_user_endpoints($request) {

		register_rest_route('wp/v2', 'users/register', array(
			'methods' => 'POST',
			'callback' => 'wc_rest_user_endpoint_handler',
		));

		function wc_rest_user_endpoint_handler($request = null) {
			require_once('Jwt.php');
			require_once('Email.php');


			global $wpdb;

			$error = new \WP_Error();
			
			$response = array();
			$parameters = $request->get_json_params();
			$username = sanitize_text_field($parameters['username']);
			$email = sanitize_text_field($parameters['email']);
			$password = sanitize_text_field($parameters['password']);
			$first_name = sanitize_text_field($parameters['first_name']);
			$last_name = sanitize_text_field($parameters['last_name']);
			$gender = sanitize_text_field(@$parameters['gender']);
			$city = sanitize_text_field(@$parameters['city']);
			$country = sanitize_text_field(@$parameters['country']);
			// $role = sanitize_text_field($parameters['role']);
			
			if (empty($username)) {
			$error->add(400, __("Username must be at least 3 characters", 'wp-rest-user'), array('status' => 400));
			return $error;
			}
			if (empty($email)) {
			$error->add(400, __("Enter email address", 'wp-rest-user'), array('status' => 400));
			return $error;
			}
			if (empty($first_name)) {
			$error->add(400, __("First Name here", 'wp-rest-user'), array('status' => 400));
			return $error;
			}
			if (empty($last_name)) {
			$error->add(400, __("Last Name here", 'wp-rest-user'), array('status' => 400));
			return $error;
			}
			if (empty($password)) {
			$error->add(400, __("Password field 'password' is required.", 'wp-rest-user'), array('status' => 400));
			return $error;
			}
			
			$user_id = username_exists($username);
			if ($user_id) {
				$error->add(400, __("Username already exists, please try again", 'wp-rest-user'), array('status' => 400));
				return $error;
			} else { 
				if (!$user_id && email_exists($email) == false) {
					
					$display_name = $parameters['first_name'] . ' ' . $parameters['last_name'];

					$user_id = wp_insert_user(
						  array(
							'user_login'  => $username,
							'user_pass' => $password,
							'first_name'  => $parameters['first_name'],
							'last_name' => $parameters['last_name'],
							'user_email'  => $email,
							'display_name'  => $parameters['first_name'] . ' ' . $parameters['last_name'],
							'nickname'  => $parameters['first_name'] . ' ' . $parameters['last_name'],
							'role'    => 'customer',
						  )
						);
						
					if (!is_wp_error($user_id)) {
						$user = get_user_by('id', $user_id);
						$user->set_role('customer');

						$target_path_final = '';

						if($parameters['image']){
			
							$upload_dir = wp_upload_dir();
							$temp_upload_path = $upload_dir['basedir'].'/ultimatemember'.'/'.$user->ID.'/';
							if(! file_exists($temp_upload_path)){
								if(! mkdir($temp_upload_path)){
									die(__('can not create a directory to save the temp files under uploads directory .', 'smpush-plugin-lang'));
								}
							}
							
							$data = $parameters['image'];
							
							$target_path = '';
							$upload_dir = wp_upload_dir();
							$target_path = $upload_dir['basedir'].'/ultimatemember/'.$user->ID.'/';
							
							
							$decoded_file = base64_decode($data); // decode the file
							$mime_type = finfo_buffer(finfo_open(), $decoded_file, FILEINFO_MIME_TYPE); // extract mime type
							$extension = mime2ext($mime_type);
							
							$nameFile = 'profile_photo.'.$extension;
							$target_path_final = $target_path.'/'.$nameFile;
			
							file_put_contents($target_path_final, base64_decode($data));
							
							add_user_meta($user->ID, 'profile_photo', $nameFile);
						
						}

						//add metas
						$gender = ($gender) ? $gender : null;
						add_user_meta($user->ID, 'gender', array($gender));
						add_user_meta($user->ID, 'city', $city);
						add_user_meta($user->ID, 'country', $country);
						add_user_meta($user->ID, 'alert_breaknews', true);
						add_user_meta($user->ID, 'alert_promotional', true);
						add_user_meta($user->ID, 'alert_updates', true);
						add_user_meta($user->ID, 'alert_general', true);

						$creds = array();

						$creds['user_login'] = $username;
						$creds['user_password'] =  $password;
						$creds['remember'] = true;
						$user = wp_signon( $creds, false );
						
						if ($user) { 
							$objJwt = new Jwt();
							$arrToken = $objJwt->generateCode($user->data);
					
							$user = $user->data;
							
							$detail = $wpdb->get_results( "SELECT meta_key, meta_value 
									   FROM wp_usermeta 
									  WHERE meta_key = 'profile_photo' AND wp_usermeta.user_id = ".$user->ID );
			
							$image = '';
							if ($detail) {
								$profile_photo = $detail[0]->meta_value;
								
								$temp_upload_path = URL_BASE.'/app/uploads/ultimatemember/'.$user->ID.'/';
								$image = ($profile_photo) ? $temp_upload_path.$profile_photo : ''; 
							}
							
							$dados = array(
								'id' => $user->ID,
								'first_name'  => $parameters['first_name'],
								'last_name' => $parameters['last_name'],
								'email' => $user->user_email,
								'access_token' => $arrToken['token'],
								'expires_at' => $arrToken['expire_at'],
								'username' => $username,
								'country' => $country, 
								'city' => $city,
								'gender' => $gender,
								'image' => $image,
								'alert_breaknews' => false,
								'alert_promotional' => false,
								'alert_updates' => false,
								'alert_general' => true,
							);
					
							$senEmail = Email::welcome( $display_name, $user->user_email);

							$response = new \WP_REST_Response();
							$response->set_data($dados);
							$response->set_status(200);
							return $response;
						} else {
							$error->add(400, __("Email already exists, please try 'Reset Password'", 'wp-rest-user'), array('status' => 400));
							return $error;
						}
						// $response['code'] = 200;
						// $response['message'] = __("User '" . $username . "' Registration was Successful", "wp-rest-user");
					} else {
						return $user_id;
					}
				} else {
					$error->add(400, __("Email already exists, please try 'Reset Password'", 'wp-rest-user'), array('status' => 400));
					return $error;
				}
			}
		}
	}

	add_action( 'rest_api_init', 'register_add_device' );
	/**
	 * add device
	 *
	 * @param  WP_REST_Request $request Full details about the request.
	 * @return array $args.
	 **/

	function register_add_device() {
		register_rest_route('wp/v2', 'adddevice', array(
			'methods' => 'POST',
			'callback' => 'add_device',
		));
	}

	function add_device($request){
		require_once('Jwt.php');

		global $wpdb;
		
		//validar token
		$jwt = new Jwt();
		$token = $jwt->getTokenHeader();


		if ($token) { 
			$tokenValido = $jwt->validateCode($token);

			if (!$tokenValido['callback']) {
				$response['code'] = 401;
				$response['message'] = __( $tokenValido['message'], "wp-rest-user");
				return new WP_REST_Response($response, 401);
			} 

			$user = $tokenValido['user'];
			
			$device = $wpdb->get_results("SELECT deviceid FROM cp_devices WHERE user_id = ".$user->uid); 
			if (!$device) { 
				//salvar o device_id
				$wpdb->insert('cp_devices', array(
					'user_id' => $user->uid,
					'deviceid' => $request['deviceid']
				));
			} else {
				$wpdb->update('cp_devices', array(
						'deviceid' => $request['deviceid'],
						'updated_at' => date("Y-m-d H:i:s")
					), array(
						'user_id' => $user->uid
					)
				);
			}	
		}

		$response['code'] = 200;
		$response['message'] = __( "Device registrado com sucesso.", "wp-rest-user");
		return new WP_REST_Response($response, 200);

	}

	

	add_action( 'rest_api_init', 'register_api_hooks' );
	/**
	 * Login user
	 *
	 * @param  WP_REST_Request $request Full details about the request.
	 * @return array $args.
	 **/

	function register_api_hooks() {
		register_rest_route('wp/v2', 'users/login', array(
			'methods' => 'POST',
			'callback' => 'um_login',
		));
	}

	function um_login($request){
		require_once('Jwt.php');

		global $wpdb;

		$creds = array();

		$creds['user_login'] = $request["username"];
		$creds['user_password'] =  $request["password"];
		$creds['remember'] = true;
		$user = wp_signon( $creds, false );
		

		if ( is_wp_error($user) ) { 
			$response['code'] = 401;
			$response['message'] = __( $user->get_error_message(), "wp-rest-user");
			return new WP_REST_Response($response, 401);
		}

		//print_r($user->data);die();
		
        $objJwt = new Jwt();
		$arrToken = $objJwt->generateCode($user->data);

		$user = $user->data;
		
		$detail = $wpdb->get_results( 'SELECT meta_key, meta_value 
									   FROM wp_usermeta 
									  WHERE wp_usermeta.user_id = '.$user->ID );

		$first_name = $last_name = $country = $city = $gender = '';
		
		for ($x = 0; $x < count($detail); $x++) {
			if ($detail[$x]->meta_key == 'first_name') {
				$first_name = $detail[$x]->meta_value;
			}
			if ($detail[$x]->meta_key == 'last_name') {
				$last_name = $detail[$x]->meta_value;
			}
			if ($detail[$x]->meta_key == 'gender') {
				$gender = unserialize($detail[$x]->meta_value);
				$gender = $gender[0];
			}
			if ($detail[$x]->meta_key == 'city') {
				$city = $detail[$x]->meta_value;
			}
			if ($detail[$x]->meta_key == 'country') {
				$country = $detail[$x]->meta_value;
			}
			if ($detail[$x]->meta_key == 'alert_breaknews') {
				$alert_breaknews = $detail[$x]->meta_value;
			}
			if ($detail[$x]->meta_key == 'alert_promotional') {
				$alert_promotional = $detail[$x]->meta_value;
			}
			if ($detail[$x]->meta_key == 'alert_updates') {
				$alert_updates = $detail[$x]->meta_value;
			}
			if ($detail[$x]->meta_key == 'alert_general') {
				$alert_general = $detail[$x]->meta_value;
			}
		}

		$detail = $wpdb->get_results( 'SELECT meta_key, meta_value 
									   FROM wp_usermeta 
									  WHERE meta_key = "profile_photo" AND wp_usermeta.user_id = '.$user->ID );
			
		$image = '';
		if ($detail) {
			$profile_photo = $detail[0]->meta_value;
			
			$temp_upload_path = URL_BASE.'/app/uploads/ultimatemember/'.$user->ID.'/';
			$image = ($profile_photo) ? $temp_upload_path.$profile_photo : ''; 
		}
		
        $dados = array(
            "id" => $user->ID,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "email" => $user->user_email,
            "access_token" => $arrToken['token'],
			"expires_at" => $arrToken['expire_at'],
			"username" => $user->user_login,
			"country" => $country, 
			"city" => $city,
			"gender" => $gender,
			"image" => $image,
			'alert_breaknews' => (bool) $alert_breaknews,
			'alert_promotional' => (bool) $alert_promotional,
			'alert_updates' => (bool) $alert_updates,
			'alert_general' => (bool) $alert_general,			
          );

		  $obj = new \stdClass();
		  $obj= (object)$dados;
		  
		  $response = new WP_REST_Response();
		  $response->set_data($obj);
		  $response->set_status(200);
		  return $response;
	
	}


	add_action( 'rest_api_init', 'validate_user_token' );
	/**
	 * Calidate token user
	 *
	 * @param  WP_REST_Request $request Full details about the request.
	 * @return array $args.
	 **/	
		
	function validate_user_token() {
		register_rest_route('wp/v2', 'users/token', array(
			'methods' => 'POST',
			'callback' => 'um_dados_user',
		));
	}
	

	function um_dados_user($request){
		$headers = getallheaders();
		$accessToken = $headers['Authentication'];
		$token = str_replace("Bearer ","",$accessToken);  
        if ( $accessToken) {
			
			$body = wp_remote_retrieve_body($token);
			$jsonUser = json_decode($body);
			$response = new \WP_REST_Response();
			$response->set_status(200);
			$response->set_data((array) $jsonUser);
			return $response;
			
		}    

	}



//Recuperar Senha
add_action( 'rest_api_init', 'register_api_forgot' );
/**
 * Login user
 *
 * @param  WP_REST_Request $request Full details about the request.
 * @return array $args.
 **/

function register_api_forgot() {
	register_rest_route('wp/v2', 'users/forgot', array(
		'methods' => 'POST',
		'callback' => 'user_forgot',
	));
}

function user_forgot($request){
	require_once('Helpers.php');
	require_once('Email.php');
	
	global $wpdb;

	$email = $request['email'];
	$pass = Helpers::randomString(8);

	if (email_exists($email)) {
		//consulta o usuario pelo email
		$user = $wpdb->get_results( 'SELECT wp_users.ID, wp_users.display_name, wp_users.user_email  
									   FROM wp_users INNER JOIN wp_usermeta ON  wp_users.ID = wp_usermeta.user_id 
								      WHERE wp_users.user_email = "'.$email.'" 
									    AND wp_usermeta.meta_key = "wp_capabilities" 
										AND wp_usermeta.meta_value LIKE "%customer%"' );
										

		//atualiza os dados do usuario
		$user_id = wp_update_user(
			array(
			  'ID'  => $user[0]->ID,
			  'user_pass' => $pass
			)
		);
		
		if (!is_wp_error($user_id)) {
			$senEmail = Email::newPassword( $user[0]->display_name, $email, $pass);
			if ($senEmail) { 
				$response['code'] = 200;
				$response['message'] = __( "Look out for our email with the link to reset your password", "wp-rest-user");
				return new WP_REST_Response($response, 200);
			} else{
				$response['code'] = 401;
				$response['message'] = __( "Uh oh! The email could not be sent. Please confirm that you entered the correct email address and try again", "wp-rest-user");
				return new WP_REST_Response($response, 401);
			}	
		} else {
			$response['code'] = 401;
			$response['message'] = __( $user_id->get_error_message(), "wp-rest-user");
			return new WP_REST_Response($response, 401);"com erro";
		}
	} else {
		$response['code'] = 401;
		$response['message'] = __( "The email address or username entered does not match our records. Please try again", "wp-rest-user");
		return new WP_REST_Response($response, 401);
	}


}

//EDIT PROFILE
add_action( 'rest_api_init', 'register_api_profile' );
/**
 * Login user
 *
 * @param  WP_REST_Request $request Full details about the request.
 * @return array $args.
 **/

function register_api_profile() {
	register_rest_route('wp/v2', 'users/profile', array(
		'methods' => 'POST',
		'callback' => 'user_profile',
	));
}

function user_profile($request){
	require_once('Jwt.php');
	require_once('Helpers.php');
	require_once('Email.php');
	
	global $wpdb;

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	
	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$userToken = $tokenValido['user'];
		$emailToken = $userToken->email;
	} else{
		$response['code'] = 401;
		$response['message'] = __( 'Invalid Token', "wp-rest-user");
		return new WP_REST_Response($response, 401);
	}


	$response = array();
	$parameters = $request->get_json_params();
	$username = sanitize_text_field($parameters['username']);
	$email = sanitize_text_field($parameters['email']);
	$gender = sanitize_text_field($parameters['gender']);
	$city = sanitize_text_field($parameters['city']);
	$country = sanitize_text_field($parameters['country']);
	$current_password = sanitize_text_field($parameters['current_password']);
	$new_user_password = sanitize_text_field($parameters['password']);
	$alert_breaknews = $parameters['alert_breaknews'];
	$alert_promotional = $parameters['alert_promotional'];
	$alert_updates = $parameters['alert_updates'];
	$alert_general = $parameters['alert_general'];	
	// $role = sanitize_text_field($parameters['role']);

		
	//consulta o usuario pelo email
	//AND wp_usermeta.meta_key = "wp_capabilities" 
	//AND wp_usermeta.meta_value LIKE "%customer%"
	$user = $wpdb->get_results( 'SELECT DISTINCT wp_users.ID, wp_users.display_name, wp_users.user_email, wp_users.user_login, wp_users.user_pass
									FROM wp_users INNER JOIN wp_usermeta ON  wp_users.ID = wp_usermeta.user_id 
									WHERE wp_users.user_email = "'.$emailToken.'" ' );

	$usermetas = $wpdb->get_results( "SELECT meta_value  FROM wp_usermeta WHERE meta_key = 'profile_photo' AND user_id = ".$user[0]->ID );
									
	
	$error = new \WP_Error();

	// if (empty($username)) {
	// $error->add(400, __("Username field 'username' is required.", 'wp-rest-user'), array('status' => 400));
	// return $error;
	// }
	if (empty($email)) {
	$error->add(400, __("Enter email address", 'wp-rest-user'), array('status' => 400));
	return $error;
	}

	if ( isset( $email ) ) {

		if ( strlen( trim( $email ) ) == 0 ) {
			$response['code'] = 400;
			$response['message'] = __(   'An email address is required to complete sign-up', 'ultimate-member');
			return new WP_REST_Response($response, 400);
		}

		if ( ! is_email( $email ) ) {
			$response['code'] = 400;
			$response['message'] = __(  'Oh No! Please provide a valid email address to complete sign up', 'ultimate-member' );
			return new WP_REST_Response($response, 400);
		}
		
		if ( email_exists( $email ) && email_exists( $email ) != $user[0]->ID ) {
			$response['code'] = 400;
			$response['message'] = __(   'It seems this email is linked to another account. Please try again using a different email address', 'ultimate-member'  );
			return new WP_REST_Response($response, 400);
		}
	}

	
	// if (!empty($username) && (validate_username( $username ) && username_exists($username) != $user[0]->ID )) {
	// 	$response['code'] = 400;
	// 	$response['message'] = __(  'Your username is invalid', 'ultimate-member' );
	// 	return new WP_REST_Response($response, 400);
	// }

	if ( isset( $first_name ) && ( strlen( trim( $first_name ) ) == 0 && $account_name_require ) ) {
		$response['code'] = 400;
		$response['message'] = __(  'You must provide a first name to complete sign up', 'ultimate-member' );
		return new WP_REST_Response($response, 400);
	}

	if ( isset( $last_name ) && ( strlen( trim( $last_name ) ) == 0 && $account_name_require ) ) {
		$response['code'] = 400;
		$response['message'] = __( 'You must provide a last name to complete sign up', 'ultimate-member' );
		return new WP_REST_Response($response, 400);
	}

	
	$changepassword = false;
	// change password
	if ( ( isset( $current_password ) && $current_password != '' ) ||
	( isset( $new_user_password ) && $new_user_password != '' )  ) {

		if ( $current_password == '' || ! wp_check_password( $current_password, $user[0]->user_pass, $user[0]->ID ) ) {
			$error->add(400, __("Oh No! The password entered does not match our records. Please try again", 'ultimate-member'), array('status' => 400));
			return $error;
		} else { // correct password

			if ( UM()->options()->get( 'account_require_strongpass' ) ) {

				if ( strlen( utf8_decode( $new_user_password ) ) < 8 ) {
					$error->add(400, __("Your password must contain at least 8 characters", 'ultimate-member'), array('status' => 400));
					return $error;
				}

				if ( strlen( utf8_decode( $new_user_password ) ) > 30 ) {
					$error->add(400, __("Your password must contain less than 30 characters", 'ultimate-member'), array('status' => 400));
					return $error;
				}

				if ( ! UM()->validation()->strong_pass( $new_user_password ) ) {
					$error->add(400, __("Password must be 8-30 characters long and contain at least one lowercase letter, one uppercase letter & one number", 'ultimate-member'), array('status' => 400));
					return $error;
				}

			}

		}
		$changepassword = true;
	}

	
	if ($changepassword) {
		$user_id = wp_update_user(
			array(
				'ID'  => $user[0]->ID,
				//'user_login'  => $username,
				'user_pass' => $new_user_password,
				'first_name'  => $parameters['first_name'],
				'last_name' => $parameters['last_name'],
				'user_email'  => $email,
				'display_name'  => $parameters['first_name'] . ' ' . $parameters['last_name'],
				'nickname'  => $parameters['first_name'] . ' ' . $parameters['last_name'],
				'user_pass' => $new_user_password
			)
		);
	} else { 
		$user_id = wp_update_user(
			array(
				'ID'  => $user[0]->ID,
				//'user_login'  => $username,
				'user_pass' => $password,
				'first_name'  => $parameters['first_name'],
				'last_name' => $parameters['last_name'],
				'user_email'  => $email,
				'display_name'  => $parameters['first_name'] . ' ' . $parameters['last_name'],
				'nickname'  => $parameters['first_name'] . ' ' . $parameters['last_name'],
			)
		);
	}	

	if (!is_wp_error($user_id)) {

		if($parameters['image']){

			
			
			$upload_dir = wp_upload_dir();
			$temp_upload_path = $upload_dir['basedir'].'/ultimatemember'.'/'.$user[0]->ID.'/';
			if(! file_exists($temp_upload_path)){
				if(! mkdir($temp_upload_path)){
					die(__('can not create a directory to save the temp files under uploads directory .', 'smpush-plugin-lang'));
				}
			}
			
			$data = $parameters['image'];
			
			$target_path = '';
			$upload_dir = wp_upload_dir();
			$target_path = $upload_dir['basedir'].'/ultimatemember/'.$user[0]->ID.'/';
			
			
			$decoded_file = base64_decode($data); // decode the file
			$mime_type = finfo_buffer(finfo_open(), $decoded_file, FILEINFO_MIME_TYPE); // extract mime type
			$extension = mime2ext($mime_type);
			
			$nameFile = 'profile_photo.'.$extension;
			$target_path_final = $target_path.'/'.$nameFile;

			file_put_contents($target_path_final, base64_decode($data));
			
			if ($usermetas) {
				update_user_meta($user[0]->ID, 'profile_photo', $nameFile);
			} else {
				add_user_meta($user[0]->ID, 'profile_photo', $nameFile);
			}

			
			$image = '';
			$temp_upload_path = URL_BASE.'/app/uploads/ultimatemember/'.$user[0]->ID.'/';
			if ($usermetas) {
				update_user_meta($user[0]->ID, 'profile_photo', $nameFile);
				$image = $temp_upload_path.$nameFile; 
			} else {
				add_user_meta($user[0]->ID, 'profile_photo', $nameFile);
			}

		} else {
			if ($usermetas) {
				$image  = URL_BASE.'/app/uploads/ultimatemember/'.$user[0]->ID.'/'.$usermetas[0]->meta_value;
			}	
		}

		update_user_meta($user[0]->ID, 'gender', array($gender));
		update_user_meta($user[0]->ID, 'city', $city);
		update_user_meta($user[0]->ID, 'country', $country);
		
		if (metadata_exists('user', $user[0]->ID, 'alert_breaknews')) { update_user_meta($user[0]->ID, 'alert_breaknews', $alert_breaknews); } else {add_user_meta($user[0]->ID, 'alert_breaknews', $alert_breaknews);}
		if (metadata_exists('user', $user[0]->ID, 'alert_promotional')) { update_user_meta($user[0]->ID, 'alert_promotional', $alert_promotional); } else {add_user_meta($user[0]->ID, 'alert_promotional', $alert_promotional);}
		if (metadata_exists('user', $user[0]->ID, 'alert_updates')) { update_user_meta($user[0]->ID, 'alert_updates', $alert_updates); } else {add_user_meta($user[0]->ID, 'alert_updates', $alert_updates);}
		if (metadata_exists('user', $user[0]->ID, 'alert_general')) { update_user_meta($user[0]->ID, 'alert_general', $alert_general); } else {add_user_meta($user[0]->ID, 'alert_general', $alert_general);}		

		$objJwt = new Jwt();
		$arrToken = $objJwt->generateCode($user[0]);

		$dados = array(
			'id' => $user[0]->ID,
			'first_name'  => $parameters['first_name'],
			'last_name' => $parameters['last_name'],
			'email' => $email,
			'access_token' => $arrToken['token'],
			'expires_at' => $arrToken['expire_at'],
			'username' => $user[0]->user_login,
			'country' => $country, 
			'city' => $city,
			'gender' => $gender,
			'image' => $image,
			'alert_breaknews' => (bool) $alert_breaknews,
			'alert_promotional' => (bool) $alert_promotional,
			'alert_updates' => (bool) $alert_updates,	
			'alert_general' => (bool) $alert_general,				
			
		);

		if ($changepassword) {
			$senEmail = Email::editPassword( $user[0]->display_name, $email);
		}

		$response = new \WP_REST_Response();
		$response->set_data($dados);
		$response->set_status(200);
        return $response;
	} else {
		$response['code'] = 401;
		$response['message'] = __( "Error performing update.", "wp-rest-user");
		return new WP_REST_Response($response, 401);
	}
			
}		
		


// WP-JSON POSTS BY SUB-CATEGORIES
add_action( 'rest_api_init', 'posts_subcategory' );

function posts_subcategory() {
	register_rest_route('wp/v2', 'app/posts/subcategorys/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_subcategorys',
	));
}

function app_posts_subcategorys($request){
	require_once('Jwt.php');

	global $wpdb;
	$favorit = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];
		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
	}

		
	$tax_query = null;
	$nameCountry = get_name_country();
	
	if (@$nameCountry && @$nameCountry != '') { 
		$getCountry = get_term_by('name', $nameCountry, 'countries');
		$countryID = $getCountry->term_id;
		$tax_query = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'countries',
				'field'    => 'term_id',
				'terms'    => $countryID,
			));
	} 

	$args = array(
		'tax_query' => array(
			array (
				'taxonomy' => 'sub-category',   // taxonomy name
				'field' => 'term_id',           // term_id, slug or name
				'terms' => $request['subcategory_id'] 
			),
			$tax_query

		),
		'post_status' =>  'publish',
		'nopaging ' => true,
		'posts_per_page' => -1,
		'orderby'        => 'post_date',
		'order'          => 'DESC',
	);

	$posts = get_posts($args);
	if (empty($posts)) {
		return new WP_Error( 'empty_sub-category', 'there is no post in this category', array('status' => 404) );
	}

	foreach($posts as $post) {
		$sub = get_the_terms( $post->ID, 'sub-category' );
		$cat = get_the_terms( $post->ID, 'category' );

		if(has_post_thumbnail($post->ID)){
			$image = get_the_post_thumbnail_url($post->ID);
		} else {
			$image = catch_that_image($post->ID);
		}

		$thumb = '';
		if (!strpos($image, "nothing.png")) { 
			if ($image) { 
				$imagem = preg_replace("/([-]\d{3,4}x\d{3}[.])/", ".", $image);
				$thumbnail = ($imagem) ? explode(".", $imagem) : '';
				$ext = end($thumbnail);
				$thumb = str_replace(".".$ext,"-150x150.".$ext, $imagem);
			}
		} else {
			$image = '';
		}

		$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
		
		$color = getCor(@$cat[0]->name);

		$guid = ($post->post_type == 'offer') ? str_replace("&webview=true", "", get_url_base($post->ID)) :  get_url_base($post->ID);
		
		$dados[] = array(
			'postID' => $post->ID,
			'title' => $post->post_title,
			'image' => $image,
			'thumb' => $thumb,
			'category' => @$cat[0]->name,
			'sub_categoty' => @$sub[0]->name,
			'favorit' => $favorit,
			'guid' => $guid,
		);	

		
	}

	$obj = new \stdClass();
	$obj->posts = $dados;
		
	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);
	return $response;

}	


// WP-JSON POSTS BY CATEGORIES
add_action( 'rest_api_init', 'posts_category_id' );

function posts_category_id() {
	register_rest_route('wp/v2', 'app/posts/categorys/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_category_id',
	));
}

function app_posts_category_id($request){
	require_once('Jwt.php');
	/*
	* 4 - NEWS
	* 3 - LIFE
	* 2 - HOT
	*/

	global $wpdb;
	$favorit = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];
		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
	}

	$args = array(
		'sub_category' => $request['category_id'],
		'post_status' =>  'publish',
		'nopaging ' => true,
		'posts_per_page' => -1,
		'orderby'        => 'post_date',
		'order'          => 'DESC',
	);

	//print_r($args);die();

	$posts = get_posts($args);
	if (empty($posts)) {
		return new WP_Error( 'empty_category', 'there is no post in this category', array('status' => 404) );
	}

	foreach($posts as $post) {
		$sub = get_the_terms( $post->ID, 'sub-category' );
		$cat = get_the_terms( $post->ID, 'category' );

		$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;

		$color = getCor(@$cat[0]->name);
		
		if(has_post_thumbnail($post->ID)){
			$image = get_the_post_thumbnail_url($post->ID);
		} else {
			$image = catch_that_image($post->ID);
		}
		
		$thumb = '';
		if (!strpos($image, "nothing.png")) { 
			if ($image) { 
				$imagem = preg_replace("/([-]\d{3,4}x\d{3}[.])/", ".", $image);
				$thumbnail = ($imagem) ? explode(".", $imagem) : '';
				$ext = end($thumbnail);
				$thumb = str_replace(".".$ext,"-150x150.".$ext, $imagem);
			}
		} else {
			$image = '';
		}
		
		$dados[] = array(
			"ID" => $post->ID,
			"type" => $post->post_type,
			"postTitle" => $post->post_title,
			"title" => $post->post_title,
			"image" => $image,
			"thumb" => $thumb,
			"favorit" => $favorit,
			"color" => $color,
			"categoty" => @$cat[0]->name,
			"sub_categoty" => @$sub[0]->name,
			"filter" => $post->filter_has_var,
			"post_date" => $post->post_date,
			"post_date_gmt" => $post->post_date_gmt,
			"post_excerpt" => $post->post_excerpt,
			"guid" => $post->guid,
		);

	}

	$response = new WP_REST_Response($dados);
	$response->set_status(200);

	return $response;

}	


// WP-JSON CATEGORIES OFFERS
add_action( 'rest_api_init', 'posts_category_id_offer' );

function posts_category_id_offer() {
	register_rest_route('wp/v2', 'app/category-offer/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_category_id_offer',
	));
}

function app_posts_category_id_offer($request){
	require_once('Jwt.php');
		
	global $wpdb;

	$terms = get_terms(array('taxonomy' => 'offer-category', 'hide_empty' => 1));
	foreach ($terms as $term)  { 
	
		$all[] = array(
			"id" => $term->term_id,
			"name" => $term->name
		);
	}

	$obj = new \stdClass();
	$obj->categories = $all;
	
	$response = new WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);
	return $response;

}	

// WP-JSON CATEGORIES OFFERS
add_action( 'rest_api_init', 'posts_location_offer' );

function posts_location_offer() {
	register_rest_route('wp/v2', 'app/locations/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_location_offer',
	));
}

function app_posts_location_offer($request){
	require_once('Jwt.php');
		
	global $wpdb;

	$terms = get_terms(array('taxonomy' => 'location', 'hide_empty' => 1));
	foreach ($terms as $term)  { 
	
		$all[] = array(
			"id" => $term->term_id,
			"name" => $term->name
		);
	}

	$obj = new \stdClass();
	$obj->categories = $all;
	
	$response = new WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);
	return $response;

}	



require_once('api-favorit.php');
require_once('api-search.php');
require_once('api-top-stories.php');
require_once('api-categorys.php');
require_once('api-offer.php');
require_once('api-out.php');
require_once('api-job.php');
require_once('api-video.php');
require_once('cron-push.php');

	
function getCor($string = ''){
	$color = '';
	switch($string){
		case 'Hot' : $color = 'rosa';  break;
		case 'Life' : $color = 'laranja'; break;
		case 'News' : $color = 'azul'; break;
		default : $color = 'none';break;
	}
	return $color;
}

	
function get_url_base($id = ''){
	return URL_BASE."?p=".$id."&webview=true"; 
}

function mime2ext($mime){
    $all_mimes = '{"png":["image\/png","image\/x-png"],"bmp":["image\/bmp","image\/x-bmp",
    "image\/x-bitmap","image\/x-xbitmap","image\/x-win-bitmap","image\/x-windows-bmp",
    "image\/ms-bmp","image\/x-ms-bmp","application\/bmp","application\/x-bmp",
    "application\/x-win-bitmap"],"gif":["image\/gif"],"jpg":["image\/jpg"],"jpeg":["image\/jpeg",
    "image\/pjpeg"],"xspf":["application\/xspf+xml"],"vlc":["application\/videolan"],
    "wmv":["video\/x-ms-wmv","video\/x-ms-asf"],"au":["audio\/x-au"],
    "ac3":["audio\/ac3"],"flac":["audio\/x-flac"],"ogg":["audio\/ogg",
    "video\/ogg","application\/ogg"],"kmz":["application\/vnd.google-earth.kmz"],
    "kml":["application\/vnd.google-earth.kml+xml"],"rtx":["text\/richtext"],
    "rtf":["text\/rtf"],"jar":["application\/java-archive","application\/x-java-application",
    "application\/x-jar"],"zip":["application\/x-zip","application\/zip",
    "application\/x-zip-compressed","application\/s-compressed","multipart\/x-zip"],
    "7zip":["application\/x-compressed"],"xml":["application\/xml","text\/xml"],
    "svg":["image\/svg+xml"],"3g2":["video\/3gpp2"],"3gp":["video\/3gp","video\/3gpp"],
    "mp4":["video\/mp4"],"m4a":["audio\/x-m4a"],"f4v":["video\/x-f4v"],"flv":["video\/x-flv"],
    "webm":["video\/webm"],"aac":["audio\/x-acc"],"m4u":["application\/vnd.mpegurl"],
    "pdf":["application\/pdf","application\/octet-stream"],
    "pptx":["application\/vnd.openxmlformats-officedocument.presentationml.presentation"],
    "ppt":["application\/powerpoint","application\/vnd.ms-powerpoint","application\/vnd.ms-office",
    "application\/msword"],"docx":["application\/vnd.openxmlformats-officedocument.wordprocessingml.document"],
    "xlsx":["application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application\/vnd.ms-excel"],
    "xl":["application\/excel"],"xls":["application\/msexcel","application\/x-msexcel","application\/x-ms-excel",
    "application\/x-excel","application\/x-dos_ms_excel","application\/xls","application\/x-xls"],
    "xsl":["text\/xsl"],"mpeg":["video\/mpeg"],"mov":["video\/quicktime"],"avi":["video\/x-msvideo",
    "video\/msvideo","video\/avi","application\/x-troff-msvideo"],"movie":["video\/x-sgi-movie"],
    "log":["text\/x-log"],"txt":["text\/plain"],"css":["text\/css"],"html":["text\/html"],
    "wav":["audio\/x-wav","audio\/wave","audio\/wav"],"xhtml":["application\/xhtml+xml"],
    "tar":["application\/x-tar"],"tgz":["application\/x-gzip-compressed"],"psd":["application\/x-photoshop",
    "image\/vnd.adobe.photoshop"],"exe":["application\/x-msdownload"],"js":["application\/x-javascript"],
    "mp3":["audio\/mpeg","audio\/mpg","audio\/mpeg3","audio\/mp3"],"rar":["application\/x-rar","application\/rar",
    "application\/x-rar-compressed"],"gzip":["application\/x-gzip"],"hqx":["application\/mac-binhex40",
    "application\/mac-binhex","application\/x-binhex40","application\/x-mac-binhex40"],
    "cpt":["application\/mac-compactpro"],"bin":["application\/macbinary","application\/mac-binary",
    "application\/x-binary","application\/x-macbinary"],"oda":["application\/oda"],
    "ai":["application\/postscript"],"smil":["application\/smil"],"mif":["application\/vnd.mif"],
    "wbxml":["application\/wbxml"],"wmlc":["application\/wmlc"],"dcr":["application\/x-director"],
    "dvi":["application\/x-dvi"],"gtar":["application\/x-gtar"],"php":["application\/x-httpd-php",
    "application\/php","application\/x-php","text\/php","text\/x-php","application\/x-httpd-php-source"],
    "swf":["application\/x-shockwave-flash"],"sit":["application\/x-stuffit"],"z":["application\/x-compress"],
    "mid":["audio\/midi"],"aif":["audio\/x-aiff","audio\/aiff"],"ram":["audio\/x-pn-realaudio"],
    "rpm":["audio\/x-pn-realaudio-plugin"],"ra":["audio\/x-realaudio"],"rv":["video\/vnd.rn-realvideo"],
    "jp2":["image\/jp2","video\/mj2","image\/jpx","image\/jpm"],"tiff":["image\/tiff"],
    "eml":["message\/rfc822"],"pem":["application\/x-x509-user-cert","application\/x-pem-file"],
    "p10":["application\/x-pkcs10","application\/pkcs10"],"p12":["application\/x-pkcs12"],
    "p7a":["application\/x-pkcs7-signature"],"p7c":["application\/pkcs7-mime","application\/x-pkcs7-mime"],"p7r":["application\/x-pkcs7-certreqresp"],"p7s":["application\/pkcs7-signature"],"crt":["application\/x-x509-ca-cert","application\/pkix-cert"],"crl":["application\/pkix-crl","application\/pkcs-crl"],"pgp":["application\/pgp"],"gpg":["application\/gpg-keys"],"rsa":["application\/x-pkcs7"],"ics":["text\/calendar"],"zsh":["text\/x-scriptzsh"],"cdr":["application\/cdr","application\/coreldraw","application\/x-cdr","application\/x-coreldraw","image\/cdr","image\/x-cdr","zz-application\/zz-winassoc-cdr"],"wma":["audio\/x-ms-wma"],"vcf":["text\/x-vcard"],"srt":["text\/srt"],"vtt":["text\/vtt"],"ico":["image\/x-icon","image\/x-ico","image\/vnd.microsoft.icon"],"csv":["text\/x-comma-separated-values","text\/comma-separated-values","application\/vnd.msexcel"],"json":["application\/json","text\/json"]}';
    $all_mimes = json_decode($all_mimes,true);
    foreach ($all_mimes as $key => $value) {
        if(array_search($mime,$value) !== false) return $key;
    }
    return false;
}

add_action( 'rest_api_init', 'get_country_app' );

function get_country_app() {
	register_rest_route('wp/v2', 'app/country', array(
		'methods' => 'GET',
		'callback' => 'app_get_country_app',
	));
}

function app_get_country_app(){
	
	require_once get_template_directory().'/IP2Location.php';
	global $wpdb;

	$ipAddress = $_SERVER['REMOTE_ADDR'];

	$db = new \IP2Location\Database(get_template_directory().'/databases/IP2LOCATION-LITE-DB1.BIN', \IP2Location\Database::FILE_IO);

	$records = $db->lookup($ipAddress, \IP2Location\Database::ALL);
	
	switch ($records['countryCode']) {
		case 'JM': $varCountry = "_jamaica" ; break;
		case 'TC': $varCountry = "_turksandcaicos" ; break;
		case 'LC': $varCountry = "_stlucia" ; break;
		//new countries to be added
		case 'BB': $varCountry = "_barbados" ; break;
		case 'BS': $varCountry = "_bahamas" ; break;
		case 'KY': $varCountry = "_cayman" ; break;
		case 'TT': $varCountry = "_trinidad" ; break;
		default: $varCountry = ""; break;
	}

	$all = array(
		"code" => $varCountry
	);
	
	$obj = new \stdClass();
	$obj->country = $all;
	
	$response = new WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);
	return $response;

}

function get_name_country(){

	$headers = array (); 
	foreach ($_SERVER as $name => $value) 
	{ 
		if (substr($name, 0, 5) == 'HTTP_') 
		{ 
			$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
		} 
	} 
		
	$headers = $headers;
	if (isset($headers['Country'])) {
		$country = $headers['Country'];
	} else {
		$objCountry = app_get_country_app();
		$country = $objCountry->data->country['code'];
	}	
	$countryName = "";
	if ( $country) {
		switch ($country) {
			case '_jamaica': $countryName = "Jamaica"; break;
			case '_stlucia': $countryName = "St. Lucia"; break;
			case '_turksandcaicos': $countryName = "Turks and Caicos"; break;
			case '_barbados': $countryName = "Barbados"; break;
			case '_bahamas': $countryName = "Bahamas"; break;
			case '_cayman': $countryName = "Cayman"; break;
			default:  $countryName = ""; break;
		}
	}    

	return $countryName; 

}

function get_slug_country(){
	$headers = array (); 
	foreach ($_SERVER as $name => $value) 
	{ 
		if (substr($name, 0, 5) == 'HTTP_') 
		{ 
			$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
		} 
	} 
        
	$headers = $headers;
	$header_country = @$headers['Country'];  
	
	return $header_country; 

}

function get_country_header(){
	$headers = array (); 
	foreach ($_SERVER as $name => $value) 
	{ 
		if (substr($name, 0, 5) == 'HTTP_') 
		{ 
			$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
		} 
	} 

	$headers = $headers;
	$return = (isset($headers['Country'])) ? true : false;
	return $return;
}

?>