<?php

class Helpers {

    public $urlBase;

    static function jsonResponse( $error = true, $message = '', $data = array() ) {

        $app               = Slim::getInstance();
        $response          = new stdClass();
        $response->error   = $error;
        $response->message = $message;
        $response->data    = $data;

        $app->response()->header('Content-Type', 'application/json');
        return $app->response()->body( json_encode($response) );

    }


    public static function criptografa($senha) {
        return hash('sha512',$senha);
    }  

    public static function getUrlBase() {
         return 'https://buzz-caribbean.com';
    } 


    public static function formatData($data) {
         $partes = explode("-", $data);
         $data = $partes[2].'-'.$partes[1]."-".$partes[0]; 
         return $data;
    }  

    public static function randomString($length = 10)
    {
        $chars = 'abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789';
        $string = '';
 
        $alphaLength = strlen($chars) - 1;
 
        for ($i = 0; $i < $length; ++$i) {
            $n = rand(0, $alphaLength);
            $string .= $chars[$n];
        }
 
        return $string;
    }
}