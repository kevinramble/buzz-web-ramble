<?php


// WP-JSON POSTS  OUT
add_action( 'rest_api_init', 'posts_video' );

function posts_video() {
	register_rest_route('wp/v2', 'app/posts/videos/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_videos',
	));
}

function app_posts_videos($request){
	// $teste = unserialize("a:5:{i:0;i:171;i:1;i:243;i:2;i:202;i:3;i:203;i:4;i:266;}");
	// echo "<pre>";print_r($teste);die();
	require_once('Jwt.php');

	global $wpdb;
	$favorit  = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];

		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
		
	}
	
	$tax_query = null;
	$nameCountry = get_name_country();

	if (@$nameCountry && @$nameCountry != '') { 
		$getCountry = get_term_by('name', $nameCountry, 'countries');
		$countryID = $getCountry->term_id;
		$tax_query = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'countries',
				'field'    => 'term_id',
				'terms'    => $countryID,
			));
	} 
	

	$args = array(
		'post_type' => "video",
		'post_status' =>  'publish',
		'nopaging ' => true,
		'posts_per_page' => -1,
		'orderby'        => 'post_date',
		'order'          => 'DESC'
		//'tax_query' => $tax_query
	);

	$posts = get_posts($args);
	if (empty($posts)) {
		return new WP_Error( 'empty_video', 'there is no post in this category', array('status' => 404) );
	}

	foreach($posts as $post) {
		
		$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
		if ($postmeta) {
			$fullprice = '';
			for ($i=0; $i< count($postmeta); $i++) {
				
				$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
				$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
				
			}
        }
        $sub = get_the_terms( $post->ID, 'sub-category' );
		$cat = get_the_terms( $post->ID, 'category' );
		
		$guid = get_url_base( $post->ID);

		
		$dados[] = array(
			"ID" => $post->ID,
			"post_date" => get_the_date('M d, Y', $post->ID),
			"post_content" => $post->post_content,
			"post_title" => $post->post_title,
			"post_modified" => $post->post_modified,
			"guid" => $guid,
			"post_type" => $post->post_type,
            "guid" => $guid,
            "category" => @$cat[0]->name,
            "sub_category" => @$sub[0]->name,
			"image" => @$thumb[0]->guid,
			"thumb" => @$thumb[0]->guid);

	}

	$obj = new \stdClass();
	$obj->posts = $dados;
	
	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;

}	

// WP-JSON POSTS  OUT ID
add_action( 'rest_api_init', 'posts_video_id' );

function posts_video_id() {
	register_rest_route('wp/v2', 'app/posts/video/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_videos_id',
	));
}

function app_posts_videos_id($request){
	require_once('Jwt.php');

	global $wpdb;
	$favorit = $offer_taked = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];

		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
	}

	$args = array(
        'ID' => $request['post_id'],
		'post_type' => "out",
	);

	$post =  get_post( $request['post_id'] );
	if (empty($post)) {
		return new WP_Error( 'empty_video', 'there is no post in this category', array('status' => 404) );
	}
            
	$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
	if ($postmeta) {
		$fullprice = '';
		for ($i=0; $i< count($postmeta); $i++) {
			
			$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
			$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
			
			
		}
	}

	
	$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
	$guid = get_url_base($post->ID);

    $sub = get_the_terms( $post->ID, 'sub-category' );
    $cat = get_the_terms( $post->ID, 'category' );

	$dados[] = array(
		"ID" => $post->ID,
		"post_date" => $post->post_date,
		"post_content" => $post->post_content,
		"post_title" => $post->post_title,
		"post_modified" => $post->post_modified,
		"guid" => $guid,
		"post_type" => $post->post_type,
		"category" => @$cat[0]->name,
		"sub_category" => @$sub[0]->name,
		"favorit" => $favorit,
		"guid" => $guid,
		"image" => @$thumb[0]->guid,
		"thumb" => @$thumb[0]->guid);

	$obj = new \stdClass();
	$obj->posts = $dados;
	
	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;

}	

