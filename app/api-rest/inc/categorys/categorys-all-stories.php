<?php

switch ($request['category']) {
    case 'hot': $catID = 2; break;
    case 'news': $catID = 4; break;
    case 'life': $catID = 3; break;
    // case 'hot': $catID = 4; break;
    // case 'news': $catID = 2; break;
    // case 'life': $catID = 3; break;
}

$paged = 1;
$posts = $wpdb->get_results( "SELECT *
                                FROM wp_posts LEFT JOIN wp_term_relationships ON wp_posts.ID = wp_term_relationships.object_id
                                WHERE wp_posts.post_status =  'publish'
                                    AND (wp_posts.post_type =  'post' OR wp_posts.post_type =  'app-post' )
                                    AND  (wp_term_relationships.term_taxonomy_id = $catID )
                                    ORDER BY wp_posts.post_date DESC limit 50");
$args = array('cat' => $catID, 'post_type' => 'post', 'posts_per_page' => 20, 'orderby' => 'date', 'order' => 'DESC'); query_posts($args);
if ($posts) {
    foreach ($posts as $post) {
        if(has_post_thumbnail($post->ID)){
            $image = get_the_post_thumbnail_url($post->ID);
        } else {
            $image = catch_that_image($post->ID);
        }

        $postTitle = get_the_title($post->ID);
        $title = $postTitle;
        $type =  get_post_type($post->ID);
        $subcategory = wp_get_post_terms($post->ID, 'sub-category');

        $image = $image;
        $authorID = get_the_author_meta('display_name');
        $date = get_the_date('M d, Y');
        $buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
        $buttonType = ($type == 'video') ? ' is-video' : '';


        $thumb = '';
        if (!strpos($image, "nothing.png")) {
            if ($image) {
                //$imagem = str_replace("-1060x655", "",$image);
                $imagem = preg_replace("/([-]\d{3,4}x\d{3}[.])/", ".", $image);
                $thumbnail = ($imagem) ? explode(".", $imagem) : '';
                $ext = end($thumbnail);
                $thumb = str_replace(".".$ext,"-150x150.".$ext, $imagem);
            }
        } else {
            $image = "";
        }

        $guid = get_url_base($post->ID);
        $favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;

        $dadosAllStories[] = array(
            "ID" => $post->ID,
            "type" => $type,
            "postTitle" => $postTitle,
            "title" => $title,
            "subcategory" => @$subcategory[0]->name,
            "image" => $image,
            "favorit" => $favorit,
            "thumb" => $thumb,
            "authorID" => $authorID,
            "buttonTitle" => $buttonTitle,
            "buttonType" => $buttonType,
            "guid" => $guid,
        );


    }
}

