<?php
    if (get_country_header()) { 
        $country = get_slug_country();
    } else { 
        $objCountry = app_get_country_app();
        $country = $objCountry->data->country['code'];
    }
	$minimumPosts = 3;
	$postAmount = null;
    $totalPosts = 0;
    $currentslug = $request['category'];
	$functionName = 'trending_now_' . $currentslug . $country;

	for ($d = 1; $d <= 9; $d++){
		$postID = $functionName('trending_now_'.$currentslug . $country.'_post_id'.$d);
		if($postID) $postAmount[] = $postID;
	}

	if($postAmount) $totalPosts = count($postAmount);
	//if($totalPosts >= $minimumPosts) :
        for ($c = 1; $c <= 9; $c++) :
            $postID = $functionName('trending_now_'.$currentslug . $country.'_post_id'.$c);
            $type = $functionName('trending_now_'.$currentslug . $country.'_type'.$c);
            $postTitle = $functionName('trending_now_'.$currentslug . $country.'_post_title'.$c);
            $customTitle = $functionName('trending_now_'.$currentslug . $country.'_title'.$c);
            $title = ($customTitle) ? $customTitle : $postTitle;
            $customImage = $functionName('trending_now_'.$currentslug . $country.'_img'.$c);
            $image = '';
            $buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
            $buttonType = ($type == 'video') ? ' is-video' : '';

            if(!$customImage){
                if(has_post_thumbnail($postID)){
                    $image = get_the_post_thumbnail_url($postID);
                } else {
                    $image = catch_that_image($postID);
                }
            } else {
                $image = $customImage;
            }


            $thumb = '';
            if (!strpos($image, "nothing.png")) {
                if ($image) {
                    $imagem = preg_replace("/([-]\d{3,4}x\d{3}[.])/", ".",$image);
                    $thumbnail = ($imagem) ? explode(".", $imagem) : '';
                    $ext = end($thumbnail);
                    $thumb = str_replace(".".$ext,"-150x150.".$ext, $imagem);
                }
            } else {
                $image = "";
            }

            $guid = get_url_base($postID);
            $favorit = (@in_array($postID, $arrFavorites)) ? true : false;

            if($postID) :
                $dadosTrending[] = array(
                    "ID" => $postID,
                    "type" => $type,
                    "postTitle" => $postTitle,
                    "customTitle" => $customTitle,
                    "title" => $title,
                    "customImage" => $customImage,
                    "image" => $image,
                    "favorit" => $favorit,
                    "thumb" => $thumb,
                    "authorID" => $authorID,
                    "buttonTitle" => $buttonTitle,
                    "buttonType" => $buttonType,
                    "guid" => $guid
                );
            endif;
        endfor;
    //endif;

