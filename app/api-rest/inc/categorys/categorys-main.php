<?php
    if (get_country_header()) { 
        $country = get_slug_country();
    } else { 
        $objCountry = app_get_country_app();
        $country = $objCountry->data->country['code'];
    }
    
    $currentslug = $request['category'];
    $functionName = 'main_banner_' . $currentslug . $country;
    
    for($c = 1; $c <= 4; $c++){
        $postID = $functionName('main_banner_'.$currentslug. $country . '_post_id'.$c);
        $type = $functionName('main_banner_'.$currentslug. $country .'_type'.$c);
        $postTitle = $functionName('main_banner_'.$currentslug. $country .'_post_title'.$c);
        $customTitle = $functionName('main_banner_'.$currentslug. $country .'_title'.$c);
        $title = ($customTitle) ? $customTitle : $postTitle;
        $customImage = $functionName('main_banner_'.$currentslug. $country .'_img'.$c);
        $image = '';
        $authorID = get_post_field('post_author', $postID);
        $postAuthor = get_the_author_meta('display_name', $authorID);
        $buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
        $buttonType = ($type == 'video') ? ' is-video' : '';

        $sub = get_the_terms( $postID, 'sub-category' );
		$cat = get_the_terms( $postID, 'category' );

        if(!$customImage){
            if(has_post_thumbnail($postID)){
                $image = get_the_post_thumbnail_url($postID);
            } else {
                $image = catch_that_image($postID);
            }
        } else {
            $image = $customImage;
        }

        $thumb = '';
        if (!strpos($image, "nothing.png")) {
            if ($image) {
                $imagem = preg_replace("/([-]\d{3,4}x\d{3}[.])/", ".",$image);
                $thumbnail = ($imagem) ? explode(".", $imagem) : '';
                $ext = end($thumbnail);
                $thumb = str_replace(".".$ext,"-150x150.".$ext, $imagem);
            }
        } else {
            $image = "";
        }

        $guid = get_url_base($postID);
        $favorit = (@in_array($postID, $arrFavorites)) ? true : false;

        if ($postID) {
            $dadosMain[] = array(
                "ID" => $postID,
                "type" => $type,
                "postTitle" => $postTitle,
                "customTitle" => $customTitle,
                "title" => $title,
                "customImage" => $customImage,
                "image" => $image,
                "favorit" => $favorit,
                "thumb" => $thumb,
                "authorID" => $authorID,
                "guid" => $guid,
                "category" => @$cat[0]->name,
                "subcategory" => @$sub[0]->name,

            );

        }
    } // for

?>