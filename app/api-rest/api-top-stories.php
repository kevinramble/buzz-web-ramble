<?php

//TOP STORIES


// WP-JSON FEATURES
add_action( 'rest_api_init', 'app_featured_route' );

function app_featured_route() {
	register_rest_route('wp/v2', 'app/featured', array(
		'methods' => 'GET',
		'callback' => 'app_app_featured',
	));
}

function app_app_featured($request){
	require_once('Jwt.php');

	global $wpdb;
	$favorit = false;

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) {
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		}

		$user = $tokenValido['user'];
		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts'];
	}

    if (get_country_header()) { 
        $country = get_slug_country();
    } else { 
        $objCountry = app_get_country_app();
        $country = $objCountry->data->country['code'];
    }


	for($f = 1; $f <= 20; $f++){

		$subCategoria = '';
		$subCat = get_option('app_featured'.$country.'_subcat'.$f);
		if ($subCat) {
			$subCategoria = $wpdb->get_results( "SELECT wp_terms.name FROM wp_terms WHERE wp_terms.term_id = ". $subCat );
		}


		$category = '';
		$cat = get_option('app_featured'.$country.'_cat'.$f);
		if ($cat) {
			$category = $wpdb->get_results( "SELECT wp_terms.name FROM wp_terms WHERE wp_terms.term_id = ". $cat );
		}


		$type = get_option('app_featured'.$country.'_type'.$f);
		$postID = get_option('app_featured'.$country.'_post_id'.$f);
		$postTitle = get_option('app_featured'.$country.'_post_title'.$f);
		$customTitle = get_option('app_featured'.$country.'_title'.$f);
		$title = ($customTitle) ? $customTitle : $postTitle;
		$customImage = get_option('app_featured'.$country.'_img'.$f);
		$style = get_option('app_featured'.$country.'_style'.$f);
		$image = '';
		$buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
		if(!$customImage){
			if(has_post_thumbnail($postID)){
				$image = get_the_post_thumbnail_url($postID);
			} else {
				$image = catch_that_image($postID);
			}
		} else {
			$image = $customImage;
		}

		$thumb = '';
		if (!strpos($image, "nothing.png")) {
			if ($image) {
				$imagem = preg_replace("/([-]\d{3,4}x\d{3}[.])/", ".",$image);
				$thumbnail = ($imagem) ? explode(".", $imagem) : '';
				$ext = end($thumbnail);
				$thumb = str_replace(".".$ext,"-150x150.".$ext, $imagem);
			}
		} else {
			$image = '';
		}

		$postOrigin = get_post( $postID );

		$dateNow = date("Y-m-d H:i:s");
		$start_date = new DateTime($dateNow);
		$since_start = $start_date->diff(new DateTime($postOrigin->post_date));

		$since = get_date_format($since_start);


		$favorit = (@in_array($postID, $arrFavorites)) ? true : false;

		$guid = get_url_base($postID);

		if($postID){
			$postInfo[] = array(
				'postID' => $postID,
				'title' => $title,
				'image' => $image,
				'thumb' => $thumb,
				'buttonTitle' => $buttonTitle,
				'category' => $category[0]->name,
				'sub_categoty' => @$subCategoria[0]->name,
				'favorit' => $favorit,
				'guid' => $guid,
				'style' => $style,
				'date' => $since,
			);
		}
	}

	$obj = new \stdClass();
	$obj->posts = $postInfo;

	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;
}




// WP-JSON BREAKING NEWS
add_action( 'rest_api_init', 'app_breaking_route' );

function app_breaking_route() {
	register_rest_route('wp/v2', 'app/breaking-news', array(
		'methods' => 'GET',
		'callback' => 'app_app_breaking',
	));
}

function app_app_breaking($request){
	require_once('Jwt.php');

	global $wpdb;
	$favorit = false;

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) {
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		}

		$user = $tokenValido['user'];
		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts'];
	}


	for($f = 1; $f <= 20; $f++){

		$subCategoria = '';
		$subCat = get_option('app_breaking_subcat'.$f);
		if ($subCat) {
			$subCategoria = $wpdb->get_results( "SELECT wp_terms.name FROM wp_terms WHERE wp_terms.term_id = ". $subCat );
		}


		$category = '';
		$cat = get_option('app_breaking_cat'.$f);
		if ($cat) {
			$category = $wpdb->get_results( "SELECT wp_terms.name FROM wp_terms WHERE wp_terms.term_id = ". $cat );
		}


		$type = get_option('app_breaking_type'.$f);
		$postID = get_option('app_breaking_post_id'.$f);
		$postTitle = get_option('app_breaking_post_title'.$f);
		$customTitle = get_option('app_breaking_title'.$f);
		$title = ($customTitle) ? $customTitle : $postTitle;
		$customImage = get_option('app_breaking_img'.$f);
		$style = get_option('app_breaking_style'.$f);
		$image = '';
		$buttonTitle = ($type == 'video') ? 'Watch now' : 'Read Now';
		if(!$customImage){
			if(has_post_thumbnail($postID)){
				$image = get_the_post_thumbnail_url($postID);
			} else {
				$image = catch_that_image($postID);
			}
		} else {
			$image = $customImage;
		}

		$thumb = '';
		if (!strpos($image, "nothing.png")) {
			if ($image) {
				$imagem = preg_replace("/([-]\d{3,4}x\d{3}[.])/", ".",$image);
				$thumbnail = ($imagem) ? explode(".", $imagem) : '';
				$ext = end($thumbnail);
				$thumb = str_replace(".".$ext,"-150x150.".$ext, $imagem);
			}
		} else {
			$image = '';
		}

		$postOrigin = get_post( $postID );

		$dateNow = date("Y-m-d H:i:s");
		$start_date = new DateTime($dateNow);
		$since_start = $start_date->diff(new DateTime($postOrigin->post_date));

		$since = get_date_format($since_start);

		$favorit = (@in_array($postID, $arrFavorites)) ? true : false;

		$guid = get_url_base($postID);

		if($postID){
			$postInfo[] = array(
				'postID' => $postID,
				'title' => $title,
				'image' => $image,
				'thumb' => $thumb,
				'buttonTitle' => $buttonTitle,
				'category' => $category[0]->name,
				'sub_categoty' => @$subCategoria[0]->name,
				'favorit' => $favorit,
				'guid' => $guid,
				'style' => $style,
				'date' => $since,
			);
		}
	}

	$obj = new \stdClass();
	$obj->posts = $postInfo;

	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;
}

function get_date_format($since_start) {
	if ($since_start->y > 0) {
		$since = $since_start->y ." years ago";
		if ($since_start->y == 1) {
			$since = $since_start->y ." year ago";
		}
	} elseif ($since_start->m > 0) {
		$since = $since_start->m ." months ago";
		if ($since_start->m == 1) {
			$since = $since_start->m ." month ago";
		}
	} elseif ($since_start->d > 0) {
		$since = $since_start->d ." days ago";
		if ($since_start->d == 1) {
			$since = $since_start->d ." day ago";
		}
	} elseif ($since_start->h > 0) {
		$since = $since_start->h ." hours ago";
		if ($since_start->h == 1) {
			$since = $since_start->h ." hour ago";
		}
	} elseif ($since_start->i > 0) {
		$since = $since_start->i ." minutes ago";
		if ($since_start->i == 1) {
			$since = $since_start->i ." minute ago";
		}
	} elseif ($since_start->s > 0) {
		$since = $since_start->s ." seconds ago";
		if ($since_start->s == 1) {
			$since = $since_start->s ." second ago";
		}
	}
	// $since_start->days.' days total<br>';
	// $since_start->y.' years<br>';
	// $since_start->m.' months<br>';
	// $since_start->d.' days<br>';
	// $since_start->h.' hours<br>';
	// $since_start->i.' minutes<br>';
	// $since_start->s.' seconds<br>';
	return $since;
}