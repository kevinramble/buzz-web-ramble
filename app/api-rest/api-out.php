<?php


// WP-JSON POSTS  OUT
add_action( 'rest_api_init', 'posts_out' );

function posts_out() {
	register_rest_route('wp/v2', 'app/posts/outs/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_outs',
	));
}

function app_posts_outs($request){
	
	require_once('Jwt.php');

	global $wpdb;
	$favorit  = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];

		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
		
	}

    if (get_country_header()) { 
        $country = get_slug_country();
    } else { 
        $objCountry = app_get_country_app();
        $country = $objCountry->data->country['code'];
    }
	if($country == '_jamaica'){
		$countryQuery = array(
			'key' => 'out_country',
			'value' => 'Jamaica',
			'compare' => '='
		);
	} elseif($country == '_stlucia'){
		$countryQuery = array(
			'key' => 'out_country',
			'value' => 'St. Lucia',
			'compare' => '='
		);
	} elseif($country == '_turksandcaicos'){
		$countryQuery = array(
			'key' => 'out_country',
			'value' => 'Turks and Caicos',
			'compare' => '='
		);
	} elseif($country == '_barbados'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Barbados',
			'compare' => '='
		);
	} elseif($country == '_bahamas'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Bahamas',
			'compare' => '='
		);
	} elseif($country == '_cayman'){
		$countryQuery = array(
			'key' => 'job_country',
			'value' => 'Cayman',
			'compare' => '='
		);
	} else {
		$countryQuery = '';
	}

	$args = array(
		'post_type' => "out",
		'post_status' =>  'publish',
		'nopaging ' => true,
		'posts_per_page' => -1,
		'orderby'        => 'post_date',
		'order'          => 'DESC'
		/*'meta_query' => array('relation' => 'AND', 
							array('key' => 'out_date', 
								  'value' => date('Y-m-d'), 
								  'compare' => '>='), 
							$countryQuery)*/
	);

	$posts = get_posts($args);
	if (empty($posts)) {
		return new WP_Error( 'empty_out', 'there is no post in this category', array('status' => 404) );
	}

	foreach($posts as $post) {
		
		$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
		if ($postmeta) {
			$fullprice = $mapurl = $address = $description = $place = $date = $time = $_thumbnail_id = $thumb = '';
			for ($i=0; $i< count($postmeta); $i++) {
				
				$mapurl = ($postmeta[$i]->meta_key == 'out_mapurl') ? $postmeta[$i]->meta_value : $mapurl;
				$address = ($postmeta[$i]->meta_key == 'out_address') ? $postmeta[$i]->meta_value : $address;
				$description = ($postmeta[$i]->meta_key == 'out_description') ? $postmeta[$i]->meta_value : $description;
				$place = ($postmeta[$i]->meta_key == 'out_country') ? $postmeta[$i]->meta_value : $place;
				$date = ($postmeta[$i]->meta_key == 'out_date') ? $postmeta[$i]->meta_value : $date;
				$time = ($postmeta[$i]->meta_key == 'out_time') ? $postmeta[$i]->meta_value : $time;
				$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
				$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
				
			}
		}

		//data expirada
		$expired = false;
		if (strtotime(date("Y-m-d")) <= strtotime(date("Y-m-d", strtotime($date)))) {
			
			$date1= date("Y-m-d", strtotime($date));
			$date2= date("Y-m-d") ;
			
			$data1 = new DateTime( $date1 );
			$data2 = new DateTime( $date2 );

			$intervalo = $data2->diff( $data1 );
			
			$expired = false;
		} else {
			$expired = true;
		}

		
		$category = get_the_terms( $post->ID, 'out-category');
		$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
		$guid = get_url_base( $post->ID);

		
		$dados[] = array(
			"ID" => $post->ID,
			"post_date" => $post->post_date,
			"description" => $description,
			"post_title" => $post->post_title,
			"post_modified" => $post->post_modified,
			"date" => $date,
			"time" => $time,
			"guid" => $guid,
			"address" => $address,
			"post_type" => $post->post_type,
			"category" => @$category[0]->name,
			"sub_category" => @$sub[0]->name,
			"favorit" => $favorit,
			"guid" => $guid,
			"place" => $place,
			"mapurl" => $mapurl,
			"image" => @$thumb[0]->guid,
			"thumb" => @$thumb[0]->guid);
		

	}

	$obj = new \stdClass();
	$obj->posts = $dados;
	
	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;

}	

// WP-JSON POSTS  OUT ID
add_action( 'rest_api_init', 'posts_out_id' );

function posts_out_id() {
	register_rest_route('wp/v2', 'app/posts/out/', array(
		'methods' => 'GET',
		'callback' => 'app_posts_outs_id',
	));
}

function app_posts_outs_id($request){
	require_once('Jwt.php');

	global $wpdb;
	$favorit = $offer_taked = false; 

	//validar token
	$jwt = new Jwt();
	$token = $jwt->getTokenHeader();

	if ($token) { 
		$tokenValido = $jwt->validateCode($token);

		if (!$tokenValido['callback']) {
			$response['code'] = 401;
			$response['message'] = __( $tokenValido['message'], "wp-rest-user");
			return new WP_REST_Response($response, 401);
		} 

		$user = $tokenValido['user'];

		$favorites = $wpdb->get_results( "SELECT meta_value as favorites FROM wp_usermeta WHERE meta_key = 'simplefavorites' AND user_id = ".$user->uid );
		$favorites = unserialize($favorites[0]->favorites);
		$arrFavorites = $favorites[0]['posts']; 
	}

	$args = array(
        'ID' => $request['post_id'],
		'post_type' => "out",
	);

	$post =  get_post( $request['post_id'] );
	if (empty($post)) {
		return new WP_Error( 'empty_out', 'there is no post in this category', array('status' => 404) );
	}
            
	$postmeta = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE post_id = ".$post->ID );
	if ($postmeta) {
		$fullprice = '';
		for ($i=0; $i< count($postmeta); $i++) {
			
			$mapurl = ($postmeta[$i]->meta_key == 'out_mapurl') ? $postmeta[$i]->meta_value : $mapurl;
			$address = ($postmeta[$i]->meta_key == 'out_address') ? $postmeta[$i]->meta_value : $address;
			$description = ($postmeta[$i]->meta_key == 'out_description') ? $postmeta[$i]->meta_value : $description;
			$place = ($postmeta[$i]->meta_key == 'out_country') ? $postmeta[$i]->meta_value : $place;
			$date = ($postmeta[$i]->meta_key == 'out_date') ? $postmeta[$i]->meta_value : $date;
			$time = ($postmeta[$i]->meta_key == 'out_time') ? $postmeta[$i]->meta_value : $time;
			$_thumbnail_id = ($postmeta[$i]->meta_key == '_thumbnail_id') ? $postmeta[$i]->meta_value : $_thumbnail_id;
			$thumb = ($_thumbnail_id) ? $wpdb->get_results( "SELECT guid FROM wp_posts WHERE post_type = 'attachment' AND ID = ".$_thumbnail_id ) : '';
			
			
		}
	}

	
	$favorit = (@in_array($post->ID, $arrFavorites)) ? true : false;
	$guid = get_url_base($post->ID);

	$category = get_the_terms( $post->ID, 'out-category');

	$dados[] = array(
		"ID" => $post->ID,
		"post_date" => $post->post_date,
		"description" => $description,
		"post_title" => $post->post_title,
		"post_modified" => $post->post_modified,
		"date" => $date,
		"time" => $time,
		"guid" => $guid,
		"address" => $address,
		"post_type" => $post->post_type,
		"category" => @$category[0]->name,
		"sub_category" => @$sub[0]->name,
		"favorit" => $favorit,
		"guid" => $guid,
		"place" => $place,
		"mapurl" => $mapurl,
		"image" => @$thumb[0]->guid,
		"thumb" => @$thumb[0]->guid);

	$obj = new \stdClass();
	$obj->posts = $dados;
	
	$response = new \WP_REST_Response();
	$response->set_data($obj);
	$response->set_status(200);

	return $response;

}	

