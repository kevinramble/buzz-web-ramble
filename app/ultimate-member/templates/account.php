<?php include_once('inc/user-menu.php'); ?>

<?php
$args['cover_enabled'] = '';
$args['photosize'] = '';
$args['show_name'] = '';
$args['show_bio'] = '';
?>

<section class="l-page-settings">
    <div class="c-contact-form">
        <div class="container">
            <h1 class="c-contact-form__title text-md-left">Account settings</h1>

            <div class="row">
                <div class="col-md-3">
                    <div class="c-contact-form__file mx-auto mx-md-0 mb-2">
                        <?php do_action('um_profile_header', $args); ?>
                    </div>
                </div>

                <div class="col-md-9">
                    <form method="post" action="">
                        <?php do_action('um_account_page_hidden_fields', $args); ?>
                        
                        <div class="um-account-side account-tabs">
                            <ul>
                                <?php foreach ( UM()->account()->tabs as $id => $info ) {
                                    if ( isset( $info['custom'] ) || UM()->options()->get( "account_tab_{$id}" ) == 1 || $id == 'general' ) { ?>
                                        <li>
                                            <a data-tab="<?php echo $id ?>" href="<?php echo UM()->account()->tab_link( $id ); ?>" class="um-account-link <?php if ( $id == UM()->account()->current_tab ) echo 'current'; ?>">
                                                <span class="um-account-title uimob800-hide"><?php echo $info['title']; ?></span>
                                            </a>
                                        </li>

                                    <?php }
                                } ?>
                            </ul>
                        </div><!-- tabs -->
                        
                        <?php do_action('um_before_form', $args ); ?>
                        <div class="um-account-main" data-current_tab="<?php echo UM()->account()->current_tab; ?>">
                            <?php foreach ( UM()->account()->tabs as $id => $info ) {
                                $current_tab = UM()->account()->current_tab;

                                if ( isset( $info['custom'] ) || UM()->options()->get( 'account_tab_' . $id ) == 1 || $id == 'general' ) { ?>
                                    <div class="um-account-tab um-account-tab-<?php echo $id ?>" data-tab="<?php echo $id ?>">
                                        <?php
                                            $info['with_header'] = true;
                                            UM()->account()->render_account_tab( $id, $info, $args );
                                        ?>
                                    </div>
                                <?php }
                            } ?>

                            <!-- CUSTOM CODE -->
                            <?php if( function_exists( 'um_mailchimp_account_tab' ) ) { ?>
                                <?php echo um_mailchimp_account_tab( '' ); ?>
                                <script type="text/javascript">
                                    jQuery(function(){
                                        jQuery('input[name^="um-mailchimp"]').on('change',function(e){
                                            var request = {
                                                _um_account: '1'
                                            };
                                            jQuery('input[name^="um-mailchimp"]').each(function(i,item){
                                                if( item.checked ){
                                                    request[item.name] = item.value;
                                                }
                                            });
                                            console.log('request', request);
                                            console.log(window.location.href);

                                            jQuery.post( window.location.href, request )
                                                .done(function() {
                                                    console.log( "done" );
                                                })
                                                .fail(function() {
                                                    console.log( "error" );
                                                });
                                        });
                                    });
                                </script>
                                <div class="um-clear"></div>
                            <?php } ?>
                            <!-- /CUSTOM CODE -->
                        </div>
                    </form>

                    <?php do_action( 'um_after_account_page_load' ); ?>
                </div>
            </div><!-- .row -->
        </div><!-- .container -->
    </div>
</section>