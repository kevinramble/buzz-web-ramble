<?php
    $articlesTab = ($type == '' || $type == 'articles') ? 'is-active' : '';
    $outsTab = ($type == 'outs') ? 'is-active' : '';
    $jobsTab = ($type == 'jobs') ? 'is-active' : '';
    $offersTab = ($type == 'offers') ? 'is-active' : '';
?>

<div class="c-filter-logged">
    <a href="?tab=favorites&type=articles" class="c-filter-logged__item c-filter-logged__item--articles <?php echo $articlesTab; ?>">Articles</a>
    <a href="?tab=favorites&type=outs" class="c-filter-logged__item c-filter-logged__item--events <?php echo $outsTab; ?>">Outs</a>
	<a href="?tab=favorites&type=jobs" class="c-filter-logged__item c-filter-logged__item--carrers <?php echo $jobsTab; ?>">Jobs</a>
    <a href="?tab=favorites&type=offers" class="c-filter-logged__item c-filter-logged__item--offers <?php echo $offersTab; ?>">Offers</a>
</div>