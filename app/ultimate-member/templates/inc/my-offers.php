<div class="l-page-list__results mt-4">
	<div class="container">
		<?php
			$allOffers = get_redeemedoffers();

			if($allOffers) :
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				
				$args = array('post_type' => 'offer', 'post__in' => $allOffers, 'posts_per_page' => 8, 'paged' => $paged, 'isoffers' => 1);
				$wp_query = new WP_Query($args);

				if($wp_query->have_posts()) : ?>
					<div class="row js-posts-list">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post();
							global $post;
							loop_redeemedoffer($post->ID);
						endwhile; ?>
					</div>
				<?php endif;

				// load more ajax
				$wp_query->query_vars['search_orderby_title'] = '';
				$load_posts = $wp_query->query_vars; $load_current_page = $wp_query->query_vars['paged']; $load_max_page = $wp_query->max_num_pages;
				loadmore_button($load_posts, $load_current_page, $load_max_page);
				if($wp_query->max_num_pages > 1){ ?>
					<span class="js-loadmore c-bt-load">Load more</span>
				<?php } else { ?>
					<span class="js-loadmore c-bt-load hidden">Load more</span>
				<?php } // end load more ajax

				wp_reset_query(); wp_reset_postdata();
			else :
				echo '<h4 class="title-nocontent">Nothing to show.</h4>';
			endif;
		?>
		
	</div>
</div>