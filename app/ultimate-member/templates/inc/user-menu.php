<div class="c-header-logged">
	<div class="container">
		<img src="<?php echo get_avatar_url(um_user('ID'), 120); ?>" class="c-header-logged__avatar">
		<p class="c-header-logged__name"><?php echo um_user('display_name'); ?></p>

		<nav class="c-header-logged__menu">
			<?php
				$tab = isset($_GET['tab']) ? $_GET['tab'] : '';
				
				$favsTab = (is_page('user') && ($tab == '' || $tab == 'favorites')) ? 'is-active' : '';
				$offersTab = ($tab == 'offers') ? 'is-active' : '';
				$settingsTab = (is_page('account')) ? 'is-active' : '';
			?>
			<a href="<?php echo get_bloginfo('url'); ?>/user/?tab=favorites" class="c-header-logged__menu-item <?php echo $favsTab; ?>">Favorites (<?php echo get_user_favorites_count(); ?>)</a>
			<a href="<?php echo get_bloginfo('url'); ?>/user/?tab=offers" class="c-header-logged__menu-item <?php echo $offersTab; ?>">My offers</a>
			<a href="<?php echo get_bloginfo('url'); ?>/account" class="c-header-logged__menu-item <?php echo $settingsTab; ?>">Settings</a>
		</nav>
	</div>
</div>