<section class="l-page-list">
    <?php
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        include_once('favorites-menu.php');

        if($type == 'articles' || $type == ''){
            include_once('favorites-articles.php');
        } elseif($type == 'jobs'){
            include_once('favorites-jobs.php');
        } elseif($type == 'offers'){
            include_once('favorites-offers.php');
        } elseif($type == 'outs'){
            include_once('favorites-outs.php');
        }
    ?>
</section>