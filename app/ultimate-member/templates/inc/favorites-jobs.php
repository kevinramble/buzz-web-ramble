<?php
$postType = 'job';
$filters = array('post_type' => array($postType));
$favorites = get_user_favorites($user_id = null, $site_id = null, $filters = $filters); ?>

<div class="l-page-list__results">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="js-posts-list">
                    <?php if($favorites) :
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                        $args = array('post_type' => $postType, 'post__in' => $favorites, 'posts_per_page' => 6, 'paged' => $paged, 'isfavorites' => 1);
                        $wp_query = new WP_Query($args);

                        if($wp_query->have_posts()) : ?>
                            <?php while ($wp_query->have_posts()) : $wp_query->the_post();
                                global $post;
                                loop_joblisting($post->ID, 'dark');
                            endwhile; ?>
                        <?php endif; ?>
                </div>

                <?php // load more ajax
                $wp_query->query_vars['search_orderby_title'] = '';
                $load_posts = $wp_query->query_vars; $load_current_page = $wp_query->query_vars['paged']; $load_max_page = $wp_query->max_num_pages;
                loadmore_button($load_posts, $load_current_page, $load_max_page);
                if($wp_query->max_num_pages > 1){ ?>
                    <span class="js-loadmore c-bt-load">Load more</span>
                <?php } else { ?>
                    <span class="js-loadmore c-bt-load hidden">Load more</span>
                <?php } // end load more ajax
            
                wp_reset_query(); wp_reset_postdata();

                else : ?>
                        <h4 class="title-nocontent">Nothing to show.</h4>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-lg-3">
                <?php
                    $args = array('post_type' => $postType, 'posts_per_page' => 4, 'orderby' => 'date', 'order' => 'DESC', 'meta_key' => 'job_expiration', 'meta_value' => date('Y-m-d'), 'meta_compare' => '>=');
                    $wp_query = new WP_Query($args);
                    if($wp_query->have_posts()): ?>
                        <div class="c-recent-jobs">
                            <h2 class="c-recent-jobs__title">Recent jobs</h2>
                            <?php while($wp_query->have_posts()) : $wp_query->the_post();
                                global $post;
                                $job_company = esc_html(get_post_meta($post->ID, 'job_company', true));
                                $job_position = esc_html(get_post_meta($post->ID, 'job_position', true));
                                $job_country = esc_html(get_post_meta($post->ID, 'job_country', true)); ?>
                                <div class="c-recent-jobs__item">
                                    <a href="<?php the_permalink(); ?>">
                                        <p><strong><?php echo $job_company; ?></strong> is looking for <strong><?php echo $job_position; ?></strong> in <?php echo $job_country; ?></p>
                                    </a>
                                </div>
                            <?php endwhile; ?>
                        </div>
                        <?php
                    endif; wp_reset_query(); wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</div>