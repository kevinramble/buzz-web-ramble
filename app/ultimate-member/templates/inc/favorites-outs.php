<?php
$postType = 'out';
$filters = array('post_type' => array($postType));
$favorites = get_user_favorites($user_id = null, $site_id = null, $filters = $filters); ?>

<div class="l-page-list__results">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="row js-posts-list">
                    <?php if($favorites) :
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                        $args = array('post_type' => $postType, 'post__in' => $favorites, 'posts_per_page' => 6, 'paged' => $paged, 'isfavorites' => 1);
                        $wp_query = new WP_Query($args);

                        if($wp_query->have_posts()) : ?>
                            <?php while ($wp_query->have_posts()) : $wp_query->the_post();
                                global $post;
                                loop_outlisting($post->ID, '3');
                            endwhile; ?>
                        <?php endif; ?>
                </div>

                <?php // load more ajax
                $wp_query->query_vars['search_orderby_title'] = '';
                $load_posts = $wp_query->query_vars; $load_current_page = $wp_query->query_vars['paged']; $load_max_page = $wp_query->max_num_pages;
                loadmore_button($load_posts, $load_current_page, $load_max_page);
                if($wp_query->max_num_pages > 1){ ?>
                    <span class="js-loadmore c-bt-load">Load more</span>
                <?php } else { ?>
                    <span class="js-loadmore c-bt-load hidden">Load more</span>
                <?php } // end load more ajax
            
                wp_reset_query(); wp_reset_postdata();

                else : ?>
                        <h4 class="title-nocontent">Nothing to show.</h4>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-lg-3">
                <?php
                    $args = array('post_type' => $postType, 'posts_per_page' => 4, 'order' => 'ASC', 'orderby' => 'meta_value', 'meta_key' => 'out_date', 'meta_value' => date('Y-m-d'), 'meta_compare' => '>=');
                    $wp_query = new WP_Query($args);
                    if($wp_query->have_posts()): ?>
                        <div class="c-post-sidebar">
                            <h2 class="c-post-sidebar__title">Recent outs</h2>
                            <?php while($wp_query->have_posts()) : $wp_query->the_post();
                                if(has_post_thumbnail()){
                                    $image = get_the_post_thumbnail_url();
                                } else {
                                    $image = catch_that_image();
                                } ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="c-post-sidebar__item">
                                    <div class="c-post-sidebar__wrap-thumb">
                                        <div class="c-post-sidebar__thumb" style="background-image:url('<?php echo $image; ?>')"></div>
                                    </div>
                                    <p class="c-post-sidebar__description"><?php the_title(); ?></p>
                                </a>
                            <?php endwhile; ?>
                        </div>
                        <?php
                    endif; wp_reset_query(); wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</div>