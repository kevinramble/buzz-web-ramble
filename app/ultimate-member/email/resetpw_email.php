<p>
	<style type="text/css">
		#outlook a {
			padding: 0;
		}

		body {
			margin: 0;
			padding: 0;
			-webkit-text-size-adjust: 100%;
			-ms-text-size-adjust: 100%;
		}

		table,
		td {
			border-collapse: collapse;
			mso-table-lspace: 0pt;
			mso-table-rspace: 0pt;
		}

		img {
			border: 0;
			height: auto;
			line-height: 100%;
			outline: none;
			text-decoration: none;
			-ms-interpolation-mode: bicubic;
		}

		p {
			display: block;
			margin: 13px 0;
		}
	</style>
</p>
<p>&nbsp;</p>
<!-- [if mso]>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
<!-- [if lte mso 11]>
        <style type="text/css">
          .outlook-group-fix { width:100% !important; }
        </style>
        <![endif]-->
<p>
	<style type="text/css">
		@media only screen and (min-width:480px) {
			.mj-column-px-470 {
				width: 470px !important;
				max-width: 470px;
			}

			.mj-column-per-100 {
				width: 100% !important;
				max-width: 100%;
			}
		}
	</style>
	<style type="text/css">
		@media only screen and (max-width:480px) {
			table.full-width-mobile {
				width: 100% !important;
			}

			td.full-width-mobile {
				width: auto !important;
			}
		}
	</style>
	<style type="text/css">
		@import url("https://emails.buzz-caribbean.com/fonts/fonts.css");

		.title {
			font-size: 32px;
			line-height: 1.5;
		}

		/* Utilities */
		.desktop-hide {
			display: none;
		}

		@media only screen and (max-device-width: 469px) {
			.title {
				font-size: 30px;
			}

			/* Utilities */
			.mobile-hide {
				display: none;
			}

			.desktop-hide {
				display: block;
			}
		}
	</style>
</p>
<p>&nbsp;</p>
<!-- Responsive -->
<!-- [if mso]>
    <style type="text/css">
      a,
      h1,
      h2,
      h3,
      h4,
      h5,
      h6,
      h6,
      body,
      table,
      td,
      div,
      span,
      p {
        font-family: Arial, Helvetica, sans-serif !important;
      }
    </style>
  <![endif]-->
<div style="background-color: #ffffff;">
	<table class="header" style="background-color: #000; width: 100%;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0" align="center" bgcolor="#000">
		<tbody>
			<tr>
				<td>
					<!-- [if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="header-outlook" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
					<div style="margin: 0px auto; max-width: 600px;">
						<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
							<tbody>
								<tr>
									<td style="direction: ltr; font-size: 0px; padding: 0; text-align: center;">
										<!-- [if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

            <tr>
              <td
                 class="" width="600px"
              >
          <![endif]-->
										<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
											<tbody>
												<tr>
													<td>
														<!-- [if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
														<div style="margin: 0px auto; max-width: 600px;">
															<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
																<tbody>
																	<tr>
																		<td style="direction: ltr; font-size: 0px; padding: 40px 0; text-align: center;">
																			<!-- [if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

        <tr>

            <td
               class="" style="width:470px;"
            >
          <![endif]-->
																			<div class="mj-column-px-470 outlook-group-fix" style="font-size: 0; line-height: 0; text-align: left; display: inline-block; width: 100%; direction: ltr;">
																				<!-- [if mso | IE]>
        <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
          <tr>

              <td
                 style="vertical-align:top;width:470px;"
              >
              <![endif]-->
																				<div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
																					<table style="vertical-align: top;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
																						<tbody>
																							<tr>
																								<td class="logo" style="font-size: 0px; padding: 0; word-break: break-word;" align="center">
																									<table style="border-collapse: collapse; border-spacing: 0px;" role="presentation" border="0" cellspacing="0" cellpadding="0">
																										<tbody>
																											<tr>
																												<td style="width: 110px;"><img style="border: 0; display: block; outline: none; text-decoration: none; height: auto; width: 100%; font-size: 13px;" src="https://emails.buzz-caribbean.com/img/logo.png" alt="Logo" width="110" height="auto" /></td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</div>
																				<!-- [if mso | IE]>
              </td>

          </tr>
          </table>
        <![endif]-->
																			</div>
																			<!-- [if mso | IE]>
            </td>

        </tr>

                  </table>
                <![endif]-->
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<!-- [if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
													</td>
												</tr>
											</tbody>
										</table>
										<!-- [if mso | IE]>
              </td>
            </tr>

                  </table>
                <![endif]-->
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- [if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
				</td>
			</tr>
		</tbody>
	</table>
	<table class="hero" style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
		<tbody>
			<tr>
				<td>
					<!-- [if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="hero-outlook" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
					<div style="margin: 0px auto; max-width: 600px;">
						<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
							<tbody>
								<tr>
									<td style="direction: ltr; font-size: 0px; padding: 0 20px; text-align: center;">
										<!-- [if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

            <tr>
              <td
                 class="" width="600px"
              >
          <![endif]-->
										<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
											<tbody>
												<tr>
													<td>
														<!-- [if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:560px;" width="560"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
														<div style="margin: 0px auto; max-width: 560px;">
															<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
																<tbody>
																	<tr>
																		<td style="direction: ltr; font-size: 0px; padding: 50px 0 40px; text-align: center;">
																			<!-- [if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

        <tr>

            <td
               class="" style="width:470px;"
            >
          <![endif]-->
																			<div class="mj-column-px-470 outlook-group-fix" style="font-size: 0; line-height: 0; text-align: left; display: inline-block; width: 100%; direction: ltr;">
																				<!-- [if mso | IE]>
        <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
          <tr>

              <td
                 style="vertical-align:top;width:470px;"
              >
              <![endif]-->
																				<div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
																					<table style="vertical-align: top;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
																						<tbody>
																							<tr>
																								<td style="font-size: 0px; padding: 0; word-break: break-word;" align="center">
																									<div style="font-family: Averta Light, sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #000000;"><span class="title" style="font-family: 'Tusker Grotesk', sans-serif; font-weight: bold; color: #000000;">We received a request to reset the password for your account. If you made this request, click the link below to change your password:</span></div>
																								</td>
																							</tr>
																							<tr>
																								<td style="background: white; font-size: 0px; word-break: break-word;">
																									<!-- [if mso | IE]>

        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="50" style="vertical-align:top;height:50px;">

    <![endif]-->
																									<div style="height: 50px;"> </div>
																									<!-- [if mso | IE]>

        </td></tr></table>

    <![endif]-->
																								</td>
																							</tr>
																							<tr>
																								<td style="font-size: 0px; padding: 0; word-break: break-word;" align="center">
																									<div style="font-family: Averta Light, sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #000000;"><span class="description" style="font-size: 14px; font-weight: 300; line-height: 1.71; color: #bfbfbf;"></span></div>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</div>
																				<!-- [if mso | IE]>
              </td>

          </tr>
          </table>
        <![endif]-->
																			</div>
																			<!-- [if mso | IE]>
            </td>

        </tr>

                  </table>
                <![endif]-->
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<!-- [if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
													</td>
												</tr>
											</tbody>
										</table>
										<!-- [if mso | IE]>
              </td>
            </tr>

            <tr>
              <td
                 class="" width="600px"
              >

      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:560px;" width="560"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
										<div style="margin: 0px auto; max-width: 560px;">
											<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
												<tbody>
													<tr>
														<td style="direction: ltr; font-size: 0px; padding: 0 0 60px; text-align: center;">
															<!-- [if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

        <tr>

            <td
               class="" style="width:470px;"
            >
          <![endif]-->
															<div class="mj-column-px-470 outlook-group-fix" style="font-size: 0; line-height: 0; text-align: left; display: inline-block; width: 100%; direction: ltr;">
																<!-- [if mso | IE]>
        <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
          <tr>

              <td
                 style="vertical-align:top;width:470px;"
              >
              <![endif]-->
																<div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
																	<table style="vertical-align: top;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td style="font-size: 0px; padding: 0; word-break: break-word;" align="center">
																					<table style="border-collapse: separate; width: 256px; line-height: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0">
																						<tbody>
																							<tr>
																								<td style="border: none; border-radius: 0; cursor: auto; height: 40px; mso-padding-alt: 0; background: #000000;" role="presentation" align="center" valign="middle" bgcolor="#000000"><a style="display: inline-block; width: 256px; background: #000000; color: #ffffff; font-family: Tusker Grotesk, sans-serif; font-size: 16px; font-weight: bold; line-height: 120%; margin: 0; text-decoration: none; text-transform: uppercase; padding: 0; mso-padding-alt: 0px; border-radius: 0;" href="{password_reset_link}" target="_blank" rel="button noopener">Reset your password</a></td>
																							</tr>
																						</tbody>
																					</table>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
																<!-- [if mso | IE]>
              </td>

          </tr>
          </table>
        <![endif]-->
															</div>
															<!-- [if mso | IE]>
            </td>

        </tr>

                  </table>
                <![endif]-->
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<!-- [if mso | IE]>
          </td>
        </tr>
      </table>

              </td>
            </tr>

                  </table>
                <![endif]-->
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- [if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
				</td>
			</tr>
		</tbody>
	</table>
	<!-- [if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="hero-outlook" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
	<div class="hero" style="background: #ececec; background-color: #ececec; margin: 0px auto; max-width: 600px;">
		<table style="background: #ececec; background-color: #ececec; width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
			<tbody>
				<tr>
					<td style="direction: ltr; font-size: 0px; padding: 30px 0; text-align: center;">
						<!-- [if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

            <tr>
              <td
                 class="" width="600px"
              >
          <![endif]-->
						<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
							<tbody>
								<tr>
									<td>
										<!-- [if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
										<div style="margin: 0px auto; max-width: 600px;">
											<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
												<tbody>
													<tr>
														<td style="direction: ltr; font-size: 0px; padding: 0 30px; text-align: center;">
															<!-- [if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

        <tr>

            <td
               class="" style="width:470px;"
            >
          <![endif]-->
															<div class="mj-column-px-470 outlook-group-fix" style="font-size: 0; line-height: 0; text-align: left; display: inline-block; width: 100%; direction: ltr;">
																<!-- [if mso | IE]>
        <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
          <tr>

              <td
                 style="vertical-align:top;width:470px;"
              >
              <![endif]-->
																<div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
																	<table style="vertical-align: top;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td style="font-size: 0px; padding: 0; word-break: break-word;" align="center">
																					<div style="font-family: Averta Light, sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #000000;"><span class="description" style="font-size: 14px; font-weight: 300; line-height: 1.71; color: #bfbfbf;">If you did not make this change and believe your account has been compromised, please <a class="text-link" style="color: #4a90e2;" href="mailto:contact@buzz-caribbean.com">contact us ASAP.</a></span></div>
																				</td>
																			</tr>
																			<tr>
																				<td style="background: #ececec; font-size: 0px; word-break: break-word;">
																					<!-- [if mso | IE]>

        <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="20" style="vertical-align:top;height:20px;">

    <![endif]-->
																					<div style="height: 20px;"> </div>
																					<!-- [if mso | IE]>

        </td></tr></table>

    <![endif]-->
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
																<!-- [if mso | IE]>
              </td>

          </tr>
          </table>
        <![endif]-->
															</div>
															<!-- [if mso | IE]>
            </td>

        </tr>

                  </table>
                <![endif]-->
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<!-- [if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
									</td>
								</tr>
							</tbody>
						</table>
						<!-- [if mso | IE]>
              </td>
            </tr>
          <![endif]-->
						<!-- Desktop -->
						<!-- [if mso | IE]>
            <tr>
              <td
                 class="" width="600px"
              >
          <![endif]-->
						<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
							<tbody>
								<tr>
									<td>
										<!-- [if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
										<div style="margin: 0px auto; max-width: 600px;">
											<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
												<tbody>
													<tr>
														<td style="direction: ltr; font-size: 0px; padding: 0; text-align: center;">
															<!-- [if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

        <tr>

            <td
               class="" style="width:470px;"
            >
          <![endif]-->
															<div class="mj-column-px-470 outlook-group-fix" style="font-size: 0; line-height: 0; text-align: left; display: inline-block; width: 100%; direction: ltr;">
																<!-- [if mso | IE]>
        <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
          <tr>

              <td
                 style="vertical-align:top;width:470px;"
              >
              <![endif]-->
																<div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
																	<table style="vertical-align: top;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td style="font-size: 0px; padding: 0; word-break: break-word;" align="left">
																					<table style="color: #000000; font-family: Averta Light, sans-serif; font-size: 13px; line-height: 80px; table-layout: auto; width: 100%; border: none;" border="0" width="100%" cellspacing="0" cellpadding="0">
																						<tbody>
																							<tr class="mobile-hide">
																								<td style="padding: 0 30px 0 64px;" width="135"><a class="block" style="display: block;" href="#"><img class="block" style="display: block;" src="https://emails.buzz-caribbean.com/img/app-store.png" alt="App Store" width="135" /></a></td>
																								<td style="padding-right: 64px;">
																									<table cellspacing="0" cellpadding="0">
																										<tbody>
																											<tr>
																												<td class="social-space" style="padding: 0 5px;" width="30"><a class="block" style="display: block;" href="http://facebook.com/werbuzz"><img class="block" style="display: block;" src="https://emails.buzz-caribbean.com/img/facebook.png" alt="Facebook" width="30" /></a></td>
																												<td class="social-space" style="padding: 0 5px;" width="30"><a class="block" style="display: block;" href="https://www.instagram.com/buzzcaribbean/"><img class="block" style="display: block;" src="https://emails.buzz-caribbean.com/img/instagram.png" alt="Instagram" width="30" /></a></td>
																												<td class="social-space" style="padding: 0 5px;" width="30"><a class="block" style="display: block;" href="https://www.youtube.com/channel/UCtSyhurnO8mOyJxXXDZ7t0A"><img class="block" style="display: block;" src="https://emails.buzz-caribbean.com/img/youtube.png" alt="Youtube" width="30" /></a></td>
																												<td class="social-space" style="padding: 0 5px;" width="30"><a class="block" style="display: block;" href="https://twitter.com/buzzcarib"><img class="block" style="display: block;" src="https://emails.buzz-caribbean.com/img/twitter.png" alt="Twitter" width="30" /></a></td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
																<!-- [if mso | IE]>
              </td>

          </tr>
          </table>
        <![endif]-->
															</div>
															<!-- [if mso | IE]>
            </td>

        </tr>

                  </table>
                <![endif]-->
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<!-- [if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
									</td>
								</tr>
							</tbody>
						</table>
						<!-- [if mso | IE]>
              </td>
            </tr>
          <![endif]-->
						<!-- Mobile -->
						<!-- [if mso | IE]>
            <tr>
              <td
                 class="" width="600px"
              >
          <![endif]-->
						<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
							<tbody>
								<tr>
									<td>
										<!-- [if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
										<div style="margin: 0px auto; max-width: 600px;">
											<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
												<tbody>
													<tr>
														<td style="direction: ltr; font-size: 0px; padding: 0; text-align: center;">
															<!-- [if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

        <tr>

            <td
               class="" style="width:470px;"
            >
          <![endif]-->
															<div class="mj-column-px-470 outlook-group-fix" style="font-size: 0; line-height: 0; text-align: left; display: inline-block; width: 100%; direction: ltr;">
																<!-- [if mso | IE]>
        <table  role="presentation" border="0" cellpadding="0" cellspacing="0">
          <tr>

              <td
                 style="vertical-align:top;width:470px;"
              >
              <![endif]-->
																<div class="mj-column-per-100 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
																	<table style="vertical-align: top;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<td style="font-size: 0px; padding: 0; word-break: break-word;" align="center">
																					<table style="color: #000000; font-family: Averta Light, sans-serif; font-size: 13px; line-height: 22px; table-layout: auto; width: 100%; border: none;" border="0" width="100%" cellspacing="0" cellpadding="0">
																						<tbody>
																							<tr class="desktop-hide" align="center">
																								<td width="135"><!--<a href="#"><img src="https://emails.buzz-caribbean.com/img/app-store.png" alt="App Store" width="135" /></a>--></td>
																							</tr>
																							<tr class="desktop-hide">
																								<td height="20px"> </td>
																							</tr>
																							<tr align="center">
																								<td>
																									<table>
																										<tbody>
																											<tr class="desktop-hide">
																												<td class="social-space" style="padding: 0 5px;" width="30"><a href="http://facebook.com/werbuzz"><img src="https://emails.buzz-caribbean.com/img/facebook.png" alt="Facebook" width="30" /></a></td>
																												<td class="social-space" style="padding: 0 5px;" width="30"><a href="https://www.instagram.com/buzzcaribbean/"><img src="https://emails.buzz-caribbean.com/img/instagram.png" alt="Instagram" width="30" /></a></td>
																												<td class="social-space" style="padding: 0 5px;" width="30"><a href="https://www.youtube.com/channel/UCtSyhurnO8mOyJxXXDZ7t0A"><img src="https://emails.buzz-caribbean.com/img/youtube.png" alt="Youtube" width="30" /></a></td>
																												<td class="social-space" style="padding: 0 5px;" width="30"><a href="https://twitter.com/buzzcarib"><img src="https://emails.buzz-caribbean.com/img/twitter.png" alt="Twitter" width="30" /></a></td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
																<!-- [if mso | IE]>
              </td>

          </tr>
          </table>
        <![endif]-->
															</div>
															<!-- [if mso | IE]>
            </td>

        </tr>

                  </table>
                <![endif]-->
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<!-- [if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
									</td>
								</tr>
							</tbody>
						</table>
						<!-- [if mso | IE]>
              </td>
            </tr>

                  </table>
                <![endif]-->
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- [if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
	<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
		<tbody>
			<tr>
				<td>
					<!-- [if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
					<div style="margin: 0px auto; max-width: 600px;">
						<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
							<tbody>
								<tr>
									<td style="direction: ltr; font-size: 0px; padding: 0 20px; text-align: center;">
										<!-- [if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

            <tr>
              <td
                 class="" width="600px"
              >
          <![endif]-->
										<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
											<tbody>
												<tr>
													<td>
														<!-- [if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:560px;" width="560"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]-->
														<div style="margin: 0px auto; max-width: 560px;">
															<table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
																<tbody>
																	<tr>
																		<td style="direction: ltr; font-size: 0px; padding: 30px 0 50px; text-align: center;">
																			<!-- [if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">

        <tr>

            <td
               class="" style="vertical-align:top;width:470px;"
            >
          <![endif]-->
																			<div class="mj-column-px-470 outlook-group-fix" style="font-size: 0px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
																				<table style="vertical-align: top;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
																					<tbody>
																						<tr>
																							<td style="font-size: 0px; padding: 0; word-break: break-word;" align="center">
																								<div style="font-family: Averta Light, sans-serif; font-size: 10px; font-weight: 300; line-height: 20px; text-align: center; color: #979797;">
																									<p>You are receiving this message because you or your organization has been identified by us or our third-party partners as someone who would be interested in receiving information about BUZZ. If this is not the case, you can opt out of receiving future emails by clicking <a href="https://buzz-caribbean.com/account/delete/" class="text-link" style="color: #4a90e2;"> unsubscribe</a>. For more information about how we process data, please see our <a href="https://buzz-caribbean.com/privacy-policy/" class="text-link" style="color: #4a90e2;">Privacy Policy</a>.</p>
																								</div>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																			</div>
																			<!-- [if mso | IE]>
            </td>

        </tr>

                  </table>
                <![endif]-->
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
														<!-- [if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
													</td>
												</tr>
											</tbody>
										</table>
										<!-- [if mso | IE]>
              </td>
            </tr>

                  </table>
                <![endif]-->
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- [if mso | IE]>
          </td>
        </tr>
      </table>
      <![endif]-->
				</td>
			</tr>
		</tbody>
	</table>
</div>