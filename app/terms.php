
<?php
    /* Template Name: Terms and conditions */
    
    $webview = isset($_GET['webview']) ? $_GET['webview'] : '';
    if(!$webview || $webview != 'true') :
    // normal, desktop e mobile

    get_header(); ?>

    <section class="l-page-privacy">
        <div class="c-privacy-intro">
            <h1 class="c-privacy-intro__title">BUZZ Terms &amp; Conditions of Use</h1>
            <h2 class="c-privacy-intro__subtitle">August 15, 2019</h2>
            <p class="c-privacy-intro__text">Welcome to www.buzz-caribbean.com (“the website”), hosted by BUZZ, a subsidiary of Ramble Media Corporation. If you wish to use the website or register to use its interactive features we invite you to read the Terms and Conditions on each visit.</p>
        </div>
        <div class="c-privacy-anchor">
            <a href="#1" class="c-privacy-anchor__item">1. Agreement</a>
            <a href="#2" class="c-privacy-anchor__item">2. Grant of rights</a>
            <a href="#3" class="c-privacy-anchor__item">3. Reserved rights</a>
            <a href="#4" class="c-privacy-anchor__item">4. Registration</a>
            <a href="#5" class="c-privacy-anchor__item">5. Guidelines for using the interactive features on the website</a>
            <a href="#6" class="c-privacy-anchor__item">6. Copyright and intellectual property rights</a>
            <a href="#7" class="c-privacy-anchor__item">7. Liability for breach of copyright or intellectual property rights</a>
            <a href="#8" class="c-privacy-anchor__item">8. Disclaimer</a>
            <a href="#9" class="c-privacy-anchor__item">9. Force majeure</a>
            <a href="#10" class="c-privacy-anchor__item">10. Privacy</a>
            <a href="#11" class="c-privacy-anchor__item">11. Waiver</a>
            <a href="#12" class="c-privacy-anchor__item">12. Termination</a>
            <a href="#13" class="c-privacy-anchor__item">13. Survival</a>
            <a href="#14" class="c-privacy-anchor__item">14. Applicable law</a>
            <a href="#15" class="c-privacy-anchor__item">15. Illegality</a>
            <a href="#16" class="c-privacy-anchor__item">16. Contact Buzz</a>
        </div>
        <div id="1" class="c-privacy-block">
            <h2 class="c-privacy-block__title">1. Agreement</h2>
            <p class="c-privacy-block__text">By using the website you agree to the Terms and Conditions of Use and may use the website for non-commercial purposes. The Terms and Conditions of Use may be changed at anytime so you should read them whenever you visit.</p>
        </div>
        <div id="2" class="c-privacy-block">
            <h2 class="c-privacy-block__title">2. Grant of rights</h2>
            <p class="c-privacy-block__text">When you submit material (which includes comments, letters, articles, photographs, videos, audio files) you are deemed to submit the material for publication and you grant BUZZ or our affiliated companies royalty free, non-exclusive permission to use the material in any of its publications and in any media throughout the world. Our use of the material includes publishing, republishing, incorporating, referring to, editing, modifying, removing, distributing all or part the material in any media we choose. We may also sub-license or transfer these rights to trusted third parties and use your material to promote our products and services.  These rights will continue to exist in perpetuity after you terminate, deactivate or cease using your account. We therefore are not obliged to comply with any request to cease using the material. Please do not submit material if you do not wish to grant us these rights.</p>
        </div>
        <div id="3" class="c-privacy-block">
            <h2 class="c-privacy-block__title">3. Reserved rights</h2>
            <p class="c-privacy-block__text">Occasionally we amend the terms of this agreement.  Amendments may include deletions, additions or alterations. This may be done without notifying you. All our updates become effective on the date they are posted on the website. If you continue to use the website your acceptance of the amended terms is implied.</p>
            <p class="c-privacy-block__text">We will have the exclusive right to decide which material is published and the media in which the material is published. When we publish your material we may disclose some or all the personal information you provide us with such as your name, town, city and country. We may also suspend or terminate your account, bar you from registering, change, modify, suspend or terminate the website at our sole discretion.  We will not publish your email address.</p>
        </div>
        <div id="4" class="c-privacy-block">
            <h2 class="c-privacy-block__title">4. Registration</h2>
            <p class="c-privacy-block__text">Persons under the age of eighteen (18) may only use the website under adult supervision. Persons wishing to use the interactive features of the website must be eighteen (18) years old and are required to register. Only one registration is permitted for each person.  We ask that you submit accurate information and refrain from impersonating others.</p>
            <p class="c-privacy-block__text">When you register you will be exclusively responsible for your account. Please do not disclose your account information to anyone, or permit anyone to use your account, or use anyone else’s account</p>
        </div>
        <div id="5" class="c-privacy-block">
            <h2 class="c-privacy-block__title">5. Guidelines for using the interactive features on the website</h2>
            <p class="c-privacy-block__text">Registered users of the website are required to adhere to socially acceptable and tasteful conduct. You shall at all times be respectful of the right of other users to freedom of expression. Please ensure that the material you submit does not breach any laws, is not defamatory, derogatory, abusive, racist, discriminatory, offensive, profane, sexually explicit, vulgar, obscene, threatening, infringe on the privacy rights of others, include links to other websites or breach the Intellectual Property Rights or Copyrights of others. The website is not to be used to incite violence against any person or group of persons, incite persons to engage in criminal activity or conduct attracting civil liability or prejudice ongoing legal proceedings. Please refrain from advertising or promoting products or services. If you wish to advertise on our website, please send your queries to advertising@buzz-caribbean.com.</p><p class="c-privacy-block__text">
            The material you submit to us can be accessed by users of varied ages, gender and beliefs please bear this in mind and remain considerate of others and mindful of the content of your material. Your material may be rated as the best or worst by other users and may also be the subject of criticism, please do not take this personally and bear in mind the right of other registered users to express their opinions.</p><p class="c-privacy-block__text">
            Please do not use this website to be abusive or disrespectful of other users. If you have a personal interest in or are otherwise connected to a story please either refrain from commenting on the story or disclose your interest or connection in the appropriate circumstances.</p><p class="c-privacy-block__text">
            For practical purposes not all comments submitted will be posted on the website and we reserve the right to limit the size and number of comments posted by an individual user in relation to a story. We reserve the exclusive right to permit comments in respect of some of our material and prohibit comments with respect to others. We ask that you refrain from submitting comments regarding the operation of the website using the comments feature.  We would be happy to address your concerns so feel free to submit comments regarding the operation of the website to community@buzz-caribbean.com.</p><p class="c-privacy-block__text">
            Registered users may submit comments or material throughout the website, subject to these terms and conditions.</p><p class="c-privacy-block__text">
            We respect the privacy of our users and therefore ask that you do not disclose your personal information or that of others in the material you submit to us.</p><p class="c-privacy-block__text">
            The objective of the interactive feature is to encourage users to exercise their right to freedom of expression in a cordial and respectful environment therefore we discourage forms of abuse meted out to our users and staff. Please report any form of abuse or breach of these terms to community@buzz-caribbean.com. Users who breach these terms may be suspended or banned from using our interactive features. If we suspend or ban you from using the interactive features of the website please do not re-register or submit material from another user’s account.</p>
        </div>
        <div id="6" class="c-privacy-block">
            <h2 class="c-privacy-block__title">6. Copyright and intellectual property rights</h2>
            <p class="c-privacy-block__text">When you submit material to us you are warranting that the material is either your original work or you have the consent of the owner(s) of the rights to the material to submit it to us for publication in accordance with these terms. Copyright and Intellectual Property Rights in the material you submit do not transfer to us and therefore you may permit other persons to use it. You however agree that we may, at our sole discretion, use your material without identifying you as the author of the material.</p>
        </div>
        <div id="7" class="c-privacy-block">
            <h2 class="c-privacy-block__title">7. Liability for breach of copyright or intellectual property rights</h2>
            <p class="c-privacy-block__text">We will not be liable for any breach of Copyright or other Intellectual Property Rights as a result of publication of your material.  You will be exclusively liable for any breach of Copyright or other Intellectual Property Rights proven by a third party with respect to your material. It shall be your duty to prove ownership of the contested material. You agree to fully indemnifyBUZZ, its heirs, successors, Directors, employees, affiliate companies and agents from any and all causes of action, loss, damage, injury or costs incurred including attorneys’ fees and settlement fees as a result of its publication of your material.</p>
        </div>
        <div id="8" class="c-privacy-block">
            <h2 class="c-privacy-block__title">8. Disclaimer</h2>
            <p class="c-privacy-block__text">We receive material from third parties such as advertisements, comments, links to other websites and content published on other websites. The material posted on this website is exclusively for educational and informational purposes only. We do not purport to give medical, legal, financial or other professional advice to any user of the website. You agree not to rely on the content published on the website without first consulting experts in particular fields for professional advice. Consequently we are not liable for any costs incurred, loss, damage, injury or death suffered by any person as a result of use of the website, third party transactions or websites or breach of any of these terms.</p><p class="c-privacy-block__text">
            By agreeing to these terms and conditions you agree not to initiate, pursue or assist in the commencement or furtherance of any cause of action in contract or tort against us for any costs incurred, loss, damage, injury or death suffered by any person who neglects to observe these terms. Nothing in these terms shall affect our liability as imposed by law. (i.e. the law applicable to Jamaica).</p><p class="c-privacy-block__text">
            We give no warranties or guarantees in relation to the website or its contents.  Efforts are made to protect against viruses, bugs Trojans, spyware, adware and other forms of malware however, we do not guarantee that the website will be free from viruses, and other forms of malware.</p>
        </div>
        <div id="9" class="c-privacy-block">
            <h2 class="c-privacy-block__title">9. Force majeure</h2>
            <p class="c-privacy-block__text">We endeavour to provide users with uninterrupted access to the website.  Occasionally we conduct scheduled and emergency maintenance or repairs on the website consequently we will not be responsible or liable for any disruption or delay in service.  Neither will we be liable for loss of access to the website as a result of acts of God, acts of Parliament, acts of third party service providers or any other factors over which we have no control.</p>
        </div>
        <div id="10" class="c-privacy-block">
            <h2 class="c-privacy-block__title">10. Privacy</h2>
            <p class="c-privacy-block__text">Your personal information will be used in accordance with the terms of our privacy policy.</p>
        </div>
        <div id="11" class="c-privacy-block">
            <h2 class="c-privacy-block__title">11. Waiver</h2>
            <p class="c-privacy-block__text">Failure of BUZZ to pursue or enforce its rights does not constitute a waiver of those rights.</p>
        </div>
        <div id="12" class="c-privacy-block">
            <h2 class="c-privacy-block__title">12. Termination</h2>
            <p class="c-privacy-block__text">Without prejudice to any other clause herein, this Agreement remains in force until either party exercises the option to terminate.  You may terminate this agreement without notice. We reserve the right to terminate your account if we believe you have breached any of these terms. Termination will result in your account being immediately deactivated.</p>
        </div>
        <div id="13" class="c-privacy-block">
            <h2 class="c-privacy-block__title">13. Survival</h2>
            <p class="c-privacy-block__text">Clauses 2 – Grant of Rights, 6 – Copyright, 7 – Liability for Breach, 8 – Disclaimer and 14 – Applicable Law shall survive this Agreement.</p>
        </div>
        <div id="14" class="c-privacy-block">
            <h2 class="c-privacy-block__title">14. Applicable law</h2>
            <p class="c-privacy-block__text">The laws of Jamaica shall apply to, and the courts of Jamaica shall have exclusive jurisdiction in all matters arising from the use of this website.</p>
        </div>
        <div id="15" class="c-privacy-block">
            <h2 class="c-privacy-block__title">15. Illegality</h2>
            <p class="c-privacy-block__text">If any of the term contained in this Agreement is deemed to be void, illegal or unenforceable in Jamaica, such void, illegal or unenforceable term shall be deemed to be severed from this Agreement and the terms remaining shall be binding and enforceable.</p>
        </div>
        <div id="16" class="c-privacy-block">
            <h2 class="c-privacy-block__title">16. Contact Buzz</h2>
            <p class="c-privacy-block__text">BUZZ<br>
            40 – 42 ½ Beechwood Avenue<br>
            Kingston 5<br>
            Jamaica<br>
            Email: community@buzz-caribbean.com<br>
            Fax: (876) 920-1440<br>
            Telephone:  (876) 936-9505<br></p>
            <p class="c-privacy-block__text">THANK YOU FOR READING THESE TERMS AND CONDITIONS OF USE, IF YOU DO NOT AGREE WITH ALL THE TERMS CONTAINED IN THIS AGREEMENT PLEASE DO NOT USE THE WEBSITE.</p>
        </div>
    </section>

    <?php get_footer(); ?>

<?php elseif($webview && $webview == 'true') :
	// single apenas do app
	include_once('inc/app/header.php'); ?>

<section class="l-page-privacy">
        <div class="c-privacy-intro">
            <h1 class="c-privacy-intro__title">BUZZ Terms &amp; Conditions of Use</h1>
            <h2 class="c-privacy-intro__subtitle">August 15, 2019</h2>
            <p class="c-privacy-intro__text">Welcome to www.buzz-caribbean.com (“the website”), hosted by BUZZ, a subsidiary of Ramble Media Corporation. If you wish to use the website or register to use its interactive features we invite you to read the Terms and Conditions on each visit.</p>
        </div>
        <div class="c-privacy-anchor">
            <a href="#1" class="c-privacy-anchor__item">1. Agreement</a>
            <a href="#2" class="c-privacy-anchor__item">2. Grant of rights</a>
            <a href="#3" class="c-privacy-anchor__item">3. Reserved rights</a>
            <a href="#4" class="c-privacy-anchor__item">4. Registration</a>
            <a href="#5" class="c-privacy-anchor__item">5. Guidelines for using the interactive features on the website</a>
            <a href="#6" class="c-privacy-anchor__item">6. Copyright and intellectual property rights</a>
            <a href="#7" class="c-privacy-anchor__item">7. Liability for breach of copyright or intellectual property rights</a>
            <a href="#8" class="c-privacy-anchor__item">8. Disclaimer</a>
            <a href="#9" class="c-privacy-anchor__item">9. Force majeure</a>
            <a href="#10" class="c-privacy-anchor__item">10. Privacy</a>
            <a href="#11" class="c-privacy-anchor__item">11. Waiver</a>
            <a href="#12" class="c-privacy-anchor__item">12. Termination</a>
            <a href="#13" class="c-privacy-anchor__item">13. Survival</a>
            <a href="#14" class="c-privacy-anchor__item">14. Applicable law</a>
            <a href="#15" class="c-privacy-anchor__item">15. Illegality</a>
            <a href="#16" class="c-privacy-anchor__item">16. Contact Buzz</a>
        </div>
        <div id="1" class="c-privacy-block">
            <h2 class="c-privacy-block__title">1. Agreement</h2>
            <p class="c-privacy-block__text">By using the website you agree to the Terms and Conditions of Use and may use the website for non-commercial purposes. The Terms and Conditions of Use may be changed at anytime so you should read them whenever you visit.</p>
        </div>
        <div id="2" class="c-privacy-block">
            <h2 class="c-privacy-block__title">2. Grant of rights</h2>
            <p class="c-privacy-block__text">When you submit material (which includes comments, letters, articles, photographs, videos, audio files) you are deemed to submit the material for publication and you grant BUZZ or our affiliated companies royalty free, non-exclusive permission to use the material in any of its publications and in any media throughout the world. Our use of the material includes publishing, republishing, incorporating, referring to, editing, modifying, removing, distributing all or part the material in any media we choose. We may also sub-license or transfer these rights to trusted third parties and use your material to promote our products and services.  These rights will continue to exist in perpetuity after you terminate, deactivate or cease using your account. We therefore are not obliged to comply with any request to cease using the material. Please do not submit material if you do not wish to grant us these rights.</p>
        </div>
        <div id="3" class="c-privacy-block">
            <h2 class="c-privacy-block__title">3. Reserved rights</h2>
            <p class="c-privacy-block__text">Occasionally we amend the terms of this agreement.  Amendments may include deletions, additions or alterations. This may be done without notifying you. All our updates become effective on the date they are posted on the website. If you continue to use the website your acceptance of the amended terms is implied.</p>
            <p class="c-privacy-block__text">We will have the exclusive right to decide which material is published and the media in which the material is published. When we publish your material we may disclose some or all the personal information you provide us with such as your name, town, city and country. We may also suspend or terminate your account, bar you from registering, change, modify, suspend or terminate the website at our sole discretion.  We will not publish your email address.</p>
        </div>
        <div id="4" class="c-privacy-block">
            <h2 class="c-privacy-block__title">4. Registration</h2>
            <p class="c-privacy-block__text">Persons under the age of eighteen (18) may only use the website under adult supervision. Persons wishing to use the interactive features of the website must be eighteen (18) years old and are required to register. Only one registration is permitted for each person.  We ask that you submit accurate information and refrain from impersonating others.</p>
            <p class="c-privacy-block__text">When you register you will be exclusively responsible for your account. Please do not disclose your account information to anyone, or permit anyone to use your account, or use anyone else’s account</p>
        </div>
        <div id="5" class="c-privacy-block">
            <h2 class="c-privacy-block__title">5. Guidelines for using the interactive features on the website</h2>
            <p class="c-privacy-block__text">Registered users of the website are required to adhere to socially acceptable and tasteful conduct. You shall at all times be respectful of the right of other users to freedom of expression. Please ensure that the material you submit does not breach any laws, is not defamatory, derogatory, abusive, racist, discriminatory, offensive, profane, sexually explicit, vulgar, obscene, threatening, infringe on the privacy rights of others, include links to other websites or breach the Intellectual Property Rights or Copyrights of others. The website is not to be used to incite violence against any person or group of persons, incite persons to engage in criminal activity or conduct attracting civil liability or prejudice ongoing legal proceedings. Please refrain from advertising or promoting products or services. If you wish to advertise on our website, please send your queries to advertising@buzz-caribbean.com.</p><p class="c-privacy-block__text">
            The material you submit to us can be accessed by users of varied ages, gender and beliefs please bear this in mind and remain considerate of others and mindful of the content of your material. Your material may be rated as the best or worst by other users and may also be the subject of criticism, please do not take this personally and bear in mind the right of other registered users to express their opinions.</p><p class="c-privacy-block__text">
            Please do not use this website to be abusive or disrespectful of other users. If you have a personal interest in or are otherwise connected to a story please either refrain from commenting on the story or disclose your interest or connection in the appropriate circumstances.</p><p class="c-privacy-block__text">
            For practical purposes not all comments submitted will be posted on the website and we reserve the right to limit the size and number of comments posted by an individual user in relation to a story. We reserve the exclusive right to permit comments in respect of some of our material and prohibit comments with respect to others. We ask that you refrain from submitting comments regarding the operation of the website using the comments feature.  We would be happy to address your concerns so feel free to submit comments regarding the operation of the website to community@buzz-caribbean.com.</p><p class="c-privacy-block__text">
            Registered users may submit comments or material throughout the website, subject to these terms and conditions.</p><p class="c-privacy-block__text">
            We respect the privacy of our users and therefore ask that you do not disclose your personal information or that of others in the material you submit to us.</p><p class="c-privacy-block__text">
            The objective of the interactive feature is to encourage users to exercise their right to freedom of expression in a cordial and respectful environment therefore we discourage forms of abuse meted out to our users and staff. Please report any form of abuse or breach of these terms to community@buzz-caribbean.com. Users who breach these terms may be suspended or banned from using our interactive features. If we suspend or ban you from using the interactive features of the website please do not re-register or submit material from another user’s account.</p>
        </div>
        <div id="6" class="c-privacy-block">
            <h2 class="c-privacy-block__title">6. Copyright and intellectual property rights</h2>
            <p class="c-privacy-block__text">When you submit material to us you are warranting that the material is either your original work or you have the consent of the owner(s) of the rights to the material to submit it to us for publication in accordance with these terms. Copyright and Intellectual Property Rights in the material you submit do not transfer to us and therefore you may permit other persons to use it. You however agree that we may, at our sole discretion, use your material without identifying you as the author of the material.</p>
        </div>
        <div id="7" class="c-privacy-block">
            <h2 class="c-privacy-block__title">7. Liability for breach of copyright or intellectual property rights</h2>
            <p class="c-privacy-block__text">We will not be liable for any breach of Copyright or other Intellectual Property Rights as a result of publication of your material.  You will be exclusively liable for any breach of Copyright or other Intellectual Property Rights proven by a third party with respect to your material. It shall be your duty to prove ownership of the contested material. You agree to fully indemnifyBUZZ, its heirs, successors, Directors, employees, affiliate companies and agents from any and all causes of action, loss, damage, injury or costs incurred including attorneys’ fees and settlement fees as a result of its publication of your material.</p>
        </div>
        <div id="8" class="c-privacy-block">
            <h2 class="c-privacy-block__title">8. Disclaimer</h2>
            <p class="c-privacy-block__text">We receive material from third parties such as advertisements, comments, links to other websites and content published on other websites. The material posted on this website is exclusively for educational and informational purposes only. We do not purport to give medical, legal, financial or other professional advice to any user of the website. You agree not to rely on the content published on the website without first consulting experts in particular fields for professional advice. Consequently we are not liable for any costs incurred, loss, damage, injury or death suffered by any person as a result of use of the website, third party transactions or websites or breach of any of these terms.</p><p class="c-privacy-block__text">
            By agreeing to these terms and conditions you agree not to initiate, pursue or assist in the commencement or furtherance of any cause of action in contract or tort against us for any costs incurred, loss, damage, injury or death suffered by any person who neglects to observe these terms. Nothing in these terms shall affect our liability as imposed by law. (i.e. the law applicable to Jamaica).</p><p class="c-privacy-block__text">
            We give no warranties or guarantees in relation to the website or its contents.  Efforts are made to protect against viruses, bugs Trojans, spyware, adware and other forms of malware however, we do not guarantee that the website will be free from viruses, and other forms of malware.</p>
        </div>
        <div id="9" class="c-privacy-block">
            <h2 class="c-privacy-block__title">9. Force majeure</h2>
            <p class="c-privacy-block__text">We endeavour to provide users with uninterrupted access to the website.  Occasionally we conduct scheduled and emergency maintenance or repairs on the website consequently we will not be responsible or liable for any disruption or delay in service.  Neither will we be liable for loss of access to the website as a result of acts of God, acts of Parliament, acts of third party service providers or any other factors over which we have no control.</p>
        </div>
        <div id="10" class="c-privacy-block">
            <h2 class="c-privacy-block__title">10. Privacy</h2>
            <p class="c-privacy-block__text">Your personal information will be used in accordance with the terms of our privacy policy.</p>
        </div>
        <div id="11" class="c-privacy-block">
            <h2 class="c-privacy-block__title">11. Waiver</h2>
            <p class="c-privacy-block__text">Failure of BUZZ to pursue or enforce its rights does not constitute a waiver of those rights.</p>
        </div>
        <div id="12" class="c-privacy-block">
            <h2 class="c-privacy-block__title">12. Termination</h2>
            <p class="c-privacy-block__text">Without prejudice to any other clause herein, this Agreement remains in force until either party exercises the option to terminate.  You may terminate this agreement without notice. We reserve the right to terminate your account if we believe you have breached any of these terms. Termination will result in your account being immediately deactivated.</p>
        </div>
        <div id="13" class="c-privacy-block">
            <h2 class="c-privacy-block__title">13. Survival</h2>
            <p class="c-privacy-block__text">Clauses 2 – Grant of Rights, 6 – Copyright, 7 – Liability for Breach, 8 – Disclaimer and 14 – Applicable Law shall survive this Agreement.</p>
        </div>
        <div id="14" class="c-privacy-block">
            <h2 class="c-privacy-block__title">14. Applicable law</h2>
            <p class="c-privacy-block__text">The laws of Jamaica shall apply to, and the courts of Jamaica shall have exclusive jurisdiction in all matters arising from the use of this website.</p>
        </div>
        <div id="15" class="c-privacy-block">
            <h2 class="c-privacy-block__title">15. Illegality</h2>
            <p class="c-privacy-block__text">If any of the term contained in this Agreement is deemed to be void, illegal or unenforceable in Jamaica, such void, illegal or unenforceable term shall be deemed to be severed from this Agreement and the terms remaining shall be binding and enforceable.</p>
        </div>
        <div id="16" class="c-privacy-block">
            <h2 class="c-privacy-block__title">16. Contact Buzz</h2>
            <p class="c-privacy-block__text">BUZZ<br>
            40 – 42 ½ Beechwood Avenue<br>
            Kingston 5<br>
            Jamaica<br>
            Email: community@buzz-caribbean.com<br>
            Fax: (876) 920-1440<br>
            Telephone:  (876) 936-9505<br></p>
            <p class="c-privacy-block__text">THANK YOU FOR READING THESE TERMS AND CONDITIONS OF USE, IF YOU DO NOT AGREE WITH ALL THE TERMS CONTAINED IN THIS AGREEMENT PLEASE DO NOT USE THE WEBSITE.</p>
        </div>
    </section>

    <?php include_once('inc/app/footer.php');
endif; ?>