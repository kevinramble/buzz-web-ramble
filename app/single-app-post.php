<?php $webview = $_GET['webview'];
    if(!$webview) :
        wp_redirect(home_url()); exit;

    elseif($webview && $webview == 'true') :
        // single apenas do app ?>

		<?php include_once('inc/app/header.php'); ?>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="container webview">
				<div class="l-page-post__top mt-3">
					<nav class="c-breadcrumb">
						<?php
							foreach((get_the_category()) as $category){
								if ($category->category_parent == 0){
									$parentcategory = $category->name;
									$catslug = $category->slug;
								}
							}

							$subcategory = wp_get_post_terms($post->ID, 'sub-category', array('fields' => 'all'));
							$subcategoryName = $subcategory[0]->name;
						
							$country = wp_get_post_terms($post->ID, 'countries', array('fields' => 'all'));
							$countryName = $country[0]->name;
						?>
						<!-- Remove country from breadcrumbs -->
						<?php //if($country) : ?>
							<!-- <span class="c-breadcrumb__item c-breadcrumb__item--country"><?php echo $countryName; ?></span> -->
						<?php //endif;
						 if($parentcategory) : ?>
							<span class="c-breadcrumb__item cat-<?php echo $catslug; ?>"><?php echo $parentcategory; ?></span>
						<?php endif; if($subcategory) : ?>
							<span class="c-breadcrumb__item c-breadcrumb__item--subcategory"><?php echo $subcategoryName; ?></span>
						<?php endif; ?>
					</nav>

					<?php
						$sponsored_img_url = esc_html(get_post_meta($post->ID, 'sponsored_img', true));
						$sponsored_url = esc_html(get_post_meta($post->ID, 'sponsored_url', true));

						if($sponsored_img_url) : ?>
							<div class="c-post-sponsored">
								<?php if($sponsored_url) : ?><a href="<?php echo $sponsored_url; ?>" target="_blank" title="<?php echo $sponsored_url; ?>"><?php endif; ?>
									<img src="<?php echo $sponsored_img_url; ?>" alt="Sponsored article" class="c-post-sponsored__img">
								<?php if($sponsored_url) : ?></a><?php endif; ?>
							</div>
					<?php endif; ?>
				</div>

				<header class="c-post-header">
					<h1><?php the_title(); ?></h1>
					<?php if(has_excerpt()) : ?>
						<h4><?php the_excerpt(); ?></h4>
					<?php endif; ?>

					<div class="c-post-author mt-3 mb-3">
						<div class="c-post-author__img" style="background-image:url('<?php echo get_avatar_url(get_the_author_meta('ID')); ?>')"></div>
						<div class="c-post-author__content">
							<p class="c-post-author__txt">By <strong><?php echo get_the_author_meta('display_name'); ?></strong></p>
							<p class="c-post-author__txt"><?php the_time('M d, Y'); ?></p>
						</div>
					</div>
                </header>
				
				<section class="c-post-content">
					<?php the_content(); ?>
				</section>

				<?php include_once('inc/app/post-sidebar.php'); ?>
			</div>
		<?php endwhile; endif; ?>

		<?php include_once('inc/app/footer.php'); ?>
	<?php endif;
?>