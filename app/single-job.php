<?php $webview = isset($_GET['webview']) ? $_GET['webview'] : '';
if(!$webview || $webview != 'true') :
// single normal, desktop e mobile ?>
	<?php get_header(); ?>

	<section class="l-page-list">
		<div class="c-list-header mb-0">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-6">
						<h1 class="c-list-header__title">Jobs</h1>
					</div>
				</div>
			</div>
		</div>

	<?php if (have_posts()) : while (have_posts()) : the_post();
		$job_company = esc_html(get_post_meta($post->ID, 'job_company', true));
		$job_position = esc_html(get_post_meta($post->ID, 'job_position', true));
		$job_application = esc_html(get_post_meta($post->ID, 'job_application', true));
		$job_expiration = esc_html(get_post_meta($post->ID, 'job_expiration', true));
		$job_country = esc_html(get_post_meta($post->ID, 'job_country', true));
		$job_article = esc_html(get_post_meta($post->ID, 'job_article', true));
		$pos_article = ($job_article) ? $job_article . ' ' : '';
		if(has_post_thumbnail($post->ID)) $image = get_the_post_thumbnail_url($post->ID); ?>

		<div class="c-header-job mb-4">
			<div class="c-header-job__wrapper container">
				<div class="c-header-job__item">
					<div class="c-header-job__thumb" style="background-image: url('<?php echo $image; ?>')"></div>
				</div>
				<div class="c-header-job__item c-header-job__item--full">
					<p class="c-header-job__description"><strong><?php echo $job_company; ?></strong> is looking for <?php echo $pos_article; ?><strong><?php echo $job_position; ?></strong> in <?php echo $job_country; ?></p>
					<p class="c-header-job__date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) .' ago'; ?></p>
				</div>
				<div class="c-header-job__item">
					<div class="c-social c-social--light d-none d-md-flex">
						<div class="c-social__book" aria-label="Bookmark"><?php the_favorites_button($post->ID, get_current_blog_id()); ?></div>
						<a class="c-social__icon c-social__icon--fb" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" aria-label="Facebook"></a>
						<a class="c-social__icon c-social__icon--tw" href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" target="_blank" aria-label="Twitter"></a>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<div class="c-post-content">
						<?php the_content(); ?>
					</div>

					<?php
						$expiration = remainingTime($job_expiration);
						if($expiration >= 0){
							if(is_user_logged_in()){
								if(!is_appliedjob($post->ID)){
									if($job_application){
										$http = 'http';
										$length = strlen($http);
										$httpCheck = (substr($job_application, 0, $length) === $http);
										if($httpCheck === false){ $job_application = 'http://' . $job_application; } ?>

										<a href="<?php echo $job_application; ?>" target="_blank" class="c-bt-apply mt-4 mt-lg-5">Apply for this job</a>
									<?php } else { ?>
										<a href="#0" class="c-bt-apply mt-4 mt-lg-5 js-open-modal js-apply-job-bt" data-modal="job" data-jobid="<?php echo $post->ID; ?>">Apply for this job</a>
										<?php include_once('inc/modals/job-apply.php');
									}
								}
							} else { ?>
								<a href="#0" class="c-bt-apply mt-4 mt-lg-5 js-open-modal" data-modal="needlogin">Apply for this job</a>
								<?php include_once('inc/modals/need-login.php');
							}
						} ?>
				</div>
				<div class="col-lg-3">
					<section class="ads">
						<div class="c-banner d-none d-lg-block"><!-- DESKTOP -->
							<!-- /21850622224/Buzz_JobPost_Right_Desktop -->
							<div id='div-gpt-ad-1570538937641-0' style='width: 300px; height: 600px;'>
								<script>
									googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570538937641-0'); });
								</script>
							</div>
						</div>
						
						<div class="c-banner d-block d-lg-none mt-4"><!-- MOBILE -->
							<!-- /21850622224/Buzz_JobsPost_Mobile -->
							<div id='div-gpt-ad-1570539208617-0' style='width: 320px; height: 250px;'>
								<script>
									googletag.cmd.push(function() { googletag.display('div-gpt-ad-1570539208617-0'); });
								</script>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	<?php endwhile; endif; ?>
	</section>

	<?php get_footer(); ?>

<?php elseif($webview && $webview == 'true') :
	// single apenas do app
	include_once('inc/app/header.php'); ?>

	<div class="container webview">
		<?php if (have_posts()) : while (have_posts()) : the_post();
		$job_company = esc_html(get_post_meta($post->ID, 'job_company', true));
		$job_position = esc_html(get_post_meta($post->ID, 'job_position', true));
		$job_application = esc_html(get_post_meta($post->ID, 'job_application', true));
		$job_expiration = esc_html(get_post_meta($post->ID, 'job_expiration', true));
		$job_country = esc_html(get_post_meta($post->ID, 'job_country', true));
		$job_article = esc_html(get_post_meta($post->ID, 'job_article', true));
		$pos_article = ($job_article) ? $job_article . ' ' : '';
		if(has_post_thumbnail($post->ID)) $image = get_the_post_thumbnail_url($post->ID); ?>

			<div class="c-header-job mb-3">
				<div class="c-header-job__wrapper container">
					<div class="c-header-job__item">
						<div class="c-header-job__thumb" style="background-image: url('<?php echo $image; ?>')"></div>
					</div>
					<div class="c-header-job__item c-header-job__item--full">
						<p class="c-header-job__description"><strong><?php echo $job_company; ?></strong> is looking for <?php echo $pos_article; ?><strong><?php echo $job_position; ?></strong> in <?php echo $job_country; ?></p>
						<p class="c-header-job__date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) .' ago'; ?></p>
					</div>
				</div>
			</div>

			<section class="c-post-content">
				<?php the_content(); ?>
			</section>

		<?php endwhile; endif; ?>
	</div>

	<?php include_once('inc/app/footer.php');
endif; ?>