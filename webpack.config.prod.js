const merge = require('webpack-merge')
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const baseConfig = require('./webpack.config.base')

module.exports = merge(baseConfig, {
	mode: 'production',
	plugins: [
		// new UglifyJsPlugin({
		// 	uglifyOptions: {
		// 		output: {
		// 			comments: false
		// 		}
		// 	}
		// }),
		new OptimizeCssAssetsPlugin()
	]
})
