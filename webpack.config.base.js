const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const WriteFilePlugin = require('write-file-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const globImporter = require('node-sass-glob-importer')
const path = require('path')

const config = {
	app: 'app',
	theme: 'web/app/themes/buzz'
}

const pathsToClean = [`${config.theme}`]

const copyTheme = new CopyWebpackPlugin(
	[
		{
			from: `${config.app}`,
			to: `${config.theme}`,
			ignore: ['scss/{**/*,*}', 'scripts/parts/{**/*,*}']
		}
	],
	{ copyUnmodified: false }
)

const extractSass = new MiniCssExtractPlugin({
	filename: `${config.theme}/styles/main.css`
})

module.exports = {
	devServer: {
		port: 8080
	},
	entry: [path.join(__dirname, 'app', 'scripts', 'main.js'), path.join(__dirname, 'app', 'scss', 'main.scss')],
	output: {
		path: path.resolve(__dirname),
		filename: `${config.theme}/scripts/[name].js`
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				options: {
					presets: ['@babel/preset-env']
				}
			},
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							url: false,
							sourceMap: true
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							ident: 'postcss',
							plugins: [require('autoprefixer')()],
							sourceMap: true
						}
					},
					{
						loader: 'sass-loader',
						options: {
							importer: globImporter(),
							includePaths: ['node_modules'],
							sourceMap: true
						}
					}
				]
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
				exclude: /node_modules/
			}
		]
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		}),
		new CleanWebpackPlugin(pathsToClean),
		new WriteFilePlugin(),
		copyTheme,
		extractSass
	]
}
