**Requirements:**

-   PHP 7.1 or higher
-   MySQL 5.7 or higher
-   NodeJS (https://nodejs.org/)
-   Composer (https://getcomposer.org/)

**In order to run this project, you need to follow these steps:**

-   Run `composer install` to install Composer dependencies;
-   Copy `.env.example` to `.env` and update with your local configuration;

**If you'd like to change any front-end behaviour, do this to compile the files:**

-   Open a command promp and go to root project;
-   Type `npm install` to install all dependencies;
-   When the installation is done, type `npm run dev` to compile; This will run `watch` in dev mode; For production, type `npm run build`.

**Path of cron for app push notifications:**

-   `app/api-rest/cron-push.php`
